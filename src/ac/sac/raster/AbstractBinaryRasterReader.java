package ac.sac.raster;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.utils.LogWriter;

public abstract class AbstractBinaryRasterReader<D extends RasterItem> {

	private static final int X_COL = 0; // longitude
	private static final int Y_COL = 1; // latitude
	private int NUM_DATA_COLS; // number of data columns
	private String[] header;
	protected RasterSet<D> dataset;
	
	public AbstractBinaryRasterReader(String[] header, int numDataCols, RasterSet<D> dataset) {
		super();
		this.header = header;
		NUM_DATA_COLS = numDataCols;
		this.dataset = dataset;
	}

	public RasterSet<D> getRasterDataFromFile(String filePath) {
		long startTime = System.currentTimeMillis();

		InputStream inputStream = null;
		BufferedInputStream reader = null;

		try {
			inputStream = new FileInputStream(filePath);
			reader = new BufferedInputStream(inputStream);

			byte[] line = new byte[(NUM_DATA_COLS + 2) * Double.BYTES];

			while (reader.read(line) > 0) {
				Double[] dline = byteArrayToDouble(line);
	
				double x = dline[X_COL];
				double y = dline[Y_COL];
	
				D item = dataset.getFromCoordinates(x, y);
				RasterKey key = dataset.getKeyFromCoordinates(x, y);
	
				Map<String, Double> rowValues = new HashMap<String, Double>(NUM_DATA_COLS);
				for (int i = 0; i < NUM_DATA_COLS; i++) {
					rowValues.put(header[i], dline[i + 2]);
				}
	
				setData(key, item, rowValues);
			}

		}
		catch (Exception e) {
			LogWriter.printlnError("Problem reading data file " + filePath);
			LogWriter.print(e);
			throw new RuntimeException(e);
		}
		finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					LogWriter.print(e);
				}
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					LogWriter.print(e);
				}
			}
		}
		LogWriter.println("Reading " + filePath + ", took " + (System.currentTimeMillis() - startTime) + " ms");
		return dataset;

	}

	private Double[] byteArrayToDouble(byte[] bytes) {
		int n = NUM_DATA_COLS + 2;
		if (n * Double.BYTES != bytes.length) {
			LogWriter.printlnError("AbstractBinaryRasterReader: byte array not of expected length");
		}
		Double[] d = new Double[n];
		for (int i = 0; i < n; i++) {
			ByteBuffer buff = ByteBuffer.allocate(Double.BYTES);
			byte[] doubleBytes = Arrays.copyOfRange(bytes, i * Double.BYTES, i * Double.BYTES + Double.BYTES);
			buff.put(doubleBytes);
			buff.flip();
			d[i] = buff.getDouble();
		}
		
		return(d);
	}
	
	abstract protected void setData(RasterKey key, D item, Map<String, Double> rowValues);
}
