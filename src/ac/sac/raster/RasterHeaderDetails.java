package ac.sac.raster;

import java.io.Serializable;

public class RasterHeaderDetails implements Serializable{
	private static final long serialVersionUID = 8931052507772280805L;
	
	private int ncolumns;
	private int nrows;
	private double xllCorner;
	private double yllCorner;
	private double xcellSize;
	private double ycellSize;
	private String nodataString;
	
	public RasterHeaderDetails(int ncolumns, int nrows, double xllCorner, double yllCorner, double xcellSize, double ycellSize, String nodataString) {
		super();
		this.ncolumns = ncolumns;
		this.nrows = nrows;
		this.xllCorner = xllCorner;
		this.yllCorner = yllCorner;
		this.xcellSize = xcellSize;
		this.ycellSize = ycellSize;
		this.nodataString = nodataString;
	}
	
	public static RasterHeaderDetails getGlobalHeaderFromCellSize(double xcellSize, double ycellSize, String nodataString) {
		int ncolumns = (int) (360.0 / xcellSize);  // perhaps should check no rounding
		int nrows = (int) (180.0 / ycellSize);  // perhaps should check no rounding

		return new RasterHeaderDetails(ncolumns, nrows, -180, -90, xcellSize, ycellSize, nodataString);
	}
	
	public RasterHeaderDetails copy() {
		return new RasterHeaderDetails(ncolumns, nrows, xllCorner, yllCorner, xcellSize, ycellSize, nodataString);
	}

	public double getOriginX() {
		return xllCorner;
	}
	
	public double getOriginY() {
		return yllCorner + ycellSize * nrows;
	}
	
	public void incrementNcolumns() {
		ncolumns++;
	}

	public void incrementNrows() {
		nrows++;
		yllCorner = yllCorner - ycellSize;
	}

	public int getNcolumns() {
		return ncolumns;
	}

	public int getNrows() {
		return nrows;
	}

	public double getXllCorner() {
		return xllCorner;
	}

	public double getYllCorner() {
		return yllCorner;
	}

	public double getXCellSize() {
		return xcellSize;
	}
	
	public double getYCellSize() {
		return ycellSize;
	}


	public String getNodataString() {
		return nodataString;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ncolumns;
		result = prime * result
				+ ((nodataString == null) ? 0 : nodataString.hashCode());
		result = prime * result + nrows;
		long temp;
		temp = Double.doubleToLongBits(xcellSize);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(xllCorner);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(ycellSize);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(yllCorner);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RasterHeaderDetails other = (RasterHeaderDetails) obj;
		if (ncolumns != other.ncolumns)
			return false;
		if (nodataString == null) {
			if (other.nodataString != null)
				return false;
		} else if (!nodataString.equals(other.nodataString))
			return false;
		if (nrows != other.nrows)
			return false;
		if (Double.doubleToLongBits(xcellSize) != Double
				.doubleToLongBits(other.xcellSize))
			return false;
		if (Double.doubleToLongBits(xllCorner) != Double
				.doubleToLongBits(other.xllCorner))
			return false;
		if (Double.doubleToLongBits(ycellSize) != Double
				.doubleToLongBits(other.ycellSize))
			return false;
		if (Double.doubleToLongBits(yllCorner) != Double
				.doubleToLongBits(other.yllCorner))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ncolumns=" + ncolumns + 
				", nrows=" + nrows + 
				", xllCorner=" + xllCorner + 
				", yllCorner=" + yllCorner + 
				", xcellSize" + xcellSize + 
				", ycellSize" + ycellSize + 
				", nodataString=" + nodataString;
	}

}
