package ac.sac.raster;

import java.util.*;

import ac.ed.lurg.utils.LogWriter;

/* Class which holds raster data.  The generics used to defines the type of raster data held.
 */
public class RasterSet<D extends RasterItem> extends HashMap<RasterKey, D> {
	
	private static final long serialVersionUID = 4180188703734898215L;
	private RasterHeaderDetails header;
		
	public RasterSet(RasterHeaderDetails header) {
		this.header = header;
	}
		
	/** Returns the raster item for the row and column. If an source header is provided these are assumed to be that format system */
	public Collection<D> getCollection(int col, int row, RasterHeaderDetails source) {
		
		Collection<D> dataPoints = new ArrayList<D>();
		int c, r;
		
		if (source!=null && !source.equals(header)) {
			int xOffset = (int) Math.round((source.getXllCorner() - header.getXllCorner())/header.getXCellSize());
			int yOffset = (int) Math.round((-source.getOriginY() + header.getOriginY())/header.getYCellSize());  // down from top

			if (source.getXCellSize() % header.getXCellSize() != 0 && source.getYCellSize() % header.getYCellSize() != 0)
				throw new RuntimeException("Cell sizes must be the same or souce an integer multiple of destination");
			
			double xcellRatio = source.getXCellSize() / header.getXCellSize();
			double ycellRatio = source.getYCellSize() / header.getYCellSize();

			for (int x = 0; x<xcellRatio; x++) {
				for (int y = 0; y<ycellRatio; y++) {
					c = (int)(col*xcellRatio) + x + xOffset;
					r = (int)(row*ycellRatio) + y + yOffset;

					D d = get(c, r);
					if (d != null)
						dataPoints.add(d);
				}
			}
		}
		else {
			D d = get(col, row);
			if (d != null)
				dataPoints.add(d);
		}
		
		return dataPoints;
	}

	/** Method to get raster item for coordinates */
	public D getFromCoordinates(double source_x, double source_y) {
		RasterKey key = getKeyFromCoordinates(source_x, source_y);
		return get(key.getCol(), key.getRow());   // can't just call get(key) as may need to create the RasterItem
	}
	
	public RasterKey getKeyFromCoordinates(double source_x, double source_y) {
        int col = (int)((source_x - header.getXllCorner())/header.getXCellSize() + 0.01); // 0.01 to avoid floating point issues in truncation to int
		// header.getNrows() - 1, lower left basis.  Minus 1 as nrows is number, but indexed from zero
        int row = header.getNrows() - 1 - (int)((source_y - header.getYllCorner())/header.getYCellSize() + 0.01);
		if (row < 0 || col < 0) {
			LogWriter.printlnWarning("Got negative row or col values: " + row + ", "+  col);
		}
		return new RasterKey(col, row);
	}

	
	public double getXCoordin(RasterKey key) {
		double x = header.getXCellSize() * key.getCol() + header.getXllCorner();
		return x;
	}
	
	public double getYCoordin(RasterKey key) {
		double y = getYCoordin(key.getRow());
		return y;
	}
	
	private double getYCoordin(int row) {
		double y = header.getYCellSize() * (header.getNrows() - 1 - row) + header.getYllCorner();
		return y;
	}

// get the RasterItem if it already exists
//    public D get(RasterKey key) {
//		return get(key);
//	}
	
	/** Method to really get the data, or create it. Assumes the col and row are in the internal header format */
	public D get(int col, int row) {
		if (header.getNcolumns() < col+1)
			header.incrementNcolumns();
		if (header.getNrows() < row+1) {
			header.incrementNrows();
		}
		
		RasterKey key = new RasterKey(col, row);
		D data = get(key);
		if (data == null) {
			data = createRasterData();
			if (data != null)
				put(key, data);
		}
		return data;
	}

	// Assuming coordinates refer to global half degree in Mha 
	public double getAreaMha(RasterKey key) {
		// https://badc.nerc.ac.uk/help/coordinates/cell-surf-area.html
		// http://mathforum.org/library/drmath/view/63767.html
		int aRow = key.getRow();
		double a = Math.sin(getRadiansFromDeg(getYCoordin(aRow)));
		double b = Math.sin(getRadiansFromDeg(getYCoordin(aRow+1)));
		double cellArea = header.getXCellSize() * Math.PI / 180.0 * (a-b) * 6371.0 * 6371.0 / 10000.0;
		return cellArea;
	}
	
	private static double getRadiansFromDeg(double deg) {
		double latRadian = deg * Math.PI / 180.0;
		return latRadian;
	}

	
	/** Some classes do not create, those that do will need to override */
	protected D createRasterData() {
		return null;
	}

	public int getNcolumns() {
		return header.getNcolumns();
	}

	public int getNrows() {
		return header.getNrows();
	}	
	
	public RasterHeaderDetails getHeaderDetails() {
		return header;
	}

	/** Check passed in header is consistent with one from this set.
	 *   Actually only checks grid size as can shift rasters*/
	public boolean isConstistentWithHeader(RasterHeaderDetails h) {
		return h.getXCellSize() == header.getXCellSize() && h.getYCellSize() == header.getYCellSize();
	}
	
	public RasterSet<D> createSubsetForKeys(Collection<RasterKey> keys) {
		return popSubsetForKeys (new RasterSet<D>(getHeaderDetails()), keys);
	}

	protected RasterSet<D> popSubsetForKeys(RasterSet<D> subset, Collection<RasterKey> keys) {		
		for (RasterKey key : keys) {
			//LogWriter.println("popSubsetForKeys: " + key.getCol() + ", "  + key.getRow() + ": " + getXCoordin(key) + ", " + getYCoordin(key));
			subset.put(key, get(key));
		}
		return subset;
	}
	
    public void putAll(RasterSet<D> rasterToAdd) {
    	if (!isConstistentWithHeader(rasterToAdd.getHeaderDetails()))
    		throw new RuntimeException("RasterSet.putAll: Headers not consistent");
    	
    	for (Map.Entry<RasterKey, D> e : rasterToAdd.entrySet()) {
    		put(e.getKey(), e.getValue());  // bit worried the extend needs to be update, but it will not be
    	}
    }
}
