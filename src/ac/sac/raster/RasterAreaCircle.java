package ac.sac.raster;

public class RasterAreaCircle {

	private double radius;
	private RasterKey center;
	
	public RasterAreaCircle (double radius, RasterKey center) {
		this.radius = radius;
		this.center = center;
	}

	public boolean contains(RasterKey location) {
		double distance = center.getDistanceTo(location);
		return distance<=radius;
	}
}
