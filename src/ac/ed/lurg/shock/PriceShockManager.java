package ac.ed.lurg.shock;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class PriceShockManager {
	private StringTabularReader priceShockReader;

	public PriceShockManager() {

		String shockFile = ModelConfig.PRICESHOCKS_PARAMETER_FILE;
		priceShockReader = new StringTabularReader(",", new String[]{"year", "crop", "shockFactor"});

		if (new File(shockFile).isFile()) 
			priceShockReader.read(shockFile);
		else
			LogWriter.println("Can't find a price shock file (" +shockFile + "), so will not shock prices");
	}
	
	public Map<CropType, Double> getShocks(Timestep timestep) {
		Map<CropType, Double> shocks = new HashMap<CropType, Double>();

		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put("year", Integer.toString(timestep.getYear()));
		
		List<Map<String, String>> rows = priceShockReader.query(queryMap);
		for (Map<String, String> row : rows) {
			String cropTypeS = row.get("crop");
			String shockFactorS = row.get("shockFactor");
			CropType crop = CropType.getForFaoName(cropTypeS);
			
			Double shockFactor = Double.valueOf(shockFactorS);
			LogWriter.println("Got price shock at " + timestep + ": " + cropTypeS + " " + shockFactorS);
			shocks.put(crop, shockFactor);
		}
		return shocks;
	}
}