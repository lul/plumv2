package ac.ed.lurg.shock;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;
import ac.ed.lurg.yield.YieldRaster;
import ac.ed.lurg.yield.YieldResponsesItem;
import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class YieldShockManager {
	private RasterHeaderDetails desiredProjection;
	private StringTabularReader yieldShockReader;
	private Map<String, RasterSet<YieldShockItem> > cacheYieldShockMaps = new WeakHashMap<String, RasterSet<YieldShockItem> >();

	public YieldShockManager (RasterHeaderDetails desiredProjection) {
		this.desiredProjection = desiredProjection;
		String shockFile = ModelConfig.YIELDSHOCKS_PARAMETER_FILE;
		yieldShockReader = new StringTabularReader(",", new String[]{"year", "mapFilename", "crop", "shockFactor"});

		if (!ModelConfig.SHOCKS_POSSIBLE)
			LogWriter.println("Not shocking yields as SHOCKS_POSSIBLE false");
		else if (new File(shockFile).isFile()) 
			yieldShockReader.read(shockFile);
		else
			LogWriter.println("Can't find a yield shock file (" +shockFile + "), so will not shock yields");
	}

	private RasterSet<YieldShockItem> getYieldShockMap(String mapFilename) {
		RasterSet<YieldShockItem> yieldShockMap = cacheYieldShockMaps.get(mapFilename);
		if (yieldShockMap == null) {
			
			yieldShockMap = new RasterSet<YieldShockItem>(desiredProjection) {
				private static final long serialVersionUID = -1991765655825933393L;

				@Override
				protected YieldShockItem createRasterData() {
					return new YieldShockItem();
				}
			};
			
			YieldShockReader yieldShockReader = new YieldShockReader(yieldShockMap);
			yieldShockReader.getRasterDataFromFile(ModelConfig.YIELDSHOCK_MAP_DIR + File.separator + mapFilename);
			cacheYieldShockMaps.put(mapFilename, yieldShockMap);
			LogWriter.println("YieldShockManager.getYieldShockMap: added " + mapFilename + " now have " + cacheYieldShockMaps.size() + "cached");
		}

		return yieldShockMap;
	}

	public void setShocks(YieldRaster yieldRaster, Timestep timestep) {
		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put("year", Integer.toString(timestep.getYear()));

		List<Map<String, String>> rows = yieldShockReader.query(queryMap);
		for (Map<String, String> row : rows) {

			String mapFilename = row.get("mapFilename");
			String cropTypeS = row.get("crop");
			String shockFactorS = row.get("shockFactor");
			double shockFactor = Double.valueOf(shockFactorS) * ModelConfig.YIELD_SHOCK_MAGNIFIER;
			LogWriter.println("Got yield shock at " + timestep + ": " + mapFilename + " " + cropTypeS + " " + shockFactorS);

			RasterSet<YieldShockItem> yieldShockMap = getYieldShockMap(mapFilename);
			if (cropTypeS.equals("allcrops")) {
				for (CropType crop : CropType.getCropsLessPasture())
					adjustForShocks(yieldShockMap, yieldRaster, crop, shockFactor);
			}
			else {
				CropType crop = CropType.getForFaoName(cropTypeS);
				adjustForShocks(yieldShockMap, yieldRaster, crop, shockFactor);
			}
		}
	}
	
	private void adjustForShocks(RasterSet<YieldShockItem> yieldShockMap, YieldRaster yieldRaster, CropType crop, double yieldAdjustment) {
		LogWriter.println("Applying yield shock to " + crop + ", rate " + yieldAdjustment, 2);
		for (Map.Entry<RasterKey, YieldShockItem> entry : yieldShockMap.entrySet()) {
			YieldShockItem value = entry.getValue();
			if (value.isShocked()) {
				YieldResponsesItem yieldRespItem = yieldRaster.get(entry.getKey());
				if (yieldRespItem != null)
					yieldRespItem.setShockRate(crop, yieldAdjustment);
			}
		}
	}
}