package ac.ed.lurg.shock;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.utils.LogWriter;

public class parameterShocksReader {

	private String delimiterRegex;
	private Map<Integer, Map<String,Double>> shocksMap = new HashMap<Integer, Map<String,Double>>(); //<Year,<Parameter,Value>>
	private int minColNum = 2; //year and a parameter to change
	protected String[] dataColNames;


	public parameterShocksReader (String delimiterRegex) {
		this.delimiterRegex = delimiterRegex;
	}


	private Map<String, Integer> handleHeader(String[] headertokens) {

		dataColNames= headertokens; 

		Map<String, Integer> headerMap = new HashMap<String, Integer>();

		if (headertokens.length < minColNum) {
			LogWriter.printlnError("Too few columns");
			throw new RuntimeException("Too few columns in tablular file");
		}

		for (int i=0; i<headertokens.length; i++) {
			headerMap.put(headertokens[i], i);
		}
		return headerMap;
	}

	public void read(String filename) {

		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(filename)); 
			String line = fileReader.readLine(); 
			Map<String, Integer> colNameMap = handleHeader(parseLine(line));


			while ((line=fileReader.readLine()) != null) {
				Integer year = null;
				Map<String, Double> valuesMap = new HashMap<String, Double>();
				String[] tokens = parseLine(line);

				if (tokens.length < dataColNames.length)
					LogWriter.printlnError("Number of columns doesn't match number of headers " + filename + ", " + line);

				for (String colName : dataColNames) {
					Integer colIndex;
					if(colName.equals("Year")) {
						colIndex = colNameMap.get(colName);
						if (colIndex == null) {
							LogWriter.printlnError("Can't find column " + colName);
						}
						year = Integer.parseInt(tokens[colIndex]);
					}
					else {
						colIndex = colNameMap.get(colName);
						if (colIndex == null) {
							LogWriter.printlnError("Can't find column " + colName);
						}
						valuesMap.put(colName,  Double.parseDouble(tokens[colIndex])); 
					}

				}
				shocksMap.put(year,valuesMap);
			} 
			fileReader.close(); 

		} catch (IOException e) {
			LogWriter.printlnError("parameterShocksReader: Failed in reading file " + filename);
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename + ", create " + shocksMap.size() + " rows");
	}


	private String[] parseLine(String line) {
		return line.trim().split(delimiterRegex);
	}
	
	public Double queryForParameter(int year, String parameter) {

		Double value= null;
		if(shocksMap.containsKey(year)) {
		Map<String,Double> parameterMap  = shocksMap.get(year);
		if(parameterMap.containsKey(parameter)) {
		  value = parameterMap.get(parameter);
		}
		}
		return value;
	}

}
