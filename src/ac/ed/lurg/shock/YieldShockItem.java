package ac.ed.lurg.shock;

import ac.sac.raster.RasterItem;

public class YieldShockItem implements RasterItem {

	private boolean shocked;
	
	public boolean isShocked() {
		return shocked;
	}
	
	public void setShock(boolean shocked) {
		this.shocked = shocked;
	}

	@Override
	public String toString() {
		return "YieldShockItem [" + shocked + "]";
	}
}
