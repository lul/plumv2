package ac.ed.lurg.shock;

import ac.sac.raster.AbstractRasterReader;
import ac.sac.raster.RasterSet;

public class YieldShockReader extends AbstractRasterReader<YieldShockItem> {	
	
	public YieldShockReader (RasterSet<YieldShockItem> dataset) {
		super(dataset);
	}

	@Override
	public void setData(YieldShockItem shockItem, String token) {
		boolean shocked = !"0".equals(token);
		shockItem.setShock(shocked);
	}
}
