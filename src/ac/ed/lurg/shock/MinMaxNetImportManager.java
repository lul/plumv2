package ac.ed.lurg.shock;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LazyHashMap;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class MinMaxNetImportManager {
	private StringTabularReader minMaxTradeReader;

	public enum LimitType {
		MIN_LIMIT_TYPE("min"),
		MAX_LIMIT_TYPE("max");

		private String limitType;

		LimitType(String limitType) {
			this.limitType = limitType;
		}

		public static LimitType findByName(String str){
		    for(LimitType p : values()){
		        if( p.limitType.equals(str)){
		            return p;
		        }
		    }
		    
		    LogWriter.printlnError("No LimitType found for " + str);
		    return null;
		}
	}

	public MinMaxNetImportManager() {

		String fileName = ModelConfig.MIN_MAX_TRADE_FILE;
		minMaxTradeReader = new StringTabularReader(",", new String[]{"year", "compositeCountry", "crop", "limitType", "netImportValue"});

		if (new File(fileName).isFile()) 
			minMaxTradeReader.read(fileName);
		else
			LogWriter.println("Can't find a min_max_trade file (" +fileName + "), so will not apply trade limits.");
	}
	
	@SuppressWarnings("serial")
	public Map<Integer, Map<LimitType, Map<CropType, Double>>> getMinMaxNetImportForCountry(CompositeCountry cc) {
		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put("compositeCountry", cc.getName());

		List<Map<String, String>> rows = minMaxTradeReader.query(queryMap);
		LazyHashMap<Integer, Map<LimitType, Map<CropType, Double>>> tradeLimits = null;

		if (rows.size() > 0) {
			tradeLimits = new LazyHashMap<Integer, Map<LimitType, Map<CropType, Double>>>() {
				protected Map<LimitType, Map<CropType, Double>> createValue() { 
					Map<LimitType, Map<CropType, Double>> theMap = new HashMap<LimitType, Map<CropType, Double>>();
					theMap.put(LimitType.MIN_LIMIT_TYPE, new HashMap<CropType, Double>());
					theMap.put(LimitType.MAX_LIMIT_TYPE, new HashMap<CropType, Double>());
					return theMap;
				}
			};

			for (Map<String, String> row : rows) {
				String cropTypeS = row.get("crop");
				String netImportValueS = row.get("netImportValue");
				String yearS = row.get("year");
				String limitTypeS = row.get("limitType");
				LogWriter.println("Got export " + limitTypeS + " trade limit " + cc.getName() + " : " + yearS + " " + cropTypeS + " " + netImportValueS);
				
				CropType crop = CropType.getForGamsName(cropTypeS);
				Double netImportValue = Double.valueOf(netImportValueS);
				Integer year = Integer.valueOf(yearS);
				LimitType limitType = LimitType.findByName(limitTypeS);
				
				Map<LimitType, Map<CropType, Double>> minMaxlimits = tradeLimits.lazyGet(year);
				Map<CropType, Double> limits = minMaxlimits.get(limitType);
				limits.put(crop, netImportValue);
			}
		}
		
		return tradeLimits;
	}
}