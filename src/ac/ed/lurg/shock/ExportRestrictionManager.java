package ac.ed.lurg.shock;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LazyHashMap;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class ExportRestrictionManager {
	private StringTabularReader exportShockReader;

	public ExportRestrictionManager() {

		String shockFile = ModelConfig.EXPORT_RESTRICTIONS_FILE;
		exportShockReader = new StringTabularReader(",", new String[]{"year", "compositeCountry", "crop", "shockFactor"});

		if (new File(shockFile).isFile()) 
			exportShockReader.read(shockFile);
		else
			LogWriter.printlnWarning("Can't find a export shock file (" +shockFile + "), so will not apply export restriction");
	}
	
	@SuppressWarnings("serial")
	public Map<Integer, Map<CropType, Double>> getShocksForCountry(CompositeCountry cc) {
		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put("compositeCountry", cc.getName());
		List<Map<String, String>> rows = exportShockReader.query(queryMap);
		LazyHashMap<Integer, Map<CropType, Double>> shocks = null;

		if (rows.size() > 0) {
			shocks = new LazyHashMap<Integer, Map<CropType, Double>>() {
				protected Map<CropType, Double> createValue() { return new HashMap<CropType, Double>(); }
			};

			for (Map<String, String> row : rows) {
				String cropTypeS = row.get("crop");
				String shockFactorS = row.get("shockFactor");
				String yearS = row.get("year");
				LogWriter.println("Got export shock " + cc.getName() + " : " + yearS + " " + cropTypeS + " " + shockFactorS);
				
				CropType crop = CropType.getForGamsName(cropTypeS);
				Double shockFactor = Double.valueOf(shockFactorS);
				Integer year = Integer.valueOf(yearS);
				
				Map<CropType, Double> shocksCC = shocks.lazyGet(year);
				shocksCC.put(crop, shockFactor);
			}
		}
		
		return shocks;
	}
}