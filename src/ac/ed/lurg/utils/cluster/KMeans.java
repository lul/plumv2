package ac.ed.lurg.utils.cluster;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.utils.LogWriter;
 
public class KMeans<K, P extends ClusteringPoint<K>> {
     
    private Collection<P> points;
    private List<Cluster<K,P>> clusters;
    
    private Map<K, Double> minValueMap = new HashMap<K, Double>();
    private Map<K, Double> maxValueMap = new HashMap<K, Double>();

    public KMeans(Collection<P> points, int numClusters) {
    	this.points = points;
    	this.clusters = new ArrayList<Cluster<K,P>>();   

        for (ClusteringPoint<K> p : points) {
	    	for (K key : p.getAllClusteringKeys()) {
	    		double currentVal = p.getClusteringValue(key);
	    		Double minV = minValueMap.get(key);
	    		Double maxV = maxValueMap.get(key);
	    		double minSoFar = minV==null ? Double.POSITIVE_INFINITY : minV;
	    		double maxSoFar = maxV==null ? Double.NEGATIVE_INFINITY : maxV;

	    		if (currentVal < minSoFar)
	    			minValueMap.put(key, currentVal);
	    		
	    		if (currentVal > maxSoFar)
	    			maxValueMap.put(key, currentVal);
	    	}
        }
    	
		//Create Clusters, with random centroids
		for (int i = 0; i < numClusters; i++) {
			
			CentriodPoint<K> p = generateRandomCentriod();
	 //   	LogWriter.println("Creating cluster at " + p);
			Cluster<K,P> cluster = new Cluster<K,P>(p);
			clusters.add(cluster);
		}
    }

	private static Random r = new Random(ModelConfig.RANDOM_SEED);

	private CentriodPoint<K> generateRandomCentriod() {
		double min, max, rand, v;

		Map<K, Double> randomCentroid = new HashMap<K, Double>();

		for (Map.Entry<K, Double> e : minValueMap.entrySet()) {
			min = e.getValue();
			max = maxValueMap.get(e.getKey());
			rand = r.nextDouble();
			v = min + (max - min) * rand;
			randomCentroid.put(e.getKey(), v);
		}
		
		return new CentriodPoint<K>(randomCentroid);
	}
    
	public void printClusters() {
		int i=0;
		int clustersWithPoints = 0;
		for (Cluster<K,P> c : clusters) {
			i++;
			LogWriter.println("[Cluster: " + i+"]", 3);
			LogWriter.println("[Centroid: " + c.getCentroid() + "]", 3);
			LogWriter.println("[Points: (" + c.getPoints().size() + " points)", 3);
			for(P p : c.getPoints())
				LogWriter.println(p.toString(), 3);
			
			if (c.getPoints().size() > 0)
				clustersWithPoints++;
			
			LogWriter.println("]\n", 3);
		}
		
		LogWriter.println(clusters.size() + " clusters, of which " + clustersWithPoints + " have points", 1);
	}

    public double calculateClusters(int maxIterations, double tolerance) {
        int iteration = 0;
		long startTime = System.currentTimeMillis();

        while(true) {        			
        	iteration++;
        	double distance = 0;

        	//Assign points to the closer cluster
        	assignCluster();
        	
            //Calculate new centroids, and total distance between new and old centroids
            for(Cluster<K,P> c : clusters)
            	distance += c.calculateCentroid();
            
       // 	LogWriter.println("#################");
       // 	LogWriter.println("Iteration: " + iteration);
       // 	LogWriter.println("Centroid distances: " + distance);
       // 	printClusters();
   	
        	if(distance <= tolerance || iteration > maxIterations) {
        		LogWriter.println("Finishing calculateClusters: Iteration " + iteration + ", centroid distances: " + distance, 3);
        		LogWriter.println("    Took " + (System.currentTimeMillis() - startTime) + " ms to run", 3);
        		return distance;
        	}
        }
    }
    
    public List<Cluster<K,P>> getClusters() {
    	return clusters;
    }
    
    private void clearPointsFromAllClusters() {
    	for(Cluster<K,P> cluster : clusters) {
    		cluster.clearPoints();
    	}
    }
    
    private void assignCluster() {
        clearPointsFromAllClusters();
       
        for(P point : points) {
        	double min = Double.MAX_VALUE;
            Cluster<K,P> cluster = null;                 
            double distance = 0.0; 
      	
            for(Cluster<K,P> c : clusters) {
                distance = c.distanceFromCentroid(point);
                if(distance < min){
                    min = distance;
                    cluster = c;
                }
            }
            cluster.addPoint(point);
        }
    }
}