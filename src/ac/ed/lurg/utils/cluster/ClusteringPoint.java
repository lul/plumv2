package ac.ed.lurg.utils.cluster;

import java.util.Collection;

/** Interface that give information used in clustering */
public interface ClusteringPoint<K> {
 
    public double getClusteringValue(K key);
	public Collection<K> getAllClusteringKeys();

}