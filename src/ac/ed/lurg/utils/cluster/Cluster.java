package ac.ed.lurg.utils.cluster;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Cluster<K, P extends ClusteringPoint<K>> {

	private Collection<P> points = new HashSet<P>();
	private CentriodPoint<K> centroid;

	public Cluster(CentriodPoint<K> centroid) {
		this.centroid = centroid;
	}

	public Collection<P> getPoints() {
		return points;
	}

	protected void addPoint(P point) {
		points.add(point);
	}

	public CentriodPoint<K> getCentroid() {
		return centroid;
	}
	
	protected double calculateCentroid() {
		int n_points = points.size();
		if(n_points == 0) return 0;

		Map<K, Double> centroidValues = new HashMap<K, Double>();

		for(ClusteringPoint<K> p : points) {
	    	for (K key : p.getAllClusteringKeys()) {
				Double soFar = centroidValues.get(key);
				Double pointVal = p.getClusteringValue(key);
				Double updated = soFar==null ? pointVal : pointVal + soFar;
				centroidValues.put(key, updated);
			}
		}

		for (Map.Entry<K, Double> e : centroidValues.entrySet())
			centroidValues.put(e.getKey(), e.getValue()/n_points);

		CentriodPoint<K> updatedCentroid = new CentriodPoint<K>(centroidValues);
		double distanceMoved = distanceFromCentroid(updatedCentroid);
		
		centroid = updatedCentroid;
		return distanceMoved;
	}
	
    //Calculates the distance between two points.
    protected double distanceFromCentroid(ClusteringPoint<K> p) {
    	double squaredTotal=0;
    	for (K key : centroid.getAllClusteringKeys()) {
    		double a = centroid.getClusteringValue(key);
    		double b = p.getClusteringValue(key);
    		squaredTotal += Math.pow(a-b, 2);
    	}
    	
        return Math.sqrt(squaredTotal);
    }


	protected void clearPoints() {
		points.clear();
	}
}