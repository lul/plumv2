package ac.ed.lurg.utils.cluster;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class KMeansTest {

	//Number of Clusters. This metric should be related to the number of points
	private int NUM_CLUSTERS = 3;    
	//Number of Points
	private int NUM_POINTS = 10;
	//Min and Max X and Y
	private static final int MIN_COORDINATE = 0;
	private static final int MAX_COORDINATE = 10;
	private int MAX_ITERATIONS = 20;

	public static void main(String[] args) {

		KMeansTest kmeansTest = new KMeansTest();
		kmeansTest.doIt();
	}
	
	public void doIt() {
		KMeans<String, ClusteringPoint<String>> kmeans = new KMeans<String, ClusteringPoint<String>>(createRandomPoints(MIN_COORDINATE,MAX_COORDINATE,NUM_POINTS), NUM_CLUSTERS);
		kmeans.printClusters();
		kmeans.calculateClusters(MAX_ITERATIONS, 0.6);
	}
	
    //Creates random point
    protected static ClusteringPoint<String> createRandomPoint(int min, int max) {
    	Random r = new Random();
    	double x = min + (max - min) * r.nextDouble();
    	double y = min + (max - min) * r.nextDouble();
    	double z = min + (max - min) * r.nextDouble();

    	Map<String, Double> values = new HashMap<String, Double>();
    	values.put("X", x);
    	values.put("Y", y);
    	values.put("Z", z);

    	ClusteringPoint<String> p = new CentriodPoint<String>(values);
    	return p;
    }
    
    protected static List<ClusteringPoint<String>> createRandomPoints(int min, int max, int number) {
    	List<ClusteringPoint<String>> points = new ArrayList<ClusteringPoint<String>>(number);
    	for(int i = 0; i < number; i++) {
    		points.add(createRandomPoint(min,max));
    	}
    	return points;
    }

}
