package ac.ed.lurg.utils.cluster;

import java.util.Collection;
import java.util.Map;

public class CentriodPoint<K> implements ClusteringPoint<K> {

	private Map<K, Double> valueMap;

	public CentriodPoint(Map<K, Double> valueMap) {
		this.valueMap = valueMap;
	}

	@Override
    public double getClusteringValue(K key) {
		return valueMap.get(key);
	}

	@Override
	public Collection<K> getAllClusteringKeys() {
		return valueMap.keySet();
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<K, Double> e : valueMap.entrySet()) {
			sb.append(e.getKey() + "=" + e.getValue() + " ");
		}
		return sb.toString();
	}
}
