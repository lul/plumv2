package ac.ed.lurg.utils.calib;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.landuse.*;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.LandProtectionType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;
import ac.ed.lurg.yield.CropCalibrationItem;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class CalibrationManager implements Serializable {
    private static final long serialVersionUID = 680116108702669848L;
    private final transient RasterSet<LandUseItem> baselineLandUse;
    private final transient Map<CompositeCountry, Map<CropType, CropUsageData>> baselineCropUsage;
    private final transient Map<CompositeCountry, Double> baselineIrrigation = new HashMap<>();
    private final transient Map<CompositeCountry, Double> irrigationEfficiency = new HashMap<>();
    private final transient Map<CompositeCountry, WoodUsageData> baselineWoodUsage;
    private Map<CropType, Double> defaultProductionCosts = new HashMap<>();
    private final Map<CompositeCountry, CalibrationItem> parameters = new HashMap<>();
    private Map<CompositeCountry, CalibrationItem> prevParams = new HashMap<>();


    // Used for calibration run
    public CalibrationManager(RasterSet<LandUseItem> initLandUse, Map<CompositeCountry, Map<CropType, CropUsageData>> cropUsage,
                              Map<CompositeCountry, WoodUsageData> woodUsage, RasterSet<CropCalibrationItem> cropCalibrations) {
        this.baselineLandUse = new RasterSet<>(initLandUse.getHeaderDetails());
        for (RasterKey key : initLandUse.keySet()) {
            baselineLandUse.put(key, new LandUseItem(initLandUse.get(key)));
        }

        baselineCropUsage = cropUsage;
        baselineWoodUsage = woodUsage;

        readIrrigAndFertData();

        // Crop production costs
        defaultProductionCosts.put(CropType.WHEAT, ModelConfig.PROD_COST_WHEAT);
        defaultProductionCosts.put(CropType.MAIZE, ModelConfig.PROD_COST_MAIZE);
        defaultProductionCosts.put(CropType.RICE, ModelConfig.PROD_COST_RICE);
        defaultProductionCosts.put(CropType.OILCROPS_NFIX, ModelConfig.PROD_COST_OILCROPS_NFIX);
        defaultProductionCosts.put(CropType.OILCROPS_OTHER, ModelConfig.PROD_COST_OILCROPS_OTHER);
        defaultProductionCosts.put(CropType.PULSES, ModelConfig.PROD_COST_PULSES);
        defaultProductionCosts.put(CropType.STARCHY_ROOTS, ModelConfig.PROD_COST_STARCHYROOTS);
        defaultProductionCosts.put(CropType.FRUITVEG, ModelConfig.PROD_COST_FRUITVEG);
        defaultProductionCosts.put(CropType.SUGAR, ModelConfig.PROD_COST_SUGAR);
        defaultProductionCosts.put(CropType.ENERGY_CROPS, ModelConfig.PROD_COST_ENERGYCROPS);
        defaultProductionCosts.put(CropType.PASTURE, ModelConfig.PROD_COST_PASTURE);
        defaultProductionCosts.put(CropType.MONOGASTRICS, ModelConfig.PROD_COST_MONOGASTRICS);
        defaultProductionCosts.put(CropType.RUMINANTS, ModelConfig.PROD_COST_RUMINANTS);

        for(CompositeCountry cc : CountryManager.getInstance().getAllCompositeCountries()) {

            // Production costs
            Map<CropType, Double> initProdCosts = new HashMap<>(defaultProductionCosts);

            CalibrationItem calibrationItem = new CalibrationItem();
            calibrationItem.setIrrigationEfficiency(irrigationEfficiency.get(cc));
            calibrationItem.setIrrigationCostFactor(1);
            calibrationItem.setCropProdCosts(initProdCosts);
            calibrationItem.setForestryCostFactor(1);

            parameters.put(cc, calibrationItem);
            prevParams.put(cc, new CalibrationItem(calibrationItem));
        }
    }

    public CalibrationItem getCalibrationItem(CompositeCountry compositeCountry) {
        return parameters.get(compositeCountry);
    }

    public void calibrateCountryParameters(CompositeCountry compositeCountry, RasterSet<LandUseItem> countryLandUse, Map<CropType,
                                    CropUsageData> cropUsage, WoodUsageData woodUsage, Timestep timestep) {


        CalibrationItem calibrationItem = parameters.get(compositeCountry);

        double lambda = Math.exp(Math.log(0.5) / (ModelConfig.END_FIRST_STAGE_CALIBRATION / 2.0) * timestep.getTimestep());

        // Irrigation
        double countryIrrig = LandUseItem.getIrrigationTotal(countryLandUse.values(), CropType.getCropsLessPasture());
        double baseCountryIrrig = baselineIrrigation.get(compositeCountry);

        {
            double adj;
	    double ratio = countryIrrig / baseCountryIrrig;
            if ((ratio > 0.5 && ratio < 1.5) || Math.abs(countryIrrig - baseCountryIrrig) < 1) {
                adj  = 1;
            } else {
                adj = 1 + 0.5 * lambda * ((countryIrrig - baseCountryIrrig) / (countryIrrig + baseCountryIrrig));
            }

            double newCost = adj * calibrationItem.getIrrigationCostFactor();
            newCost = Math.min(Math.max(newCost, 0.1), 10);
            calibrationItem.setIrrigationCostFactor(newCost);
            //LogWriter.println("Irrigation " + compositeCountry.getName() + " = " + countryIrrig + ". Baseline = " + baseCountryIrrig + ". AdjCost = " + newCost);
        }

        // Crop production costs

        for (CropType crop : CropType.getImportedTypes()) {
            if (crop.equals(CropType.ENERGY_CROPS)) { // no data so skipping
                continue;
            }

            double baselineProd = baselineCropUsage.get(compositeCountry).get(crop).getProductionExpected();
            double prod = cropUsage.get(crop).getProductionExpected();
            double adj;
            double ratio = prod / baselineProd;
            if (prod + baselineProd < 1 || (ratio > 0.67 && ratio < 1.33)) {
                adj = 1;
            } else {
                adj = 1 + 0.5 * lambda * ((prod - baselineProd) / (baselineProd + prod));
            }

            double newCost = calibrationItem.getCropProdCosts().get(crop) * adj;
            double defaultCost = defaultProductionCosts.get(crop);
            newCost = Math.min(Math.max(newCost, defaultCost * 0.67), defaultCost * 1.33);
            calibrationItem.setCropProdCost(crop, newCost);
        }

        // Forestry costs
        if (ModelConfig.IS_FORESTRY_ON) {
            double prod = woodUsage.getProduction();
            double baselineProd = baselineWoodUsage.get(compositeCountry).getProduction();
            double adj;
            double ratio = prod / baselineProd;
            if (prod + baselineProd < 1 || (ratio > 0.67 && ratio < 1.33)) {
                adj = 1;
            } else {
                adj = 1 + 0.5 * lambda * ((prod - baselineProd) / (baselineProd + prod));
            }

            double newCost = calibrationItem.getForestryCostFactor() * adj;
            newCost = Math.min(Math.max(newCost, 0.67), 1.33);
            calibrationItem.setForestryCostFactor(newCost);
        }


    }

    private void readIrrigAndFertData() {
        StringTabularReader reader = new StringTabularReader(",", new String[]{"Iso3", "Irrigation", "IrrigationEfficiency","Fertiliser"});
        reader.read(ModelConfig.DATA_DIR + File.separator + "irrigation_fertiliser.csv");
        List<Map<String, String>> rowList = reader.getRowList();
        for (Map<String, String> row : rowList) {
            SingleCountry sc = CountryManager.getInstance().getForCode(row.get("Iso3"));
            CompositeCountry cc = CountryManager.getInstance().getForSingleCountry(sc);
            double irrig = Double.parseDouble(row.get("Irrigation"));
            double effic = Double.parseDouble(row.get("IrrigationEfficiency"));
            baselineIrrigation.merge(cc, irrig, Double::sum);
            if (irrigationEfficiency.containsKey(cc)) {
                // Efficiency average weighted by total irrigation
                double aggEffic = (irrigationEfficiency.get(cc) * baselineIrrigation.get(cc) + irrig * effic) / (baselineIrrigation.get(cc) + irrig);
                irrigationEfficiency.put(cc, aggEffic);
            } else {
                irrigationEfficiency.put(cc, effic);
            }
        }
    }

    public static CalibrationManager deserializeCalibrationManager() {
        try {
            CalibrationManager calibrationManager;
            FileInputStream fileIn = new FileInputStream(ModelConfig.CALIB_DIR + File.separator + "calibrationManager.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            calibrationManager = (CalibrationManager) in.readObject();
            in.close();
            fileIn.close();
            LogWriter.println("Deserialized " + "calibrationManager.ser");
            return calibrationManager;
        } catch (IOException i) {
            LogWriter.printlnError("Problem deserializing " + "calibrationManager.ser");
            LogWriter.print(i);
            return null;
        } catch (ClassNotFoundException c) {
            LogWriter.printlnError("RasterSet<LandUseItem> class not found");
            c.printStackTrace();
            return null;
        }
    }

}
