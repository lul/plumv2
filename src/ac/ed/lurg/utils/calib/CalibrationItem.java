package ac.ed.lurg.utils.calib;

import ac.ed.lurg.types.CropType;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CalibrationItem implements Serializable {
    private static final long serialVersionUID = 7012118381657442621L;
    private Map<CropType, Double> cropProdCosts = new HashMap<>();
    private double irrigationCostFactor;
    private double irrigationEfficiency;
    private double forestryCostFactor;

    public CalibrationItem() {}

    public CalibrationItem(CalibrationItem itemToCopy) {
        super();
        this.cropProdCosts.putAll(itemToCopy.cropProdCosts);
        this.irrigationCostFactor = itemToCopy.irrigationCostFactor;
        this.irrigationEfficiency = itemToCopy.irrigationEfficiency;
        this.forestryCostFactor = itemToCopy.forestryCostFactor;
    }

    public double getIrrigationCostFactor() {
        return irrigationCostFactor;
    }

    public void setIrrigationCostFactor(double irrigationCostFactor) {
        this.irrigationCostFactor = irrigationCostFactor;
    }

    public double getIrrigationEfficiency() {
        return irrigationEfficiency;
    }

    public void setIrrigationEfficiency(double irrigationEfficiency) {
        this.irrigationEfficiency = irrigationEfficiency;
    }

    public void setCropProdCost(CropType crop, Double cost) {
        cropProdCosts.put(crop, cost);
    }

    public void setCropProdCosts(Map<CropType, Double> cropProdCosts) {
        this.cropProdCosts = cropProdCosts;
    }

    public Map<CropType, Double> getCropProdCosts() {
        return cropProdCosts;
    }

    public void setForestryCostFactor(double forestryCostFactor) {
        this.forestryCostFactor = forestryCostFactor;
    }

    public double getForestryCostFactor() {
        return forestryCostFactor;
    }

}
