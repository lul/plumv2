package ac.ed.lurg;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;


public class Timestep implements Serializable {

	private static final long serialVersionUID = 718573167735145325L;
	private int timestep;
	
	public Timestep(int timestep) {
		this.timestep = timestep;
	}
	
	public int getTimestep() {
		return timestep;
	}

	private static int getYear(int tstep) {
		return ModelConfig.TIMESTEP_SIZE * tstep + ModelConfig.BASE_YEAR;
	}

	public int getYear() {
		return getYear(timestep);
	}
	
	public int getYieldYear() {
		if (isInitialTimestep()) {
			return getYear(ModelConfig.START_TIMESTEP);
		}
		else {
			return getPreviousTimestep().getYear();
		}
	}

	@Override
	public String toString() {
		return "Timestep: " + timestep + ", year:" + getYear();
	}

	public boolean isInitialTimestep() {
		return timestep == ModelConfig.START_TIMESTEP;
	}
	
	public Timestep getPreviousTimestep() {
		if (timestep == ModelConfig.START_TIMESTEP)
			throw new RuntimeException("Can't getPreviousTimestep for " + timestep);
		
		return new Timestep(timestep-1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + timestep;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Timestep other = (Timestep) obj;
		if (timestep != other.timestep)
			return false;
		return true;
	}

	public Collection<Integer> getYearsFromLast() {
		Collection<Integer> years = new ArrayList<Integer>();
		int currentYear = getYear();
		
		if (isInitialTimestep()) {
			years.add(currentYear);
			return years;
		}
		
		for (int i=0; i<ModelConfig.TIMESTEP_SIZE; i++) {
			years.add(currentYear - i);
		}
		
		return years;
	}
	
	public String getYearSubDir(String rootDir) {
		int targetYear = ModelConfig.CHANGE_YIELD_DATA_YEAR ? getYieldYear() : ModelConfig.BASE_YEAR;
		
		int endYear = (targetYear/ModelConfig.LPJG_TIMESTEP_SIZE) * ModelConfig.LPJG_TIMESTEP_SIZE;  // truncation in division as int
		
		return getYearSubDir(rootDir, endYear);
	}
	
	public static String getYearSubDir(String rootDir, int endYear) {
		endYear += ModelConfig.LPJ_YEAR_OFFSET;
		return rootDir + File.separator + (endYear-ModelConfig.LPJG_TIMESTEP_SIZE+1) + "-" + endYear;
	}
}
