package ac.ed.lurg.carbon;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.landuse.LandUseItem;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.utils.LogWriter;
import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class CarbonFluxReader {
	private static final double CONVERSION_FACTOR = 10.0 * 44.0/12.0; // convert kgC/m2 to tCO2-eq/ha
	private final RasterHeaderDetails rasterProj;
	private int modelYear;
	private Map<Integer, RasterKey> keyMap;
	
	public CarbonFluxReader(RasterHeaderDetails rasterProj) {
		this.rasterProj = rasterProj;
	}
	
	public CarbonFluxRasterSet getCarbonFluxes(RasterSet<LandUseItem> landUseRaster, Timestep timestep) {
		CarbonFluxRasterSet cFluxData = new CarbonFluxRasterSet(rasterProj);
		keyMap = getRasterKeys(cFluxData);
		modelYear = ModelConfig.IS_CALIBRATION_RUN || !ModelConfig.CHANGE_YIELD_DATA_YEAR ? ModelConfig.BASE_YEAR : timestep.getYear();
		long startTime = System.currentTimeMillis();

		//readData(ModelConfig.C_FLUX_NTRL_FILENAME, cFluxData, landUseRaster, LandCoverType.NATURAL);
		//readData(ModelConfig.C_FLUX_PAST_FILENAME, cFluxData, landUseRaster, LandCoverType.PASTURE);
		readData(ModelConfig.C_FLUX_FORS_FILENAME, cFluxData, landUseRaster, LandCoverType.CARBON_FOREST);
		//readData(ModelConfig.C_FLUX_FORS_FILENAME, cFluxData, landUseRaster, LandCoverType.TIMBER_FOREST);
		//readData(ModelConfig.C_FLUX_CROP_FILENAME, cFluxData, landUseRaster, LandCoverType.CROPLAND);
		
		LogWriter.println("Reading carbon data took " + (System.currentTimeMillis() - startTime) + " ms");
		return cFluxData;

	}
	
	public void readData(String fileName, CarbonFluxRasterSet carbonFluxData, RasterSet<LandUseItem> landUseRaster, LandCoverType lcType) {
		long startTime = System.currentTimeMillis();
		int maxYear = Math.floorDiv(modelYear - 10, ModelConfig.CARBON_DATA_TIMESTEP_SIZE) * ModelConfig.CARBON_DATA_TIMESTEP_SIZE + 10;
		int maxAge = modelYear - ModelConfig.CARBON_DATA_MIN_YEAR;
		Map<RasterKey, Double[]> map = new HashMap<RasterKey, Double[]>();
		for (RasterKey key : keyMap.values()) {
			map.put(key, new Double[maxAge + 1]);
		}
		for (int dataYear = maxYear; dataYear >= ModelConfig.CARBON_DATA_MIN_YEAR; dataYear -= ModelConfig.CARBON_DATA_TIMESTEP_SIZE) {
			int nCol = keyMap.size();
			int endAge = modelYear - dataYear;
			int startAge = Math.max(0, endAge - ModelConfig.CARBON_DATA_TIMESTEP_SIZE + 1);
			String filePath = getDataDir(fileName, dataYear); 
			try {
				RandomAccessFile reader = new RandomAccessFile(filePath, "r");
				int startPos = nCol * startAge * Double.BYTES;
				int nRead = (endAge - startAge + 1) * nCol * Double.BYTES;
				reader.seek(startPos);
				byte[] byteData = new byte[nRead];
				reader.read(byteData);
				Double[] data = byteArrayToDouble(byteData);
				for (int i = 0; i < nCol; i++) {
					Double[] locData = map.get(keyMap.get(i));
					for (int j = 0; j <= endAge - startAge; j++) {
						locData[j + startAge] = data[i + j * nCol] * CONVERSION_FACTOR;
					}
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (RasterKey key : map.keySet()) {
			if (landUseRaster.containsKey(key)) {
				CarbonFluxItem cfItem = carbonFluxData.get(key.getCol(), key.getRow());
				cfItem.calcCarbonCreditRate(map.get(key), lcType);
			}
		}
		LogWriter.println("Reading " + fileName + " took " + (System.currentTimeMillis() - startTime) + " ms");
	}
	
	private Double[] byteArrayToDouble(byte[] bytes) {
		int n = bytes.length / Double.BYTES;
		Double[] d = new Double[n];
		for (int i = 0; i < n; i++) {
			ByteBuffer buff = ByteBuffer.allocate(Double.BYTES);
			byte[] doubleBytes = Arrays.copyOfRange(bytes, i * Double.BYTES, i * Double.BYTES + Double.BYTES);
			buff.put(doubleBytes);
			buff.flip();
			d[i] = buff.getDouble();
		}
		
		return(d);
	}
	
	public Map<Integer, RasterKey> getRasterKeys(CarbonFluxRasterSet carbonFluxData) {
		Map<Integer, RasterKey> keyMap = new HashMap<Integer, RasterKey>();
		String filePath = ModelConfig.FORESTRY_DIR_BASE + File.separator + "coordinates.dat";
		byte[] bytes;
		try {
			bytes = Files.readAllBytes(Paths.get(filePath));
			Double[] data = byteArrayToDouble(bytes);
			int nCol = 2;
			int nRow = data.length / nCol;
			for (int i = 0; i < nRow; i++) {
				RasterKey key = carbonFluxData.getKeyFromCoordinates(data[i * nCol], data[i * nCol + 1]);
				keyMap.put(i, key);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return keyMap;
	}

	private String getDataDir(String filename, int dataYear) {
		return ModelConfig.FORESTRY_DIR_BASE + File.separator + ModelConfig.FORESTRY_DIR_TOP + File.separator + dataYear + File.separator + filename;
	}
}
