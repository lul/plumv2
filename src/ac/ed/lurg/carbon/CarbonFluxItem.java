package ac.ed.lurg.carbon;

import ac.ed.lurg.landuse.LandCoverChangeKey;
import ac.sac.raster.RasterItem;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.LandCoverType;

public class CarbonFluxItem implements RasterItem, Serializable {
	private static final long serialVersionUID = 440720456140537815L;
	Map<LandCoverChangeKey, Double> carbonCreditRate = new HashMap<>();
	
	public void calcCarbonCreditRate(Double[] cFluxes, LandCoverType lcType) {
		double totalFlux = 0;
		for (int i = 0; i <= ModelConfig.CARBON_HORIZON; i++) {
			totalFlux += cFluxes[i];
		}

		totalFlux *= -1; // flip sign so positive mean net carbon uptake

		// Tallying up difference in net carbon uptake for each LCC
		// so that carbonCreditRate(A -> B) = net C uptake for land cover b - net C uptake for land cover A
		for (LandCoverType otherLc : LandCoverType.getConvertibleTypes()) {
			if (otherLc.equals(lcType))
				continue;
			carbonCreditRate.merge(new LandCoverChangeKey(otherLc, lcType), totalFlux, Double::sum);
		}
	}

	public void setCarbonCreditRate(LandCoverChangeKey key, double cFlux) {
		carbonCreditRate.put(key, cFlux);
	}
	
	public void setCarbonCreditRate(LandCoverType fromLc, LandCoverType toLc, double cFlux) {
		carbonCreditRate.put(new LandCoverChangeKey(fromLc, toLc), cFlux);
	}

	public double getCarbonCreditRate(LandCoverType fromLc, LandCoverType toLc) {
		return carbonCreditRate.getOrDefault(new LandCoverChangeKey(fromLc, toLc), Double.NaN);
	}

	public static CarbonFluxItem getDefault() {
		CarbonFluxItem item = new CarbonFluxItem();
		for (LandCoverType fromLc : LandCoverType.getConvertibleTypes()) {
			for (LandCoverType toLc : LandCoverType.getConvertibleTypes()) {
				LandCoverChangeKey key = new LandCoverChangeKey(fromLc, toLc);
				item.setCarbonCreditRate(key, 0.0);
			}
		}
		return item;
	}

}
