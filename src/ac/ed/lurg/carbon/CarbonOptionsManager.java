package ac.ed.lurg.carbon;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class CarbonOptionsManager {
	private final StringTabularReader carbonOptionsReader;

	public CarbonOptionsManager() {
		String fileName = ModelConfig.CARBON_OPTIONS_FILE;
		carbonOptionsReader = new StringTabularReader(",", new String[]{"From", "To", "Enabled"});
		
		if (new File(fileName).isFile()) 
			carbonOptionsReader.read(fileName);
		else
			LogWriter.printlnError("Can't find a carbon options file (" + fileName + ").");
	}
	
	public boolean isCarbonFluxEnabled(LandCoverType fromLc, LandCoverType toLc) {
		Map<String, String> query = new HashMap<String, String>();
		query.put("From", fromLc.getName());
		query.put("To", toLc.getName());
		
		Map<String, String> row = carbonOptionsReader.querySingle(query);
		boolean isEnabled =  Boolean.parseBoolean(row.get("Enabled"));
		return isEnabled;		
	}
}
