package ac.ed.lurg.carbon;

import java.util.Collection;
import java.util.Set;

import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class CarbonFluxRasterSet extends RasterSet<CarbonFluxItem> {

	private static final long serialVersionUID = 4986837799421955684L;

	public CarbonFluxRasterSet(RasterHeaderDetails header) {
		super(header);
	}
	
	protected CarbonFluxItem createRasterData() {
		return new CarbonFluxItem();
	}
	
	@Override
	public CarbonFluxRasterSet createSubsetForKeys(Collection<RasterKey> keys) {
		CarbonFluxRasterSet subsetCarbonFluxRaster = new CarbonFluxRasterSet(getHeaderDetails());
		popSubsetForKeys(subsetCarbonFluxRaster, keys);		
		return subsetCarbonFluxRaster;
	}

	public void fillWithDefaults(Set<RasterKey> keys) {
		for (RasterKey key : keys) {
			put(key, CarbonFluxItem.getDefault());
		}
	}
}
