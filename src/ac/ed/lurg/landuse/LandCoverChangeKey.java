package ac.ed.lurg.landuse;

import ac.ed.lurg.types.LandCoverType;

import java.io.Serializable;
import java.util.Objects;

public class LandCoverChangeKey implements Serializable {
    private static final long serialVersionUID = -1508179950561288035L;
    private final LandCoverType fromLc;
    private final LandCoverType toLc;

    public LandCoverChangeKey(LandCoverType fromLc, LandCoverType toLc) {
        this.fromLc = fromLc;
        this.toLc = toLc;
    }

    public LandCoverType getFromLc() {
        return fromLc;
    }

    public LandCoverType getToLc() {
        return toLc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LandCoverChangeKey that = (LandCoverChangeKey) o;
        return fromLc == that.fromLc && toLc == that.toLc;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromLc, toLc);
    }
}
