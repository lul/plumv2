package ac.ed.lurg.landuse;

import java.io.Serializable;
import java.util.Objects;

import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.LandProtectionType;

public class LandKey implements Serializable {
	private static final long serialVersionUID = -1173322425708587699L;

	private final LandCoverType lcType;
	private final LandProtectionType lpType;
	
	public LandKey(LandCoverType lcType, LandProtectionType lpType) {
		super();
		this.lcType = lcType;
		this.lpType = lpType;
	}

	public LandCoverType getLcType() {
		return lcType;
	}

	public LandProtectionType getLpType() {
		return lpType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LandKey landKey = (LandKey) o;
		return lcType == landKey.lcType && lpType == landKey.lpType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(lcType, lpType);
	}

	@Override
	public String toString() {
		return "LandKey{" +
				"lcType=" + lcType +
				", lpType=" + lpType +
				'}';
	}
}
