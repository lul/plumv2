package ac.ed.lurg.landuse;

import java.io.Serializable;

public class WoodUsageData implements Serializable {

	private static final long serialVersionUID = -3329080279189782763L;
	
	private double expectedProduction;
	private double netImport;
	private final double prodCost;
	
	public WoodUsageData(double harvest, double netImport, double prodCost) {
		this.expectedProduction = harvest;
		this.netImport = netImport;
		this.prodCost = prodCost;
	}

	public double getProduction() {
		return expectedProduction;
	}

	public void setProduction(double production) {
		this.expectedProduction = production;
	}

	public double getNetImport() {
		return netImport;
	}

	public void setNetImports(double netImport) {
		this.netImport = netImport;
	}


    public double getProdCostRate() {
		return prodCost / expectedProduction;
    }
}
