package ac.ed.lurg.landuse;

import java.io.Serializable;

public class LandCoverDetail implements Serializable {
    private static final long serialVersionUID = -6738825942958097252L;
    private double area;

    public LandCoverDetail(double area) {
        this.area = area;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

}
