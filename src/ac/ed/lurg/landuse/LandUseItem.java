package ac.ed.lurg.landuse;

import java.io.Serializable;
import java.util.*;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.*;
import ac.ed.lurg.utils.Interpolator;
import ac.ed.lurg.utils.LogWriter;
import ac.sac.raster.InterpolatingRasterItem;

public class LandUseItem implements InterpolatingRasterItem<LandUseItem>, Serializable {
	private static final long serialVersionUID = 763526147224355158L;
	private Map<LandKey, LandCoverDetail> landCoverAreas = new HashMap<>();
	private final Map<LandChangeKey, Double> landCoverChange = new HashMap<>(); // land cover change for previous timestep
	private double protectedFraction; // protected fraction of total area
	private Map<LandKey, Double> maxLandCoverAreas = new HashMap<>(); // maximum area for cropland and solar
	private Map<LandChangeKey, Double> maxLandCoverChange = new HashMap<>(); // maximum allowed LCC due to slope and other constraints
	
	public LandUseItem() {}

	public LandUseItem(LandCoverItem landCover) {
		this();

		landCoverAreas.putAll(landCover.getInitialLandCoverAreas());
		setProtectedFraction(landCover.getProtectedFraction(landCoverAreas));

		AgricultureDetail cropDetail = (AgricultureDetail) getLandCoverDetail(new LandKey(LandCoverType.CROPLAND, LandProtectionType.UNPROTECTED));
		AgricultureDetail avDetail = (AgricultureDetail) getLandCoverDetail(new LandKey(LandCoverType.AGRIVOLTAICS, LandProtectionType.UNPROTECTED));

		for (CropType crop : CropType.getCropsLessPasture()) {
			cropDetail.setCropFraction(crop, landCover.getCropFraction(crop));
			cropDetail.setIntensity(crop, new Intensity(0.5, 0.5, 0.5));

			avDetail.setCropFraction(crop, 0);
			avDetail.setIntensity(crop, new Intensity(0.5, 0.5, 0.5));
		}

		AgricultureDetail pastureDetail = (AgricultureDetail) getLandCoverDetail(new LandKey(LandCoverType.PASTURE, LandProtectionType.UNPROTECTED));
		pastureDetail.setCropFraction(CropType.PASTURE, 1.0);
		pastureDetail.setIntensity(CropType.PASTURE, new Intensity(0, 0, 0.5));

		// Assuming homogeneity between protected and unprotected areas so need to distribute maximum areas between them
		for (LandCoverType lc : LandCoverType.getConvertibleTypes()) {
			double maxArea;
			switch (lc) {
				case CROPLAND: maxArea = landCover.getMaxCroplandArea(); break;
				case PHOTOVOLTAICS: maxArea = landCover.getMaxSolarArea(); break;
				case AGRIVOLTAICS: maxArea = Math.min(landCover.getMaxCroplandArea(), landCover.getMaxSolarArea()); break;
				default: maxArea = getSuitableArea(); break;
			}
			// Take maximum in case current unprotected area exceeds max area if distributed proportionally
			double unprotArea = Math.max(maxArea * (1 - protectedFraction), getLandCoverArea(new LandKey(lc, LandProtectionType.UNPROTECTED)));
			double protArea = Math.max(maxArea - unprotArea, 0);

			maxLandCoverAreas.put(new LandKey(lc, LandProtectionType.UNPROTECTED), unprotArea);
			maxLandCoverAreas.put(new LandKey(lc, LandProtectionType.PROTECTED), protArea);
		}

		calcMaxLandCoverChange();

	}

	public LandUseItem(LandUseItem luItemToCopy) {
		this();
		landCoverAreas.putAll(luItemToCopy.landCoverAreas);
		landCoverChange.putAll(luItemToCopy.landCoverChange);
		protectedFraction = luItemToCopy.protectedFraction;
		maxLandCoverAreas.putAll(luItemToCopy.maxLandCoverAreas);
		maxLandCoverChange.putAll(luItemToCopy.maxLandCoverChange);
	}

	public void setProtectedFraction(double protFrac) {
		if (!ModelConfig.PROTECTED_AREAS_ENABLED) {
			this.protectedFraction = 0;
			return;
		}
		if (!Double.isNaN(ModelConfig.CONSTANT_PROTECTED_AREA_RATE)) {
			this.protectedFraction = ModelConfig.CONSTANT_PROTECTED_AREA_RATE;
		} else {
			this.protectedFraction = Math.max(protFrac, ModelConfig.MIN_NATURAL_RATE);
		}
	}

	public LandCoverDetail getLandCoverDetail(LandKey key) {
		return landCoverAreas.computeIfAbsent(key, this::getMissingDetail);
	}

	private LandCoverDetail getMissingDetail(LandKey key) {
		switch(key.getLcType()) {
			case CROPLAND:
			case PASTURE:
			case AGRIVOLTAICS:
				return new AgricultureDetail(0);
			case TIMBER_FOREST:
			case CARBON_FOREST:
				return new ForestDetail(0);
			case NATURAL:
				return new NaturalDetail(0);
			default:
				return new LandCoverDetail(0);
		}
	}

	public Set<LandKey> getLandKeys() {
		return landCoverAreas.keySet();
	}

	public Set<LandKey> getLandKeysForLandCover(LandCoverType landCoverType) {
		Set<LandKey> keys = new HashSet<>();
		for (LandKey key : getLandKeys()) {
			if (key.getLcType().equals(landCoverType)) {
				keys.add(key);
			}
		}
		return keys;
	}

	public Set<LandKey> getAgricultureLandKeys() {
		Set<LandKey> keys = new HashSet<>();
		keys.addAll(getLandKeysForLandCover(LandCoverType.CROPLAND));
		keys.addAll(getLandKeysForLandCover(LandCoverType.PASTURE));
		keys.addAll(getLandKeysForLandCover(LandCoverType.AGRIVOLTAICS));
		return keys;
	}

	public double getLandCoverArea(LandKey key) {
		return landCoverAreas.containsKey(key) ? landCoverAreas.get(key).getArea() : 0;
	}

	public double getLandCoverArea(LandCoverType lcType) {
		double total = 0;
		for (LandProtectionType lpType : LandProtectionType.values()) {
			total += getLandCoverArea(new LandKey(lcType, lpType));
		}
		return total;
	}

	public double getLandCoverArea(LandProtectionType lpType) {
		double total = 0;
		for (LandCoverType lcType : LandCoverType.values()) {
			total += getLandCoverArea(new LandKey(lcType, lpType));
		}
		return total;
	}

	public void setLandCoverArea(LandKey key, double area) {
		getLandCoverDetail(key).setArea(area);
	}

	public double getTotalLandArea() {
		double area = 0;
		for (LandCoverDetail lcDetail : landCoverAreas.values()) {
				area += lcDetail.getArea();
		}
		return area;
	}
	
	public double getLandCoverFract(LandCoverType lcType) {
		double totalArea = getTotalLandArea();
		double a = getLandCoverArea(lcType);
		return totalArea == 0 ? 0.0 : a / totalArea;
	}

	public void setMaxLandCoverArea(LandKey key, double area) {
		maxLandCoverAreas.put(key, area);
	}

	public double getMaxLandCoverArea(LandKey key) {
		return maxLandCoverAreas.getOrDefault(key, 0.0);
	}

	public void setMaxLandCoverChange(LandChangeKey key, double area) {
		this.maxLandCoverChange.put(key, area);
	}

	public double getMaxLandCoverChange(LandChangeKey key) {
		return maxLandCoverChange.getOrDefault(key, 0.0);
	}

	public Map<LandChangeKey, Double> getMaxLandCoverChangeMap() {
		return maxLandCoverChange;
	}

	public void calcMaxLandCoverChange() {
		// Cropland, photovoltaics and agrivoltaics areas are limited due to slope and accessibility
		// Need to calculate maximum allowed conversion to these land covers on a grid cell basis
		// so that maximum allowed conversion is consistent on cluster and grid-cell level.

		maxLandCoverChange.clear();

		for (LandCoverType fromLc : LandCoverType.getConvertibleTypes()) {
			for (LandCoverType toLc : LandCoverType.getConvertibleTypes()) {
				for (LandProtectionType lpType : LandProtectionType.values()) {
					LandKey fromKey = new LandKey(fromLc, lpType);
					LandKey toKey = new LandKey(toLc, lpType);
					LandChangeKey lcKey = new LandChangeKey(fromKey, toKey);

					double conversionLimit = getAvailableArea(lcKey);
					if (conversionLimit > 0) {
						maxLandCoverChange.put(lcKey, conversionLimit);
					}
				}
			}
		}

	}

	public double getRemainingArea(LandKey landKey) {
		// Calculates how much more of given land cover can be created, respecting maximum area limits
		LandCoverType lcType = landKey.getLcType();
		LandProtectionType lpType = landKey.getLpType();

		double maxNewArea;
		double cropArea = getLandCoverArea(new LandKey(LandCoverType.CROPLAND, lpType));
		double pvArea = getLandCoverArea(new LandKey(LandCoverType.PHOTOVOLTAICS, lpType));
		double avArea = getLandCoverArea(new LandKey(LandCoverType.AGRIVOLTAICS, lpType));

		switch (lcType) {
			case CROPLAND:
				maxNewArea = getMaxLandCoverArea(landKey) - cropArea - avArea; // max cropland minus existing cropland and AV
				break;
			case PHOTOVOLTAICS:
				maxNewArea = getMaxLandCoverArea(landKey) - pvArea - avArea; // max solar minus existing PV and AV
				break;
			case AGRIVOLTAICS:
				maxNewArea = getMaxLandCoverArea(landKey) - avArea - cropArea - pvArea; // AV counts as both cropland and photovoltaics
				break;
			default:
				maxNewArea = getMaxLandCoverArea(landKey) - getLandCoverArea(landKey);
				break;
		}
		return Math.max(maxNewArea, 0);
	}

	public double getAvailableArea(LandChangeKey lcKey) {
		// Calculates how much area is available for conversion between two land covers

		if (lcKey.getFromLandKey().equals(lcKey.getToLandKey())) { // conversion of land cover to itself is just current area
			return getLandCoverArea(lcKey.getFromLandKey());

		} else {
			double maxNewArea = getRemainingArea(lcKey.getToLandKey()); // how much more of target land cover can be created
			double baseArea = getLandCoverArea(lcKey.getFromLandKey()); // how much source land cover
			return Math.min(maxNewArea, baseArea);
		}
	}

	public double getSuitableArea() {
		return getTotalLandArea() - getLandCoverArea(LandCoverType.BARREN) - getLandCoverArea(LandCoverType.URBAN);
	}

	public void setLandCoverChanges(Map<LandChangeKey, Double> changesMap) {
		landCoverChange.clear();
		landCoverChange.putAll(changesMap);
	}
	
	public Map<LandChangeKey, Double> getLandCoverChanges() {
		return landCoverChange;
	}

	public void moveAreas(LandKey fromKey, LandKey toKey, double area) {
		double fromArea = getLandCoverArea(fromKey);
		double toArea = getLandCoverArea(toKey);
		if (area - fromArea > 1e-6) {
			LogWriter.println("" + (area - fromArea));
			throw new RuntimeException("LandUseItem.moveAreas: trying to move too much area.");
		}
		area = Math.min(area, fromArea);

		// Update unmanaged forest fraction (increases if converting from managed forest, decreased otherwise)
		if (toKey.getLcType().equals(LandCoverType.NATURAL) && !fromKey.getLcType().equals(LandCoverType.NATURAL)) {
			NaturalDetail naturalDetail = (NaturalDetail) getLandCoverDetail(toKey);
			double prevUnmanForest = naturalDetail.getArea() * naturalDetail.getUnmanagedForestFraction();

			double newFract;
			if (fromKey.getLcType().equals(LandCoverType.TIMBER_FOREST) || fromKey.getLcType().equals(LandCoverType.CARBON_FOREST)) {
				newFract = (prevUnmanForest + area) / (toArea + area);

			} else {
				newFract = prevUnmanForest / (toArea + area);
			}
			naturalDetail.setUnmanagedForestFraction(newFract);
		}

		if ((fromKey.getLcType().equals(LandCoverType.TIMBER_FOREST) || fromKey.getLcType().equals(LandCoverType.CARBON_FOREST))
				&& toKey.getLcType().equals(LandCoverType.NATURAL)) {
			NaturalDetail naturalDetail = (NaturalDetail) getLandCoverDetail(toKey);
			double prevUnmanForest = naturalDetail.getArea() * naturalDetail.getUnmanagedForestFraction();
			double newUnmanForest = prevUnmanForest + area;
			double newFract = newUnmanForest / (toArea + area);
			naturalDetail.setUnmanagedForestFraction(newFract);
		}

		setLandCoverArea(fromKey, fromArea - area);
		setLandCoverArea(toKey, toArea + area);

		calcMaxLandCoverChange();
	}

	public void updateProtectedArea(double protFract) { // for changing protected fraction during model run
		double totalArea = getTotalLandArea();
		double maxProtectableFraction = (totalArea - getLandCoverArea(LandCoverType.URBAN)) / totalArea;
		this.protectedFraction = Math.min(protFract, maxProtectableFraction); // adjusting for urban area which can't be protected

		double previousProtectedTotal = getLandCoverArea(LandProtectionType.PROTECTED);
		double newProtectedTotal = protectedFraction * (getTotalLandArea());
		double diffArea = newProtectedTotal - previousProtectedTotal;
		boolean isMore = diffArea > 0; // true if increasing protected area, false otherwise
		diffArea = Math.abs(diffArea);

		// Order in which to add protected area
		final List<LandCoverType> priority = Arrays.asList(LandCoverType.BARREN, LandCoverType.NATURAL, LandCoverType.CARBON_FOREST,
				LandCoverType.TIMBER_FOREST, LandCoverType.PASTURE, LandCoverType.CROPLAND, LandCoverType.AGRIVOLTAICS, LandCoverType.PHOTOVOLTAICS);

		if (isMore) {
			for (LandCoverType lcType : priority) {
				if (diffArea < 1e-9) {
					break;
				}
				LandKey fromKey = new LandKey(lcType, LandProtectionType.UNPROTECTED);
				// If ENABLE_LAND_SHARING or is BARREN then LandCoverType remains the same otherwise it becomes natural
				LandCoverType toLc = ModelConfig.ENABLE_LAND_SHARING || lcType.equals(LandCoverType.BARREN) ? lcType : LandCoverType.NATURAL;
				LandKey toKey = new LandKey(toLc, LandProtectionType.PROTECTED);
				double a = Math.min(getLandCoverArea(fromKey), diffArea);
				if (a < 1e-10) {
					continue;
				}
				moveAreas(fromKey, toKey, a);
				diffArea -= a;
			}
		} else {
			Collections.reverse(priority); // unprotect in reverse order
			for (LandCoverType lcType : priority) {
				if (diffArea < 1e-9) {
					break;
				}
				LandKey fromKey = new LandKey(lcType, LandProtectionType.PROTECTED);
				LandKey toKey = new LandKey(lcType, LandProtectionType.UNPROTECTED);
				double a = Math.min(getLandCoverArea(fromKey), diffArea);
				if (a < 1e-10) {
					continue;
				}
				moveAreas(fromKey, toKey, a);
				diffArea -= a;
			}
		}

		if (diffArea > 1e-9) {
			LogWriter.printlnError("LandUseItem.updateProtectedArea cannot move enough area");
		}

		// If protectedFraction changed then we need to update maximum areas since those are distributed proportionally
		// between protected and unprotected
		updateMaximumLandCoverAreas();
	}

	private void updateMaximumLandCoverAreas() {
		// Redistribute maximum areas based on protected fraction
		double totalMaxCropland = 0;
		double totalMaxSolar = 0;
		for (LandProtectionType lpType : LandProtectionType.values()) {
			totalMaxCropland += getMaxLandCoverArea(new LandKey(LandCoverType.CROPLAND, lpType));
			totalMaxSolar += getMaxLandCoverArea(new LandKey(LandCoverType.PHOTOVOLTAICS, lpType));
		}
		maxLandCoverAreas.put(new LandKey(LandCoverType.CROPLAND, LandProtectionType.UNPROTECTED), totalMaxCropland * (1 - protectedFraction));
		maxLandCoverAreas.put(new LandKey(LandCoverType.CROPLAND, LandProtectionType.PROTECTED), totalMaxCropland * protectedFraction);
		maxLandCoverAreas.put(new LandKey(LandCoverType.PHOTOVOLTAICS, LandProtectionType.UNPROTECTED), totalMaxSolar * (1 - protectedFraction));
		maxLandCoverAreas.put(new LandKey(LandCoverType.PHOTOVOLTAICS, LandProtectionType.PROTECTED), totalMaxSolar * protectedFraction);

		//TODO rest of land covers
	}

	public double getTotalCropArea(CropType crop) {
		double total = 0;
		for (LandKey landKey : getAgricultureLandKeys()) {
			AgricultureDetail detail = (AgricultureDetail) getLandCoverDetail(landKey);
			total += detail.getCropArea(crop);
		}
		return total;
	}

	public double getAverageIrrigationIntensity(CropType crop) {
		double intensityArea = 0;
		double totalArea = 0;
		for (LandKey key : getAgricultureLandKeys()) {
			AgricultureDetail detail = (AgricultureDetail) getLandCoverDetail(key);
			intensityArea += detail.getIrrigationIntensity(crop) * detail.getCropArea(crop);
			totalArea += detail.getCropArea(crop);
		}
		return totalArea > 0 ? intensityArea / totalArea : 0;
	}

	public double getAverageFertiliserRate(CropType crop) {
		double intensityArea = 0;
		double totalArea = 0;
		for (LandKey key : getAgricultureLandKeys()) {
			AgricultureDetail detail = (AgricultureDetail) getLandCoverDetail(key);
			intensityArea += detail.getFertiliserRate(crop) * detail.getCropArea(crop);
			totalArea += detail.getCropArea(crop);
		}
		return totalArea > 0 ? intensityArea / totalArea : 0;
	}

	public double getAverageCropFraction(CropType crop) {
		double cropArea = 0;
		for (LandKey key : getAgricultureLandKeys()) {
			AgricultureDetail detail = (AgricultureDetail) getLandCoverDetail(key);
			cropArea += detail.getCropArea(crop);
		}
		double totalArea = crop.equals(CropType.PASTURE) ? getLandCoverArea(LandCoverType.PASTURE) :
				getLandCoverArea(LandCoverType.CROPLAND);

		return totalArea > 0 ? cropArea / totalArea : 0;
	}

	public static double getFertiliserTotal(Collection<? extends LandUseItem> items, Collection<CropType> crops) {
		double total = 0;
		for (LandUseItem a : items) {
			for (LandKey key : a.getAgricultureLandKeys()) {
				AgricultureDetail detail = (AgricultureDetail) a.getLandCoverDetail(key);
				for (CropType c : crops) {
					total += detail.getFertiliserAmount(c);
				}
			}
		}
		return total;
	}

	public static double getIrrigationTotal(Collection<? extends LandUseItem> items, Collection<CropType> crops) {
		double total = 0;
		for (LandUseItem a : items) {
			for (LandKey key : a.getAgricultureLandKeys()) {
				AgricultureDetail detail = (AgricultureDetail) a.getLandCoverDetail(key);
				for (CropType c : crops) {
					total += detail.getIrrigationAmount(c);
				}
			}
		}
		return total;
	}

	public static double getManagementIntensityTotal(Collection<? extends LandUseItem> items, Collection<CropType> crops) {
		double total = 0;
		for (LandUseItem a : items) {
			for (LandKey key : a.getAgricultureLandKeys()) {
				AgricultureDetail detail = (AgricultureDetail) a.getLandCoverDetail(key);
				for (CropType c : crops) {
					total += detail.getManagementIntensityArea(c);
				}
			}
		}
		return total;
	}
	
	public static double getSuitableTotal(Collection<? extends LandUseItem> landUses) {
		double total = 0;
		for (LandUseItem a : landUses) {
			if (a != null) {
				double suitable = a.getSuitableArea();
					total += suitable;
			}
		}
		return total;
	}

	private boolean isZeroOrNull(Double d) {
		return d == null || d == 0;
	}
	 
	@Override
	public void interpolateAll(LandUseItem fromItem, LandUseItem toItem, double factor) {
		landCoverAreas = new HashMap<>();
/*
		for ()

		Double fromCropCover = fromItem.getLandCoverArea(LandCoverType.CROPLAND);
		Double toCropCover = toItem.getLandCoverArea(LandCoverType.CROPLAND);

		if (!isZeroOrNull(fromCropCover) && isZeroOrNull(toCropCover)) { // if start with crop but end with none, take starting crop fractions
			cropFractions.putAll(fromItem.cropFractions);
			intensityMap.putAll(fromItem.intensityMap);
		}
		else if (isZeroOrNull(fromCropCover) && !isZeroOrNull(toCropCover)) { // if start with no crop but end with some, take end crop fractions
			cropFractions.putAll(toItem.cropFractions);
			intensityMap.putAll(toItem.intensityMap);
		}
		else { // otherwise we need to interpolate crop fractions
			for (CropType crop : CropType.values()) {
				Double from = fromItem.cropFractions.get(crop);
				Double to = toItem.cropFractions.get(crop);
				Double d = Interpolator.interpolate(from, to, factor);
				cropFractions.put(crop, d);
				
				Intensity fromIntensity = fromItem.intensityMap.get(crop);
				Intensity toIntensity = toItem.intensityMap.get(crop);
				Intensity interpolateIntensity = toIntensity;  // might still be null
				
				if (fromIntensity != null && toIntensity != null)
					interpolateIntensity = new Intensity(fromIntensity, toIntensity, factor);  // both non-null really interpolate
				else if (fromIntensity != null)
					interpolateIntensity = fromIntensity; // just fromIntensity non-null
				
				intensityMap.put(crop, interpolateIntensity);
			}
		}
*/
		Set<LandKey> keys = new HashSet<>();
		keys.addAll(fromItem.getLandKeys());
		keys.addAll(toItem.getLandKeys());


		for (LandKey key : keys) {
			Double from = fromItem.getLandCoverArea(key);
			Double to = toItem.getLandCoverArea(key);
			Double d = Interpolator.interpolate(from, to, factor);
			setLandCoverArea(key, d);
		}
	}
	
	public static double getTotalLandCover(Collection<? extends LandUseItem> landUses, LandCoverType landCover) {
		double total = 0;
		for (LandUseItem a : landUses) {
			if (a!=null) {
				double d = a.getLandCoverArea(landCover);
				total += d;
			}
		}
		return total;
	}

	public static double getTotalUnmanagedForestArea(Collection<? extends LandUseItem> landUses) {
		double total = 0;
		for (LandUseItem luItem : landUses) {
			for (LandKey landKey : luItem.getLandKeys()) {
				if (landKey.getLcType().equals(LandCoverType.NATURAL)) {
					NaturalDetail detail = (NaturalDetail) luItem.getLandCoverDetail(landKey);
					total += detail.getUnmanagedForestFraction() * detail.getArea();
				}
			}
		}
		return total;
	}

	public static double getTotalCropArea(Collection<LandUseItem> landUses, CropType crop) {
		double total = 0;
		for (LandUseItem luItem : landUses)
			for (LandKey landKey : luItem.getAgricultureLandKeys()) {
					AgricultureDetail detail = (AgricultureDetail) luItem.getLandCoverDetail(landKey);
					total += detail.getCropArea(crop);
			}
		return total;
	}

	@Override
	public String toString() {
		Map<LandCoverType, Double> convertibleAreas = new HashMap<>();
		Map<LandCoverType, Double> protectedAreas = new HashMap<>();
		for (LandCoverType lcType : LandCoverType.values()) {
			convertibleAreas.put(lcType, getLandCoverArea(new LandKey(lcType, LandProtectionType.UNPROTECTED)));
			protectedAreas.put(lcType, getLandCoverArea(new LandKey(lcType, LandProtectionType.PROTECTED)));
		}
		return "LandUseItem: [landCoverAreas=" + convertibleAreas + ", protectedArea=" + protectedAreas + "]";
	}
}