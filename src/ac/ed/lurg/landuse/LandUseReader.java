package ac.ed.lurg.landuse;

import java.util.Map;

import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.LandProtectionType;
import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

// This class is currently not use as using serialization instead, but could be handy at some stage
public class LandUseReader extends AbstractTabularRasterReader<LandUseItem> {	

	private static final int MIN_COLS = 20;

	public LandUseReader(RasterSet<LandUseItem> landCover) {
		super(",", MIN_COLS, landCover);
	}

	@Override
	protected void setData(RasterKey key, LandUseItem luData, Map<String, Double> rowValues) {
		
		for (LandCoverType cover : LandCoverType.values()) {
			luData.setLandCoverArea(new LandKey(cover, LandProtectionType.UNPROTECTED), getValueForCol(rowValues, cover.getName()));
		}
		
		luData.setProtectedFraction(getValueForCol(rowValues, "protected") / luData.getTotalLandArea());

		for (CropType crop : CropType.getAllItems()) {
  			double cropFract = getValueForCol(rowValues, crop.getGamsName() + "_A");
			double fertI = getValueForCol(rowValues, crop.getGamsName() + "_FI");
			double irrigI = getValueForCol(rowValues, crop.getGamsName() + "_II");
			double otherI = getValueForCol(rowValues, crop.getGamsName() + "_OI");
			Intensity intensity = new Intensity(fertI, irrigI, otherI);
			if (CropType.PASTURE.equals(crop)) {  // pasture isn't a crop so cropFract will always be zero for it, but still want to set intensities
				AgricultureDetail agricultureDetail = (AgricultureDetail) luData.getLandCoverDetail(new LandKey(LandCoverType.PASTURE, LandProtectionType.UNPROTECTED));
				agricultureDetail.setCropFraction(crop, cropFract);
				agricultureDetail.setIntensity(crop, intensity);
			} else if (cropFract > 0) {
				AgricultureDetail agricultureDetail = (AgricultureDetail) luData.getLandCoverDetail(new LandKey(LandCoverType.CROPLAND, LandProtectionType.UNPROTECTED));
				agricultureDetail.setCropFraction(crop, cropFract);
				agricultureDetail.setIntensity(crop, intensity);
			}
		}
	}
}
