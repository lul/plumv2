package ac.ed.lurg.landuse;

import ac.ed.lurg.ModelConfig;
import ac.sac.raster.AbstractRasterReader;
import ac.sac.raster.RasterSet;

public class IrrigiationCostReader extends AbstractRasterReader<IrrigationItem> {	
	
	public IrrigiationCostReader (RasterSet<IrrigationItem> dataset) {
		super(dataset);
	}

	@Override
	public void setData(IrrigationItem item, String token) {
		if (!"nan".equals(token)) {	
			double irrigCost = Double.parseDouble(token);
			item.setIrrigCost(irrigCost * ModelConfig.getAdjParam("IRRIG_COST_SCALE_FACTOR"));
		}
	}
}
