package ac.ed.lurg.landuse;

import ac.sac.raster.AbstractRasterReader;
import ac.sac.raster.IntegerRasterItem;
import ac.sac.raster.RasterSet;

public class FPUBoundaryReader extends AbstractRasterReader<IntegerRasterItem> {	

	public FPUBoundaryReader (RasterSet<IntegerRasterItem> dataset) {
		super(dataset);
	}

	@Override
	public void setData(IntegerRasterItem item, String value) {
		
		item.setInt(Integer.parseInt(value));
	}
	
	
}
