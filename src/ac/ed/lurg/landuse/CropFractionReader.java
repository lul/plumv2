package ac.ed.lurg.landuse;

import ac.ed.lurg.types.CropType;
import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.util.Map;

public class CropFractionReader extends AbstractTabularRasterReader<LandCoverItem> {
    private static final int MIN_COLS = 12;

    public CropFractionReader(RasterSet<LandCoverItem> landCover) {
        super(",", MIN_COLS, landCover);
    }

    @Override
    protected void setData(RasterKey key, LandCoverItem item, Map<String, Double> rowValues) {
        for (CropType crop : CropType.getCropsLessPasture()) {
           item.setCropFraction(crop, getValueForCol(rowValues, crop.getGamsName()));
        }
    }
}
