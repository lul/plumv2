package ac.ed.lurg.landuse;

import java.io.Serializable;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.utils.Interpolator;

public class Intensity implements Serializable {
	private static final long serialVersionUID = -1698567941247993827L;
	
	private double fertiliserIntensity;
	private double irrigationIntensity;
	private double otherIntensity;
	private double yield;
	private double unitEnergy;
	private double maxIrrigRate;
	
	public Intensity(double fertiliserIntensity, double irrigationIntensity, double otherIntensity) {
		super();
		this.fertiliserIntensity = fertiliserIntensity;
		this.irrigationIntensity = irrigationIntensity;
		this.otherIntensity = otherIntensity;
	}
	
	public Intensity(double fertiliserIntensity, double irrigationIntensity, double otherIntensity, double yield, double unitEnergy, double maxIrrigRate) {
		this(fertiliserIntensity, irrigationIntensity, otherIntensity);
		this.yield = yield;
		this.unitEnergy = unitEnergy;
		this.maxIrrigRate = maxIrrigRate;
	}

	public double getFertiliserIntensity() {
		return fertiliserIntensity;
	}
	
	public double getFertiliserRate() {
		return ModelConfig.MIN_FERT_AMOUNT + fertiliserIntensity * (ModelConfig.MAX_FERT_AMOUNT - ModelConfig.MIN_FERT_AMOUNT);
	}

	public double getIrrigationIntensity() {
		return irrigationIntensity;
	}

	public double getOtherIntensity() {
		return otherIntensity;
	}

	public double getYield() {
		return yield;
	}

	public double getUnitEnergy() {
		return unitEnergy;
	}

	/** irrigation rate in litre/m2 */
	public double getIrrigationRate() {
		return maxIrrigRate * irrigationIntensity;
	}

	public double getMaxIrrigRate() {
		return maxIrrigRate;
	}

	public Intensity(Intensity from, Intensity to, double factor) {
		this(
			Interpolator.interpolate(from.fertiliserIntensity, to.fertiliserIntensity, factor), 
			Interpolator.interpolate(from.irrigationIntensity, to.irrigationIntensity, factor), 
			Interpolator.interpolate(from.otherIntensity, to.otherIntensity, factor), 
			Interpolator.interpolate(from.yield, to.yield, factor), 
			Interpolator.interpolate(from.unitEnergy, to.unitEnergy, factor), 
			Interpolator.interpolate(from.maxIrrigRate, to.maxIrrigRate, factor));
	}
}
