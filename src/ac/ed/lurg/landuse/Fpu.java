package ac.ed.lurg.landuse;

import ac.sac.raster.RasterItem;

/** This is currently really just a marker class to aid type safety, using Integer directly would have worked, but been less clear*/

public class Fpu implements RasterItem {
	private Integer id;
	
	Fpu(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Fpu" + id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fpu other = (Fpu) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
