package ac.ed.lurg.landuse;

import java.io.Serializable;

public class CropUsageData implements Serializable  {

	private static final long serialVersionUID = 1200684888140690926L;
	
	private double ruminantFeed;
	private double monogastricFeed;
	private double netImportsExpected;
	private double prodShock;
	private double netImportCostExpected;
	private double prod;
	private double prodCost;
	private double area;

	public CropUsageData(double prod) {
		this.prod = prod;
	}
	
	public CropUsageData(double ruminantFeed, double monogastricFeed, double netImportsExpected, double netImportCost, double prod, double prodCost, double area, double prodShock) {
		this.ruminantFeed = ruminantFeed;
		this.monogastricFeed = monogastricFeed;
		this.netImportsExpected = netImportsExpected;
		this.netImportCostExpected = netImportCost;
		this.prod = prod;
		this.prodCost = prodCost;
		this.area= area;
		this.prodShock = prodShock;
	}
	
	public CropUsageData(double ruminantFeed, double monogastricFeed, double netImportsExpected, double prod) {
		this.ruminantFeed = ruminantFeed;
		this.monogastricFeed = monogastricFeed;
		this.netImportsExpected = netImportsExpected;
		this.prod = prod;
	}
		
	public double getRuminantFeed() {
		return ruminantFeed;
	}
	
	public double getMonogastricFeed() {
		return monogastricFeed;
	}

	public double getNetImportCostExpected() {
		return netImportCostExpected;
	}
	
	public double getNetImportsExpected() {
		return netImportsExpected;
	}
	
	public double getProductionExpected() {
		return prod;
	}
	
	public double getShockedNetImports() {
		return netImportsExpected + prodShock;
	}
	
	public double getProductionShock() {
		return prodShock;
	}
	
	public double getTotalProdCost() {
		return prodCost;
	}
	
	public double getProdCostRate() {
		if (prod - prodShock <= 0.0)
			return Double.NaN;
		else
			return prodCost / (prod - prodShock);
	}

	public void updateNetImports(double netImports) {
		this.netImportsExpected = netImports;
	}
	
	public double getArea(){
		return area;
	}

	public double getNetSupply() {
		return prod + netImportsExpected;
	}
}