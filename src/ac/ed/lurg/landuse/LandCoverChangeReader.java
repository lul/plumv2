package ac.ed.lurg.landuse;

import ac.ed.lurg.types.LandCoverType;
import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.util.Map;

public class LandCoverChangeReader extends AbstractTabularRasterReader<ForcedLccItem> {
    private static final int MIN_COLS = 3;

    public LandCoverChangeReader(RasterSet<ForcedLccItem> lccRaster) {
        super(" +", MIN_COLS, lccRaster);
    }

    @Override
    protected void setData(RasterKey key, ForcedLccItem item, Map<String, Double> rowValues) {
        for (LandCoverType fromLc : LandCoverType.values()) {
            for (LandCoverType toLc : LandCoverType.values()) {
                String colName = fromLc.getName() + "->" + toLc.getName();
                if (rowValues.containsKey(colName)) {
                    double fract = getValueForCol(rowValues, colName);
                    item.setFraction(fromLc, toLc, fract);
                }
            }
        }
    }
}
