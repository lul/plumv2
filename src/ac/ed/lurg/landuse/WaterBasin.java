package ac.ed.lurg.landuse;

public class WaterBasin {

	private String basinId;

	public WaterBasin (String basinId) {
		this.basinId = basinId;
	}

	public String getId() {
		return basinId;
	}

	@Override
	public String toString() {
		return "WaterBasin " + basinId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((basinId == null) ? 0 : basinId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WaterBasin other = (WaterBasin) obj;
		if (basinId == null) {
			if (other.basinId != null)
				return false;
		} else if (!basinId.equals(other.basinId))
			return false;
		return true;
	}
}
