package ac.ed.lurg.landuse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;
import ac.sac.raster.IntegerRasterItem;
import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class FPUManager {
	private static final int FPU_COL = 0;
	private static final int YEAR_COL = 1;
	private static final int USAGE_COL = 2;

	private final StringTabularReader fpuGroups;
	private final Map<WaterBasin, List<RasterKey>> basinToRasterKeysMap = new HashMap<>();
	private final Map<WaterBasin, Map<Integer, Double>> basinToOtherUses = new HashMap<>();
	private final RasterSet<Fpu> fpuMap; // for clustering

	public FPUManager(RasterHeaderDetails desiredProjection) {		
		fpuGroups = new StringTabularReader(",", new String[]{"fpu", "basin"});
		fpuGroups.read(ModelConfig.FPU_GROUPING_FILE);

		fpuMap = new RasterSet<>(desiredProjection);

		// Handle rasterkeys
		Map<Fpu, List<RasterKey>> fpuToRasterKeys = getMapFpuToKeys(desiredProjection);		
		for (Map.Entry<Fpu, List<RasterKey>> entry : fpuToRasterKeys.entrySet()) {
			WaterBasin basin = getBasin(entry.getKey());

			List<RasterKey> basinRasterKeys = basinToRasterKeysMap.computeIfAbsent(basin, k -> new ArrayList<>());
			basinRasterKeys.addAll(entry.getValue());
		}
		
		// Handle other water uses
		Map<Fpu, Map<Integer, Double>> fpuToOtherUses = getFpuToOtherUses();
		for (Map.Entry<Fpu, Map<Integer, Double>> entry : fpuToOtherUses.entrySet()) {
			WaterBasin basin = getBasin(entry.getKey());

			Map<Integer, Double> basinOtherUses = basinToOtherUses.computeIfAbsent(basin, k -> new HashMap<>());

			for (Map.Entry<Integer, Double> fpuOuEntry : entry.getValue().entrySet()) {
				Integer year = fpuOuEntry.getKey();
				Double newOtherUse = fpuOuEntry.getValue();
				if (basinOtherUses.containsKey(year)) 
					newOtherUse += basinOtherUses.get(year);

				basinOtherUses.put(year, newOtherUse);
			}			
		}
	}

	private WaterBasin getBasin(Fpu fpu) {
		String basin = fpu.toString();

		List<Map<String, String>> rowList = fpuGroups.getRowList();
		for (Map<String, String> row : rowList) {
			if (row.get("fpu").equals(fpu.toString())) {
				basin = row.get("basin");
				break;
			}
		}

		return new WaterBasin(basin);
	}

	private Map<Fpu, List<RasterKey>> getMapFpuToKeys(RasterHeaderDetails desiredProjection) {
		RasterSet<IntegerRasterItem> fpuBoundaries = new RasterSet<IntegerRasterItem>(desiredProjection) {
			private static final long serialVersionUID = -8620255271155259176L;
			protected IntegerRasterItem createRasterData() {
				return new IntegerRasterItem(0);
			}
		};
		
		// read FPU boundary raster
		FPUBoundaryReader fpuReader = new FPUBoundaryReader(fpuBoundaries);
		fpuReader.getRasterDataFromFile(ModelConfig.FPU_BOUNDARIES_FILE);
		
		Map<Fpu, List<RasterKey>> invertedBoundaries = new HashMap<>();
		
		for (Map.Entry<RasterKey, IntegerRasterItem> entry : fpuBoundaries.entrySet()) {
			Fpu fpu = new Fpu(entry.getValue().getInt());
			List<RasterKey> rasterKeys = invertedBoundaries.computeIfAbsent(fpu, k -> new ArrayList<>());

			rasterKeys.add(entry.getKey());

			fpuMap.put(entry.getKey(), fpu);
 		}

		return invertedBoundaries;
	}

	private Map<Fpu, Map<Integer, Double>> getFpuToOtherUses() {
		Map<Fpu, Map<Integer, Double>> otherUsesMap = new HashMap<>();

		String filename = ModelConfig.OTHER_WATER_USES_FILE;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line;
			int fpuId, year;
			double waterUsage;
			Fpu fpu;

			reader.readLine(); // read header

			while ((line = reader.readLine()) != null) {
				String[] tokens = line.split(",");

				if (tokens.length < 3)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);

				fpuId = Integer.parseInt(tokens[FPU_COL]);
				fpu = new Fpu(fpuId);
				year = Integer.parseInt(tokens[YEAR_COL]);
				waterUsage = Double.parseDouble(tokens[USAGE_COL]);

				Map<Integer, Double> fpuData = otherUsesMap.computeIfAbsent(fpu, k -> new HashMap<>());

				fpuData.put(year, waterUsage);
			}
			reader.close();

		} catch (IOException e) {
			LogWriter.printlnError("Failed in reading water usage");
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename + ", create " + otherUsesMap.size() + " water usage maps values");
		return otherUsesMap;
	}

	public List<RasterKey> getKeysFor(WaterBasin basin) {
		List<RasterKey> keys = basinToRasterKeysMap.get(basin);
		if (keys == null)
			keys = new ArrayList<>();
		return keys;
	}

	public double getOtherWaterUse(WaterBasin basin, Integer year) {
		return basinToOtherUses.get(basin).get(year);
	}

	public Collection<WaterBasin> getWaterBasins() {
		return basinToRasterKeysMap.keySet();
	}

	public RasterSet<Fpu> getFpuRasterSet() {
		return fpuMap;
	}
}