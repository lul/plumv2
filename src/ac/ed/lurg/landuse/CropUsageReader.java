package ac.ed.lurg.landuse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LazyHashMap;
import ac.ed.lurg.utils.LogWriter;

public class CropUsageReader {
	private static final int COUNTRY_COL = 0; 
	private static final int COMMODITY_COL = 2;
	private static final int IMPORT_COL = 9;
	private static final int EXPORT_COL = 10;
	private static final int PRODUCTION_COL = 8;
	private static final int FEED_COL = 11;
	
	public CropUsageReader() {
	}

	@SuppressWarnings("serial")
	public Map<CompositeCountry, Map<CropType, CropUsageData>> getCommodityData() {
		
		LazyHashMap<CompositeCountry, Map<CropType, CropUsageData>> commodityMap = new LazyHashMap<CompositeCountry, Map<CropType, CropUsageData>>() {
			protected Map<CropType, CropUsageData> createValue() { 
				Map<CropType, CropUsageData> countryData = new HashMap<CropType, CropUsageData>();
				countryData.put(CropType.ENERGY_CROPS, new CropUsageData(0, 0, 0.0, Double.NaN, 0, Double.NaN, Double.NaN, 0)); // 2nd generation energy crops not in FAO data, so adding zero values.
				return countryData;
			}
		};
		
		String filename = ModelConfig.BASELINE_CONSUMP_FILE;
		try {
			BufferedReader fitReader = new BufferedReader(new FileReader(filename)); 
			String line, countryCode, commodityName;
			double netImports, production, feed;
			fitReader.readLine(); // read header

			while ((line=fitReader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 12)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				countryCode = tokens[COUNTRY_COL];
				commodityName = tokens[COMMODITY_COL];
				netImports = Double.parseDouble(tokens[IMPORT_COL]) - Double.parseDouble(tokens[EXPORT_COL]);
				production = Double.parseDouble(tokens[PRODUCTION_COL]);
				feed = Double.parseDouble(tokens[FEED_COL]);

				SingleCountry country = CountryManager.getInstance().getForCode(countryCode);
				
				CropType crop = CropType.getForFaoName(commodityName);
				CompositeCountry cc = CountryManager.getInstance().getForSingleCountry(country);

				Map<CropType, CropUsageData> countryData = commodityMap.lazyGet(cc);
				CropUsageData oldData = countryData.get(crop);
				
				// aggregate if multiple countries for cc
				if (oldData != null) {
					netImports += oldData.getNetImportsExpected();
					production += oldData.getProductionExpected();
					feed += oldData.getMonogastricFeed();
				}
				
				CropUsageData newData = new CropUsageData(0, feed, netImports, Double.NaN, production, Double.NaN, Double.NaN, 0);
				countryData.put(crop, newData);
			} 
			fitReader.close(); 
		
		} catch (IOException e) {
			LogWriter.printlnError("Failed in reading commodity usage data");
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename + ", create " + commodityMap.size() + " country commodity maps values", 2);
		
		return commodityMap;
	}
}
