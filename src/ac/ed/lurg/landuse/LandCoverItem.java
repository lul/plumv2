package ac.ed.lurg.landuse;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.LandProtectionType;
import ac.ed.lurg.utils.LogWriter;
import ac.sac.raster.RasterItem;

/** Used to hold less detailed land-cover information
 *  This is used in the initalisation phase, after that land-use is used */
public class LandCoverItem implements RasterItem {
	
	private final Map<LandCoverType, Double> landCoverFract = new HashMap<>(); // Land cover fraction
	private final Map<CropType, Double> cropFractions = new HashMap<>();
	private double totalArea;
	private double maxCroplandFraction; // due to slope
	private double protectedFraction = ModelConfig.MIN_NATURAL_RATE;
	private double unmanagedForestFraction;
	private double maxSolarFraction; // slope and distance from urban

	/** Area in Mha */ 
	private Double getLandCoverArea(LandCoverType landType) {
		return getLandCoverFract(landType) * totalArea;
	}
	
	private Double getLandCoverFract(LandCoverType landType) {
		return landCoverFract.getOrDefault(landType, 0.0);
	}

	/** Area in Mha */ 
	public double getTotalArea() {
		return totalArea;
	}

	/** Area in Mha */ 
	public void setTotalArea(double totalArea) {
		this.totalArea = totalArea;
	}
	
	public void setLandCoverFract(LandCoverType landType, double d) {
		landCoverFract.put(landType, d);
	}
	
	public double getMaxCroplandArea(){
		// In case of disagreement, trust land cover data
		return Math.max(maxCroplandFraction * getTotalArea(), getLandCoverArea(LandCoverType.CROPLAND));
	}
	
	public void setMaxCropFraction(double maxCropFraction){
		this.maxCroplandFraction = maxCropFraction;
	}

	public void setProtectedFraction(double protectedFract) {
		this.protectedFraction = Math.max(ModelConfig.MIN_NATURAL_RATE, protectedFract);
	}

	public void setUnmanagedForestFraction(double unmanagedForestFraction) {
		this.unmanagedForestFraction = unmanagedForestFraction;
	}

	public double getUnmanagedForestFraction() {
		return unmanagedForestFraction;
	}

	public void setCropFraction(CropType crop, double fract) {
		cropFractions.put(crop, fract);
	}

	public double getCropFraction(CropType crop) {
		return cropFractions.getOrDefault(crop, 0.0);
	}

	public Map<LandKey, LandCoverDetail> getInitialLandCoverAreas() {
		Map<LandKey, LandCoverDetail> initAreas = new HashMap<>();

		double protectableFraction = getLandCoverFract(LandCoverType.NATURAL) + getLandCoverFract(LandCoverType.BARREN);
		// need to scale protectedFraction by protectableFraction and cap at 1.0. Gives fraction of protectable LC types to protect
		double adjProtectedFraction = Math.min(1.0, protectedFraction / protectableFraction);

		initAreas.put(new LandKey(LandCoverType.URBAN, LandProtectionType.UNPROTECTED), new LandCoverDetail(getLandCoverArea(LandCoverType.URBAN)));

		initAreas.put(new LandKey(LandCoverType.BARREN, LandProtectionType.PROTECTED), new LandCoverDetail(getLandCoverArea(LandCoverType.BARREN) * adjProtectedFraction));
		initAreas.put(new LandKey(LandCoverType.BARREN, LandProtectionType.UNPROTECTED), new LandCoverDetail(getLandCoverArea(LandCoverType.BARREN) * (1 - adjProtectedFraction)));

		NaturalDetail protectedNatural = new NaturalDetail(getLandCoverArea(LandCoverType.NATURAL) * adjProtectedFraction);
		NaturalDetail unprotectedNatural = new NaturalDetail(getLandCoverArea(LandCoverType.NATURAL) * (1 - adjProtectedFraction));
		protectedNatural.setUnmanagedForestFraction(getUnmanagedForestFraction());
		unprotectedNatural.setUnmanagedForestFraction(getUnmanagedForestFraction());
		initAreas.put(new LandKey(LandCoverType.NATURAL, LandProtectionType.PROTECTED), protectedNatural);
		initAreas.put(new LandKey(LandCoverType.NATURAL, LandProtectionType.UNPROTECTED), unprotectedNatural);

		initAreas.put(new LandKey(LandCoverType.CARBON_FOREST, LandProtectionType.UNPROTECTED), new ForestDetail(getLandCoverArea(LandCoverType.CARBON_FOREST)));

		initAreas.put(new LandKey(LandCoverType.TIMBER_FOREST, LandProtectionType.UNPROTECTED), new ForestDetail(getLandCoverArea(LandCoverType.TIMBER_FOREST)));

		initAreas.put(new LandKey(LandCoverType.PASTURE, LandProtectionType.UNPROTECTED), new AgricultureDetail(getLandCoverArea(LandCoverType.PASTURE)));

		initAreas.put(new LandKey(LandCoverType.CROPLAND, LandProtectionType.UNPROTECTED), new AgricultureDetail(getLandCoverArea(LandCoverType.CROPLAND)));

		initAreas.put(new LandKey(LandCoverType.PHOTOVOLTAICS, LandProtectionType.UNPROTECTED), new LandCoverDetail(getLandCoverArea(LandCoverType.PHOTOVOLTAICS)));

		// Remove
		Map<LandKey, LandCoverDetail> initAreasNoZeroes = new HashMap<>();
		for (LandKey landKey : initAreas.keySet()) {
			if (initAreas.get(landKey).getArea() > 0) {
				initAreasNoZeroes.put(landKey, initAreas.get(landKey));
			}
		}

		// Check that areas have been allocated correctly
		Map<LandCoverType, Double> areasBefore = new HashMap<>();
		for (LandCoverType lc : LandCoverType.values()) {
			areasBefore.put(lc, getLandCoverArea(lc));
		}

		Map<LandCoverType, Double> areasAfter = new HashMap<>();
		for (LandKey key : initAreasNoZeroes.keySet()) {
			areasAfter.merge(key.getLcType(), initAreasNoZeroes.get(key).getArea(), Double::sum);
		}

		runAreaCheck(areasBefore, areasAfter);

		return initAreasNoZeroes;
	}

	private void runAreaCheck(Map<LandCoverType, Double> areasBefore, Map<LandCoverType, Double> areasAfter) {
		final double THRESHOLD = 1e-7;
		for (LandCoverType lcType : LandCoverType.values()) {
			double b = areasBefore.getOrDefault(lcType, 0.0);
			double a = areasAfter.getOrDefault(lcType, 0.0);
			if (Math.abs(b - a) > THRESHOLD) {
				LogWriter.printlnError(String.format("LandCoverItem: area before and area after different: %s, %s, %s", lcType, b, a));
			}
		}
	}

	// Recalculate protected fraction as this could be different from initial value due to
	// disagreement between protected areas map and land cover map (e.g. grid cell is protected but only contains cropland)
	public double getProtectedFraction(Map<LandKey, LandCoverDetail> areas) {
		double protectedArea = 0;
		for (LandKey key : areas.keySet()) {
			if (key.getLpType().equals(LandProtectionType.PROTECTED)) {
				protectedArea += areas.get(key).getArea();
			}
		}
		return protectedArea / getTotalArea();
	}

	public void setMaxSolarFraction(double maxSolarFraction) {
		this.maxSolarFraction = maxSolarFraction;
	}

	public double getMaxSolarArea() {
		return Math.max(maxSolarFraction * getTotalArea(), getLandCoverArea(LandCoverType.PHOTOVOLTAICS));
	}
}