package ac.ed.lurg.landuse;

import ac.ed.lurg.types.CropType;

import java.util.*;

public class AgricultureDetail extends LandCoverDetail {
    private static final long serialVersionUID = 5065585226135379L;
    private final Map<CropType, Intensity> intensityMap = new HashMap<>();
    private final Map<CropType, Double> cropFractions = new HashMap<>();

    public AgricultureDetail(double area) {
        super(area);
    }

    public Set<CropType> getCrops() {
        Set<CropType> crops = new HashSet<>();
        crops.addAll(intensityMap.keySet());
        crops.addAll(cropFractions.keySet());
        return crops;
    }

    public Intensity getIntensity(CropType cropType) {
        return intensityMap.get(cropType);
    }

    public void setIntensity(CropType cropType, Intensity intensity) {
        intensityMap.put(cropType, intensity);
    }

    public double getCropFraction(CropType cropType) {
        return cropFractions.getOrDefault(cropType, 0.0);
    }

    public void setCropFraction(CropType cropType, double areaFraction) {
        cropFractions.put(cropType, areaFraction);
    }

    public double getCropArea(CropType crop) {
        return getArea() * getCropFraction(crop);
    }

    public double getFertiliserIntensity(CropType crop) {
        return intensityMap.containsKey(crop) ? intensityMap.get(crop).getFertiliserIntensity() : 0.0;
    }

    /** Fertiliser rate in kg/ha */
    public double getFertiliserRate(CropType crop) {
        return intensityMap.containsKey(crop) ? intensityMap.get(crop).getFertiliserRate() : 0.0;
    }

    /** Fertiliser amount in kt (1000 tonnes), for this location */
    public double getFertiliserAmount(CropType crop) {
        return getFertiliserRate(crop) * getCropArea(crop);
    }

    /** Irrigation Intensity (unit less) */
    public double getIrrigationIntensity(CropType crop) {
        return intensityMap.containsKey(crop) ? intensityMap.get(crop).getIrrigationIntensity() : 0.0;
    }

    /** Irrigation rate in litre/m2 */
    public double getIrrigationRate(CropType crop) {
        return intensityMap.containsKey(crop) ? intensityMap.get(crop).getIrrigationRate() : 0.0;
    }

    /** Irrigation amount in km3 or 10^9 m3, for this location */
    public double getIrrigationAmount(CropType crop) {
        // rate(10^-3m or mm or l/m2) * area(10^6ha) = 10^3 m.ha = 10^7 m3 = 1/100 km3
        return getIrrigationRate(crop) * getCropArea(crop) * 0.01;
    }

    public double getManagementIntensity(CropType crop) {
        return intensityMap.containsKey(crop) ? intensityMap.get(crop).getOtherIntensity() : 0.0;
    }

    /** Management intensity index area (index * area) */
    public double getManagementIntensityArea(CropType crop) {
        return getManagementIntensity(crop) * getCropArea(crop);
    }

}
