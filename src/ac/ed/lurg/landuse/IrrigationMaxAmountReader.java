package ac.ed.lurg.landuse;

import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.FertiliserRate;
import ac.ed.lurg.types.IrrigationRate;
import ac.ed.lurg.types.YieldType;
import ac.ed.lurg.yield.LPJCropTypes;
import ac.ed.lurg.yield.YieldRaster;
import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class IrrigationMaxAmountReader extends AbstractTabularRasterReader<IrrigationItem> {	

	private static final int MIN_COLS = 6;
	private YieldRaster yieldRaster;

	public IrrigationMaxAmountReader(RasterSet<IrrigationItem> irigCosts, YieldRaster yieldRaster) {
		super("\\s+", MIN_COLS, irigCosts);
		this.yieldRaster = yieldRaster;
	}
		
	@Override
	protected void setData(RasterKey key, IrrigationItem item, Map<String, Double> rowValues) {
		
		String highFertIrrigString = YieldType.getYieldType(FertiliserRate.MAX_FERT, IrrigationRate.MAX_IRRIG).getFertIrrigString();
		
		double c3Cereals = getValueForCol(rowValues, LPJCropTypes.C3_CEREALS + highFertIrrigString);
		double c4Cereals = getValueForCol(rowValues, LPJCropTypes.C4_CEREALS + highFertIrrigString);
		double rice = getValueForCol(rowValues, LPJCropTypes.RICE + highFertIrrigString);
		double oilcropsNFix = getValueForCol(rowValues, LPJCropTypes.OILCROPS_NFIX + highFertIrrigString);
		double oilcropsOther = getValueForCol(rowValues, LPJCropTypes.OILCROPS_OTHER + highFertIrrigString);
		double pulses = getValueForCol(rowValues, LPJCropTypes.PULSES + highFertIrrigString);
		double starchy_roots = getValueForCol(rowValues, LPJCropTypes.STARCHY_ROOTS + highFertIrrigString);
		double fruitveg = getValueForCol(rowValues, LPJCropTypes.FRUITVEG + highFertIrrigString);
		LPJCropTypes sugarCrop = yieldRaster.get(key).getSugarCrop();
		double sugar = getValueForCol(rowValues, sugarCrop + highFertIrrigString);

		item.setMaxIrrigAmount(CropType.WHEAT, c3Cereals);
		item.setMaxIrrigAmount(CropType.MAIZE, c4Cereals);
		item.setMaxIrrigAmount(CropType.ENERGY_CROPS, c4Cereals);
		item.setMaxIrrigAmount(CropType.RICE, rice + 200.0); // an additional 30cm of water is required to flood rice paddy fields for crop protection that is not technically irrigation, but still requires water
		item.setMaxIrrigAmount(CropType.OILCROPS_NFIX, oilcropsNFix);
		item.setMaxIrrigAmount(CropType.OILCROPS_OTHER, oilcropsOther);
		item.setMaxIrrigAmount(CropType.PULSES, pulses);
		item.setMaxIrrigAmount(CropType.STARCHY_ROOTS, starchy_roots);
		item.setMaxIrrigAmount(CropType.FRUITVEG, fruitveg);
		item.setMaxIrrigAmount(CropType.SUGAR, sugar);
		item.setMaxIrrigAmount(CropType.SETASIDE, 0.0);
	}
}