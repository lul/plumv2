package ac.ed.lurg.landuse;

import ac.sac.raster.AbstractRasterReader;
import ac.sac.raster.RasterSet;

public class InitProtectedAreasReader extends AbstractRasterReader<LandCoverItem> {
	public InitProtectedAreasReader (RasterSet<LandCoverItem> dataset) {
		super(dataset);
	}

	@Override
	public void setData(LandCoverItem lcData, String token) {
		double value = Double.parseDouble(token);
		if (value >= 0) { // no data is -9999
			lcData.setProtectedFraction(value);
		}
	}	
}
