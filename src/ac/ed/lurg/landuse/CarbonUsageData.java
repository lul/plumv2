package ac.ed.lurg.landuse;

import java.io.Serializable;

public class CarbonUsageData implements Serializable {
	private static final long serialVersionUID = -7324234146849230386L;
	
	private final double carbonCredits;
	private double netCarbonImport;
	private double netCarbonFlux;
	
	public CarbonUsageData(double carbonCredits, double netCarbonImport, double netCarbonFlux) {
		super();
		this.carbonCredits = carbonCredits;
		this.netCarbonImport = netCarbonImport;
	}

	public double getCarbonCredits() {
		return carbonCredits;
	}

	public double getNetCarbonImport() {
		return netCarbonImport;
	}

	public double getNetCarbonFlux() {
		return netCarbonFlux;
	}

	public void updateNetImports(double netImports) {
		this.netCarbonImport = netImports;
	}
}
