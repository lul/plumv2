package ac.ed.lurg.landuse;

import ac.ed.lurg.types.LandCoverType;
import ac.sac.raster.RasterItem;

import java.util.HashMap;
import java.util.Map;

public class ForcedLccItem implements RasterItem {
    private final Map<LandCoverType, Map<LandCoverType, Double>> fractions;

    public ForcedLccItem() {
        fractions = new HashMap<>();
    }

    public void setFraction(LandCoverType fromLc, LandCoverType toLc, double fract) {
        Map<LandCoverType, Double> toMap = fractions.computeIfAbsent(fromLc, k -> new HashMap<>());
        toMap.put(toLc, fract);
    }

    public Map<LandCoverType, Map<LandCoverType, Double>> getFractions() {
        return fractions;
    }
}
