package ac.ed.lurg.landuse;

import ac.sac.raster.RasterItem;

public class LandCoverChangeItem implements RasterItem {
	private LandKey fromLandKey;
	private LandKey toLandKey;
	private double area;
	
	public LandCoverChangeItem(LandKey fromLandKey, LandKey toLandKey, double area) {
		this.fromLandKey = fromLandKey;
		this.toLandKey = toLandKey;
		this.area = area;
	}

	public LandKey getFromLandKey() {
		return fromLandKey;
	}

	public LandKey getToLandKey() {
		return toLandKey;
	}

	public double getArea() {
		return area;
	}
}
