package ac.ed.lurg.landuse;

import java.util.Map;

import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class IrrigationConstraintReader extends AbstractTabularRasterReader<IrrigationItem> {	

	private static final int MIN_COLS = 3;

	public IrrigationConstraintReader(RasterSet<IrrigationItem> irigCosts) {
		super("[ |\t]+", MIN_COLS, irigCosts);
	}
		
	@Override
	protected void setData(RasterKey key, IrrigationItem item, Map<String, Double> rowValues) {
		item.setBaselineIrrigConstraint(getValueForCol(rowValues, "Max_irr_water")); // 1 to convert from mm to litres/m2
	}
}
