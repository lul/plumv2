package ac.ed.lurg.landuse;

import java.util.Map;

import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class MaxCropAreaReader extends AbstractTabularRasterReader<LandCoverItem> {	

	private static final int MIN_COLS = 3;

	public MaxCropAreaReader(RasterSet<LandCoverItem> dataset) {
		super("[ |\t]+", MIN_COLS, dataset);
	}
		
	@Override
	protected void setData(RasterKey key, LandCoverItem item, Map<String, Double> rowValues) {
		item.setMaxCropFraction(getValueForCol(rowValues, "maxcropfrac"));
	}
}

