package ac.ed.lurg.landuse;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.sac.raster.RasterItem;

public class IrrigationItem implements RasterItem {
	// we have gone from CGIAR Aridity Index to an aridity cost by doing the following:  1 - log10(aridityIndex+1)/5
	// when is the limited to be >= 0 (only Greenland had negative values)
	// this gives a value from 1 to 0, with 1 being the most costly and 0 the least.
	
	private double irrigationCost; // cost per 10000 Mlitre or 10^10 l or 10^7 m3 or 10^-2 km3, when multipled by rates from areas in Mha makes sense to have it in this apparently odd unit
	private double irrigConstraint;  // mm or litre/m2 (they are the same) of water available
	private double baseIrrigConstraint; // irrig constraint in a baseline year (2010?), from Elliott blue_water file
	private double baseRunOff; // lpj runoff in baseline year 
	private double runOff; // lpj runoff in current year (which is updated)
	private double constraintOffset; // calculated difference to Elliott data in baseline year
	private Map<CropType, Double> maxIrrigAmounts = new HashMap<CropType, Double>(); // amount of water (litres/m2) used at maximum irrigation, i.e. to achieve no water stress

	public double getIrrigCost() {
		return irrigationCost;
	}
	
	public void setIrrigCost(double aridityIndex) {
		this.irrigationCost = aridityIndex;
	}
	
	public double getMaxIrrigAmount(CropType crop) {
		if (CropType.PASTURE == crop)
			return ModelConfig.PASTURE_MAX_IRRIGATION_RATE;

		Double d = maxIrrigAmounts.get(crop);
		
		//double irrigEffMultiplier = (!ModelConfig.SHOCKS_POSSIBLE) ? ModelConfig.IRRIG_EFF_MULTIPLIER : ModelConfig.getParameter(IRRIG_EFF_MULTIPLIER, timestep.getYear());
		
		
		return d==null ? 0.0 : d.doubleValue();
	}
	
	public void setMaxIrrigAmount(CropType crop, double maxIrrig) {
		maxIrrigAmounts.put(crop, maxIrrig);
	}
	
	public double getIrrigConstraint() {
		double d;
		if (ModelConfig.USE_BLUE_WATER_FILE_IRRIG_CONSTRAINT)
			d = baseIrrigConstraint;
		else
			d = irrigConstraint;
		
		return (d < 0) ? 0.0 : d;  // a negative constraint does not make sense
	}
	
	public void setIrrigConstraint(double irrigConstraint) {
		this.irrigConstraint = irrigConstraint;
	}
	
	public void setBaselineIrrigConstraint(double baseIrrigConstraint) {
		this.baseIrrigConstraint = baseIrrigConstraint;
	}
	
	public double getBaselineIrrigConstraint() {
		return baseIrrigConstraint;
	}

	public double getRunOff() {
		return runOff;
	}
	
	public void setRunOff(double runOff) {
		this.runOff = runOff;
	}
	
	public double getBaseRunOff() {
		return baseRunOff;
	}

	public void setBaseRunOff(double baseRunOff) {
		this.baseRunOff = baseRunOff;
	}

	public double getConstraintOffset() {
		return constraintOffset;
	}
	
	public void setConstraintOffsets(double constraintOffset) {
		this.constraintOffset = constraintOffset;
	}

	public static IrrigationItem getDefault() {
		IrrigationItem item = new IrrigationItem();
		item.setIrrigCost(ModelConfig.IRRIG_COST_SCALE_FACTOR * 100);
		item.setBaselineIrrigConstraint(0.0);
		item.setBaseRunOff(0.0);
		for (CropType crop: CropType.values()) {
			item.setMaxIrrigAmount(crop, 0.0);
		}
		return item;
	}
}
