package ac.ed.lurg.landuse;

import ac.ed.lurg.ModelConfig;

public class ForestDetail extends LandCoverDetail {

    private static final long serialVersionUID = -6033325801415372035L;
    private double rotationIntensity = 1.0 / ModelConfig.CARBON_WOOD_MAX_TIME;

    public ForestDetail(double area) {
        super(area);
    }

    public double getRotationIntensity() {
        return rotationIntensity;
    }

    public void setRotationIntensity(double rotationIntensity) {
        this.rotationIntensity = rotationIntensity;
    }

}
