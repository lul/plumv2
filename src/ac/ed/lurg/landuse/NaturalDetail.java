package ac.ed.lurg.landuse;

public class NaturalDetail extends LandCoverDetail {
    private static final long serialVersionUID = 3301174579688174L;
    private double unmanagedForestFraction;

    public NaturalDetail(double area) {
        super(area);
    }

    public double getUnmanagedForestFraction() {
        return unmanagedForestFraction;
    }

    public void setUnmanagedForestFraction(double unmanagedForestFraction) {
        this.unmanagedForestFraction = unmanagedForestFraction;
    }
}
