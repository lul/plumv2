package ac.ed.lurg.landuse;

import ac.sac.raster.AbstractRasterReader;
import ac.sac.raster.RasterSet;

public class ProtectedAreasReader extends AbstractRasterReader<ProtectedAreaItem> {

	public ProtectedAreasReader(RasterSet<ProtectedAreaItem> dataset) {
		super(dataset);
	}

	@Override
	public void setData(ProtectedAreaItem data, String token) {
		double value = Double.parseDouble(token);
		if (value >= 0) { // no data is -9999
			data.setProtectedFraction(value);
		}
	}	
}