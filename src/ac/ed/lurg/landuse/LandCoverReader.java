package ac.ed.lurg.landuse;

import java.util.Map;

import ac.ed.lurg.types.LandCoverType;
import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class LandCoverReader extends AbstractTabularRasterReader<LandCoverItem> {	

	private static final int MIN_COLS = 9;

	
	public LandCoverReader(RasterSet<LandCoverItem> landCover) {
		super(" +", MIN_COLS, landCover);
	}
		
	@Override
	protected void setData(RasterKey key, LandCoverItem lcData, Map<String, Double> rowValues) {
	
		lcData.setTotalArea(dataset.getAreaMha(key));		
		lcData.setLandCoverFract(LandCoverType.CROPLAND, getValueForCol(rowValues, "cropland"));
		lcData.setLandCoverFract(LandCoverType.PASTURE, getValueForCol(rowValues, "pasture"));
		lcData.setLandCoverFract(LandCoverType.TIMBER_FOREST, getValueForCol(rowValues, "managed_forest"));
		lcData.setLandCoverFract(LandCoverType.CARBON_FOREST, 0.0); // TODO custom initial timber/carbon forest allocation
		lcData.setLandCoverFract(LandCoverType.BARREN, getValueForCol(rowValues, "barren"));
		lcData.setLandCoverFract(LandCoverType.URBAN, getValueForCol(rowValues, "urban"));
		lcData.setLandCoverFract(LandCoverType.PHOTOVOLTAICS, getValueForCol(rowValues, "photovoltaics"));

		double otherNatural = getValueForCol(rowValues, "natural");
		double unmanagedForest = getValueForCol(rowValues, "unmanaged_forest");
		lcData.setLandCoverFract(LandCoverType.NATURAL, otherNatural + unmanagedForest); // one natural class which is disaggregated for outputs
		double unmanForestFract = otherNatural + unmanagedForest > 0 ? unmanagedForest / (otherNatural + unmanagedForest) : 0;
		lcData.setUnmanagedForestFraction(unmanForestFract);
	}
	
	@Override
	protected int getXCol() {
		return getColForName("lon");
	}
	
	@Override
	protected int getYCol() {
		return getColForName("lat");
	}
}
