package ac.ed.lurg.landuse;

import ac.sac.raster.RasterItem;

public class ProtectedAreaItem implements RasterItem {
    private double protectedFraction;

    public void setProtectedFraction(double protectedFraction) {
        this.protectedFraction = protectedFraction;
    }

    public double getProtectedFraction() {
        return protectedFraction;
    }
}
