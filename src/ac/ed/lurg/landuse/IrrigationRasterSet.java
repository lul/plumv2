package ac.ed.lurg.landuse;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class IrrigationRasterSet extends RasterSet<IrrigationItem> {

	private static final long serialVersionUID = 4794790389538053286L;
	private FPUManager fpuManager;
	
	public IrrigationRasterSet(RasterHeaderDetails header, FPUManager fpuManager) {
		super(header);
		this.fpuManager = fpuManager;
	}

	protected IrrigationItem createRasterData() {
		return new IrrigationItem();
	}

	public void updateIrrigConstraints(Timestep timestep) {
		int currentYear = ModelConfig.IS_CALIBRATION_RUN ? ModelConfig.BASE_YEAR : timestep.getYear();
		Map<RasterKey, Double> lpjIrrigConstraints = getIrrigFromLpjRunoff(currentYear, false);
		
		for (Map.Entry<RasterKey, IrrigationItem> entry : entrySet()) {
			IrrigationItem irrigItem = entry.getValue();
			if (irrigItem != null) {
				Double lpjConstraint = lpjIrrigConstraints.get(entry.getKey());
				double d;
				if (lpjConstraint != null && lpjConstraint > 0 && irrigItem.getConstraintOffset() > 0)
					d = irrigItem.getConstraintOffset() * lpjConstraint;
				else
					d = irrigItem.getBaselineIrrigConstraint();
				
				irrigItem.setIrrigConstraint(d);
			}
		}
	}

	public void calcIrrigConstraintOffsets() {
		Map<RasterKey, Double> lpjIrrigConstraints = getIrrigFromLpjRunoff(ModelConfig.ELLIOTT_BASEYEAR, true);

		for (Map.Entry<RasterKey, IrrigationItem> entry : entrySet()) {
			IrrigationItem irrigItem = entry.getValue();
			if (irrigItem != null) {
				Double d = lpjIrrigConstraints.get(entry.getKey());
				if (d != null) {
					double offset = irrigItem.getBaselineIrrigConstraint()/d;
					irrigItem.setConstraintOffsets(offset);
				}
			}
		}
	}
	
	private Map<RasterKey, Double> getIrrigFromLpjRunoff(int otherWaterUseYear, boolean useBaselineRunoff) {
		Map<RasterKey, Double> lpjIrrigConstraint = new HashMap<RasterKey, Double>();
		
		Collection<WaterBasin> waterBasins = fpuManager.getWaterBasins();

		for (WaterBasin basin : waterBasins) {
			Collection<RasterKey> rasterKeys = fpuManager.getKeysFor(basin);
			RasterSet<IrrigationItem> irrigData = createSubsetForKeys(rasterKeys);
			double basinRunOff = 0.0, otherUses = 0.0, areaOfBasin = 0.0;

			for (Map.Entry<RasterKey, IrrigationItem> entry : irrigData.entrySet()) {
				IrrigationItem item = entry.getValue();
				if (item != null) {
					double area = irrigData.getAreaMha(entry.getKey()); // (initLU.containsKey(entry.getKey())) ? initLU.get(entry.getKey()).getTotalLandCoverArea() : 0.0;
					double runoff = useBaselineRunoff ? item.getBaseRunOff() : item.getRunOff();
					basinRunOff += runoff * area * 0.01;
					areaOfBasin += area;
				}
			}
			
			Double fpuRunOffAvailable = basinRunOff * ModelConfig.ENVIRONMENTAL_WATER_CONSTRAINT; 
			otherUses = fpuManager.getOtherWaterUse(basin, otherWaterUseYear) * ModelConfig.getAdjParam("OTHER_WATER_USE_FACTOR");
			double waterAvailabileMm = (fpuRunOffAvailable - otherUses) / areaOfBasin / 0.01;
			
			for (RasterKey rasterKey : irrigData.keySet()) {
				lpjIrrigConstraint.put(rasterKey, waterAvailabileMm);
			}
		}
		return lpjIrrigConstraint;
	}

	public RasterSet<Fpu> getFpuRasterSet() {
		return fpuManager.getFpuRasterSet();
	}
}
