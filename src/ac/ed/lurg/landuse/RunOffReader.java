package ac.ed.lurg.landuse;

import java.util.Map;

import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class RunOffReader extends AbstractTabularRasterReader<IrrigationItem> { 
	
	private static final int MIN_COLS = 6;
	private boolean isBaseRunOff;
	
	public RunOffReader(RasterSet<IrrigationItem> runOff, boolean isBaseRunOff) {
		super("\\s+", MIN_COLS, runOff);
		this.isBaseRunOff = isBaseRunOff;
	}
	
	protected void setData(RasterKey key, IrrigationItem item, Map<String, Double> rowValues) {
		double d = getValueForCol(rowValues, "Total");
		if (isBaseRunOff)
			item.setBaseRunOff(d);
		else
			item.setRunOff(d);
	}

}
