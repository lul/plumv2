package ac.ed.lurg.landuse;

import java.io.Serializable;
import java.util.Objects;

public class LandChangeKey implements Serializable {
	private static final long serialVersionUID = -7937259663055246235L;
	private final LandKey fromLandKey;
	private final LandKey toLandKey;

	public LandChangeKey(LandKey fromLandKey, LandKey toLandKey) {
		this.fromLandKey = fromLandKey;
		this.toLandKey = toLandKey;
	}

	public LandKey getFromLandKey() {
		return fromLandKey;
	}

	public LandKey getToLandKey() {
		return toLandKey;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		if(((LandChangeKey) o).getFromLandKey().equals(fromLandKey) && ((LandChangeKey) o).getToLandKey().equals(toLandKey)) return  true;
		LandChangeKey that = (LandChangeKey) o;
		return Objects.equals(fromLandKey, that.fromLandKey) && Objects.equals(toLandKey, that.toLandKey);
	}

	@Override
	public int hashCode() {
		return Objects.hash(fromLandKey, toLandKey);
	}

	@Override
	public String toString() {
		return "LandChangeKey{" +
				"fromLandKey=" + fromLandKey +
				", toLandKey=" + toLandKey +
				'}';
	}
}
