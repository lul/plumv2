package ac.ed.lurg.landuse;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.utils.LogWriter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ConversionCostManager {
	
	private static final int FROM_COL = 0;
	private static final int TO_COL = 1;
	private static final int COST_COL = 2;
	private final Map<LandCoverChangeKey, Double> conversionCosts = new HashMap<>();

	private static final ConversionCostManager instance = new ConversionCostManager();

	public static ConversionCostManager getInstance() {
		return instance;
	}

	
	public ConversionCostManager() {
		if (ModelConfig.CONVERSION_COSTS_FROM_FILE) {
			read();
		} else {
			calcFromConfig();
		}
	}

	public void read() {
		String filename = ModelConfig.CONVERSION_COST_FILE;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename)); 
			String line, toName, fromName;
			double cost;
			reader.readLine(); // read header

			while ((line=reader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 3)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				fromName = tokens[FROM_COL];
				toName = tokens[TO_COL];
				cost = Double.parseDouble(tokens[COST_COL]);
				
				LandCoverType fromLc = LandCoverType.getForName(fromName);
				LandCoverType toLc = LandCoverType.getForName(toName);
				
				conversionCosts.put(new LandCoverChangeKey(fromLc, toLc), cost);
			} 
			reader.close(); 
		
		} catch (IOException e) {
			LogWriter.printlnError("Failed in reading conversion costs file");
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename, 2);
	}
	
	public void calcFromConfig() {
		double costAdjFactor = ModelConfig.getAdjParam("LAND_CONVERSION_COST_FACTOR");
		for (LandCoverType fromLc : LandCoverType.getConvertibleTypes()) {
			for (LandCoverType toLc : LandCoverType.getConvertibleTypes()) {
				LandCoverChangeKey key = new LandCoverChangeKey(fromLc, toLc);

				if (fromLc.equals(toLc)) {
					// cost of maintaining managed land cover
					if (!fromLc.equals(LandCoverType.NATURAL)) { // no cost to natural
						conversionCosts.put(key,  ModelConfig.getAdjParam("LAND_BASE_COST") * costAdjFactor);
					}
					continue;
				}

				switch (fromLc) {
					case CROPLAND:
						if (toLc.equals(LandCoverType.NATURAL)) {
							conversionCosts.put(key, ModelConfig.getAdjParam("CROPLAND_ABANDONMENT_COST") * costAdjFactor);
						} else {
							conversionCosts.put(key, ModelConfig.getAdjParam("CROPLAND_CONVERSION_COST") * costAdjFactor);
						}
						break;
					case PASTURE:
						if (toLc.equals(LandCoverType.NATURAL)) {
							conversionCosts.put(key, ModelConfig.getAdjParam("PASTURE_ABANDONMENT_COST") * costAdjFactor);
						} else {
							conversionCosts.put(key, ModelConfig.getAdjParam("PASTURE_CONVERSION_COST") * costAdjFactor);
						}
						break;
					case PHOTOVOLTAICS:
					case AGRIVOLTAICS:
						conversionCosts.put(key, ModelConfig.getAdjParam("SOLAR_CONVERSION_COST") * costAdjFactor);
						break;
					case TIMBER_FOREST:
						if (toLc.equals(LandCoverType.NATURAL)) {
							conversionCosts.put(key, ModelConfig.getAdjParam("TIMBER_FOREST_ABANDONMENT_COST") * costAdjFactor);
						} else {
							conversionCosts.put(key, ModelConfig.getAdjParam("TIMBER_FOREST_CONVERSION_COST") * costAdjFactor);
						}
						break;
					case CARBON_FOREST:
						conversionCosts.put(key, ModelConfig.getAdjParam("CARBON_FOREST_CONVERSION_COST") * costAdjFactor);
						break;
					case NATURAL:
						conversionCosts.put(key, ModelConfig.getAdjParam("NATURAL_CONVERSION_COST") * costAdjFactor);
						break;
				}

				// Agricultural expansion cost factor
				if (fromLc.equals(LandCoverType.NATURAL) && (toLc.equals(LandCoverType.CROPLAND) || toLc.equals(LandCoverType.PASTURE) || toLc.equals(LandCoverType.AGRIVOLTAICS))) {
					double baseCost = conversionCosts.get(key);
					conversionCosts.put(key, baseCost * ModelConfig.getAdjParam("AGRI_LAND_EXPANSION_COST_FACTOR"));
				}

				// Additional cost of managed forest establishment
				if (toLc.equals(LandCoverType.TIMBER_FOREST)) {
					double baseCost = conversionCosts.get(key);
					conversionCosts.put(key, baseCost + ModelConfig.getAdjParam("TIMBER_FOREST_ESTABLISHMENT_COST"));
				}

			}
		}
	}

	public Map<LandCoverChangeKey, Double> getConversionCosts() {
		return conversionCosts;
	}

}
