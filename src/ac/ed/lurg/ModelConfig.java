package ac.ed.lurg;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Properties;
import java.lang.Long;

import ac.ed.lurg.carbon.CarbonOptionsManager;
import ac.ed.lurg.shock.parameterShocksReader;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.ModelFitType;
import ac.ed.lurg.types.PriceType;
import ac.ed.lurg.utils.LogWriter;

public class ModelConfig {

	private Properties configFile;
	private static ModelConfig modelConfig;
	public static final String CONFIG_FILE = System.getProperty("CONFIG_FILE");

	private static parameterShocksReader shocksReader;
	private static CarbonOptionsManager carbonOptionsManager;
	private static Properties calibConfigFile;
	private Timestep timestep;

	private ModelConfig() {
		configFile = new Properties();
		try {
			System.out.println("Config. file is " + CONFIG_FILE);
			if (CONFIG_FILE != null)
				configFile.load(Files.newInputStream(Paths.get(CONFIG_FILE)));
		}
		catch (IOException e) {
			System.err.println("Problems reading config file");
			System.err.println(e.getMessage());
		}
	}

	public static void readCalibConfigFile() {
		calibConfigFile = new Properties();
		try {
			calibConfigFile.load(Files.newInputStream(Paths.get(ModelConfig.CALIB_CONFIG_FILE)));
		} catch	(IOException e) {
			e.printStackTrace();
		}
	}

	public static String getSetupDetails() {
		String buildVerion = System.getProperty("BUILDVER");
		StringBuffer sb = new StringBuffer("Build version: " + buildVerion + "\n");

		Properties props = getModelConfig().configFile;
		Enumeration<?> em = props.keys();
		while(em.hasMoreElements()) {
			String str = (String) em.nextElement();
			sb.append(str + ": " + props.get(str) + "\n");
		}

		return sb.toString();
	}

	public static ModelConfig getModelConfig() {
		if (modelConfig == null)
			modelConfig = new ModelConfig();

		return modelConfig;
	}

	public void setTimestep(Timestep timestep) {
		this.timestep = timestep;
	}

	private static String getProperty(String prop) {
		return getModelConfig().getProp(prop);
	}
	private static String getProperty(String prop, String defaultString) {
		String propValue = getProperty(prop);
		return propValue == null ? defaultString : propValue;
	}
	private String getProp(String prop) {
		return configFile.getProperty(prop);
	}

	private static Integer getIntProperty(String prop, Integer defaultInt) {
		Integer propValue = getModelConfig().getIntProp(prop);
		return propValue == null ? defaultInt : propValue;
	}
	private Integer getIntProp(String prop) {
		String v = configFile.getProperty(prop);
		return v==null ? null : Integer.valueOf(v);
	}

	@SuppressWarnings("unused")
	private static Long getLongProperty(String prop, Long defaultLong) {
		Long propValue = getModelConfig().getLongProp(prop);
		return propValue == null ? defaultLong : propValue;
	}
	private Long getLongProp(String prop) {
		String v = configFile.getProperty(prop);
		return v==null ? null : Long.valueOf(v);
	}

	private static Double getDoubleProperty(String prop, Double defaultDouble) {
		Double propValue = getModelConfig().getDoubleProp(prop);
		return propValue == null ? defaultDouble : propValue;
	}
	private Double getDoubleProp(String prop) {
		String v = configFile.getProperty(prop);
		return v==null ? null :  Double.valueOf(v);
	}

	private static Boolean getBooleanProperty(String prop, Boolean defaultBoolean) {
		return getModelConfig().getBooleanProp(prop, defaultBoolean);
	}
	private boolean getBooleanProp(String prop, Boolean defaultBoolean) {
		String v = configFile.getProperty(prop);
		return v==null ? defaultBoolean :  Boolean.valueOf(v);
	}	

	public static double updateParameterForShocks(int year, String parameter) {

		Double value = null;
		try {
			value =  getModelConfig().getClass().getField(parameter).getDouble(parameter);

			if(SHOCKS_POSSIBLE) {

				if (shocksReader == null) {
					shocksReader = new parameterShocksReader(",");
					shocksReader.read(SHOCKS_PARAMETER_FILE);
				}
				Double updatedValue = shocksReader.queryForParameter(year, parameter);

				if(updatedValue !=null) 
					value = updatedValue;
			}

		}
		catch (IllegalAccessException | NoSuchFieldException e) {
			LogWriter.printlnError("cannot find parameter in model config to shock: " + parameter);
			LogWriter.print(e);
		}
		return value;
	}

	// Gradually adjust parameter from calibrated value to current value to avoid sudden shocks (except when intentionally shocked)
	public static double getAdjParam(String param, Timestep timestep) {
		if (ModelConfig.SHOCKS_POSSIBLE) {
			return updateParameterForShocks(timestep.getYear(), param);
		}

		if (calibConfigFile == null & !ModelConfig.IS_CALIBRATION_RUN) {
			readCalibConfigFile();
		}

		double endParam = Double.NaN;
		try {
			endParam = getModelConfig().getClass().getField(param).getDouble(param);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}

		if (IS_CALIBRATION_RUN | ModelConfig.CALIB_TRANSITION_TIME == 0) {
			return endParam;
		} else {
			double startParam;
			if (calibConfigFile.getProperty(param) != null) {
				startParam = Double.parseDouble(calibConfigFile.getProperty(param));
			} else { // Can't find value used in calibration so use current value
				startParam = endParam;
			}

			// Weighted average
			double weighting = Math.min(((double) timestep.getTimestep()) / ModelConfig.CALIB_TRANSITION_TIME, 1.0);
			return endParam * weighting + startParam * (1 - weighting);
		}
	}

	public static double getAdjParam(String param) {
		if (getModelConfig().timestep == null) { // need this during initial setup
			getModelConfig().setTimestep(new Timestep(ModelConfig.START_TIMESTEP));
		}
		return getAdjParam(param, getModelConfig().timestep);
	}
	
	public static boolean getCarbonOption(LandCoverType fromLc, LandCoverType toLc) {
		if (IS_CARBON_ON) {
			if (carbonOptionsManager == null) {
				carbonOptionsManager = new CarbonOptionsManager();
			}
			return carbonOptionsManager.isCarbonFluxEnabled(fromLc, toLc);
		} else {
			return false;
		}
	}

	public static final boolean SUPPRESS_STD_OUTPUT = getBooleanProperty("SUPPRESS_STD_OUTPUT", Boolean.FALSE);

	// Directory information
	public static final String BASE_DIR = getProperty("BASE_DIR");  // this must to be set in config file
    public static final String OUTPUT_DIR = getProperty("OUTPUT_DIR", ".");
	public static final String TEMP_DIR = getProperty("TEMP_DIR", OUTPUT_DIR + File.separator + "GamsTmp");
	public static final String DATA_DIR = getProperty("DATA_DIR", BASE_DIR + File.separator + "data");
	public static final String GAMS_DIR = getProperty("GAMS_DIR", BASE_DIR + File.separator + "GAMS");
	public static final boolean CLEANUP_GAMS_DIR = getBooleanProperty("CLEANUP_GAMS_DIR", true);

	public static final boolean ORIG_LEAST_COST_MIN = getBooleanProperty("ORIG_LEAST_COST_MIN", true);
	public static final String GAMS_MODEL_NAME = getProperty("GAMS_MODEL_NAME", ORIG_LEAST_COST_MIN ? "IntExtOpt.gms" : "LUOpt.gms");
	public static final String GAMS_MODEL = getProperty("GAMS_MODEL", GAMS_DIR + File.separator + GAMS_MODEL_NAME);
	public static final String DEMAND_GAMS_MODEL = getProperty("DEMAND_GAMS_MODEL", GAMS_DIR + File.separator + "elasticDemand.gms");
	public static final String DEMAND_PARAM_FILE = getProperty("DEMAND_PARAM_FILE", DATA_DIR + File.separator + "DemandParamConv.gdx");

	public static final String WOOD_DEMAND_GAMS_MODEL = getProperty("WOOD_DEMAND_GAMS_MODEL", GAMS_DIR + File.separator + "woodDemand.gms");
	public static final String WOOD_DEMAND_PARAM_FILE = getProperty("WOOD_DEMAND_PARAM_FILE", DATA_DIR + File.separator + "WoodDemandParamConv.gdx");

	// Multithreading options
	public static final int MULTITHREAD_NUM = getIntProperty("MULTITHREAD_NUM", 1); // number of parallel threads
	public static final int THREAD_TIMEOUT = getIntProperty("THREAD_TIMEOUT", 60); // timeout limit for threads, minutes

	// Country (non-gridded) data
	public static final boolean DEMAND_FROM_FILE = getBooleanProperty("DEMAND_FROM_FILE", false); // used in hindcasting
	public static final boolean PRICE_ELASTIC_DEMAND = getBooleanProperty("PRICE_ELASTIC_DEMAND", true);
	public static final boolean DONT_REBASE_DEMAND = getBooleanProperty("DONT_REBASE_DEMAND", false);
	public static final String DEMAND_CURVES_FILE = getProperty("DEMAND_CURVES_FILE", DATA_DIR + File.separator + "com_curves.csv");  // either DEMAND_CURVES_FILE or DEMAND_CONSUMPTION_FILE is used, but not both
	public static final String DEMAND_CONSUMPTION_FILE = getProperty("DEMAND_CONSUMPTION_FILE", DATA_DIR + File.separator + "hist_comsump.csv");
	public static final PriceType PRICE_CALCULATION = PriceType.findByName(getProperty("PRICE_CALCULATION", "weightedImportsExports"));
	public static final String SSP_FILENAME = getProperty("SSP_FILENAME", "ssp.csv");
	public static final String SSP_FILE = getProperty("SSP_FILE", DATA_DIR + File.separator + SSP_FILENAME);	
	public static final String BASELINE_CONSUMP_FILE = DATA_DIR + File.separator + "base_consump.csv";
	public static final String COUNTRIES_FILE = DATA_DIR + File.separator + "countries.csv";
	public static final String ENERGY_DEMAND_FILE = getProperty("ENERGY_DEMAND_FILE", DATA_DIR + File.separator + "energy_demand.csv");
	public static final String TRADE_BARRIERS_FILENAME = getProperty("TRADE_BARRIERS_FILENAME", "tradeBarriers.csv");
	public static final String TRADE_BARRIERS_FILE = getProperty("TRADE_BARRIERS_FILE", DATA_DIR + File.separator + TRADE_BARRIERS_FILENAME);
	public static final String STOCKS_FILE = DATA_DIR + File.separator + "global_stocks.csv";
	public static final String OTHER_WATER_USES_FILE = DATA_DIR + File.separator + "other_water_uses.csv";
	public static final String SHOCKS_PARAMETER_FILE = OUTPUT_DIR + File.separator+ "shocks.csv";
	public static final String SUBSIDY_RATE_FILENAME = getProperty("SUBSIDY_RATE_FILENAME", "subsidyrates.csv");
	public static final String SUBSIDY_RATE_FILE = getProperty("SUBSIDY_RATE_FILE", DATA_DIR + File.separator + SUBSIDY_RATE_FILENAME);
	public static final String ANIMAL_RATES_FILE = DATA_DIR + File.separator + "animal_numbers.csv";
	public static final String INITIAL_CONSUMER_PRICE_FILE = DATA_DIR + File.separator + "consumerprices.csv";
	public static final String GDP_FRACTIONS_FILE = DATA_DIR + File.separator + "agri_gdp_fraction.csv";

	// yield data
	public static final String YIELD_DIR_BASE = getProperty("YIELD_DIR_BASE");
	public static final String YIELD_DIR_TOP = getProperty("YIELD_DIR_TOP");
	public static final String YIELD_DIR = getProperty("YIELD_DIR", YIELD_DIR_BASE + File.separator + YIELD_DIR_TOP);
	public static final int LPJG_MONITOR_TIMEOUT_SEC = getIntProperty("LPJG_MONITOR_TIMEOUT", 60*60*2);
	public static final String ANPP_FILENAME = getProperty("ANPP_FILENAME", "anpp.out");
	public static final String YIELD_FILENAME = getProperty("YIELD_FILENAME", "yield.out");
	public static final boolean PASTURE_FERT_RESPONSE_FROM_LPJ = getBooleanProperty("PASTURE_FERT_RESPONSE_FROM_LPJ", false);

	public static final double CALIB_FACTOR_PASTURE = getDoubleProperty("CALIB_FACTOR_PASTURE", 1.0);

	public static final String C3_CEREALS_COLUMN = getProperty("C3_CEREALS_COLUMN", "CerealsC3");
	public static final String C4_CEREALS_COLUMN = getProperty("C4_CEREALS_COLUMN", "CerealsC4");
	public static final String RICE_COLUMN  = getProperty("RICE_COLUMN", "Rice");
	public static final String PULSES_COLUMN  = getProperty("PULSES_COLUMN", "Pulses");
	public static final String OILCROPS_NFIX_COLUMN  = getProperty("OILCROPS_NFIX_COLUMN", "OilNfix");
	public static final String OILCROPS_OTHER_COLUMN  = getProperty("OILCROPS_OTHER_COLUMN", "OilOther");
	public static final String STARCHY_ROOTS_COLUMN  = getProperty("STARCHY_ROOTS_COLUMN", "StarchyRoots");
	public static final String FRUITVEG_COLUMN  = getProperty("FRUITVEG_COLUMN", "FruitAndVeg");
	public static final String SUGAR_BEET_COLUMN  = getProperty("SUGAR_BEET_COLUMN", "Sugarbeet");
	public static final String SUGAR_CANE_COLUMN  = getProperty("SUGAR_CANE_COLUMN", "Sugarcane");
	public static final String PASTURE_C3_COLUMN  = getProperty("PASTURE_C3_COLUMN", "C3G_pas");
	public static final String PASTURE_C4_COLUMN  = getProperty("PASTURE_C4_COLUMN", "C4G_pas");

	// These are production prices in PLUM style feed equivalent terms
	public static final double INITIAL_PRICE_WHEAT = getDoubleProperty("INITIAL_PRICE_WHEAT", 0.1);
	public static final double INITIAL_PRICE_MAIZE = getDoubleProperty("INITIAL_PRICE_MAIZE", 0.1);
	public static final double INITIAL_PRICE_RICE = getDoubleProperty("INITIAL_PRICE_RICE", 0.12);
	public static final double INITIAL_PRICE_OILCROPS_NFIX = getDoubleProperty("INITIAL_PRICE_OILCROPS_NFIX", 0.09);
	public static final double INITIAL_PRICE_OILCROPS_OTHER = getDoubleProperty("INITIAL_PRICE_OILCROPS_OTHER", 0.09);
	public static final double INITIAL_PRICE_PULSES = getDoubleProperty("INITIAL_PRICE_PULSES", 0.15);
	public static final double INITIAL_PRICE_STARCHYROOTS = getDoubleProperty("INITIAL_PRICE_STARCHYROOTS", 0.05);
	public static final double INITIAL_PRICE_MONOGASTRICS = getDoubleProperty("INITIAL_PRICE_MONOGASTRICS", 0.15);
	public static final double INITIAL_PRICE_RUMINANTS = getDoubleProperty("INITIAL_PRICE_RUMINANTS", 0.14);
	public static final double INITIAL_PRICE_ENERGYCROPS = getDoubleProperty("INITIAL_PRICE_ENERGYCROPS", 0.01);
	public static final double INITIAL_PRICE_FRUITVEG = getDoubleProperty("INITIAL_PRICE_FRUITVEG", 0.16);
	public static final double INITIAL_PRICE_SUGAR = getDoubleProperty("INITIAL_PRICE_SUGAR", 0.035);

	public static final double PROD_COST_WHEAT = getDoubleProperty("PROD_COST_WHEAT", 0.3);
	public static final double PROD_COST_MAIZE = getDoubleProperty("PROD_COST_MAIZE", 0.3);
	public static final double PROD_COST_RICE = getDoubleProperty("PROD_COST_RICE", 0.8);
	public static final double PROD_COST_OILCROPS_NFIX = getDoubleProperty("PROD_COST_OILCROPS_NFIX", 0.4);
	public static final double PROD_COST_OILCROPS_OTHER = getDoubleProperty("PROD_COST_OILCROPS_OTHER", 0.25);
	public static final double PROD_COST_PULSES = getDoubleProperty("PROD_COST_PULSES", 0.6);
	public static final double PROD_COST_STARCHYROOTS = getDoubleProperty("PROD_COST_STARCHYROOTS", 3.0);
	public static final double PROD_COST_MONOGASTRICS = getDoubleProperty("PROD_COST_MONOGASTRICS", 0.05);
	public static final double PROD_COST_RUMINANTS = getDoubleProperty("PROD_COST_RUMINANTS", 0.05);
	public static final double PROD_COST_PASTURE = getDoubleProperty("PROD_COST_PASTURE", 0.1);
	public static final double PROD_COST_ENERGYCROPS = getDoubleProperty("PROD_COST_ENERGYCROPS", 0.3);
	public static final double PROD_COST_FRUITVEG = getDoubleProperty("PROD_COST_FRUITVEG", 8.0);
	public static final double PROD_COST_SUGAR = getDoubleProperty("PROD_COST_SUGAR", 3.0);


	// These are initial demand system prices in 2000 kcal terms
	public static final double INITAL_DEMAND_PRICE_CEREALS_STARCHY_ROOTS = getDoubleProperty("INITAL_DEMAND_PRICE_CEREALS_STARCHY_ROOTS", 110.23);
	public static final double INITAL_DEMAND_PRICE_OILCROPS = getDoubleProperty("INITAL_DEMAND_PRICE_OILCROPS", 153.273);
	public static final double INITAL_DEMAND_PRICE_PULSES = getDoubleProperty("INITAL_DEMAND_PRICE_PULSES", 373.78);
	public static final double INITAL_DEMAND_PRICE_MONOGASTRICS = getDoubleProperty("INITAL_DEMAND_PRICE_MONOGASTRICS", 880.23);
	public static final double INITAL_DEMAND_PRICE_RUMINANTS = getDoubleProperty("INITAL_DEMAND_PRICE_RUMINANTS", 1200.97);
	public static final double INITAL_DEMAND_PRICE_FRUITVEG = getDoubleProperty("INITAL_DEMAND_PRICE_FRUITVEG", 1062.75);
	public static final double INITAL_DEMAND_PRICE_SUGAR = getDoubleProperty("INITAL_DEMAND_PRICE_SUGAR", 33.92);

	// Spatial (gridded) data
	public static final double CELL_SIZE_X = getDoubleProperty("CELL_SIZE_X", 0.5);
	public static final double CELL_SIZE_Y = getDoubleProperty("CELL_SIZE_Y", CELL_SIZE_X);
	public static final String SPATIAL_DIR_NAME = getProperty("SPATIAL_DIR_NAME", "halfdeg");
	public static final String SPATIAL_DATA_DIR = getProperty("SPATIAL_DATA_DIR", DATA_DIR + File.separator + SPATIAL_DIR_NAME);
	public static final String INITAL_LAND_COVER_FILENAME = getProperty("INITAL_LAND_COVER_FILENAME", "hilda_plus_2020_pv.txt");
	public static final String INITAL_LAND_COVER_FILE = SPATIAL_DATA_DIR + File.separator + INITAL_LAND_COVER_FILENAME;
	public static final String CROP_FRACTIONS_FILE = SPATIAL_DATA_DIR + File.separator + "crop_fractions.csv";
	public static final String COUNTRY_BOUNDARY_FILE = SPATIAL_DATA_DIR + File.separator + "country_boundaries.csv";
	public static final String IRRIGATION_COST_FILE = SPATIAL_DATA_DIR + File.separator + "irrigation_cost.asc";
	public static final String IRRIGATION_CONSTRAINT_FILE = SPATIAL_DATA_DIR + File.separator + "blue_water_available_pseudoCRU_rcp8p5_2004_2013_grid_allhdyro_mm.txt";
	public static final String FPU_BOUNDARIES_FILE = SPATIAL_DATA_DIR + File.separator + "FPU.asc";
	public static final String FPU_GROUPING_FILE = SPATIAL_DATA_DIR + File.separator + "fpuGrouping.txt";
	public static final String IRRIG_MAX_WATER_FILENAME = getProperty("IRRIG_MAX_WATER_FILENAME", "gsirrigation.out");
	public static final String IRRIG_RUNOFF_FILE = getProperty("IRRIG_RUNOFF_FILE", "tot_runoff.out");
	public static final String PROTECTED_AREAS_FILENAME = getProperty("PROTECTED_AREAS_FILENAME","protectedAreas2019.asc");
	public static final String PROTECTED_AREAS_FILE = getProperty("PROTECTED_AREAS_FILE",SPATIAL_DATA_DIR + File.separator + PROTECTED_AREAS_FILENAME);
	public static final String FORCED_PROTECTED_AREAS_FILE = getProperty("FORCED_PROTECTED_AREAS_FILE",SPATIAL_DATA_DIR + File.separator + "global_biodiversity_priorities50perc.asc");
	public static final String HIGH_SLOPE_AREAS_FILE = SPATIAL_DATA_DIR + File.separator + "maxcropfrac2.txt";
	public static final String YIELDSHOCK_MAP_DIR = SPATIAL_DATA_DIR + File.separator + "yieldshockmaps";
	public static final String YIELDSHOCKS_PARAMETER_FILE = getProperty("YIELDSHOCKS_PARAMETER_FILE", OUTPUT_DIR + File.separator+ "yieldshocks.csv");
	public static final String PRICESHOCKS_PARAMETER_FILE = getProperty("PRICESHOCKS_PARAMETER_FILE", OUTPUT_DIR + File.separator+ "priceshocks.csv");
	public static final String EXPORT_RESTRICTIONS_FILE = getProperty("EXPORT_RESTRICTIONS_FILE", OUTPUT_DIR + File.separator+ "exportrestictions.csv");
	public static final String CROP_CALIB_FILE = getProperty("CROP_CALIB_FILE", SPATIAL_DATA_DIR + File.separator + "crop_calibration.csv");

	// Wood/carbon data
	public static final String FORESTRY_DIR_BASE = getProperty("FORESTRY_DIR_BASE");
	public static final String FORESTRY_DIR_TOP = getProperty("FORESTRY_DIR_TOP");
	public static final String WOOD_YIELD_ROTA_FORS_FILENAME = getProperty("WOOD_YIELD_ROTA_FORS_FILENAME", "landsymm_pcutW_sts_forC.dat");
	public static final String C_FLUX_FORS_FILENAME = getProperty("C_FLUX_FORS_FILENAME", "cflux_landsymm_sts_forC.dat");

	public static final int CARBON_DATA_TIMESTEP_SIZE = getIntProperty("WOOD_AND_CARBON_TIMESTEP_SIZE", 20); // years
	public static final double WOOD_YIELD_CALIB_FACTOR = getDoubleProperty("WOOD_YIELD_CALIB_FACTOR", 1.0); 
	public static final int CARBON_DATA_MIN_YEAR = getIntProperty("CARBON_DATA_MIN_YEAR", 1850); // Carbon and wood data

	// Output
	public static final String LAND_COVER_OUTPUT_FILE = OUTPUT_DIR + File.separator + "lc.txt";
	public static final String PRICES_OUTPUT_FILE = OUTPUT_DIR + File.separator + "prices.txt";
	public static final String DEMAND_OUTPUT_FILE = OUTPUT_DIR + File.separator + "demand.txt";
	public static final String DOMESTIC_OUTPUT_FILE = OUTPUT_DIR + File.separator + "domestic.txt";
	public static final String COUNTRY_DEMAND_FILE = OUTPUT_DIR + File.separator + "countryDemand.txt";
	public static final String DEMAND_OPTIMISATION_OUTPUT_FILE = OUTPUT_DIR + File.separator + "countryDemandOpt.txt";
	public static final String FOOD_BALANCE_SHEET_FILE = OUTPUT_DIR + File.separator + "fbs.txt";
	public static final String ANIMAL_NUMBERS_OUTPUT_FILE = OUTPUT_DIR + File.separator + "animals.txt";
	public static final String WOOD_OUTPUT_FILE = OUTPUT_DIR + File.separator + "wood.txt";
	public static final String CARBON_OUTPUT_FILE = OUTPUT_DIR + File.separator + "carbon.txt";

	public static final boolean OUTPUT_FOR_LPJG = getBooleanProperty("OUTPUT_FOR_LPJG", true);
	
	// Calibration related stuff
	public static final boolean IS_CALIBRATION_RUN = getBooleanProperty("IS_CALIBRATION_RUN", false);
	public static final String CALIB_DIR = IS_CALIBRATION_RUN ? OUTPUT_DIR : getProperty("CALIB_DIR", OUTPUT_DIR);
	public static final int END_FIRST_STAGE_CALIBRATION = getIntProperty("END_FIRST_STAGE_CALIBRATION", 20); // Keep land cover fixed

	public static final String SERIALIZED_LAND_USE_FILENAME = "landUseRaster.ser";
	public static final String SERIALIZED_CROP_USAGE_FILENAME = "countryCropUsages.ser";
	public static final String SERIALIZED_INTERNATIONAL_MARKET_FILENAME = "internationalMarket.ser";
	public static final String SERIALIZED_WOOD_USAGE_FILENAME = "countryWoodUsage.ser";
	public static final String SERIALIZED_CARBON_USAGE_FILENAME = "countryCarbonUsage.ser";
	public static final String SERIALIZED_LAND_COVER_FILENAME = "landCoverData.dat";
	public static final String SERIALIZED_LAND_USE_FILE = CALIB_DIR + File.separator +  SERIALIZED_LAND_USE_FILENAME;
	public static final String SERIALIZED_CROP_USAGE_FILE = CALIB_DIR + File.separator +  SERIALIZED_CROP_USAGE_FILENAME;
	public static final String SERIALIZED_INTERNATIONAL_MARKET_FILE = CALIB_DIR + File.separator +  SERIALIZED_INTERNATIONAL_MARKET_FILENAME;
	public static final String SERIALIZED_DEMAND_MANAGER_FILENAME = "demandManager.ser";
	public static final String SERIALIZED_DEMAND_MANAGER_FILE = CALIB_DIR + File.separator + SERIALIZED_DEMAND_MANAGER_FILENAME;
	public static final String SERIALIZED_LAND_COVER_FILE = CALIB_DIR + File.separator + SERIALIZED_LAND_COVER_FILENAME;
	public static final String SERIALIZED_WOOD_USAGE_FILE = CALIB_DIR + File.separator + SERIALIZED_WOOD_USAGE_FILENAME;
	public static final String SERIALIZED_CARBON_USAGE_FILE = CALIB_DIR + File.separator + SERIALIZED_CARBON_USAGE_FILENAME;
	public static final String CALIB_CONFIG_FILENAME = "calib_config.properties";
	public static final String CALIB_CONFIG_FILE = CALIB_DIR + File.separator + CALIB_CONFIG_FILENAME;
	public static final String CHECKPOINT_LAND_USE_FILE = getProperty("CHECKPOINT_LAND_USE_FILE", OUTPUT_DIR + File.separator +  SERIALIZED_LAND_USE_FILENAME);
	public static final String CHECKPOINT_DEMAND_MANAGER_FILE = getProperty("CHECKPOINT_DEMAND_MANAGER_FILE", OUTPUT_DIR + File.separator +  SERIALIZED_DEMAND_MANAGER_FILENAME);
	public static final String CHECKPOINT_CROP_USAGE_FILE = getProperty("CHECKPOINT_CROP_USAGE_FILE", OUTPUT_DIR + File.separator +  SERIALIZED_CROP_USAGE_FILENAME);
	public static final String CHECKPOINT_INTERNATIONAL_MARKET_FILE = getProperty("CHECKPOINT_INTERNATIONAL_MARKET_FILE", OUTPUT_DIR + File.separator +  SERIALIZED_INTERNATIONAL_MARKET_FILENAME);
	public static final String CHECKPOINT_LAND_COVER_FILE = getProperty("CHECKPOINT_LAND_COVER_FILE", OUTPUT_DIR + File.separator +  SERIALIZED_LAND_COVER_FILENAME);
	public static final String CHECKPOINT_WOOD_USAGE_FILE = getProperty("CHECKPOINT_WOOD_USAGE_FILE", OUTPUT_DIR + File.separator +  SERIALIZED_WOOD_USAGE_FILENAME);
	public static final String CHECKPOINT_CARBON_USAGE_FILE = getProperty("CHECKPOINT_CARBON_USAGE_FILE", OUTPUT_DIR + File.separator +  SERIALIZED_CARBON_USAGE_FILENAME);
	public static final boolean USE_INITIAL_CROP_USAGE_DATA = getBooleanProperty("USE_INITIAL_CROP_USAGE_DATA", false); // Read crop usage from file rather than serialized
	public static final int CALIB_TRANSITION_TIME = getIntProperty("CALIB_TRANSITION_TIME", 20); // transition from calibration to current values over this time

	public static final boolean MARKET_ADJ_PRICE = getBooleanProperty("MARKET_ADJ_PRICE", true);
	public static final boolean CHANGE_YIELD_DATA_YEAR = IS_CALIBRATION_RUN ? false : getBooleanProperty("CHANGE_YIELD_DATA_YEAR", true);
	public static final String CLUSTERED_YIELD_FILE = getProperty("CLUSTERED_YIELD_FILE", CALIB_DIR + File.separator + "cluster.asc");
	public static final boolean GENERATE_NEW_YIELD_CLUSTERS = getBooleanProperty("GENERATE_NEW_YIELD_CLUSTERS", IS_CALIBRATION_RUN);
	

	// Temporal configuration
	public static final int START_TIMESTEP = getIntProperty("START_TIMESTEP", 0);
	public static final int END_TIMESTEP = getIntProperty("END_TIMESTEP", 80);
	public static final int TIMESTEP_SIZE = getIntProperty("TIMESTEP_SIZE", 1);
	public static final int BASE_YEAR = getIntProperty("BASE_YEAR", 2020);

	// Import export limits
	public static final double ANNUAL_MAX_IMPORT_CHANGE = getDoubleProperty("ANNUAL_MAX_IMPORT_CHANGE", 0.1);
	public static final double MAX_IMPORT_CHANGE = getDoubleProperty("MAX_IMPORT_CHANGE", ANNUAL_MAX_IMPORT_CHANGE*TIMESTEP_SIZE);
	public static final double TRADE_ADJUSTMENT_COST_RATE = getDoubleProperty("TRADE_ADJUSTMENT_COST_RATE", 0.005); // Cost of changing imports/exports

	// Price caps
	public static final String PRICE_CAP_FILE = DATA_DIR + File.separator + "price_caps.csv";

	// Fertiliser application rates in kg/ha
	public static final double MIN_FERT_AMOUNT = getDoubleProperty("MIN_FERT_AMOUNT", 0.0);
	public static final double MID_FERT_AMOUNT = getDoubleProperty("MID_FERT_AMOUNT", 200.0);
	public static final double MAX_FERT_AMOUNT = getDoubleProperty("MAX_FERT_AMOUNT", 1000.0);
	public static final int FERT_AMOUNT_PADDING = getIntProperty("FERT_AMOUNT_PADDING", 4);

	// SSP shift parameters
	public static final double SSP_POPULATION_FACTOR = getDoubleProperty("SSP_POPULATION_FACTOR", 1.0);
	public static final double SSP_GDP_PC_FACTOR = getDoubleProperty("SSP_GDP_PC_FACTOR", 1.0);

	// Other model parameters
	public static final boolean CHANGE_DEMAND_YEAR = IS_CALIBRATION_RUN ? false : getBooleanProperty("CHANGE_DEMAND_YEAR", true);
	public static final double DEMAND_INCOME_CAP = getDoubleProperty("DEMAND_INCOME_CAP", 50000.0);
	public static final String SSP_SCENARIO = getProperty("SSP_SCENARIO", "SSP2");
	public static final ModelFitType DEMAND_ANIMAL_PROD_FIT = ModelFitType.findByName(getProperty("DEMAND_ANIMAL_PROD_FIT", "loglinear"));
	public static final ModelFitType DEMAND_NON_ANIMAL_PROD_FIT = ModelFitType.findByName(getProperty("DEMAND_NON_ANIMAL_PROD_FIT", "loglinear"));
	public static final boolean DEMAND_FRACT_BY_COST = getBooleanProperty("DEMAND_FRACT_BY_COST", true);
	public static final double DEMAND_FRACT_ELASTICITY = getDoubleProperty("DEMAND_FRACT_ELASTICITY", 0.5); // commodity elasticity of substitution
	public static final double MAX_INCOME_PROPORTION_FOOD_SPEND = getDoubleProperty("MAX_INCOME_PROPORTION_FOOD_SPEND", 0.7);
	
	public static final double PASTURE_HARVEST_FRACTION = getDoubleProperty("PASTURE_HARVEST_FRACTION", 0.5);
	public static final double MEAT_EFFICIENCY = getDoubleProperty("MEAT_EFFICIENCY", 1.0);  // 'meat' is includes feed conversion ratio already, this is tech. change or similar
	public static final double IRRIGATION_EFFICIENCY_FACTOR = getDoubleProperty("IRRIGATION_EFFICIENCY_FACTOR", 1.0);
	public static final int ELLIOTT_BASEYEAR = 2010;
	public static final double ENVIRONMENTAL_WATER_CONSTRAINT = getDoubleProperty("ENVIRONMENTAL_WATER_CONSTRAINT", 0.4); // change with care, as due to normalisation it might not have the impact you first imagine
	public static final double OTHER_WATER_USE_FACTOR = getDoubleProperty("OTHER_WATER_USE_FACTOR", 1.0);
	public static final boolean USE_BLUE_WATER_FILE_IRRIG_CONSTRAINT = getBooleanProperty("USE_BLUE_WATER_FILE_IRRIG_CONSTRAINT", false);

	public static final boolean CONVERSION_COSTS_FROM_FILE = getBooleanProperty("CONVERSION_COSTS_FROM_FILE", false);
	public static final String CONVERSION_COST_FILE = getProperty("CONVERSION_COST_FILE", DATA_DIR + File.separator + "conversion_costs.csv"); // cost of converting from one land cover to another, $1000/ha
	public static final double LAND_CONVERSION_COST_FACTOR = getDoubleProperty("LAND_CONVERSION_COST_FACTOR", 1.0); // for monte carlo
	public static final double AGRI_LAND_EXPANSION_COST_FACTOR = getDoubleProperty("AGRI_LAND_EXPANSION_COST_FACTOR", 1.0); // for monte carlo
	public static final double LAND_BASE_COST = getDoubleProperty("LAND_BASE_COST", 0.01); // base cost of managed land $1000/ha
	// Cost of managed land abandonment set equal to LAND_BASE_COST to not favour abandonment nor maintenance
	public static final double CROPLAND_ABANDONMENT_COST = getDoubleProperty("CROPLAND_ABANDONMENT_COST", LAND_BASE_COST); // cropland to natural
	public static final double CROPLAND_CONVERSION_COST = getDoubleProperty("CROPLAND_CONVERSION_COST", 0.3);
	public static final double PASTURE_ABANDONMENT_COST = getDoubleProperty("PASTURE_ABANDONMENT_COST", LAND_BASE_COST); // pasture to natural
	public static final double PASTURE_CONVERSION_COST = getDoubleProperty("PASTURE_CONVERSION_COST", 0.2);
	public static final double TIMBER_FOREST_ABANDONMENT_COST = getDoubleProperty("TIMBER_FOREST_ABANDONMENT_COST", LAND_BASE_COST);
	public static final double TIMBER_FOREST_CONVERSION_COST = getDoubleProperty("TIMBER_FOREST_CONVERSION_COST", 0.5);
	public static final double TIMBER_FOREST_ESTABLISHMENT_COST = getDoubleProperty("TIMBER_FOREST_ESTABLISHMENT_COST", 0.05);
	public static final double CARBON_FOREST_CONVERSION_COST = getDoubleProperty("CARBON_FOREST_CONVERSION_COST", 10.0);
	public static final double SOLAR_CONVERSION_COST = getDoubleProperty("SOLAR_CONVERSION_COST", 10.0);
	public static final double NATURAL_CONVERSION_COST = getDoubleProperty("NATURAL_CONVERSION_COST", 0.08);
	// controls additional cost of natural conversion. Higher cost if higher proportion of land is natural
	public static final double INFRASTRUCTURE_COST_FACTOR = getDoubleProperty("INFRASTRUCTURE_COST_FACTOR", 0.6);


	public static final double TECHNOLOGY_CHANGE_ANNUAL_RATE = getDoubleProperty("TECHNOLOGY_CHANGE_ANNUAL_RATE", 0.002);
	public static final int TECHNOLOGY_CHANGE_START_STEP = getIntProperty("TECHNOLOGY_CHANGE_START_STEP", 0);

	public static final double DISCOUNT_RATE = getDoubleProperty("DISCOUNT_RATE", 0.03); // used to calculate NPV
	public static final double MONOCULTURE_PENALTY = getDoubleProperty("MONOCULTURE_PENALTY", 0.04); // Controls penalty for crop monocultures
	public static final double PASTURE_INTENSITY_PENALTY = getDoubleProperty("PASTURE_INTENSITY_PENALTY", 0.1); // penalty for concentrating pasture production

	public static final String ENERGY_DEMAND_MODEL = getProperty("ENERGY_DEMAND_MODEL", "ENSEMBLE-MEAN");
	public static final String ENERGY_DEMAND_SCENARIO = getProperty("ENERGY_DEMAND_SCENARIO", "SSP2_RCP45");
	public static final boolean ENABLE_GEN2_BIOENERGY = getBooleanProperty("ENABLE_GEN2_BIOENERGY", true);

	public static final double BIOENERGY_DEMAND_SHIFT = IS_CALIBRATION_RUN ? 1.0 : getDoubleProperty("BIOENERGY_DEMAND_SHIFT", 1.0);
	public static final boolean RESET_ENERGYCROP_PRICE = getBooleanProperty("RESET_ENERGYCROP_PRICE", false); // Resets price after calibration to avoid problems due to low initial demand

	public static final double MARKET_LAMBDA = getDoubleProperty("MARKET_LAMBDA", 0.5); // controls international market price adjustment rate
	public static final double DEFAULT_STOCK_USE_RATIO = getDoubleProperty("DEFAULT_STOCK_USE_RATIO", 0.3);

	public static final String PRICE_UPDATE_METHOD = getProperty("PRICE_UPDATE_METHOD", "AdaptiveLambda"); // Options: AdaptiveLambda, MarketImbalance, StockChange
	public static final double MAX_PRICE_INCREASE = getDoubleProperty("MAX_PRICE_INCREASE", 1.5);
	public static final double MAX_PRICE_DECREASE = getDoubleProperty("MAX_PRICE_DECREASE", .75);
	public static final int DEMAND_RECALC_MAX_ITERATIONS = IS_CALIBRATION_RUN ? 0 : getIntProperty("DEMAND_RECALC_MAX_ITERATIONS", 0);  // 0 is original behaviour
	public static final boolean DEMAND_RECALC_ON_NEGATIVE_STOCK = IS_CALIBRATION_RUN ? false : getBooleanProperty("DEMAND_RECALC_ON_NEGATIVE_STOCK", false);

	public static final boolean PREDEFINED_COUNTRY_GROUPING = getBooleanProperty("PREDEFINED_COUNTRY_GROUPING", true);

	public static final double UNHANDLED_CROP_RATE = getDoubleProperty("UNHANDLED_CROP_RATE", 0.01323);  // mostly forage crops and fibers
	public static final double SETASIDE_RATE = getDoubleProperty("SETASIDE_RATE", 0.08298);  // includes aside, fallow and failed cropland areas

	public static final double OTHER_INTENSITY_COST = getDoubleProperty("OTHER_INTENSITY_COST", 1.0);
	public static final double OTHER_INTENSITY_PARAM = getDoubleProperty("OTHER_INTENSITY_PARAM", 3.22);

	public static final double IRRIG_COST_SCALE_FACTOR = getDoubleProperty("IRRIG_COST_SCALE_FACTOR", 0.0002);
	public static final double IRRIG_COST_MULTIPLIER = getDoubleProperty("IRRIG_COST_MULTIPLIER", 1.0);
	public static final double FERTILISER_COST_PER_T = getDoubleProperty("FERTILISER_COST_PER_T", 1.4); // assuming 50/50 urea and DAP works out to roughly $1000 / tonne N
	public static final double FERTILISER_MAX_COST = ModelConfig.getAdjParam("FERTILISER_COST_PER_T") * MAX_FERT_AMOUNT/1000;

	public static final double DOMESTIC_PRICE_MARKUP = getDoubleProperty("DOMESTIC_PRICE_MARKUP", 1.0);
	public static final double TRANSPORT_LOSSES = getDoubleProperty("TRANSPORT_LOSSES", 0.05);  // in international trade
	public static final double TRANSPORT_COST = getDoubleProperty("TRANSPORT_COST", 0.1);  // 10% transport cost
	public static final double TRADE_BARRIER_FACTOR_DEFAULT = getDoubleProperty("TRADE_BARRIER_FACTOR_DEFAULT", 0.2);  // price factor in international trade, transport cost and real trade barriers
	public static final double TRADE_BARRIER_MULTIPLIER = getDoubleProperty("TRADE_BARRIER_MULTIPLIER", 1.0);
	public static final boolean ACTIVE_TRADE_BARRIERS = getBooleanProperty("ACTIVE_TRADE_BARRIERS", true);  // if set to true read in barrier information from file, otherwise use default as above
	public static final double TRADE_BARRIER_ENERGY_CROPS = getDoubleProperty("TRADE_BARRIER_ENERGY_CROPS", 0.01);  // price factor in international trade, transport cost and real trade barriers
	public static final boolean SHOCKS_POSSIBLE = getBooleanProperty("SHOCKS_POSSIBLE", false);
	public static final double YIELD_SHOCK_MAGNIFIER = getDoubleProperty("YIELD_SHOCK_MAGNIFIER", 1.0);

	public static final boolean ENABLE_SUBSIDIES = getBooleanProperty("ENABLE_SUBSIDIES", false); // currently disabled by default as causes issues.

	public static final boolean PROTECTED_AREAS_ENABLED = getBooleanProperty("PROTECTED_AREAS_ENABLED", true);
	public static final double MIN_NATURAL_RATE = getDoubleProperty("MIN_NATURAL_RATE", 0.05);

	public static final boolean DEBUG_JUST_DEMAND_OUTPUT = getBooleanProperty("DEBUG_JUST_DEMAND_OUTPUT", false);
	public static final int DEBUG_LOG_LEVEL = getIntProperty("DEBUG_LEVEL", 2); // Print only events below this level. Errors & Warnings always printed (0); Solve status (1); Global events (2); Country events (3)
	public static final boolean DEBUG_LIMIT_COUNTRIES = getBooleanProperty("DEBUG_LIMIT_COUNTRIES", false);
	public static final String DEBUG_COUNTRY_NAME = getProperty("DEBUG_COUNTRY_NAME", "China");
	public static final String GAMS_COUNTRY_TO_SAVE = getProperty("GAMS_COUNTRY_TO_SAVE", "none");
	public static final boolean EXCLUDE_COUNTRIES_IN_LIST = getBooleanProperty("EXCLUDE_COUNTRIES_IN_LIST", false);
	public static final String EXCLUDED_COUNTRIES_FILE = DATA_DIR + File.separator + "countries_excluded.csv";
	
	public static final double PASTURE_MAX_IRRIGATION_RATE = getDoubleProperty("DEFAULT_MAX_IRRIGATION_RATE", 50.0); // shouldn't need this but some areas crops don't have a value, but was causing them to be selected
	public static final int LPJG_TIMESTEP_SIZE = getIntProperty("LPJG_TIMESTEP_SIZE", 5);
	public static final int LPJ_YEAR_OFFSET = getIntProperty("LPJ_YEAR_OFFSET", -1);
	
	public static final int NUM_YIELD_CLUSTERS = getIntProperty("NUM_YIELD_CLUSTERS", 50);
	public static final long RANDOM_SEED = getIntProperty("RANDOM_SEED", 1974329);  // any number will do
	
	public static final String CHECKPOINT_YEARS = getProperty("CHECKPOINT_YEARS", null); // 2020,2050
	public static final boolean SERIALIZE_FINAL_TIMESTEP_ONLY = getBooleanProperty("SERIALIZE_FINAL_TIMESTEP_ONLY", false);
	
	// Protected areas forcing parameters
	public static final boolean FORCE_PROTECTED_AREAS =  getBooleanProperty("FORCE_PROTECTED_AREAS", false);
	public static final int FORCE_PROTECTED_AREAS_START_YEAR  = getIntProperty("FORCE_PROTECTED_AREAS_START_YEAR", 2020);
	public static final int FORCE_PROTECTED_AREAS_END_YEAR  = getIntProperty("FORCE_PROTECTED_AREAS_END_YEAR", 2040);
	public static final boolean ENABLE_LAND_SHARING = getBooleanProperty("ENABLE_LAND_SHARING", true); // if false, protected agricultural land becomes natural
	public static final double CONSTANT_PROTECTED_AREA_RATE = getDoubleProperty("CONSTANT_PROTECTED_AREA_RATE", Double.NaN);
	public static final String RESTRICT_PROTECTED_LAND_CONVERSION_FOR = getProperty("RESTRICT_LAND_CONVERSION_FOR", "natural,carbonForest"); // if protected, cannot convert
	public static final int INTENSITY_LIMIT_START_YEAR = getIntProperty("INTENSITY_LIMIT_START_YEAR", 2020);
	public static final int INTENSITY_LIMIT_END_YEAR = getIntProperty("INTENSITY_LIMIT_END_YEAR", 2040);	
	public static final double PROTECTED_CROPLAND_FERTILISER_LIMIT = getDoubleProperty("PROTECTED_CROPLAND_FERTILISER_LIMIT", 1.0); // intensity 0 to 1
	public static final double PROTECTED_IRRIGATION_CONSTRAINT_FACTOR = getDoubleProperty("PROTECTED_IRRIGATION_CONSTRAINT_FACTOR", 1.0); // intensity 0 to 1
	public static final double PROTECTED_CROPLAND_OTHER_LIMIT = getDoubleProperty("PROTECTED_CROPLAND_OTHER_LIMIT", 1.0); // intensity 0 to 1
	public static final double UNPROTECTED_CROPLAND_FERTILISER_LIMIT = getDoubleProperty("UNPROTECTED_CROPLAND_FERTILISER_LIMIT", 1.0); // intensity 0 to 1
	public static final double UNPROTECTED_IRRIGATION_CONSTRAINT_FACTOR = getDoubleProperty("UNPROTECTED_IRRIGATION_CONSTRAINT_FACTOR", 1.0); // intensity 0 to 1
	public static final double UNPROTECTED_CROPLAND_OTHER_LIMIT = getDoubleProperty("UNPROTECTED_CROPLAND_OTHER_LIMIT", 1.0); // intensity 0 to 1
	public static final int INTENSITY_LIMIT_DECAY_FACTOR = getIntProperty("INTENSITY_LIMIT_DECAY_FACTOR", 4);

	public static final boolean FORCE_LCC = getBooleanProperty("FORCE_LCC", false);
	public static final String FORCED_LCC_FILES_DIR = getProperty("FORCED_LCC_FILES_DIR", OUTPUT_DIR);

	public static final boolean USE_CRAFTY_COUNTRIES = getBooleanProperty("USE_CRAFTY_COUNTRIES", false);
	public static final String CRAFTY_COUNTRIES_FILE= getProperty("CRAFTY_COUNTRIES_FILE", DATA_DIR + File.separator + "craftyCountries.csv");
	public static final String CRAFTY_PRODUCTION_DIR = getProperty("CRAFTY_PRODUCTION_DIR", OUTPUT_DIR + File.separator + "crafty");
	public static final boolean ENABLE_CRAFTY_IMPORTS_UPDATE = getBooleanProperty("ENABLE_CRAFTY_IMPORTS_UPDATE", false);

	public static final boolean EXTRAPOLATE_YIELD_FERT_RESPONSE = getBooleanProperty("EXTRAPOLATE_YIELD_FERT_RESPONSE", false);
	
	public static final boolean ADJUST_DIET_PREFS = getBooleanProperty("ADJUST_DIET_PREFS", false);
	public static final int DIET_CHANGE_START_YEAR = getIntProperty("DIET_CHANGE_START_YEAR", 2020);
	public static final int DIET_CHANGE_END_YEAR = getIntProperty("DIET_CHANGE_END_YEAR", 2050);
	public static final double DIET_CHANGE_RATE = getDoubleProperty("DIET_CHANGE_RATE", 5.3467 / (DIET_CHANGE_END_YEAR - DIET_CHANGE_START_YEAR));
	public static final double DIET_CONVERGENCE_PARAM = getDoubleProperty("DIET_CONVERGENCE_PARAM", 0.0); // How much diets converge to prediction
	public static final String TARGET_DIET_FILENAME = getProperty("TARGET_DIET_FILENAME", "target_diet.csv");
	public static final String TARGET_DIET_FILE = getProperty("TARGET_DIET_FILE", DATA_DIR + File.separator + TARGET_DIET_FILENAME);
	public static final String TARGET_DIET_SCENARIO = getProperty("TARGET_DIET_SCENARIO", SSP_SCENARIO);

	public static final boolean APPLY_EXPORT_TAXES = getBooleanProperty("APPLY_EXPORT_TAXES", false);
	public static final double EXPORT_TAX_RATE = getDoubleProperty("EXPORT_TAX_RATE", 1.0);
	public static final double EXPORT_TAX_THRESHOLD = getDoubleProperty("EXPORT_TAX_THRESHOLD", 0.1);
	public static final int RESET_STOCK_YEAR = IS_CALIBRATION_RUN ? 0 : getIntProperty("RESET_STOCK_YEAR", BASE_YEAR);
	public static final String MIN_MAX_TRADE_FILE = getProperty("MIN_MAX_TRADE_FILE", OUTPUT_DIR + File.separator + "minMaxNetImport.csv");
	
	// Forestry
	public static final boolean IS_FORESTRY_ON = getBooleanProperty("IS_FORESTRY_ON", true);
	public static final String WOOD_DEMAND_FILENAME = getProperty("WOOD_DEMAND_FILENAME", "wood_base_demand.csv");
	public static final String WOOD_DEMAND_FILE = getProperty("WOOD_DEMAND_FILE", DATA_DIR + File.separator + WOOD_DEMAND_FILENAME);
	public static final double INIT_WOOD_STOCK = getDoubleProperty("INIT_WOOD_STOCK", 1200.0); // million m3
	public static final double WOOD_BIOMASS_CONVERSION_FACTOR = getDoubleProperty("WOOD_BIOMASS_CONVERSION_FACTOR", 0.3); // m3 to tC-eq p.16 [https://doi.org/10.5194/gmd-13-5425-2020]
	public static final double FOREST_MANAGEMENT_COST = IS_FORESTRY_ON ? getDoubleProperty("FOREST_MANAGEMENT_COST", 2.0) : 0.0; // establishment, management etc. $1000/ha
	public static final double WOOD_TRADE_BARRIER = getDoubleProperty("WOOD_TRADE_BARRIER", 0.2); //$1000/m3
	public static final double INIT_WOOD_PRICE = IS_FORESTRY_ON ? getDoubleProperty("INIT_WOOD_PRICE", 0.03) : 0.0; // $1000/m3
	public static final int CARBON_WOOD_MAX_TIME = getIntProperty("CARBON_WOOD_MAX_TIME", 160); // upper data limit, years
	public static final double PROTECTED_TIMBER_INTENSITY_LIMIT = getDoubleProperty("PROTECTED_TIMBER_INTENSITY_LIMIT", 0.02); // = 1/rotation_period
	public static final double UNPROTECTED_TIMBER_INTENSITY_LIMIT = getDoubleProperty("UNPROTECTED_TIMBER_INTENSITY_LIMIT", 0.1);
	
	// Carbon
	public static final boolean IS_CARBON_ON = !IS_CALIBRATION_RUN && getBooleanProperty("IS_CARBON_ON", false);
	public static final String CARBON_DEMAND_FILENAME = getProperty("CARBON_DEMAND_FILENAME", "carbon_demand.csv");
	public static final String CARBON_DEMAND_FILE = getProperty("CARBON_DEMAND_FILE", DATA_DIR + File.separator + CARBON_DEMAND_FILENAME);
	public static final int CARBON_HORIZON = getIntProperty("CARBON_HORIZON", 60); // time period for calculating carbon credits
	public static final double INIT_CARBON_PRICE = IS_CARBON_ON ? getDoubleProperty("INIT_CARBON_PRICE", 0.005) : 0.0; // $1000/tC-eq
	public static final double INIT_CARBON_STOCK = getDoubleProperty("INIT_CARBON_STOCK", 100.0); // MtC-eq
	public static final String CARBON_OPTIONS_FILENAME = getProperty("CARBON_OPTIONS_FILENAME", "carbon_options.csv"); // config for what is included in carbon market
	public static final String CARBON_OPTIONS_FILE = getProperty("CARBON_OPTIONS_FILE", DATA_DIR + File.separator + CARBON_OPTIONS_FILENAME);
	public static final double CARBON_FOREST_MAX_PROPORTION = getDoubleProperty("CARBON_FOREST_MAX_PROPORTION", 1.0); // maximum proportion of land cover as carbon forest
	public static final double CARBON_TRADE_ADJ_COST_MULTIPLIER = getDoubleProperty("CARBON_TRADE_ADJ_COST_MULTIPLIER", 0.0); // No physical goods traded so can have lower cost
	// How demand is allocated. Options: global (countries can export carbon credits to market), country (global demand allocated by GDP), price (no demand, exogenous price)
	public static final String CARBON_DEMAND_METHOD = getProperty("CARBON_DEMAND_METHOD", "global");

	// Inequality
	public static final boolean IS_INEQUALITY_ON = getBooleanProperty("IS_INEQUALITY_ON", false);
	public static final String INCOME_DECILES_FILENAME = getProperty("INCOME_DECILES_FILENAME", "income_deciles.csv");
	public static final String INCOME_DECILES_FILE = getProperty("INCOME_DECILES_FILE", DATA_DIR + File.separator + INCOME_DECILES_FILENAME);
	public static final boolean CHANGE_INEQUALITY_YEAR = IS_CALIBRATION_RUN ? false : getBooleanProperty("CHANGE_INEQUALITY_YEAR", CHANGE_DEMAND_YEAR);

    // Solar
	public static final boolean IS_PHOTOVOLTAICS_ON = getBooleanProperty("IS_PHOTOVOLTAICS_ON", false);
	public static final boolean IS_AGRIVOLTAICS_ON = getBooleanProperty("IS_AGRIVOLTAICS_ON", false);
	public static final String MAX_SOLAR_FRACTION_FILE = ModelConfig.SPATIAL_DATA_DIR + File.separator + "solar_max_fraction.csv";
	public static final String AGRIVOLTAICS_YIELD_PARAM_FILE = getProperty("AGRIVOLTAICS_YIELD_PARAM_FILE", DATA_DIR + File.separator + "agrivoltaic_yield_params.csv");
	public static final String SOLAR_POTENTIAL_DIR_BASE = getProperty("SOLAR_POTENTIAL_DIR_BASE");
	public static final String SOLAR_POTENTIAL_DIR_TOP = getProperty("SOLAR_POTENTIAL_DIR_TOP");
	public static final String SOLAR_POTENTIAL_FILENAME = getProperty("SOLAR_POTENTIAL_FILENAME", "pv_yield.csv");
	public static final int SOLAR_POTENTIAL_DATA_STEP_SIZE = getIntProperty("SOLAR_POTENTIAL_DATA_STEP_SIZE", 10);
	public static final double MAX_SOLAR_FRACTION = getDoubleProperty("MAX_SOLAR_FRACTION", 0.1); // Maximum PV/AV fraction in grid cell

	// PV costs from IRENA, Renewable Power Generation Costs in 2022
	public static final double PV_INSTALLED_COST = getDoubleProperty("PV_INSTALLED_COST", 0.876); // $1000/kW
	public static final double PV_MAINTENANCE_COST = getDoubleProperty("PV_MAINTENANCE_COST", 0.0132); // $1000/kW/yr
	public static final double PV_LIFESPAN = getDoubleProperty("PV_LIFESPAN", 25.0); // years
	// https://www.ise.fraunhofer.de/en/publications/studies/photovoltaics-report.html
	public static final double BASE_PV_EFFICIENCY = getDoubleProperty("BASE_PV_EFFICIENCY", 0.209); // initial PV efficiency
	public static final double PV_EFFICIENCY_GROWTH_RATE = getDoubleProperty("PV_EFFICIENCY_GROWTH_RATE", 0.001);

	// GSR and performance ratio from https://doi.org/10.1016/j.energy.2015.10.108
	public static final double PV_GSR = getDoubleProperty("PV_GSR", 0.75); // Generator to System Ratio (area of (PV + spacing) / total area)

	// Typical GCR, cost factor and yield factor from https://doi.org/10.1007/s10457-023-00906-3
	public static final double AV_GCR = getDoubleProperty("AV_GCR", 0.25); // Ground Coverage Ratio of agrivoltaics
	public static final double AV_COST_FACTOR = getDoubleProperty("AV_COST_FACTOR", 1.3); // Cost multiplier for AV compared to PV
	public static final double AV_YIELD_FACTOR = getDoubleProperty("AV_YIELD_FACTOR", 0.9); // Yield reduction due to area taken by infrastructure

	// Bioenergy
	// Miscanthus energy density (5.1 MWh/t) from https://doi.org/10.1104%2Fpp.15.00066
	// Assuming biomass to electricity using Integrated gasification combined cycle (IGCC). 42% Efficiency from https://doi.org/10.1007/s10584-013-0940-z
	public static final double ENERGYCROPS_CONVERSION_FACTOR = getDoubleProperty("ENERGYCROPS_CONVERSION_FACTOR", 2.142); // 5.1 * 0.42 MWh/tDM.

	public static final double ENERGY_PRICE = getDoubleProperty("ENERGY_PRICE", 0.1); // $1000/MWh

}
