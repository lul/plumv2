package ac.ed.lurg.country;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.types.CropToDoubleMap;



public class TradeManager {

	private static final int COUNTRY_COL = 0; 
	private static final int ITEM_COL = 1; 
	private static final int TRADE_BARRIER_COL = 2; 

	private Map<SingleCountry, CropToDoubleMap> singleCountryBarriers = new HashMap<>();

	public TradeManager() {
		if(ModelConfig.ACTIVE_TRADE_BARRIERS)
			read();
	}
	
	public CropToDoubleMap getTradeBarriers(CompositeCountry cc) {

		CropToDoubleMap tradeBarriers = new CropToDoubleMap();

		// If not using data, use default value for all
		if (!ModelConfig.ACTIVE_TRADE_BARRIERS) {
			for (CropType c : CropType.getImportedTypes()) {
				tradeBarriers.put(c, ModelConfig.TRADE_BARRIER_FACTOR_DEFAULT);
			}
			return tradeBarriers;
		}

		// Use observed trade barriers
		Collection<SingleCountry> singleCountries = CountryManager.getInstance().getAllForCompositeCountry(cc);
		int n = 0;
		for (SingleCountry sc : singleCountries) {
			CropToDoubleMap countryData = singleCountryBarriers.get(sc);
			for (CropType crop : CropType.getImportedTypes()) {
				if (countryData.containsKey(crop)) {
					tradeBarriers.incrementValue(crop, countryData.get(crop));
				} else {
					tradeBarriers.incrementValue(crop, ModelConfig.TRADE_BARRIER_FACTOR_DEFAULT);
				}
			}
			n++;
		}
		tradeBarriers.divideBy(n); // Take average. TODO weigh by population or demand
		tradeBarriers.put(CropType.ENERGY_CROPS, ModelConfig.TRADE_BARRIER_ENERGY_CROPS);

		return tradeBarriers;
	}
	
	
	public void read() {
		String filename = ModelConfig.TRADE_BARRIERS_FILE;
	
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename)); 
			String line, countryCode, itemName;
			double barrierValue;
			reader.readLine(); // read header

			while ((line=reader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 3)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				try {

					countryCode = tokens[COUNTRY_COL];
					itemName = tokens[ITEM_COL];
					barrierValue = Double.parseDouble(tokens[TRADE_BARRIER_COL]);

					if (!CountryManager.getInstance().isCountry(countryCode)) {
						continue;
					}
					
					SingleCountry country = CountryManager.getInstance().getForCode(countryCode);
					CropType crop = CropType.getForFaoName(itemName);
					CropToDoubleMap countryData = singleCountryBarriers.computeIfAbsent(country, k -> new CropToDoubleMap());
					countryData.put(crop, barrierValue);

				} catch (Exception e) {
					LogWriter.printlnError("Failed in processing trade barriers line " + line);
					LogWriter.print(e);
				} 
			} 
			reader.close(); 
		
		} catch (Exception e) {
			LogWriter.printlnError("Failed in reading trade barriers data file");
			LogWriter.print(e);
		} 
		LogWriter.println("Processed " + filename + ", create " + singleCountryBarriers.size() + " barriers entries");

	}
	
}
