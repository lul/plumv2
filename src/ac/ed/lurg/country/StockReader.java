package ac.ed.lurg.country;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;

public class StockReader {

	private static final int ITEM_COL = 0; 
	private static final int STOCK_COL = 1; 
		
	public Map<CropType, Double> read() {
		Map<CropType, Double> stockLevels = new HashMap<CropType, Double>();
		
		String filename = ModelConfig.STOCKS_FILE;
		try {
			BufferedReader fitReader = new BufferedReader(new FileReader(filename)); 
			String line, faoItemName;
			Double stockLevel;
			fitReader.readLine(); // read header

			while ((line=fitReader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 2)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				faoItemName = tokens[ITEM_COL];
				stockLevel = Double.parseDouble(tokens[STOCK_COL]);
				
				CropType crop = CropType.getForFaoName(faoItemName);
				
				stockLevels.put(crop,stockLevel);
			} 
			fitReader.close(); 
		
		} catch (IOException e) {
			LogWriter.printlnError("Failed in reading stocks file");
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename);
		
		return stockLevels;
	}
}
