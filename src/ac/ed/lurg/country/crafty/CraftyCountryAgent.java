package ac.ed.lurg.country.crafty;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.AbstractCountryAgent;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.country.CountryPrice;
import ac.ed.lurg.country.GlobalPrice;
import ac.ed.lurg.demand.AbstractDemandManager;
import ac.ed.lurg.landuse.CarbonUsageData;
import ac.ed.lurg.landuse.CropUsageData;
import ac.ed.lurg.landuse.WoodUsageData;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;

/**
 * Country agent that is interface over data transferred to and from CRAFTY.
 * Need to configure country mapping to that there is
 * */
public class CraftyCountryAgent extends AbstractCountryAgent {

	private Map<CropType, CropUsageData> cropUsageData;
	private WoodUsageData woodUsageData;

	public CraftyCountryAgent(AbstractDemandManager demandManager,CompositeCountry country, Map<CropType, Double> tradeBarriers,
							  Map<CropType, CropUsageData> initialCropUsageData, WoodUsageData initialWoodData) {
		super(demandManager, country, tradeBarriers);
		this.cropUsageData = initialCropUsageData;
		if (initialWoodData == null) {
			initialWoodData = new WoodUsageData(0, 0, 0);

		}
		this.woodUsageData = initialWoodData;
	}

	public Map<CropType, CropUsageData> getCropUsageData() {
		return cropUsageData;
	}

	public void updateProduction(Map<CropType, CropUsageData> cropUsageMap, Map<CropType, GlobalPrice> worldPrices,
								 GlobalPrice carbonPrice, GlobalPrice woodPrice) {
		this.cropUsageData = cropUsageMap;
		calculateCountryPricesAndDemand(worldPrices, carbonPrice, woodPrice, false);
		if (ModelConfig.ENABLE_CRAFTY_IMPORTS_UPDATE)
			updateNetImportsFromProdAndDemand(getTotalDemand(), cropUsageMap);
	}

	protected CountryPrice createCountryPrices(CropType crop, GlobalPrice worldPrice, CountryPrice refCountryPrice) {
		CropUsageData cropUsage = cropUsageData.get(crop);

		double weighting = Math.max(0, cropUsage.getShockedNetImports()/(cropUsage.getProductionExpected()+cropUsage.getShockedNetImports()));
		double prodCost = cropUsage.getProdCostRate();
		Double tb = tradeBarriers.get(crop);
		double tradeBarrier = tb==null ? 0 : tb;
		double exportTaxRateForCrop = CommodityType.CEREALS_STARCHY_ROOTS.getCropTypes().contains(crop) ? exportTaxRate : 0.0;
		CountryPrice cp = new CountryPrice(crop, worldPrice, tradeBarrier, prodCost, weighting, exportTaxRateForCrop,
				refCountryPrice);
		return cp;
	}

	protected CountryPrice createWoodCountryPrice(GlobalPrice worldPrice) {
		WoodUsageData woodUsageData = getWoodUsageData();
		double prod = woodUsageData.getProduction();
		double netImports = woodUsageData.getNetImport();
		double weighting = prod > 0 ? Math.max(0, netImports / (prod + netImports)) : 1.0;
		double prodCost = woodUsageData.getProdCostRate();
		CountryPrice cp = new CountryPrice(null, worldPrice, ModelConfig.WOOD_TRADE_BARRIER, prodCost, weighting, 0,
				currentWoodPrice);
		return cp;
	}

	@Override
	public WoodUsageData getWoodUsageData() {
		return woodUsageData;
	}

	@Override
	public CarbonUsageData getCarbonUsageData() {
		// TODO Auto-generated method stub
		return null;
	}


}