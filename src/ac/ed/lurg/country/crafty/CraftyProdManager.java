package ac.ed.lurg.country.crafty;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.country.GlobalPrice;
import ac.ed.lurg.landuse.CropUsageData;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;
import ac.ed.lurg.utils.WatchForFile;

public class CraftyProdManager {

	private Collection<CompositeCountry> craftyCountries;
	
	public synchronized Collection<CompositeCountry> getCraftyCountries() {
		if (craftyCountries == null) {
			craftyCountries = new ArrayList<CompositeCountry>();
			StringTabularReader 	tabularReader = new StringTabularReader(",", new String[]{"Country"});
			List<Map<String, String>> rows = tabularReader.read(ModelConfig.CRAFTY_COUNTRIES_FILE);
			for (Map<String, String> row : rows) {
				craftyCountries.add(new CompositeCountry(row.get("Country"), "craftycountry"));
			}
		}
		return craftyCountries;
	}

	public void updateWithCraftyData(Collection<CraftyCountryAgent> craftyCountryAgents, Timestep timestep, Map<CropType, GlobalPrice> worldPrices,
									 GlobalPrice carbonPrice, GlobalPrice woodPrice) {
		int currentYear = (ModelConfig.IS_CALIBRATION_RUN || !ModelConfig.CHANGE_DEMAND_YEAR) ? ModelConfig.BASE_YEAR : timestep.getYear();
		String rootDir = ModelConfig.CRAFTY_PRODUCTION_DIR + File.separator + currentYear;
		long startTime = System.currentTimeMillis();
		
		WatchForFile fileWatcher = new WatchForFile(new File(rootDir + File.separator + "done"));
		boolean foundFile = fileWatcher.await(ModelConfig.LPJG_MONITOR_TIMEOUT_SEC);
		if (!foundFile) {
			LogWriter.printlnError("Not able to find marker file.  May have timed out.");
			throw new RuntimeException("Not able to find marker file.  May have timed out.");
		}
		LogWriter.println("Found marker file in " + (System.currentTimeMillis() - startTime) + " ms", 2);
		
		StringTabularReader 	tabularReader = new StringTabularReader(",", new String[]{"Country","Crop","Production","MonogastricFeed","RuminantFeed","NetImportsExpected"});
		tabularReader.read(rootDir + File.separator + "production.csv");

		
		for (CraftyCountryAgent cca : craftyCountryAgents) {
			Map<CropType, CropUsageData> cropUsageMap = new HashMap<CropType, CropUsageData>();
	
			Map<String, String> queryMap = new HashMap<String, String>();
			queryMap.put("Country", cca.getCountry().getName());

			try {
				List<Map<String, String>> rows = tabularReader.query(queryMap);
				for (Map<String, String> row : rows) {
					String cropS = row.get("Crop");
					CropType crop = CropType.getForGamsName(cropS);
					String prodS = row.get("Production");
					Double prod = Double.valueOf(prodS);
					String monoFeedS = row.get("MonogastricFeed");
					Double monoFeed = Double.valueOf(monoFeedS);
					String rumFeedS = row.get("RuminantFeed");
					Double rumFeed = Double.valueOf(rumFeedS);
					String netImpS = row.get("NetImportsExpected");
					Double netImp = Double.valueOf(netImpS);
					
					CropUsageData cropUsageItem = new CropUsageData(rumFeed, monoFeed, netImp, prod);
					cropUsageMap.put(crop, cropUsageItem);
				}
				if (cropUsageMap.size() < CropType.getImportedTypes().size()) {  // Don't need setaside or pasture, which aren't imported either
					LogWriter.printlnError("Not all crops present in Crafty production for country: " + cca.getCountry() + " only " + cropUsageMap.size());
				}
				cca.updateProduction(cropUsageMap, worldPrices, carbonPrice, woodPrice);
			}
			catch (Exception e) {
				LogWriter.printlnWarning("Problem getting Crafty data for: " + cca.getCountry());
			}
		}
	}
}