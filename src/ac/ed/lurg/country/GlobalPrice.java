package ac.ed.lurg.country;

import java.io.Serializable;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.utils.LogWriter;

public class GlobalPrice implements Serializable {

	private static final long serialVersionUID = 2477952576838887039L;
	
	private Timestep timestep;
	private double exportPrice;
	private double importAmount;
	private double exportAmountBeforeLoss;
	private double transportLosses;
	private double stockLevel;
	private double stockUseRatio;
	private double referencePrice;
	private double prevStockChange;
	private double production;

	private GlobalPrice(Timestep timestep, double exportPrice, double stockLevel, double importAmount, double exportAmountBeforeLoss,
						double transportLosses, double referencePrice, double stockUseRatio, double prevStockChange,
						double production) {
		this.timestep = timestep;
		this.exportPrice = exportPrice;
		this.stockLevel = stockLevel;
		this.stockUseRatio = stockUseRatio;
		this.importAmount = importAmount;
		this.exportAmountBeforeLoss = exportAmountBeforeLoss;
		this.transportLosses = transportLosses;
		this.referencePrice = referencePrice;
		this.prevStockChange = prevStockChange;
		this.production = production;
		LogWriter.println("Created " + this, 3);
	}
	
	public static GlobalPrice createInitial(double exportPrice, double intitalStock) {
		return new GlobalPrice(new Timestep(ModelConfig.START_TIMESTEP-1), exportPrice, intitalStock, Double.NaN,
				Double.NaN, Double.NaN, Double.NaN, Double.NaN, 0.0, Double.NaN);
	}
	
	public double getExportPrice(){
		return exportPrice;
	}

	public double getStockLevel(){
		return stockLevel;
	}
	
	public double getCountryImportPrice(double countryTradeBarrier) {

		double tradeBarrierMultiplier = ModelConfig.getAdjParam("TRADE_BARRIER_MULTIPLIER");
		double transportLossesRate =ModelConfig.getAdjParam("TRANSPORT_LOSSES");
		double transportCosts = ModelConfig.getAdjParam("TRANSPORT_COST");

		double importPrice = exportPrice * (1 + transportCosts) * (1.0 + countryTradeBarrier*tradeBarrierMultiplier) / (1.0 - transportLossesRate);
		return importPrice;
	}

	public double getImportAmount() {
		return importAmount;
	}
	
	public double getExportsAfterTransportLosses() {
		return exportAmountBeforeLoss - transportLosses;
	}
	
	public double getExportsBeforeTransportLoss() {
		return exportAmountBeforeLoss ;
	}
	
	public double getTransportLosses() {
		return transportLosses;
	}

	public GlobalPrice createWithUpdatedMarketPrices(double newImports, double newExportAmountBeforeLoss, Timestep thisTimeStep,
													 double production, boolean hasTransportLosses, double targetStockUseRatio) {

		if (newImports > 0 || newExportAmountBeforeLoss > 0) {
			double oldDiff = timestep.equals(thisTimeStep) ? exportAmountBeforeLoss - transportLosses - importAmount : 0.0; // if recomputing for same year need to back our previous adjustment
			double transportLossRate = hasTransportLosses ? ModelConfig.updateParameterForShocks(thisTimeStep.getYear(), "TRANSPORT_LOSSES") : 0;
			double newTransportLosses = newExportAmountBeforeLoss * transportLossRate;
			double stockChange = newExportAmountBeforeLoss - newTransportLosses - newImports - oldDiff;
			
			double updatedStock = stockLevel + stockChange;

			double targetStock = production * targetStockUseRatio;
			double updatedStockUseRatio = production > 0 ? updatedStock / production : 0;

			LogWriter.println(String.format("     imports %.2f, exports %.2f", newImports, newExportAmountBeforeLoss - newTransportLosses), 2);
			LogWriter.println(String.format("     updatedStock %.2f, previous %.2f (last timestep %.2f), stockChange %.2f, oldDiff %.2f", updatedStock, stockLevel, stockLevel-oldDiff, stockChange, oldDiff), 2);
			
			double adjustment = 1;
			double marketLambda = ModelConfig.MARKET_LAMBDA;

			if (!ModelConfig.MARKET_ADJ_PRICE) {
				adjustment = 1;
			}
			else if (ModelConfig.PRICE_UPDATE_METHOD.equals("AdaptiveLambda")) {
				// Market lambda lower when the rate of change in stock is decreasing
				// Acts as a damping mechanism to reduce price fluctuation
				double lambda = Math.atan2(Math.abs(stockChange), Math.abs(prevStockChange)) / (0.5 * Math.PI) * marketLambda;
				LogWriter.println("     lambda="+lambda);

				// Prices increase as a proportion of stockLevel and decrease as a proportion of production
				double denominator = stockChange < 0 ? Math.max(0, stockLevel) : production;
				adjustment = 1 - lambda * stockChange / denominator;

			}

			else if (ModelConfig.PRICE_UPDATE_METHOD.equals("MarketImbalance")) {
				double ratio = stockChange/(production-stockChange);
				adjustment =  Math.exp(-ratio * marketLambda);
			}
			else if (ModelConfig.PRICE_UPDATE_METHOD.equals("StockChange")) {
				if (stockChange == 0 && production == 0)
					adjustment=1;
				else if (stockChange >= 0) // stock increasing, so decrease price.  stockChange is positive so adjustment < 1
					adjustment = 1 - marketLambda * stockChange/production;
				else // stock decreasing, so increase price.  stockChange is negative so adjustment > 1
					adjustment = 1 - marketLambda * stockChange/Math.max(0, stockLevel);
			}
			else {
				LogWriter.printlnError("Price adjustment method not set!");
			}

			LogWriter.print(String.format("     initally adjustment= %.4f", adjustment), 2);

			if (adjustment < ModelConfig.MAX_PRICE_DECREASE)
				adjustment = ModelConfig.MAX_PRICE_DECREASE;  // default to max down change

			if (adjustment > ModelConfig.MAX_PRICE_INCREASE)
				adjustment = ModelConfig.MAX_PRICE_INCREASE;  // default to max up change
			
			LogWriter.println(String.format(", after limit %.4f", adjustment), 2);

			double newPrice = exportPrice * adjustment;
			double refPrice = ModelConfig.IS_CALIBRATION_RUN ? exportPrice : referencePrice;  // during calibration reference price isn't fixed, but it is after that

			if (ModelConfig.IS_CALIBRATION_RUN) {
				updatedStock = production * targetStockUseRatio;
			}

			return new GlobalPrice(thisTimeStep, newPrice, updatedStock, newImports, newExportAmountBeforeLoss,
					newTransportLosses, refPrice, updatedStockUseRatio, stockChange, production);
		}
		else {
			LogWriter.printlnWarning(String.format("Price for not updated (still %s), as no imports and no exports for it", exportPrice));
			return this;
		}
	}

	public GlobalPrice createWithSetPrice(double newPrice, double newImports, double newExportAmountBeforeLoss, Timestep thisTimeStep,
								  double production) {
		LogWriter.println(String.format("     imports %.2f, exports %.2f", newImports, newExportAmountBeforeLoss), 2);

		return new GlobalPrice(thisTimeStep, newPrice, stockLevel, newImports, newExportAmountBeforeLoss,
				0, exportPrice, 0, 0, production);
	}

	@Override
	public String toString() {
		return "GlobalPrice [timestep=" + timestep.getTimestep() + ", exportPrice=" + exportPrice + ", importAmount=" + importAmount
				+ ", stockLevel=" + stockLevel + ", referencePrice=" + referencePrice + ", production=" + production + "]";
	}

	public double getReferencePrice() {
		return referencePrice;
	}
	
	public double getStockChange() {
		return exportAmountBeforeLoss - transportLosses - importAmount;
	}

	public GlobalPrice createPriceAdjustedByFactor(double factor) {
		return new GlobalPrice(timestep, exportPrice * factor, stockLevel, importAmount, exportAmountBeforeLoss,
				transportLosses, referencePrice, stockUseRatio, prevStockChange, production);
	}

	public void setStockToTargetLevel(double targetStockUseRatio) {
		if (!Double.isNaN(production)) {
			stockLevel = production * targetStockUseRatio;
		}
	}

	public double getTradeChangeUp(double maxOfProdOrSupply) {
		if (maxOfProdOrSupply < 1e-6) {
			return 0.1;
		}

		return maxOfProdOrSupply * ModelConfig.MAX_IMPORT_CHANGE;
	}

	public double getTradeChangeDown(double maxOfProdOrSupply) {
		if (maxOfProdOrSupply < 1e-6) {
			return 0.1;
		}

		return maxOfProdOrSupply * ModelConfig.MAX_IMPORT_CHANGE;
	}

}