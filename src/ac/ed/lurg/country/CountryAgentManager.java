package ac.ed.lurg.country;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import ac.ed.lurg.InternationalMarket;
import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.utils.calib.CalibrationItem;
import ac.ed.lurg.utils.calib.CalibrationManager;
import ac.ed.lurg.carbon.CarbonFluxItem;
import ac.ed.lurg.carbon.CarbonFluxRasterSet;
import ac.ed.lurg.country.crafty.CraftyCountryAgent;
import ac.ed.lurg.country.crafty.CraftyProdManager;
import ac.ed.lurg.demand.AbstractDemandManager;
import ac.ed.lurg.forestry.WoodYieldItem;
import ac.ed.lurg.forestry.WoodYieldRasterSet;
import ac.ed.lurg.landuse.*;
import ac.ed.lurg.shock.ExportRestrictionManager;
import ac.ed.lurg.shock.MinMaxNetImportManager;
import ac.ed.lurg.shock.MinMaxNetImportManager.LimitType;
import ac.ed.lurg.solar.SolarPotentialItem;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.yield.YieldRaster;
import ac.sac.raster.IntegerRasterItem;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class CountryAgentManager {
	private AbstractDemandManager demandManager;
	private TradeManager tradeBarrierManager;
	private SubsidyRateManager subsidyRateManager;
	private InternationalMarket internationalMarket;
	private ExportRestrictionManager exportRestrictionManager;
	private MinMaxNetImportManager minMaxNetImportManager;
	private CountryBoundaryRaster countryBoundaryRaster;
	private RasterSet<IntegerRasterItem> clusterIdRaster;
	private RasterSet<LandUseItem> globalLandUseRaster;
	private CalibrationManager calibrationManager;
	
	private Collection<AbstractCountryAgent> countryAgents = new ArrayList<AbstractCountryAgent>();
	private Collection<CountryAgent> gamsCountryAgents = new ArrayList<CountryAgent>();
	private Collection<CraftyCountryAgent> craftyCountryAgents = new ArrayList<CraftyCountryAgent>();
	private CraftyProdManager craftyManager;

	public CountryAgentManager(AbstractDemandManager demandManager, CountryBoundaryRaster countryBoundaryRaster,
							   InternationalMarket internationalMarket, RasterSet<IntegerRasterItem> clusterIdRaster,
							   RasterSet<LandUseItem> globalLandUseRaster, CalibrationManager calibrationManager) {
		this.demandManager = demandManager;
		this.countryBoundaryRaster = countryBoundaryRaster;
		this.clusterIdRaster = clusterIdRaster;
		this.internationalMarket = internationalMarket;
		this.globalLandUseRaster = globalLandUseRaster;
		this.calibrationManager = calibrationManager;
		tradeBarrierManager = new TradeManager();
		subsidyRateManager = new SubsidyRateManager();
		exportRestrictionManager = new ExportRestrictionManager();
		minMaxNetImportManager = new MinMaxNetImportManager();
		craftyManager = new CraftyProdManager();
	}
	
	public void addForCountry(CompositeCountry cc, Map<CompositeCountry, Map<CropType, CropUsageData>> cropUsageDataMap, 
			RasterSet<LandUseItem> initLU, Map<CompositeCountry, WoodUsageData> woodUsageDataMap,
			Map<CompositeCountry, CarbonUsageData> carbonUsageDataMap) {
		
		if (ModelConfig.DEBUG_LIMIT_COUNTRIES) { // DEBUG code
			if (!(cc.getName().equals(ModelConfig.DEBUG_COUNTRY_NAME)))
				return;
		}

		if (CountryManager.getInstance().isCountryExcluded(cc)) {
			return;
		}

		Map<CropType, Double> tradeBarriers = tradeBarrierManager.getTradeBarriers(cc);
		Map<CropType, CropUsageData> countryCommodityData = cropUsageDataMap.get(cc);
		WoodUsageData countryWoodData = woodUsageDataMap.get(cc);
		
		if (ModelConfig.USE_CRAFTY_COUNTRIES && craftyManager.getCraftyCountries().contains(cc)) {
			LogWriter.println("Creating CRAFTY agent for: " + cc, 2);
			CraftyCountryAgent cca = new CraftyCountryAgent(demandManager, cc, tradeBarriers, countryCommodityData, countryWoodData);
			craftyCountryAgents.add(cca);
			countryAgents.add(cca);
		}
		else { //GAMS
			LogWriter.println("Creating GAMS agent for: " + cc);
			List<RasterKey> keys = countryBoundaryRaster.getKeysFor(cc);
			CarbonUsageData countryCarbonData = carbonUsageDataMap.get(cc);
	
			if (countryCommodityData == null) {
				LogWriter.printlnError("No commodities data for " + cc + ", so skipping");
			} else {
				RasterSet<LandUseItem> initCountryLandUse = initLU.createSubsetForKeys(keys);
				RasterSet<IntegerRasterItem> yieldClusters = ModelConfig.GENERATE_NEW_YIELD_CLUSTERS ? null : clusterIdRaster.createSubsetForKeys(keys);
	
				if (initCountryLandUse.size() == 0) {
					LogWriter.printlnError("No initial land use for " + cc + ", so skipping");
				}
				else {
					Map<CropType, Double> subsidyRates = subsidyRateManager.getSubsidyRates(cc);
					Map<Integer, Map<CropType, Double>> exportRestrictions = exportRestrictionManager.getShocksForCountry(cc);
					Map<Integer, Map<LimitType, Map<CropType, Double>>> minMaxTradeLimits = minMaxNetImportManager.getMinMaxNetImportForCountry(cc);
					CalibrationItem calibrationItem = calibrationManager.getCalibrationItem(cc);

					CountryAgent ca = new CountryAgent(demandManager, cc, initCountryLandUse, countryCommodityData, tradeBarriers, yieldClusters, 
							subsidyRates, countryWoodData, countryCarbonData, exportRestrictions, minMaxTradeLimits, calibrationItem);
					gamsCountryAgents.add(ca);
					countryAgents.add(ca);
				}
			}
		}
	}
	
	public Collection<AbstractCountryAgent> getAll() {
		return countryAgents;
	}

	public void determineProductionForAll(Timestep timestep, YieldRaster yieldSurfaces, IrrigationRasterSet currentIrrigationData,
										  CarbonFluxRasterSet currentCarbonFluxData, WoodYieldRasterSet currentWoodYieldData,
										  RasterSet<SolarPotentialItem> solarPotentialData) {

		for (AbstractCountryAgent aca : countryAgents) {		
			aca.setCurrentTimestep(timestep);
		}

		// Create thread pool
		ExecutorService execService = Executors.newFixedThreadPool(ModelConfig.MULTITHREAD_NUM);

		for (CountryAgent ca : gamsCountryAgents) {		
			LogWriter.println("Country " + ca.getCountry(), 3);
			Collection<RasterKey> countryKeys = countryBoundaryRaster.getKeysFor(ca.getCountry());
			YieldRaster countryYieldSurfaces = yieldSurfaces.createSubsetForKeys(countryKeys);
			RasterSet<IrrigationItem> irrigData = currentIrrigationData.createSubsetForKeys(countryKeys);
			RasterSet<CarbonFluxItem> carbonFluxData = currentCarbonFluxData.createSubsetForKeys(countryKeys);
			RasterSet<WoodYieldItem> woodYieldData = currentWoodYieldData.createSubsetForKeys(countryKeys);
			RasterSet<Fpu> fpuRasterSet = currentIrrigationData.getFpuRasterSet().createSubsetForKeys(countryKeys);
			
			// Need to instantiate Runnable object for multithreading
			execService.execute(new Runnable() {
				@Override
				public void run() {

					try {
						ca.determineProduction(countryYieldSurfaces, irrigData, internationalMarket.getWorldPrices(), carbonFluxData, woodYieldData, 
								solarPotentialData, internationalMarket.getCarbonPrice(), internationalMarket.getWoodPrice(), fpuRasterSet);

						// update global rasters
						globalLandUseRaster.putAll(ca.getLandUses());

						// if first timestep and calibration get the clustering info, which doesn't change through time
						if (ModelConfig.GENERATE_NEW_YIELD_CLUSTERS && timestep.isInitialTimestep()) {
							clusterIdRaster.putAll(ca.getYieldClusters());
						}

						// Update calibration parameters
						if (ModelConfig.IS_CALIBRATION_RUN && timestep.getTimestep() < ModelConfig.END_FIRST_STAGE_CALIBRATION) {
							calibrationManager.calibrateCountryParameters(ca.getCountry(), ca.getLandUses(),
									ca.getCropUsageData(), ca.getWoodUsageData(), timestep);
						}

					} catch (Exception e) {
						LogWriter.printlnError("Exception processing " + ca.getCountry() + " will continue with other countries");
						LogWriter.print(e);
					}

				}
			});
		}

		try {
			execService.shutdown();
			boolean termStatus = execService.awaitTermination(ModelConfig.THREAD_TIMEOUT, TimeUnit.MINUTES);
			if (!termStatus) {
				LogWriter.printlnWarning("CountryAgentManager.determineProductionForAll: thread pool timed out");
			}
		} catch (InterruptedException e) {
			LogWriter.printlnError(e.toString());
		}

		if (craftyCountryAgents.size() > 0) {
			craftyManager.updateWithCraftyData(craftyCountryAgents, timestep, internationalMarket.getWorldPrices(), 
					internationalMarket.getCarbonPrice(), internationalMarket.getWoodPrice());  // this will wait for the marker file from CRAFTY
		}
	}
	
	public void recalculateDemandAndNetImportsForAll() {
		for (CountryAgent ca : gamsCountryAgents)
			ca.recalculateDemandAndNetImports(internationalMarket.getWorldPrices(), internationalMarket.getCarbonPrice(),
					internationalMarket.getWoodPrice());
	}	

	public void forceLandCoverChangesForAll(RasterSet<ForcedLccItem> lccRaster) {
		for (CountryAgent ca : gamsCountryAgents) {
			Collection<RasterKey> countryKeys = countryBoundaryRaster.getKeysFor(ca.getCountry());
			RasterSet<ForcedLccItem> lccRasterForCountry = new RasterSet<ForcedLccItem>(lccRaster.getHeaderDetails());
			for (RasterKey key : countryKeys) {
				if (lccRaster.containsKey(key)) {
					lccRasterForCountry.put(key, lccRaster.get(key));
				}
			}
			if (!lccRasterForCountry.isEmpty()) {
				ca.forceLandCoverChanges(lccRasterForCountry);
			}

		}
	}
	
	public void serializeCropUsageForAll() {
		Map<CompositeCountry, Map<CropType, CropUsageData>> cropUsageDataMap = new HashMap<CompositeCountry, Map<CropType, CropUsageData>>();
		for (CountryAgent ca : gamsCountryAgents) {
			cropUsageDataMap.put(ca.country, ca.getCropUsageData());
		}
		
		try {
			String fileStr = ModelConfig.IS_CALIBRATION_RUN ? ModelConfig.SERIALIZED_CROP_USAGE_FILE : ModelConfig.CHECKPOINT_CROP_USAGE_FILE;
			LogWriter.println("Starting serializing CropUsages to " + fileStr, 2);
			FileOutputStream fileOut = new FileOutputStream(fileStr);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(cropUsageDataMap);
			out.close();
			fileOut.close();
			LogWriter.println("Serialized crop usage data is saved", 2);
		} catch (IOException i) {
			i.printStackTrace();
		}	
	}
	
	public void serializeWoodUsageForAll() {
		Map<CompositeCountry, WoodUsageData> woodUsageMap = new HashMap<>();
		for (CountryAgent ca : gamsCountryAgents) {
			woodUsageMap.put(ca.country, ca.getWoodUsageData());
		}
		
		try {
			LogWriter.println("Starting serializing WoodUsage to " + ModelConfig.SERIALIZED_WOOD_USAGE_FILE, 2);
			FileOutputStream fileOut = new FileOutputStream(ModelConfig.SERIALIZED_WOOD_USAGE_FILE);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(woodUsageMap);
			out.close();
			fileOut.close();
			LogWriter.println("Serialized WoodUsage is saved", 2);
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
	
	public void serializeCarbonUsageForAll() {
		Map<CompositeCountry, CarbonUsageData> carbonUsageMap = new HashMap<CompositeCountry, CarbonUsageData>();
		for (CountryAgent ca : gamsCountryAgents) {
			carbonUsageMap.put(ca.country, ca.getCarbonUsageData());
		}
		
		try {
			LogWriter.println("Starting serializing CarbonUsage to " + ModelConfig.SERIALIZED_CARBON_USAGE_FILE, 2);
			FileOutputStream fileOut = new FileOutputStream(ModelConfig.SERIALIZED_CARBON_USAGE_FILE);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(carbonUsageMap);
			out.close();
			fileOut.close();
			LogWriter.println("Serialized carbonUsage is saved", 2);
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	public void serializeCalibrationManager() {
		String file = ModelConfig.CALIB_DIR + File.separator + "calibrationManager.ser";
		try {
			LogWriter.println("Starting serializing LandUse to " + file);
			FileOutputStream fileOut = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(calibrationManager);
			out.close();
			fileOut.close();
			LogWriter.println("Serialized data is saved");
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
	
}