package ac.ed.lurg.country;

import java.io.Serializable;

public class SingleCountry implements Serializable {

	private static final long serialVersionUID = 8038110793407130013L;
	private String iso3;
	private String countryName;
	private String region;
	private String incomeGroup;
	
	public SingleCountry(String countryName, String iso3CharCode, String region, String incomeGroup) {
		super();
		this.countryName = countryName;
		this.iso3 = iso3CharCode;
		this.region = region;
		this.incomeGroup = incomeGroup;
	}

	public String getCountryCode() {
		return iso3;
	}

	public String getCountryName() {
		return countryName;
	}
	
	public String getRegion() {
		return region;
	}
	
	public String getIncomeGroup(){
		return incomeGroup;
	}

	@Override
	public String toString() {
		return countryName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((countryName == null) ? 0 : countryName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SingleCountry other = (SingleCountry) obj;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		return true;
	}
}