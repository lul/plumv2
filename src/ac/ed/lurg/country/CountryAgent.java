package ac.ed.lurg.country;

import java.util.*;
import java.util.Map.Entry;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.utils.calib.CalibrationItem;
import ac.ed.lurg.carbon.CarbonFluxItem;
import ac.ed.lurg.country.gams.GamsCountryInput;
import ac.ed.lurg.country.gams.GamsRasterInput;
import ac.ed.lurg.country.gams.GamsRasterOptimiser;
import ac.ed.lurg.country.gams.GamsRasterOutput;
import ac.ed.lurg.demand.AbstractDemandManager;
import ac.ed.lurg.forestry.WoodYieldItem;
import ac.ed.lurg.landuse.*;
import ac.ed.lurg.shock.MinMaxNetImportManager;
import ac.ed.lurg.shock.MinMaxNetImportManager.LimitType;
import ac.ed.lurg.solar.SolarPotentialItem;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.LandProtectionType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.cluster.Cluster;
import ac.ed.lurg.utils.cluster.KMeans;
import ac.ed.lurg.yield.YieldClusterPoint;
import ac.ed.lurg.yield.YieldRaster;
import ac.ed.lurg.yield.YieldResponsesItem;
import ac.sac.raster.IntegerRasterItem;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class CountryAgent extends AbstractCountryAgent {

	private GamsRasterOutput previousGamsRasterOutput;
	private RasterSet<IntegerRasterItem> yieldClusters;
	private Map<CropType, Double> subsidyRates;
	private Map<Integer, Map<CropType, Double>> exportRestrictions;
	private Map<Integer, Map<LimitType, Map<CropType, Double>>> minMaxTradeLimits;
	private CalibrationItem calibrationItem;
	
	public CountryAgent(AbstractDemandManager demandManager,CompositeCountry country, RasterSet<LandUseItem> cropAreaRaster,
			Map<CropType, CropUsageData> cropUsageData, Map<CropType, Double> tradeBarriers, RasterSet<IntegerRasterItem> yieldClusters,
			Map<CropType, Double> subsidyRates, WoodUsageData countryWoodData, CarbonUsageData countryCarbonData,
			Map<Integer, Map<CropType, Double>> exportRestrictions, Map<Integer, Map<LimitType, Map<CropType, Double>>> minMaxTradeLimits,
			CalibrationItem calibrationItem) {

		super(demandManager, country, tradeBarriers);
		this.yieldClusters = yieldClusters;
		this.subsidyRates = subsidyRates;
		this.exportRestrictions = exportRestrictions;
		this.minMaxTradeLimits = minMaxTradeLimits;
		this.calibrationItem = calibrationItem;

		GamsRasterOutput initialData = new GamsRasterOutput(cropAreaRaster, cropUsageData, countryWoodData, countryCarbonData);
		previousGamsRasterOutput = initialData;
	}

	public RasterSet<IntegerRasterItem> getYieldClusters() {
		return yieldClusters;
	}

	private RasterSet<IntegerRasterItem> calcYieldClusters(RasterSet<IrrigationItem> irrigData, YieldRaster countryYieldSurfaces,
														   RasterSet<WoodYieldItem> woodYieldData, RasterSet<SolarPotentialItem> solarData,
														   RasterSet<Fpu> fpuRasterSet) {

		LogWriter.println("calcYieldClusters: for " + ModelConfig.NUM_YIELD_CLUSTERS + " clusters", 3);

		Map<Fpu, Set<RasterKey>> fpuMap = new HashMap<>();
		for (Map.Entry<RasterKey, Fpu> entry : fpuRasterSet.entrySet()) {
			Set<RasterKey> keySet = fpuMap.computeIfAbsent(entry.getValue(), k -> new HashSet<>());
			keySet.add(entry.getKey());
		}

		RasterSet<IntegerRasterItem> mapping = new RasterSet<IntegerRasterItem>(countryYieldSurfaces.getHeaderDetails());

		int clusterId = 1;
		// Calculate clusters within each FPU
		for (Fpu fpu : fpuMap.keySet()) {
			Set<RasterKey> keySet = fpuMap.get(fpu);
			HashSet<YieldClusterPoint> clusteringPoints = new HashSet<YieldClusterPoint>();

			// create clustering points
			for (RasterKey key : keySet) {
				if (countryYieldSurfaces.get(key).isMissing()) {
					continue; // missing yields, skipping. Later we put all cells with missing yield into one cluster
				}

				YieldResponsesItem yieldresp = countryYieldSurfaces.get(key);
				IrrigationItem irrigItem = irrigData.get(key);
				LandUseItem luItem = previousGamsRasterOutput.getLandUses().get(key);
				double totalLand = luItem.getTotalLandArea()-luItem.getLandCoverArea(LandCoverType.URBAN);
				double protectedAreaFrac = (totalLand <= 0) ? 0.0 : luItem.getLandCoverArea(LandProtectionType.PROTECTED) / totalLand;
				WoodYieldItem wyItem = woodYieldData.get(key);
				SolarPotentialItem solarItem = solarData.get(key);

				clusteringPoints.add(new YieldClusterPoint(key, yieldresp, irrigItem, protectedAreaFrac, wyItem, solarItem));
			}

			// do the clustering
			KMeans<String, YieldClusterPoint> kmeans = new KMeans<String, YieldClusterPoint>(clusteringPoints, ModelConfig.NUM_YIELD_CLUSTERS);
			kmeans.calculateClusters(100, 0.1);

			// reformat output
			List<Cluster<String, YieldClusterPoint>> yieldClusters = kmeans.getClusters();

			for (Cluster<String, YieldClusterPoint> c : yieldClusters) {
				if (!c.getPoints().isEmpty()) {
					for (YieldClusterPoint p : c.getPoints()) {
						mapping.put(p.getRasterKey(), new IntegerRasterItem(clusterId));
					}
					clusterId++;
				}
			}
		}

		// Add cells with missing yields to one cluster
		for (RasterKey key : countryYieldSurfaces.keySet()) {
			if (countryYieldSurfaces.get(key).isMissing()) {
				IntegerRasterItem item = new IntegerRasterItem(clusterId);
				mapping.put(key, item);
			}
		}

		LogWriter.println(String.format("%s calculated %d clusters", country, clusterId));

		return mapping;
	}
	
	public GamsRasterOutput determineProduction(YieldRaster countryYieldSurfaces, RasterSet<IrrigationItem> irrigData, 
			Map<CropType, GlobalPrice> worldPrices, RasterSet<CarbonFluxItem> carbonFluxData,
			RasterSet<WoodYieldItem> woodYieldData, RasterSet<SolarPotentialItem> solarPotentialData,
			GlobalPrice carbonPrice, GlobalPrice woodPrice, RasterSet<Fpu> fpuRasterSet) {

		// project demand
		calculateCountryPricesAndDemand(worldPrices, carbonPrice, woodPrice, true);

		applyIrrigationEfficiency(irrigData);
		
		if (ModelConfig.APPLY_EXPORT_TAXES && !ModelConfig.IS_CALIBRATION_RUN) {
			calculateExportTax();
			savePreviousProducerCropPrices();
		}
		
		if (currentFoodDemand.size() == 0) {
			LogWriter.printlnError("No demand for country " + country + " so skipping it");
		}
		else if (countryYieldSurfaces.size() == 0 ) {
			LogWriter.printlnError("No yield values for " + country + " so skipping it");
		}
		else if (ModelConfig.DEBUG_JUST_DEMAND_OUTPUT) { // if this debug flag is set we don't do the optimisation
		}
		else {			
			if (yieldClusters==null) {
				yieldClusters = calcYieldClusters(irrigData, countryYieldSurfaces, woodYieldData, solarPotentialData, fpuRasterSet);  // this should only be on the first timestep
			}
			// optimize areas and intensity 
			GamsRasterInput input = getGamsRasterInput(irrigData, countryYieldSurfaces, woodYieldData, carbonFluxData, solarPotentialData);
			GamsRasterOptimiser opti = new GamsRasterOptimiser(input, yieldClusters);
			LogWriter.println("Running " + country.getName() + ", currentTimestep " + currentTimestep, 3);

			GamsRasterOutput result = opti.run();

			previousGamsRasterOutput = result;

			return result;
		}

		throw new RuntimeException("Skipping optimisation of country " + country);
	}
	
	public void recalculateDemandAndNetImports(Map<CropType, GlobalPrice> worldPrices, GlobalPrice carbonPrice, GlobalPrice woodPrice) {
		shockGDP();
		calculateCountryPricesAndDemand(worldPrices, carbonPrice, woodPrice, true);
		updateNetImportsFromProdAndDemand(getTotalDemand(), previousGamsRasterOutput.getCropUsageData());
	}
	
	private GamsRasterInput getGamsRasterInput(RasterSet<IrrigationItem> irrigData, YieldRaster countryYieldSurfaces,
											   RasterSet<WoodYieldItem> woodYieldData, RasterSet<CarbonFluxItem> carbonFluxData,
											   RasterSet<SolarPotentialItem> solarPotentialData) {
		
		Map<CropType, TradeConstraint> importConstraints = new HashMap<CropType, TradeConstraint>();
		Map<CropType, Double> currentExportRestrictions = (exportRestrictions == null) ? null : exportRestrictions.get(currentTimestep.getYear());

		Map<CropType, Double> totalDemand = getTotalDemand();

		for (Map.Entry<CropType, CropUsageData> entry : previousGamsRasterOutput.getCropUsageData().entrySet()) {
			CropUsageData cropUsage = entry.getValue();
			CropType crop = entry.getKey();
			double baseTrade = cropUsage.getNetImportsExpected();

			double maxOfProdOrSupply = cropUsage.getProductionExpected() + Math.max(baseTrade, 0);
			double currentDemand = totalDemand.get(crop);
			double prevDemand = cropUsage.getNetSupply() - cropUsage.getMonogastricFeed() - cropUsage.getRuminantFeed();

			double change = Math.max(ModelConfig.MAX_IMPORT_CHANGE * maxOfProdOrSupply, 1); // max of 1 otherwise if only importing then can never export
			double minNetImport = currentDemand < baseTrade ? currentDemand * 0.95 : baseTrade - change; // min imports can't be lower than demand
			double maxNetImport = Math.max(baseTrade + change, baseTrade + currentDemand - prevDemand); // increase in demand can be imported

			double restiction = 0;
			if (currentExportRestrictions != null) {
				Double r = currentExportRestrictions.get(crop);
				if (r != null) 
					restiction = r;
			}
			
			Double minLimit=null, maxLimit=null;
			if (minMaxTradeLimits != null) {
				Map<LimitType, Map<CropType, Double>> minMaxLimits = minMaxTradeLimits.get(currentTimestep.getYear());
				minLimit = minMaxLimits.get(MinMaxNetImportManager.LimitType.MIN_LIMIT_TYPE).get(crop);
				maxLimit = minMaxLimits.get(MinMaxNetImportManager.LimitType.MAX_LIMIT_TYPE).get(crop);
			}

			LogWriter.print(crop.toString(), 3);
			TradeConstraint tradeConstraint = new TradeConstraint(minNetImport, maxNetImport, restiction, minLimit, maxLimit);

			importConstraints.put(crop, tradeConstraint);
		}

		// Carbon trade constraints
		TradeConstraint carbonTradeConstraint;
		CarbonUsageData carbonUsageData = previousGamsRasterOutput.getCarbonUsageData();
		
		if (ModelConfig.IS_CARBON_ON) {
			double baseTrade = carbonUsageData.getNetCarbonImport();
			double maxOfProdOrSupply = carbonUsageData.getCarbonCredits() + Math.max(0, carbonUsageData.getNetCarbonImport());
			double currentDemand = getCurrentCarbonDemand();
			double prevDemand = carbonUsageData.getCarbonCredits() + carbonUsageData.getNetCarbonImport();

			double maxNetImport, minNetImport;
			double change = Math.max(ModelConfig.MAX_IMPORT_CHANGE * maxOfProdOrSupply, 1); // max of 1 otherwise if only importing then can never export

			if (ModelConfig.CARBON_DEMAND_METHOD.equals("country")) {
				minNetImport = currentDemand < baseTrade ? currentDemand * 0.95 : baseTrade - change; // min imports can't be lower than demand
				maxNetImport = Math.max(baseTrade + change, baseTrade + currentDemand - prevDemand); // increase in demand can be imported
			} else {
				minNetImport = baseTrade - change;
				maxNetImport = baseTrade + change;
			}

			carbonTradeConstraint = new TradeConstraint(minNetImport, maxNetImport);

		} else {
			carbonTradeConstraint = new TradeConstraint(0, 0);
		}

		// Timber import/export constraints
		TradeConstraint woodTradeConstraint;
		if (ModelConfig.IS_FORESTRY_ON) {
			WoodUsageData woodUsageData = previousGamsRasterOutput.getWoodUsageData();
			double baseTrade = woodUsageData.getNetImport();
			double maxOfProdOrSupply = woodUsageData.getProduction() + Math.max(baseTrade, 0);
			double currentDemand = getTotalWoodDemand();
			double prevDemand = woodUsageData.getNetImport() + woodUsageData.getProduction();

			double change = Math.max(ModelConfig.MAX_IMPORT_CHANGE * maxOfProdOrSupply, 1); // max of 1 otherwise if only importing then can never export
			double minNetImport = currentDemand < baseTrade ? currentDemand * 0.95 : baseTrade - change; // min imports can't be lower than demand
			double maxNetImport = Math.max(baseTrade + change, baseTrade + currentDemand - prevDemand); // increase in demand can be imported

			woodTradeConstraint = new TradeConstraint(minNetImport, maxNetImport);
		} else {
			woodTradeConstraint = new TradeConstraint(0, 0); // No exports or imports if forestry off
		}

		// Calibrate parameters

		GamsCountryInput countryLevelInputs = new GamsCountryInput(country, getTotalDemand(), currentCountryPrices, importConstraints,
				previousGamsRasterOutput.getCropUsageData(), subsidyRates, currentCarbonDemand, currentSolarDemand,
				currentCarbonPrice, carbonTradeConstraint, previousGamsRasterOutput.getCarbonUsageData(),
				getTotalWoodDemand(), currentWoodPrice, woodTradeConstraint, previousGamsRasterOutput.getWoodUsageData());

		GamsRasterInput input = new GamsRasterInput(currentTimestep, countryYieldSurfaces, previousGamsRasterOutput.getLandUses(), irrigData, 
				woodYieldData, carbonFluxData, solarPotentialData, calibrationItem, countryLevelInputs);

		return input;
	}

	@Override
	protected CountryPrice createCountryPrices(CropType crop, GlobalPrice worldPrice, CountryPrice refCountryPrice) {
		Map<CropType, CropUsageData> cropUsageMap = previousGamsRasterOutput.getCropUsageData();
		CropUsageData cropUsageData = cropUsageMap.get(crop);
		double prod = cropUsageData.getProductionExpected();
		double netImports = cropUsageData.getShockedNetImports();
		double weighting = prod > 0 ? Math.max(0, netImports / (prod + netImports)) : 1.0;
		double prodCost = cropUsageData.getProdCostRate();
		double tradeBarrier = tradeBarriers.getOrDefault(crop, 0.0);
		double exportTaxRateForCrop = CommodityType.CEREALS_STARCHY_ROOTS.getCropTypes().contains(crop) ? exportTaxRate : 0.0;
		CountryPrice cp = new CountryPrice(crop, worldPrice, tradeBarrier, prodCost, weighting, exportTaxRateForCrop, refCountryPrice);
		return cp;
	}

	protected CountryPrice createWoodCountryPrice(GlobalPrice worldPrice) {
		WoodUsageData woodUsageData = previousGamsRasterOutput.getWoodUsageData();
		double prod = woodUsageData.getProduction();
		double netImports = woodUsageData.getNetImport();
		double weighting = prod > 0 ? Math.max(0, netImports / (prod + netImports)) : 1.0;
		double prodCost = woodUsageData.getProdCostRate();
		CountryPrice refPrice = currentTimestep.isInitialTimestep() ?
				new CountryPrice(worldPrice) : currentWoodPrice;
		CountryPrice cp = new CountryPrice(null, worldPrice, ModelConfig.WOOD_TRADE_BARRIER, prodCost, weighting, 0,
				refPrice);
		return cp;
	}

	public void shockGDP() {

		double totalProduction = getTotalProduction();
		double shockMagnitude = 0.0;

		for (Map.Entry<CropType, CropUsageData> entry : previousGamsRasterOutput.getCropUsageData().entrySet()) {
			CropUsageData cropUsage = entry.getValue();
			CropType crop = entry.getKey();
			if(crop.isImportedCrop() && cropUsage.getProductionExpected() != 0) 
				shockMagnitude += (cropUsage.getProductionShock() /cropUsage.getProductionExpected()) * (cropUsage.getProductionExpected()/totalProduction);	
		}
		int year = (!ModelConfig.CHANGE_DEMAND_YEAR)? ModelConfig.BASE_YEAR: currentTimestep.getYear();
		demandManager.updateGdpLossesFromShock(country, year, shockMagnitude);
	}

	public RasterSet<LandUseItem> getLandUses() {
		return previousGamsRasterOutput.getLandUses();
	}

	public Map<CropType, CropUsageData> getCropUsageData() {
		return previousGamsRasterOutput.getCropUsageData();
	}
	
	public double getTotalProduction() {
		double totalProduction = 0;
		
		for (Map.Entry<CropType, CropUsageData> entry : previousGamsRasterOutput.getCropUsageData().entrySet()) {
			CropUsageData cropUsage = entry.getValue();
			CropType crop = entry.getKey();
			if(crop.isImportedCrop()) //assuming pasture and set aside not part of agricultural value, valid or not?
				totalProduction += cropUsage.getProductionExpected();
		}
		return totalProduction;
	}

	public void forceLandCoverChanges(RasterSet<ForcedLccItem> lccRaster) {
		for (Map.Entry<RasterKey, ForcedLccItem> entry : lccRaster.entrySet()) {
			RasterKey key = entry.getKey();
			ForcedLccItem lccItem = entry.getValue();
			if (previousGamsRasterOutput.getLandUses().containsKey(key)) {
				LandUseItem landUseItem = previousGamsRasterOutput.getLandUses().get(key);
				Map<LandCoverType, Map<LandCoverType, Double>> lccMap = lccItem.getFractions();
				for (LandCoverType fromLc : lccMap.keySet()) {
					Map<LandCoverType, Double> toMap = lccMap.get(fromLc);
					for (LandCoverType toLc : toMap.keySet()) {
						LandKey fromLandKey = new LandKey(fromLc, LandProtectionType.UNPROTECTED);
						LandKey toLandKey = new LandKey(toLc, LandProtectionType.UNPROTECTED);
						double fraction = toMap.get(toLc);
						double previousArea = landUseItem.getLandCoverArea(new LandKey(fromLc, LandProtectionType.UNPROTECTED));
						double areaToMove = previousArea * fraction;
						landUseItem.moveAreas(fromLandKey, toLandKey, areaToMove);
					}
				}
			}
		}
	}
	
	public CarbonUsageData getCarbonUsageData() {
		return previousGamsRasterOutput.getCarbonUsageData();
	}
	
	public WoodUsageData getWoodUsageData() {
		return previousGamsRasterOutput.getWoodUsageData();
	}

	private double getTotalWoodDemand() {
		double d = 0;
		for (WoodType woodType : currentWoodDemand.keySet()) {
			d += currentWoodDemand.get(woodType);
		}
		return  d;
	}

	private void applyIrrigationEfficiency(RasterSet<IrrigationItem> irrigData) {
		double irrigEfficiencyFactor = ModelConfig.getAdjParam("IRRIGATION_EFFICIENCY_FACTOR");
		for (IrrigationItem item : irrigData.values()) {
			for(CropType crop : CropType.getNonMeatTypes()) {
				double baseMaxIrrig = item.getMaxIrrigAmount(crop);
				double adjMaxIrrig = baseMaxIrrig / calibrationItem.getIrrigationEfficiency() * irrigEfficiencyFactor;
				item.setMaxIrrigAmount(crop, adjMaxIrrig);
			}

		}
	}
}