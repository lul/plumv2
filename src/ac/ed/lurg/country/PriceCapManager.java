package ac.ed.lurg.country;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;

public class PriceCapManager {
	
	private static final int CATEGORY_COL = 0;
	private static final int ITEM_COL = 1;
	private static final int MIN_PRICE_COL = 2;
	private static final int MAX_PRICE_COL = 3;
	
	private Map<CropType, Map<PriceCapType, Double>> cropPriceCaps = new HashMap<CropType, Map<PriceCapType, Double>>();
	private Map<PriceCapType, Double> woodPriceCaps = new HashMap<PriceCapType, Double>();
	private Map<PriceCapType, Double> carbonPriceCaps = new HashMap<PriceCapType, Double>();
	
	public PriceCapManager() {
		readPriceCaps();
	}
	
	public enum PriceCapType {
		MIN_PRICE(),
		MAX_PRICE();
		PriceCapType() {}		
	}
	
	private void readPriceCaps() {
		String filename = ModelConfig.PRICE_CAP_FILE;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line;
			String category, item;
			double minPrice, maxPrice;
			reader.readLine(); // header
			
			while ((line=reader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 4)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				category = tokens[CATEGORY_COL];
				item = tokens[ITEM_COL];
				minPrice = Double.valueOf(tokens[MIN_PRICE_COL]);
				maxPrice = Double.valueOf(tokens[MAX_PRICE_COL]);
				
				switch(category) {
				case "crop":
					CropType cropType = CropType.getForGamsName(item);
					Map<PriceCapType, Double> priceCapMap = new HashMap<PriceCapType, Double>();
					priceCapMap.put(PriceCapType.MIN_PRICE, minPrice);
					priceCapMap.put(PriceCapType.MAX_PRICE, maxPrice);
					cropPriceCaps.put(cropType, priceCapMap);
					break;
				case "wood":
					woodPriceCaps.put(PriceCapType.MIN_PRICE, minPrice);
					woodPriceCaps.put(PriceCapType.MAX_PRICE, maxPrice);
					break;
				case "carbon":
					carbonPriceCaps.put(PriceCapType.MIN_PRICE, minPrice);
					carbonPriceCaps.put(PriceCapType.MAX_PRICE, maxPrice);
					break;
				}
				
			} 
			reader.close();
			
		} catch (Exception e) {
			LogWriter.printlnError("Failed in reading price caps data");
			LogWriter.print(e);
		}
	}
	
	private double calcCappedPriceFactor(double price, double minPrice, double maxPrice) {
		double cappedPrice =  Math.max(Math.min(price, maxPrice), minPrice);
		double factor = cappedPrice / price;
		return factor;
	}
	
	public Map<CropType, GlobalPrice> capCropPrices(Map<CropType, GlobalPrice> cropPrices) {
		Map<CropType, GlobalPrice> updatedPrices = new HashMap<CropType, GlobalPrice>();
		for (CropType cropType : cropPrices.keySet()) {
			GlobalPrice prevPrice = cropPrices.get(cropType);
			double factor = calcCappedPriceFactor(prevPrice.getExportPrice(), cropPriceCaps.get(cropType).get(PriceCapType.MIN_PRICE), 
					cropPriceCaps.get(cropType).get(PriceCapType.MAX_PRICE));
			GlobalPrice updatedPrice = prevPrice.createPriceAdjustedByFactor(factor);
			updatedPrices.put(cropType, updatedPrice);
		}
		return updatedPrices;
	}
	
	public GlobalPrice capWoodPrices(GlobalPrice woodPrice) {
		double factor = calcCappedPriceFactor(woodPrice.getExportPrice(), woodPriceCaps.get(PriceCapType.MIN_PRICE), woodPriceCaps.get(PriceCapType.MAX_PRICE));
		return woodPrice.createPriceAdjustedByFactor(factor);
	}
	
	public GlobalPrice capCarbonPrices(GlobalPrice price) {
		double factor = calcCappedPriceFactor(price.getExportPrice(), carbonPriceCaps.get(PriceCapType.MIN_PRICE), carbonPriceCaps.get(PriceCapType.MAX_PRICE));
		return price.createPriceAdjustedByFactor(factor);
	}
	
}
