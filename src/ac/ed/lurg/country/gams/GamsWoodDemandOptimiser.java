package ac.ed.lurg.country.gams;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.WoodCommodityType;
import ac.ed.lurg.utils.LogWriter;
import com.gams.api.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class GamsWoodDemandOptimiser {

    private GamsWoodDemandInput inputData;
    private boolean saveGamsGdxFiles;

    public GamsWoodDemandOptimiser (GamsWoodDemandInput inputData) {
        this.inputData = inputData;
        saveGamsGdxFiles = (inputData.getCountry().getCountryName().equals(ModelConfig.GAMS_COUNTRY_TO_SAVE));
    }

    public GamsWoodDemandOutput getDemandPc() {
        File tempDir = new File(ModelConfig.TEMP_DIR);
        tempDir.mkdir();

        Path workingDirectory = null;
        try {
            workingDirectory = Files.createTempDirectory(Paths.get(ModelConfig.TEMP_DIR), "wood");
        } catch (IOException e) {
            LogWriter.printlnError(e.toString());
        }

        GAMSWorkspaceInfo wsInfo  = new GAMSWorkspaceInfo();
        wsInfo.setWorkingDirectory(workingDirectory.toAbsolutePath().toString());

        GAMSWorkspace ws = new GAMSWorkspace(wsInfo);
        GAMSOptions opt = ws.addOptions();
        GAMSJob gamsJob = ws.addJobFromFile(ModelConfig.WOOD_DEMAND_GAMS_MODEL);

        GAMSDatabase inDB = ws.addDatabase();
        setupPriceGdpDB(inDB);
        ArrayList<GAMSDatabase> dbs = new ArrayList<GAMSDatabase>();
        dbs.add(inDB);
        opt.defines("gdx_prices_and_gdp", inDB.getName());

        opt.defines("gdx_parameters", new File(ModelConfig.WOOD_DEMAND_PARAM_FILE).getAbsolutePath());

        long startTime = System.currentTimeMillis();
        gamsJob.run(opt, dbs.toArray(new GAMSDatabase[dbs.size()]));

        GamsWoodDemandOutput gamsOutput = handleResults(gamsJob.OutDB());

        LogWriter.println("Took " + (System.currentTimeMillis() - startTime) + " ms to run", 3);

        if (saveGamsGdxFiles)
            saveGDXFile("wood", workingDirectory.toFile());

        if (ModelConfig.CLEANUP_GAMS_DIR)  {
            gamsJob.OutDB().dispose();
            deleteDirectory(workingDirectory.toFile());
        }

        return gamsOutput;
    }

    private void setupPriceGdpDB(GAMSDatabase inDB) {
        GAMSParameter gdpPcP = inDB.addParameter("gdp_pc", 0);
        double gdpPc = inputData.getGdpPc();
        LogWriter.println(String.format("gdp_pc = %.5f", gdpPc), 3);
        setGamsParamValue(gdpPcP.addRecord(), gdpPc, 5);
        GamsWoodDemandOutput previousGamsDemands = inputData.getPreviousDemands();

        GAMSParameter prevUP = inDB.addParameter("previousU", 0);
        double previousUtility = previousGamsDemands == null ? 0.0 : previousGamsDemands.getUtility();

        setGamsParamValue(prevUP.addRecord(), previousUtility, 5);

        LogWriter.println("\nPrice", 3);
        GAMSParameter priceP = inDB.addParameter("price", 1);
        GAMSParameter prevSubsP = inDB.addParameter("previousSubs", 1);
        GAMSParameter prevDiscP = inDB.addParameter("previousDisc", 1);

        for (Map.Entry<WoodCommodityType, Double> entry : inputData.getPrices().entrySet()) {
            WoodCommodityType comm = entry.getKey();
            double priceComm = entry.getValue();

            doCommodity(priceP, prevSubsP, prevDiscP, previousGamsDemands, comm, priceComm, 1, 1);
        }
    }

    private void doCommodity(GAMSParameter priceP, GAMSParameter prevSubsP, GAMSParameter prevDiscP,
                             GamsWoodDemandOutput previousGamsDemands, WoodCommodityType comm, double price, double defaultSubs, double defaultDisc) {

        Vector<String> v = new Vector<String>();
        v.add(comm.getGamsName());
        double prevSubs = defaultSubs, prevDisc = defaultDisc;

        if (previousGamsDemands != null) {
            GamsWoodCommodityDemand demand = previousGamsDemands.getGamsDemands(comm);
            prevSubs = demand.getSubsistence();
            prevDisc = demand.getDiscretionary();
        }

        LogWriter.println(String.format("%14s:  price=%.4f, previousSubs=%.4f, previousDisc=%.4f", comm.getGamsName(), price, prevSubs, prevDisc), 3);
        setGamsParamValue(priceP.addRecord(v), price, 4);
        setGamsParamValue(prevSubsP.addRecord(v), prevSubs, 4);
        setGamsParamValue(prevDiscP.addRecord(v), prevDisc, 4);
    }

    private double setGamsParamValue(GAMSParameterRecord param, double val, int places) {
        double dOut = Math.round(val * Math.pow(10,places)) / Math.pow(10,places);
        param.setValue(dOut);
        return dOut;
    }

    private GamsWoodDemandOutput handleResults(GAMSDatabase outDB) {
        int modelStatusInt = (int) outDB.getParameter("ms").findRecord().getValue();
        GAMSGlobals.ModelStat modelStatus = GAMSGlobals.ModelStat.lookup(modelStatusInt);

        LogWriter.println(String.format("\nWood Demand %s: Modelstatus (%d) %s, Solvestatus %s", inputData.getCountry(),
                modelStatusInt, modelStatus.toString(), GAMSGlobals.SolveStat.lookup((int) outDB.getParameter("ss").findRecord().getValue())));

        GAMSVariable varU = outDB.getVariable("u");
        double utility = varU.getFirstRecord().getLevel();
        LogWriter.println(String.format("u = %.4f", utility), 3);

        double leastSquareError = Math.abs(outDB.getVariable("error").getFirstRecord().getLevel());

        switch (modelStatus) {
            case SOLVED: break;
            case UNDEFINED_STAT: break;
            case OPTIMAL_LOCAL:
                if (leastSquareError < 1e-5)
                    LogWriter.printlnWarning("No exact solution. Approximate solution found. Residual = " + leastSquareError);
                else
                    LogWriter.printlnError("Critical! No exact solution. Approximate solution found with high error. Residual = " + leastSquareError);
                break;
            default:
                LogWriter.printlnError("Critical! Solution failed.");
        }

        GAMSVariable varSubs = outDB.getVariable("SubsistenceC");
        GAMSVariable varDisc = outDB.getVariable("DiscretionaryC");

        HashSet<GamsWoodCommodityDemand> demandMap = new HashSet<>();

        Map<WoodCommodityType, Double> subsMap = new HashMap<>();
        Map<WoodCommodityType, Double> discMap = new HashMap<>();

        for (GAMSVariableRecord rec : varSubs) {
            String commodityName = rec.getKeys()[0];
            WoodCommodityType commodity = WoodCommodityType.getForGamsName(commodityName, true);
            double d = rec.getLevel();
            subsMap.put(commodity, d);
        }

        for (GAMSVariableRecord rec : varDisc) {
            String commodityName = rec.getKeys()[0];
            WoodCommodityType commodity = WoodCommodityType.getForGamsName(commodityName, true);
            double d = rec.getLevel();
            discMap.put(commodity, d);
        }

        for (WoodCommodityType commodity : WoodCommodityType.values()) {
            double subsGams, discGams;
            if (subsMap.containsKey(commodity)) {
                subsGams = subsMap.get(commodity);
            } else {
                subsGams = 0;
                LogWriter.println(commodity + " not found in Gams output so adding with zero demand for subsistence", 3);
            }

            if (discMap.containsKey(commodity)) {
                discGams = discMap.get(commodity);
            } else {
                discGams = 0;
                LogWriter.println(commodity + " not found in Gams output so adding with zero demand for discretionary", 3);
            }

            GamsWoodCommodityDemand demand = new GamsWoodCommodityDemand(commodity, subsGams, discGams);
            LogWriter.println(demand.toString(), 3);
            demandMap.add(demand);
        }
        return new GamsWoodDemandOutput(modelStatus.toString(), demandMap, utility);
    }

    boolean deleteDirectory(File dir) {
        File[] dirContents = dir.listFiles();
        if (dirContents != null) {
            for (File file : dirContents) {
                deleteDirectory(file);
            }
        }
        return dir.delete();
    }

    private void saveGDXFile(String ext, File tempDir) {
        // some hacky code for debug purposes that keeps each gams gdx file
        String country = inputData.getCountry().getCountryName().replaceAll("\\s+","");
        int year = inputData.getYear();
        try {
            Files.copy(
                    FileSystems.getDefault().getPath(tempDir + File.separator + "_gams_java_gdb1.gdx"),
                    FileSystems.getDefault().getPath(ModelConfig.TEMP_DIR + File.separator + country + year + ext + ".gdx"),
                    StandardCopyOption.REPLACE_EXISTING);
            Files.copy(
                    FileSystems.getDefault().getPath(tempDir + File.separator + "_gams_java_gjo1.lst"),
                    FileSystems.getDefault().getPath(ModelConfig.TEMP_DIR + File.separator + country + year + ext + "Listing" + ".lst"),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            LogWriter.print(e);
        }
    }
}
