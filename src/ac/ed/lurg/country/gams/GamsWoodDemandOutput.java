package ac.ed.lurg.country.gams;

import ac.ed.lurg.types.WoodCommodityType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class GamsWoodDemandOutput {
    private String status;
    private Collection<GamsWoodCommodityDemand> demands;
    private double utility;

    public GamsWoodDemandOutput(String status, Collection<GamsWoodCommodityDemand> demands, double utility) {
        this.status = status;
        this.demands = demands;
        this.utility = utility;
    }

    public Map<WoodCommodityType, Double> getPlumDemands() {
        Map<WoodCommodityType, Double> plumDemands = new HashMap<WoodCommodityType, Double>();

        for (GamsWoodCommodityDemand demand : demands)
            plumDemands.put(demand.getCommodity(), demand.getPlumDemand());

        return plumDemands;
    }

    public GamsWoodCommodityDemand getGamsDemands(WoodCommodityType comm) {
        for (GamsWoodCommodityDemand demand : demands)
            if (demand.getCommodity() == comm)
                return demand;

        return null;
    }

    public double getUtility() {
        return utility;
    }
    public String getStatus() {
        return status;
    }
}
