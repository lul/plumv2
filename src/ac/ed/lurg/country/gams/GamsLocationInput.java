package ac.ed.lurg.country.gams;

import java.util.Map;
import java.util.Set;

import ac.ed.lurg.Timestep;
import ac.ed.lurg.utils.calib.CalibrationItem;
import ac.ed.lurg.carbon.CarbonFluxItem;
import ac.ed.lurg.forestry.WoodYieldData;
import ac.ed.lurg.landuse.IrrigationItem;
import ac.ed.lurg.landuse.LandUseItem;
import ac.ed.lurg.solar.SolarPotentialItem;
import ac.ed.lurg.yield.YieldResponsesItem;

public class GamsLocationInput {
	
	private Timestep timestep;
	private Set<Integer> locations;
	private Map<Integer, ? extends YieldResponsesItem> yields;
	private Map<Integer, ? extends LandUseItem> previousLandUse;
	private Map<Integer, ? extends IrrigationItem> irrigationCosts;
	private Map<Integer, ? extends CarbonFluxItem> carbonFluxes;
	private Map<Integer, ? extends WoodYieldData> woodYields;
	private Map<Integer, ? extends SolarPotentialItem> solarPotentials;
	private CalibrationItem calibrationItem;
	private GamsCountryInput countryInput;
	
	public GamsLocationInput(Timestep timestep, Set<Integer> locations, Map<Integer, ? extends YieldResponsesItem> yields,
							 Map<Integer, ? extends LandUseItem> previousLandUse,
							 Map<Integer, ? extends IrrigationItem> irrigationCosts, Map<Integer, ? extends CarbonFluxItem> carbonFluxes,
							 Map<Integer, ? extends WoodYieldData> woodYields,
							 Map<Integer, ? extends SolarPotentialItem> solarPotentials,
							 CalibrationItem calibrationItem, GamsCountryInput countryInput) {
		super();
		this.timestep = timestep;
		this.locations = locations;
		this.yields = yields;
		this.previousLandUse = previousLandUse;
		this.irrigationCosts = irrigationCosts;
		this.carbonFluxes = carbonFluxes;
		this.woodYields = woodYields;
		this.solarPotentials = solarPotentials;
		this.calibrationItem = calibrationItem;
		this.countryInput = countryInput;
	}
		
	public Map<Integer, ? extends YieldResponsesItem> getYields() {
		return yields;
	}
	
	public Map<Integer, ? extends LandUseItem> getPreviousLandUse() {
		return previousLandUse;
	}
	
	public Map<Integer, ? extends IrrigationItem> getIrrigationCosts() {
		return irrigationCosts;
	}
	
	public Map<Integer, ? extends CarbonFluxItem> getCarbonFluxes() {
		return carbonFluxes;
	}
	
	public Map<Integer, ? extends WoodYieldData> getWoodYields() {
		return woodYields;
	}

	public Map<Integer, ? extends SolarPotentialItem> getSolarPotentials() {
		return solarPotentials;
	}

	public CalibrationItem getCalibrationItem() {
		return calibrationItem;
	}

	public GamsCountryInput getCountryInput() {
		return countryInput;
	}

	public Timestep getTimestep() {
		return timestep;
	}

	public Set<Integer> getLocations() {
		return locations;
	}

}
