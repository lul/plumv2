package ac.ed.lurg.country.gams;

import java.util.Map;

import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.CommodityType;

public class GamsDemandInput {
	private SingleCountry country;
	private int year;
	private double gdpPc;
	private Map<CommodityType, Double> prices;
	private GamsDemandOutput previousGamsDemands;
	
	public GamsDemandInput(SingleCountry country, int year, double gdpPc, Map<CommodityType, Double> prices,
						   GamsDemandOutput previousGamsDemands) {
		super();
		this.country = country;
		this.year = year;
		this.gdpPc = gdpPc;
		this.prices = prices;
		this.previousGamsDemands = previousGamsDemands;
	}
	
	public SingleCountry getCountry() {
		return country;
	}

	public int getYear() {
		return year;
	}

	public double getGdpPc() {
		return gdpPc;
	}

	public Map<CommodityType, Double> getPrices() {
		return prices;
	}

	public GamsDemandOutput getPreviousDemands() {
		return previousGamsDemands;
	}
}