package ac.ed.lurg.country.gams;

import ac.ed.lurg.Timestep;
import ac.ed.lurg.utils.calib.CalibrationItem;
import ac.ed.lurg.carbon.CarbonFluxItem;
import ac.ed.lurg.forestry.WoodYieldItem;
import ac.ed.lurg.landuse.LandUseItem;
import ac.ed.lurg.landuse.IrrigationItem;
import ac.ed.lurg.solar.SolarPotentialItem;
import ac.ed.lurg.yield.YieldRaster;
import ac.sac.raster.RasterSet;

public class GamsRasterInput {
	
	private Timestep timestep;
	private YieldRaster yields;
	private RasterSet<LandUseItem> previousLandsUses;
	private RasterSet<IrrigationItem> irrigationCost;
	private RasterSet<WoodYieldItem> woodYields;
	private RasterSet<CarbonFluxItem> carbonFluxes;
	private RasterSet<SolarPotentialItem> solarPotentialData;
	private CalibrationItem calibrationItem;
	private GamsCountryInput countryInput;

	public GamsRasterInput(Timestep timestep, YieldRaster yields, RasterSet<LandUseItem> previousLandsUses, RasterSet<IrrigationItem> irrigationCost,
						   RasterSet<WoodYieldItem> woodYields, RasterSet<CarbonFluxItem> carbonFluxes, RasterSet<SolarPotentialItem> solarPotentialData,
						   CalibrationItem calibrationItem, GamsCountryInput countryInput) {
		super();
		this.timestep = timestep;
		this.yields = yields;
		this.previousLandsUses = previousLandsUses;
		this.irrigationCost = irrigationCost;
		this.woodYields = woodYields;
		this.carbonFluxes = carbonFluxes;
		this.solarPotentialData = solarPotentialData;
		this.calibrationItem = calibrationItem;
		this.countryInput = countryInput;
	}

	public YieldRaster getYields() {
		return yields;
	}

	public RasterSet<LandUseItem> getPreviousLandUses() {
		return previousLandsUses;
	}
	
	public RasterSet<IrrigationItem> getIrrigationData() {
		return irrigationCost;
	}
	
	public RasterSet<WoodYieldItem> getWoodYields() {
		return woodYields;
	}

	public RasterSet<CarbonFluxItem> getCarbonFluxes() {
		return carbonFluxes;
	}

	public RasterSet<SolarPotentialItem> getSolarPotentialData() {
		return solarPotentialData;
	}

	public CalibrationItem getCalibrationItem() {
		return calibrationItem;
	}

	public GamsCountryInput getCountryInput() {
		return countryInput;
	}
	
	public Timestep getTimestep() {
		return timestep;
	}
}
