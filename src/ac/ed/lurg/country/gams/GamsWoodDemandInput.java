package ac.ed.lurg.country.gams;

import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.WoodCommodityType;

import java.util.Map;

public class GamsWoodDemandInput {
    private SingleCountry country;
    private int year;
    private double gdpPc;
    private Map<WoodCommodityType, Double> prices;
    private GamsWoodDemandOutput previousGamsDemands;

    public GamsWoodDemandInput(SingleCountry country, int year, double gdpPc, Map<WoodCommodityType, Double> prices,
                               GamsWoodDemandOutput previousGamsDemands) {
        super();
        this.country = country;
        this.year = year;
        this.gdpPc = gdpPc;
        this.prices = prices;
        this.previousGamsDemands = previousGamsDemands;
    }

    public SingleCountry getCountry() {
        return country;
    }

    public int getYear() {
        return year;
    }

    public double getGdpPc() {
        return gdpPc;
    }

    public Map<WoodCommodityType, Double> getPrices() {
        return prices;
    }

    public GamsWoodDemandOutput getPreviousDemands() {
        return previousGamsDemands;
    }
}
