package ac.ed.lurg.country.gams;

import java.util.List;
import java.util.Map;

import ac.ed.lurg.landuse.*;
import ac.ed.lurg.types.LandProtectionType;
import com.gams.api.GAMSGlobals.ModelStat;

import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;

public class GamsLocationOutput {
	ModelStat status;
	
	Map<Integer, LandUseItem> landUses;  // data mapped from id (not raster)
	private Map<CropType, CropUsageData> cropUsageData;
	Map<Integer, List<LandCoverChangeItem>> landCoverChanges;
	private CarbonUsageData carbonUsageData;
	private WoodUsageData woodUsageData;
	private Map<LandProtectionType, Map<Integer, Double>> rotationIntensities;
	
	public GamsLocationOutput(ModelStat status,
                              Map<Integer, LandUseItem> landUses,
                              Map<CropType, CropUsageData> cropUsageData,
                              Map<Integer, List<LandCoverChangeItem>> landCoverChanges,
                              CarbonUsageData carbonUsageData, WoodUsageData woodUsageData,
							  Map<LandProtectionType, Map<Integer, Double>> rotationIntensities) {
		super();
		this.status = status;
		this.landUses = landUses;
		this.cropUsageData = cropUsageData;
		this.landCoverChanges = landCoverChanges;
		this.carbonUsageData = carbonUsageData;
		this.woodUsageData = woodUsageData;
		this.rotationIntensities = rotationIntensities;
	}
	
	public ModelStat getStatus() {
		return status;
	}
	public Map<Integer, LandUseItem> getLandUses() {
		return landUses;
	}

	public Map<CropType, CropUsageData> getCommoditiesData() {
		return cropUsageData;
	}
	
	public Map<Integer, List<LandCoverChangeItem>> getLandCoverChanges() {
		return landCoverChanges;
	}

	public CarbonUsageData getCarbonUsageData() {
		return carbonUsageData;
	}

	public WoodUsageData getWoodUsageData() {
		return woodUsageData;
	}

	public Map<LandProtectionType, Map<Integer, Double>> getRotationIntensities() {
		return rotationIntensities;
	}
}
