package ac.ed.lurg.country.gams;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

import com.gams.api.GAMSDatabase;
import com.gams.api.GAMSGlobals;
import com.gams.api.GAMSJob;
import com.gams.api.GAMSOptions;
import com.gams.api.GAMSParameter;
import com.gams.api.GAMSParameterRecord;
import com.gams.api.GAMSVariable;
import com.gams.api.GAMSVariableRecord;
import com.gams.api.GAMSWorkspace;
import com.gams.api.GAMSWorkspaceInfo;
import com.gams.api.GAMSGlobals.ModelStat;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.utils.LogWriter;

public class GamsDemandOptimiser {

	private GamsDemandInput inputData;
	private boolean saveGamsGdxFiles;

	public GamsDemandOptimiser(GamsDemandInput inputData) {
		this.inputData = inputData;
		saveGamsGdxFiles = (ModelConfig.GAMS_COUNTRY_TO_SAVE != null && inputData.getCountry().getCountryName().equals(ModelConfig.GAMS_COUNTRY_TO_SAVE));
	}
	
	public GamsDemandOutput getDemandPc() {
		File tempDir = new File(ModelConfig.TEMP_DIR);
		tempDir.mkdir();

		Path workingDirectory = null;
		try {
			workingDirectory = Files.createTempDirectory(Paths.get(ModelConfig.TEMP_DIR), "dem");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			LogWriter.printlnError(e.toString());
		}
		//File workingDirectory = new File(ModelConfig.TEMP_DIR);
		GAMSWorkspaceInfo  wsInfo  = new GAMSWorkspaceInfo();
		wsInfo.setWorkingDirectory(workingDirectory.toAbsolutePath().toString());

		GAMSWorkspace ws = new GAMSWorkspace(wsInfo);
		GAMSOptions opt = ws.addOptions();
		GAMSJob gamsJob = ws.addJobFromFile(ModelConfig.DEMAND_GAMS_MODEL);
		
		GAMSDatabase inDB = ws.addDatabase();
		setupPriceGdpDB(inDB);
		ArrayList<GAMSDatabase> dbs = new ArrayList<GAMSDatabase>();
		dbs.add(inDB);
		opt.defines("gdx_prices_and_gdp", inDB.getName());

		if (ModelConfig.ADJUST_DIET_PREFS) {
			LogWriter.println("Adjusting diet params", 3);
			GAMSDatabase originalParamDb = ws.addDatabaseFromGDX(new File(ModelConfig.DEMAND_PARAM_FILE).getAbsolutePath());
			GAMSDatabase adjustedParamDb = ws.addDatabase(originalParamDb);
			adjustDietParams(adjustedParamDb);
			dbs.add(adjustedParamDb);
			opt.defines("gdx_parameters", adjustedParamDb.getName());
		}
		else
			opt.defines("gdx_parameters", new File(ModelConfig.DEMAND_PARAM_FILE).getAbsolutePath());

		long startTime = System.currentTimeMillis();
		gamsJob.run(opt, dbs.toArray(new GAMSDatabase[dbs.size()]));
		
		GamsDemandOutput gamsOutput = handleResults(gamsJob.OutDB());
		
		LogWriter.println("Took " + (System.currentTimeMillis() - startTime) + " ms to run", 3);
		
		if (saveGamsGdxFiles)
			saveGDXFile("demand", workingDirectory.toFile());
		
		if (ModelConfig.CLEANUP_GAMS_DIR)  {
			gamsJob.OutDB().dispose();
			deleteDirectory(workingDirectory.toFile());
		}
		
		return(gamsOutput);
	}

	private void adjustDietParams(GAMSDatabase adjustedParamDb) {
		// get original parameters
		GAMSParameter tauParam = adjustedParamDb.getParameter("tau");
		GAMSParameter alphaParam = adjustedParamDb.getParameter("alpha");
		DietParamCalculationManager paramManager = DietParamCalculationManager.getInstance();
		
		for (GAMSParameterRecord rec : tauParam) {
			String key = rec.getKeys()[0];
			double initialTau = rec.getValue();
			
			CommodityType commodity = CommodityType.getForGamsName(key);
			double adjusted = paramManager.getFinalTau(inputData.getYear(), initialTau, commodity);
			LogWriter.println(String.format("%14s:  initialTau=%.6f, adjusted=%.6f", key, initialTau, adjusted), 3);
			rec.setValue(adjusted);
		}

		for (GAMSParameterRecord rec : alphaParam) {
			String key = rec.getKeys()[0];
			double initialAlpha = rec.getValue();

			CommodityType commodity = CommodityType.getForGamsName(key);
			double adjusted = paramManager.getFinalAlpha(inputData.getYear(), initialAlpha, commodity);
			LogWriter.println(String.format("%14s:  initialTau=%.6f, adjusted=%.6f", key, initialAlpha, adjusted), 3);
			rec.setValue(adjusted);
		}
	}

	private void setupPriceGdpDB(GAMSDatabase inDB) {
		GAMSParameter gdpPcP = inDB.addParameter("gdp_pc", 0);
		double gdpPc = inputData.getGdpPc();
		LogWriter.println(String.format("gdp_pc = %.5f", gdpPc), 3); 
		setGamsParamValue(gdpPcP.addRecord(), gdpPc, 5);
		GamsDemandOutput previousGamsDemands = inputData.getPreviousDemands();
		
		GAMSParameter prevUP = inDB.addParameter("previousU", 0);
		double previousUtility = previousGamsDemands == null ? 0.9452 * Math.log(gdpPc) - 8.8032 : previousGamsDemands.getUtility();
		LogWriter.println(String.format("previousU = %.5f, estimated = %.5f", previousUtility, 0.9452 * Math.log(gdpPc) - 8.8032), 3);
		setGamsParamValue(prevUP.addRecord(), previousUtility, 5);
		
		GAMSParameter maxIncomePropFoodSpendP = inDB.addParameter("maxIncomePropFoodSpend", 0);
		setGamsParamValue(maxIncomePropFoodSpendP.addRecord(), ModelConfig.MAX_INCOME_PROPORTION_FOOD_SPEND, 4);

		LogWriter.println("\nPrice", 3);	
		GAMSParameter priceP = inDB.addParameter("price", 1);	
		GAMSParameter prevSubsP = inDB.addParameter("previousSubs", 1);
		GAMSParameter prevDiscP = inDB.addParameter("previousDisc", 1);

		for (Map.Entry<CommodityType, Double> entry : inputData.getPrices().entrySet()) {
			CommodityType comm = entry.getKey();
			double priceComm = entry.getValue();

			doCommodity(priceP, prevSubsP, prevDiscP, previousGamsDemands, comm, priceComm,
					comm==CommodityType.NONFOOD ? 5 : 0.05, comm==CommodityType.NONFOOD ? 0.5 : 0.05);
		}
	}

	private void doCommodity(GAMSParameter priceP, GAMSParameter prevSubsP, GAMSParameter prevDiscP,
							 GamsDemandOutput previousGamsDemands, CommodityType comm, double price, double defaultSubs, double defaultDisc) {
		
		Vector<String> v = new Vector<String>();
		v.add(comm.getGamsName());
		double prevSubs = defaultSubs, prevDisc = defaultDisc;

		if (previousGamsDemands != null) {
			GamsCommodityDemand demand = previousGamsDemands.getGamsDemands(comm);
			prevSubs = demand.getSubsistence();
			prevDisc = demand.getDiscretionary();
		}
		
		LogWriter.println(String.format("%14s:  price=%.4f, previousSubs=%.4f, previousDisc=%.4f", comm.getGamsName(), price, prevSubs, prevDisc), 3);
		setGamsParamValue(priceP.addRecord(v), price, 4);
		setGamsParamValue(prevSubsP.addRecord(v), prevSubs, 4);
		setGamsParamValue(prevDiscP.addRecord(v), prevDisc, 4);
	}

	private double setGamsParamValue(GAMSParameterRecord param, double val, int places) {
		double dOut = Math.round(val * Math.pow(10,places)) / Math.pow(10,places);
		param.setValue(dOut);
		return dOut;
	}
		
	private GamsDemandOutput handleResults(GAMSDatabase outDB) {
		int modelStatusInt = (int) outDB.getParameter("ms").findRecord().getValue();
		ModelStat modelStatus = GAMSGlobals.ModelStat.lookup(modelStatusInt);

		LogWriter.println(String.format("\nFood Demand %s: Modelstatus (%d) %s, Solvestatus %s", inputData.getCountry(),
				modelStatusInt, modelStatus.toString(), GAMSGlobals.SolveStat.lookup((int) outDB.getParameter("ss").findRecord().getValue())));
		
		GAMSVariable varU = outDB.getVariable("u");
		double utility = varU.getFirstRecord().getLevel();
		LogWriter.println(String.format("u = %.4f", utility), 3); 
		
		double leastSquareError = Math.abs(outDB.getVariable("error").getFirstRecord().getLevel());

		switch (modelStatus) {
			case SOLVED: break;
			case UNDEFINED_STAT: break;
			case OPTIMAL_LOCAL:
				if (leastSquareError < 1e-5)
					LogWriter.printlnWarning("No exact solution. Approximate solution found. Residual = " + leastSquareError);
				else
					LogWriter.printlnError("Critical! No exact solution. Approximate solution found with high error. Residual = " + leastSquareError);
				break;
			default:
				LogWriter.printlnError("Critical! Solution failed.");			
		}
		
		GAMSVariable varSubs = outDB.getVariable("SubsistenceC");
		GAMSVariable varDisc = outDB.getVariable("DiscretionaryC");

		HashSet<GamsCommodityDemand> demandMap = new HashSet<GamsCommodityDemand>();

		Map<CommodityType, Double> subsMap = new HashMap<>();
		Map<CommodityType, Double> discMap = new HashMap<>();

		for (GAMSVariableRecord rec : varSubs) {
			String commodityName = rec.getKeys()[0];
			CommodityType commodity = CommodityType.getForGamsName(commodityName, true);
			double d = rec.getLevel();
			subsMap.put(commodity, d);
		}

		for (GAMSVariableRecord rec : varDisc) {
			String commodityName = rec.getKeys()[0];
			CommodityType commodity = CommodityType.getForGamsName(commodityName, true);
			double d = rec.getLevel();
			discMap.put(commodity, d);
		}
		
		for (CommodityType commodity : CommodityType.values()) {
			double subsGams, discGams;
			if (subsMap.containsKey(commodity)) {
				subsGams = subsMap.get(commodity);
			} else {
				subsGams = 0;
				LogWriter.println(commodity + " not found in Gams output so adding with zero demand for subsistence", 3);
			}

			if (discMap.containsKey(commodity)) {
				discGams = discMap.get(commodity);
			} else {
				discGams = 0;
				LogWriter.println(commodity + " not found in Gams output so adding with zero demand for discretionary", 3);
			}

			GamsCommodityDemand demand = new GamsCommodityDemand(commodity, subsGams, discGams);
			LogWriter.println(demand.toString(), 3);
			demandMap.add(demand);
		}

		GAMSParameter parConsumpFact = outDB.getParameter("desiredConsumpFactor");
		double desiredConsumpFactor = parConsumpFact.getFirstRecord().getValue();
		LogWriter.println(String.format("desiredConsumpFactor=%.4f", desiredConsumpFactor), 3);
		
		return new GamsDemandOutput(modelStatus.toString(), demandMap, utility, desiredConsumpFactor);
	}
	
	
	boolean deleteDirectory(File dir) {
	    File[] dirContents = dir.listFiles();
	    if (dirContents != null) {
	        for (File file : dirContents) {
	            deleteDirectory(file);
	        }
	    }
	    return dir.delete();
	}
	
	private void saveGDXFile(String ext, File tempDir) {
		// some hacky code for debug purposes that keeps each gams gdx file
		String country = inputData.getCountry().getCountryName().replaceAll("\\s+","");
		int year = inputData.getYear();
		try {
			Files.copy(
					FileSystems.getDefault().getPath(tempDir + File.separator + "_gams_java_gdb1.gdx"),
					FileSystems.getDefault().getPath(ModelConfig.TEMP_DIR + File.separator + country + year + ext + ".gdx"),
					StandardCopyOption.REPLACE_EXISTING);
			Files.copy(
					FileSystems.getDefault().getPath(tempDir + File.separator + "_gams_java_gjo1.lst"),
					FileSystems.getDefault().getPath(ModelConfig.TEMP_DIR + File.separator + country + year + ext + "Listing" + ".lst"),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			LogWriter.print(e);
		}
	}
}
