package ac.ed.lurg.country.gams;

import ac.ed.lurg.types.CommodityType;

public class GamsCommodityDemand {

	private CommodityType commodity;
	private double subsistence;
	private double discretionary;

	public GamsCommodityDemand(CommodityType commodity, double subsistence, double discretionary) {
		this.commodity = commodity;
		this.subsistence = subsistence;
		this.discretionary = discretionary;
	}

	public double getSubsistence() {
		return subsistence;
	}

	public double getDiscretionary() {
		return discretionary;
	}

	public double getKcalTotal() {
		return (subsistence + discretionary) * 2000;
	}

	public CommodityType getCommodity() {
		return commodity;
	}

	@Override
	public String toString() {
		return String.format("%14s:  subs=%.3f,\tdisc=%.3f,\tdemand(kcal)=%.1f", commodity, subsistence, discretionary, getKcalTotal());
	}
}