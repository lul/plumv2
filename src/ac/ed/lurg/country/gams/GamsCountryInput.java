package ac.ed.lurg.country.gams;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.country.CountryPrice;
import ac.ed.lurg.country.TradeConstraint;
import ac.ed.lurg.landuse.CarbonUsageData;
import ac.ed.lurg.landuse.CropUsageData;
import ac.ed.lurg.landuse.WoodUsageData;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.WoodType;

public class GamsCountryInput {

	private CompositeCountry country; // not really required but useful for debugging
	private Map<CropType, Double> projectedDemand;
	private Map<CropType, TradeConstraint> tradeConstraints;
	private Map<CropType, CountryPrice> countryPrices;
	private Map<CropType, CropUsageData> previousCropUsageData;

	private Map<CropType, Double> subsidyRates;
	private double carbonDemand;
	private Map<LandCoverType, Double> solarDemand; // TWh
	private CountryPrice carbonPrice;
	private TradeConstraint carbonTradeConstraint;
	private CarbonUsageData previousCarbonUsage;
	private double woodDemand;
	private CountryPrice woodPrice;
	private TradeConstraint woodTradeConstraint;
	private WoodUsageData previousWoodUsageData;

	public GamsCountryInput(CompositeCountry country, Map<CropType, Double> projectedDemand, Map<CropType, CountryPrice> countryPrices,
			Map<CropType, TradeConstraint> importConstraints, Map<CropType, CropUsageData> previousCropUsageData, 
			Map<CropType, Double> subsidyRates, double carbonDemand, Map<LandCoverType, Double> solarDemand,
			CountryPrice carbonPrice, TradeConstraint carbonTradeConstraint, CarbonUsageData previousCarbonUsage,
			double woodDemand, CountryPrice woodPrice, TradeConstraint woodTradeConstraint,
			WoodUsageData previousWoodUsageData) {

		super();
		this.country = country;
		this.projectedDemand = projectedDemand;
		this.tradeConstraints = importConstraints;
		this.countryPrices = countryPrices;
		this.previousCropUsageData = previousCropUsageData;
		this.subsidyRates = subsidyRates;
		this.carbonDemand = carbonDemand;
		this.previousCarbonUsage = previousCarbonUsage;
		this.solarDemand = solarDemand;
		this.carbonPrice = carbonPrice;
		this.carbonTradeConstraint = carbonTradeConstraint;
		this.woodDemand = woodDemand;
		this.woodPrice = woodPrice;
		this.woodTradeConstraint = woodTradeConstraint;
		this.previousWoodUsageData = previousWoodUsageData;
	}
		
	public CompositeCountry getCountry() { 
		return country;
	}
	
	public Map<CropType, Double> getProjectedDemand() {
		return projectedDemand;
	}

	public Map<CropType, CountryPrice> getCountryPrices() {
		return countryPrices;
	}
	
	public Map<CropType, TradeConstraint> getTradeConstraints() {
		return tradeConstraints;
	}
	
	public Map<CropType, CropUsageData> getPreviousCropUsageData() {
		return previousCropUsageData;
	}

	public Map<CropType, Double> getMinTrade() {
		Map<CropType, Double> netImport = new HashMap<CropType, Double>();
		for (Map.Entry<CropType, TradeConstraint> entry : tradeConstraints.entrySet()) {
			netImport.put(entry.getKey(), entry.getValue().getMinConstraint());
		}
		return netImport;
	}

	public Map<CropType, Double> getMaxTrade() {
		Map<CropType, Double> netImport = new HashMap<CropType, Double>();
		for (Map.Entry<CropType, TradeConstraint> entry : tradeConstraints.entrySet()) {
			netImport.put(entry.getKey(), entry.getValue().getMaxConstraint());
		}
		return netImport;
	}
	
	public Map<CropType, Double> getSubsidyRates() {
		return subsidyRates;
	}
	
	public double getCarbonDemand() {
		return carbonDemand;
	}

	public Map<LandCoverType, Double> getSolarDemand() {
		return solarDemand;
	}

	public CountryPrice getCarbonPrice() {
		return carbonPrice;
	}
	
	public TradeConstraint getCarbonTradeConstraint() {
		return carbonTradeConstraint;
	}

	public CarbonUsageData getPreviousCarbonUsage() {
		return previousCarbonUsage;
	}

	public double getWoodDemand() {
		return woodDemand;
	}

	public CountryPrice getWoodPrice() {
		return woodPrice;
	}

	public TradeConstraint getWoodTradeConstraint() {
		return woodTradeConstraint;
	}

	public WoodUsageData getPreviousWoodUsageData() {
		return previousWoodUsageData;
	}

	public void setTradeConstraints(Map<CropType, TradeConstraint> tradeConstraints) {
		this.tradeConstraints = tradeConstraints;
	}

}