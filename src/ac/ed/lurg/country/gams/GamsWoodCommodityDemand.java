package ac.ed.lurg.country.gams;

import ac.ed.lurg.types.WoodCommodityType;

public class GamsWoodCommodityDemand {
    private WoodCommodityType commodity;
    private double subsistence;
    private double discretionary;

    public GamsWoodCommodityDemand(WoodCommodityType commodity, double subsistence, double discretionary) {
        this.commodity = commodity;
        this.subsistence = subsistence;
        this.discretionary = discretionary;
    }

    public double getSubsistence() {
        return subsistence;
    }

    public double getDiscretionary() {
        return discretionary;
    }

    public double getPlumDemand() {
        return discretionary + subsistence;
    }

    public WoodCommodityType getCommodity() {
        return commodity;
    }

    @Override
    public String toString() {
        return String.format("%14s:  subs=%.3f,\tdisc=%.3f,\tdemand(plum)= %.3f", commodity, subsistence, discretionary, getPlumDemand());
    }
}
