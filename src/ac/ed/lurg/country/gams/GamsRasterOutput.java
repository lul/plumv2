package ac.ed.lurg.country.gams;

import java.util.Map;

import ac.ed.lurg.landuse.*;
import com.gams.api.GAMSGlobals.ModelStat;

import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;
import ac.sac.raster.RasterSet;

public class GamsRasterOutput {
	
	private ModelStat status;
	private RasterSet<LandUseItem> landUses;
	private Map<CropType, CropUsageData> cropUsageData;
	private CarbonUsageData carbonUsageData;
	private WoodUsageData woodUsageData;

	public GamsRasterOutput(RasterSet<LandUseItem> landUses, Map<CropType, CropUsageData> cropUsageData,
							WoodUsageData woodUsageData, CarbonUsageData carbonUsageData) {
		super();
		this.landUses = landUses;
		this.cropUsageData = cropUsageData;
		this.carbonUsageData = carbonUsageData;
		this.woodUsageData = woodUsageData;
	}

	public GamsRasterOutput(ModelStat status, RasterSet<LandUseItem> intensityRaster, Map<CropType, CropUsageData> cropUsageData,
							CarbonUsageData carbonUsageData, WoodUsageData woodUsageData) {
		this(intensityRaster, cropUsageData, woodUsageData, carbonUsageData);
		this.status = status;
	}
	
	public ModelStat getStatus() {
		return status;
	}
	
	public RasterSet<LandUseItem> getLandUses() {
		return landUses;
	}

	public Map<CropType, CropUsageData> getCropUsageData() {
		return cropUsageData;
	}

	public CarbonUsageData getCarbonUsageData() {
		return carbonUsageData;
	}

	public WoodUsageData getWoodUsageData() {
		return woodUsageData;
	}

}
