package ac.ed.lurg.country.gams;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.Map.Entry;

import ac.ed.lurg.utils.calib.CalibrationItem;
import ac.ed.lurg.landuse.*;
import ac.ed.lurg.solar.AgrivoltaicsYieldManager;
import ac.ed.lurg.solar.SolarPotentialItem;
import ac.ed.lurg.types.*;
import com.gams.api.GAMSDatabase;
import com.gams.api.GAMSException;
import com.gams.api.GAMSGlobals;
import com.gams.api.GAMSGlobals.ModelStat;
import com.gams.api.GAMSJob;
import com.gams.api.GAMSOptions;
import com.gams.api.GAMSParameter;
import com.gams.api.GAMSParameterRecord;
import com.gams.api.GAMSSet;
import com.gams.api.GAMSVariable;
import com.gams.api.GAMSVariableRecord;
import com.gams.api.GAMSWorkspace;
import com.gams.api.GAMSWorkspaceInfo;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.carbon.CarbonFluxItem;
import ac.ed.lurg.country.CountryPrice;
import ac.ed.lurg.country.TradeConstraint;
import ac.ed.lurg.forestry.WoodYieldData;
import ac.ed.lurg.utils.LazyHashMap;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.yield.YieldResponsesItem;

public class GamsLocationOptimiser {

	private static final boolean DEBUG = false;
	private boolean saveGamsGdxFiles;

	private GamsLocationInput inputData;

	public GamsLocationOptimiser(GamsLocationInput inputData) {
		this.inputData = inputData;
		saveGamsGdxFiles = (ModelConfig.GAMS_COUNTRY_TO_SAVE != null && inputData.getCountryInput().getCountry().getName().equals(ModelConfig.GAMS_COUNTRY_TO_SAVE));
	}

	public GamsLocationOutput run() {
		File tempDir = new File(ModelConfig.TEMP_DIR);
		tempDir.mkdir();

		Path workingDirectory = null;
		try {
			workingDirectory = Files.createTempDirectory(Paths.get(ModelConfig.TEMP_DIR), "loc");
		} catch (IOException e) {
			LogWriter.printlnError(e.toString());
		}

		GAMSWorkspaceInfo  wsInfo  = new GAMSWorkspaceInfo();
		wsInfo.setWorkingDirectory(workingDirectory.toAbsolutePath().toString());
		//	wsInfo.setDebugLevel(DebugLevel.VERBOSE);

		GAMSWorkspace ws = new GAMSWorkspace(wsInfo);
		GAMSDatabase inDB = ws.addDatabase();

		setupInDB(inDB);

		GAMSJob gamsJob = ws.addJobFromFile(ModelConfig.GAMS_MODEL);
		GAMSOptions opt = ws.addOptions();

		opt.setAllModelTypes("conopt4");

		opt.defines("gdxincname", inDB.getName());

		long startTime = System.currentTimeMillis();
		gamsJob.run(opt, inDB);
		
		GamsLocationOutput gamsOutput = handleResults(gamsJob.OutDB());
		
		LogWriter.println("Took " + (System.currentTimeMillis() - startTime) + " ms to run", 3);
		
		if (saveGamsGdxFiles)
			saveGDXFile("landuse", workingDirectory.toFile());
		
		if (ModelConfig.CLEANUP_GAMS_DIR && gamsOutput.getStatus() == ModelStat.OPTIMAL_LOCAL)  {
			gamsJob.OutDB().dispose();
			deleteDirectory(workingDirectory.toFile());
		}

		return(gamsOutput);
	}

	private void setupInDB(GAMSDatabase inDB) {
		//if (DEBUG) LogWriter.println("\nLocation set");
		GAMSSet locationSet = inDB.addSet("location", 1);
		for (Integer locId : inputData.getPreviousLandUse().keySet()) {
			//if (DEBUG) LogWriter.println("     " + locId);
			locationSet.addRecord(locId.toString());
		}
		
		if (DEBUG) LogWriter.println("\nPrevious crop and land areas");	
		GAMSParameter prevCropP = inDB.addParameter("previousCropArea", 3);
		GAMSParameter prevFertIP = inDB.addParameter("previousFertIntensity", 3);
		GAMSParameter prevIrrigIP = inDB.addParameter("previousIrrigIntensity", 3);
		GAMSParameter prevOtherIP = inDB.addParameter("previousOtherIntensity", 3);
		
		GAMSParameter previousRuminantFeedP = inDB.addParameter("previousRuminantFeed", 1);
		GAMSParameter previousMonogastricFeedP = inDB.addParameter("previousMonogastricFeed", 1);
		GAMSParameter previousImportAmountP = inDB.addParameter("previousImportAmount", 1);
		GAMSParameter previousExportAmountP = inDB.addParameter("previousExportAmount", 1);
	
		GAMSParameter landP = inDB.addParameter("suitableLandArea", 1);
		GAMSParameter seedAndWasteRateP = inDB.addParameter("seedAndWasteRate", 1);	
		
		GAMSParameter prevLandCoverP = inDB.addParameter("previousLandCoverArea", 3);
		GAMSParameter maxLandCoverAreaP = inDB.addParameter("maxLandCoverArea", 3);

		GAMSParameter prevRotaIntP = inDB.addParameter("previousRotationIntensity", 2);

		@SuppressWarnings("unused")
		double totalAgriLand = 0;
		@SuppressWarnings("unused")
		double totalSuitable = 0;

		for (Map.Entry<Integer, ? extends LandUseItem> entry : inputData.getPreviousLandUse().entrySet()) {
			Integer locationId = entry.getKey();
			String locString = Integer.toString(locationId);
			LandUseItem landUseItem = entry.getValue();
			
			double suitableLand = landUseItem.getSuitableArea();
			totalSuitable += suitableLand;
			if (DEBUG) LogWriter.println(String.format("  %d   %15s,\t %.3f", locationId, "suitableLand", suitableLand));
			setGamsParamValueTruncate(landP.addRecord(locString), suitableLand, 5);

			// Cropland and pasture
			for (LandKey landKey : landUseItem.getAgricultureLandKeys()) {
				if (landKey.getLcType().equals(LandCoverType.AGRIVOLTAICS) && landKey.getLpType().equals(LandProtectionType.PROTECTED)) {
					continue;
				}
				AgricultureDetail agricultureDetail = (AgricultureDetail) landUseItem.getLandCoverDetail(landKey);

				for (CropType crop : CropType.getNonMeatTypes()) {
					if((landKey.getLcType().equals(LandCoverType.PASTURE) && !crop.equals(CropType.PASTURE)) ||
							(landKey.getLcType().equals(LandCoverType.CROPLAND) && crop.equals(CropType.PASTURE))) {
						continue;
					}

					Vector<String> v = new Vector<>();
					v.add(crop.getGamsName());
					LandProtectionType lpType = landKey.getLpType();
					LandCoverType lcType = landKey.getLcType();

					if (lcType.equals(LandCoverType.AGRIVOLTAICS)) {
						v.add(FarmingType.AGRIVOLTAICS.getName());
					} else if (lpType.equals(LandProtectionType.UNPROTECTED)) {
						v.add(FarmingType.CONVENTIONAL.getName());
					} else if (lpType.equals(LandProtectionType.PROTECTED)) {
						v.add(FarmingType.RESTRICTED.getName());
					}

					v.add(locString);

					double area = agricultureDetail.getCropArea(crop);
					/*if (area == 0) {
						continue;
					}*/
					double prevOtherI = agricultureDetail.getManagementIntensity(crop);
					double prevFertI = agricultureDetail.getFertiliserIntensity(crop);
					double prevIrrigI = agricultureDetail.getIrrigationIntensity(crop);
					// Setting other intensity to 0.5 if 0 to help optimisation
					prevOtherI = prevOtherI == 0 ? 0.5 : prevOtherI;

					if (DEBUG) LogWriter.println(String.format("  %d   %15s,\t %.2f,\t %.3f,\t %.3f,\t %.3f", locationId,
							crop.getGamsName(), area, prevFertI, prevIrrigI, prevOtherI));

					setGamsParamValue(prevCropP.addRecord(v), area, -1);
					setGamsParamValue(prevFertIP.addRecord(v), prevFertI, -1);
					setGamsParamValue(prevIrrigIP.addRecord(v), prevIrrigI, -1);
					setGamsParamValue(prevOtherIP.addRecord(v), prevOtherI, -1);

					totalAgriLand += area;
				}
			}
			
			// Previous land covers
			for (LandKey landKey : landUseItem.getLandKeys()) {
				if (landKey.getLcType().isConvertible()) {
					Vector<String> v = new Vector<>();
					v.add(landKey.getLcType().getName());
					v.add(landKey.getLpType().getGamsName());
					v.add(locString);
					double area = landUseItem.getLandCoverArea(landKey);
					setGamsParamValue(prevLandCoverP.addRecord(v), area, 7);
				}
			}

			for (LandCoverType lcType : LandCoverType.getConvertibleTypes()) {
				for (LandProtectionType lpType : LandProtectionType.values()) {
					LandKey key = new LandKey(lcType, lpType);
					setGamsParamValue(maxLandCoverAreaP.addRecord(lcType.getName(), lpType.getGamsName(), locString), landUseItem.getMaxLandCoverArea(key), 7);
				}
			}

			// Forestry
			for (LandKey landKey : landUseItem.getLandKeysForLandCover(LandCoverType.TIMBER_FOREST)) {
				ForestDetail forestDetail = (ForestDetail) landUseItem.getLandCoverDetail(landKey);
				Vector<String> v = new Vector<>();
				v.add(landKey.getLpType().getGamsName());
				v.add(locString);

				setGamsParamValue(prevRotaIntP.addRecord(v), forestDetail != null ? forestDetail.getRotationIntensity() : 0.02, -1);
			}

		}
		
		if (DEBUG) LogWriter.println(String.format("  Total agricultural %.1f,\t suitable %.1f", totalAgriLand, totalSuitable));

		if (DEBUG) LogWriter.println("\nIrrigation data (cost, constraint)");		
		GAMSParameter irrigCostP = inDB.addParameter("irrigCost", 1);		
		GAMSParameter irrigConstraintLocationP = inDB.addParameter("irrigConstraintLocation", 1);
		GAMSParameter irrigConstraintFarmingTypeP = inDB.addParameter("irrigConstraintFarmingType", 2);
		Map<Integer, ? extends IrrigationItem> irrigationData = inputData.getIrrigationCosts();
		
		double irrigCostMultiplier = ModelConfig.getAdjParam("IRRIG_COST_MULTIPLIER");
		irrigCostMultiplier *= inputData.getCalibrationItem().getIrrigationCostFactor();	   
		
		for (Entry<Integer, ? extends IrrigationItem> entry : irrigationData.entrySet()) {
			Integer locationId = entry.getKey();
			IrrigationItem irrigCostItem = entry.getValue();
			double irrigCost = irrigCostItem.getIrrigCost()*irrigCostMultiplier;
			double irrigConstraint = irrigCostItem.getIrrigConstraint();
			if (DEBUG) LogWriter.println(String.format("  %d  \t %.5f,\t %.1f", locationId, irrigCost, irrigConstraint));
			setGamsParamValue(irrigCostP.addRecord(Integer.toString(locationId)), irrigCost, -1);
			setGamsParamValue(irrigConstraintLocationP.addRecord(Integer.toString(locationId)), irrigConstraint, -1);
			
	          for (FarmingType farmingType : FarmingType.values()) {
	              Vector<String> v = new Vector<>();
	              v.add(farmingType.getName());
	              v.add(Integer.toString(locationId));	              
	              
	              double irrigConstraintFactor;
	              switch (farmingType) {
	            case CONVENTIONAL:
	             irrigConstraintFactor = getNegativeExponentialProgression(ModelConfig.UNPROTECTED_IRRIGATION_CONSTRAINT_FACTOR, false);
	                break;
	            case RESTRICTED:
	            irrigConstraintFactor = getNegativeExponentialProgression(ModelConfig.PROTECTED_IRRIGATION_CONSTRAINT_FACTOR, false);
	                break;
	            default:
	             irrigConstraintFactor = 1.0;
	             }                   
	              double irrigConstraintByFarmingType = irrigConstraint * irrigConstraintFactor;     
	                 setGamsParamValue(irrigConstraintFarmingTypeP.addRecord(v), irrigConstraintByFarmingType, -1);
	        }
			
		}

		if (DEBUG) LogWriter.println("\nDemand: " + inputData.getCountryInput().getCountry() + " " + inputData.getTimestep().getYear());
		GamsCountryInput countryInput = inputData.getCountryInput();

		GAMSParameter demandP = inDB.addParameter("demand", 1);
		Map<CropType, Double> demands = countryInput.getProjectedDemand();

		for (CropType crop : demands.keySet()) {
			setGamsParamValue(demandP.addRecord(crop.getGamsName()), demands.get(crop), -1);
		}

		Map<LandCoverType, Double> solarDemand = inputData.getCountryInput().getSolarDemand();
		setGamsParamValue(demandP.addRecord("energyPV"), solarDemand.get(LandCoverType.PHOTOVOLTAICS), -1);
		setGamsParamValue(demandP.addRecord("energyAV"), solarDemand.get(LandCoverType.AGRIVOLTAICS), -1);

		if (DEBUG) LogWriter.println("\nYield (fert/irrig) None/None, Max/None, None/Max, Max/Max, Shock,\t [fert p],\t [irrig p],\t {max irrig}");
		GAMSParameter yNoneP = inDB.addParameter("yieldNone", 2);
		GAMSParameter y_fert = inDB.addParameter("yieldFertOnly", 2);
		GAMSParameter y_irrig = inDB.addParameter("yieldIrrigOnly", 2);
		GAMSParameter y_both = inDB.addParameter("yieldBoth", 2);
		GAMSParameter y_shock = inDB.addParameter("yieldShock", 2);
		GAMSParameter fert_p = inDB.addParameter("fertParam", 2);
		GAMSParameter irrig_p = inDB.addParameter("irrigParam", 2);
		GAMSParameter irrigMaxP = inDB.addParameter("irrigMaxRate", 2);
		GAMSParameter other_p = inDB.addParameter("otherIParam", 2);
		
		// old
		for (Entry<Integer, ? extends YieldResponsesItem> entry : inputData.getYields().entrySet()) {
			Integer locationId = entry.getKey();
			String locString = Integer.toString(locationId);
			YieldResponsesItem yresp = entry.getValue();
			IrrigationItem irrigationItem = irrigationData.get(locationId);

			for (CropType crop : CropType.getNonMeatTypes()) {
				Vector<String> v = new Vector<String>();
				v.add(crop.getGamsName());
				v.add(locString);
				
				if (crop.equals(CropType.SETASIDE)) {
					setGamsParamValue(irrigMaxP.addRecord(v), 1000, 3);  // need to set this to any positive value to give an incentive not to irrigate setaside
					continue;
				}
				
				double maxIrrig = irrigationItem.getMaxIrrigAmount(crop);
				
				if (DEBUG) LogWriter.println(String.format("%d      %15s,\t %.1f,\t %.1f, \t %.1f,\t %.1f,\t %.2f,\t\t [%.2f],\t [%.2f],\t {%.2f}", 
						locationId, crop.getGamsName(), 
						yresp.getExtrapolatedYield(YieldType.NO_FERT_NO_IRRIG, crop), 
						yresp.getExtrapolatedYield(YieldType.FERT_MAX_NO_IRRIG, crop), 
						yresp.getExtrapolatedYield(YieldType.NO_FERT_IRRIG_MAX, crop), 
						yresp.getExtrapolatedYield(YieldType.FERT_MAX_IRRIG_MAX, crop), 
						yresp.getShockRate(crop), yresp.getFertParam(crop), yresp.getIrrigParam(crop), maxIrrig));

				setGamsParamValue(yNoneP.addRecord(v), yresp.getExtrapolatedYield(YieldType.NO_FERT_NO_IRRIG, crop), -1);
				setGamsParamValue(y_fert.addRecord(v), yresp.getExtrapolatedYield(YieldType.FERT_MAX_NO_IRRIG, crop), -1);
				setGamsParamValue(y_irrig.addRecord(v), yresp.getExtrapolatedYield(YieldType.NO_FERT_IRRIG_MAX, crop), -1);
				setGamsParamValue(y_both.addRecord(v), yresp.getExtrapolatedYield(YieldType.FERT_MAX_IRRIG_MAX, crop), -1);
				setGamsParamValue(y_shock.addRecord(v), yresp.getShockRate(crop), 4);
				setGamsParamValue(fert_p.addRecord(v), yresp.getFertParam(crop), 4);
				setGamsParamValue(irrig_p.addRecord(v), yresp.getIrrigParam(crop), 4);
				setGamsParamValue(irrigMaxP.addRecord(v), maxIrrig, 3);
				setGamsParamValue(other_p.addRecord(v), ModelConfig.OTHER_INTENSITY_PARAM / yresp.getMultiCropFactor(crop), -1);
			}
		}

		// Calibrated crop production costs
		CalibrationItem calibrationItem = inputData.getCalibrationItem();

		GAMSParameter cropProdCostsP = inDB.addParameter("prodCost", 1);
		GAMSParameter animalProdCostsP = inDB.addParameter("animalProdCost", 1);
		GAMSParameter costCalibP = inDB.addParameter("costCalibFactor", 1);
		Map<CropType, Double> prodCosts = calibrationItem.getCropProdCosts();
		for (CropType crop : prodCosts.keySet()) {
			Vector<String> v = new Vector<>();
			v.add(crop.getGamsName());
			if (crop.isMeat()) {
				setGamsParamValue(animalProdCostsP.addRecord(v), prodCosts.get(crop), -1);
			} else {
				setGamsParamValue(cropProdCostsP.addRecord(v), prodCosts.get(crop), -1);
			}
		}

		if (DEBUG) LogWriter.println("\nCrop, subsidy rate");
		GAMSParameter subsideRateP = inDB.addParameter("subsidyRate", 1);
		for (CropType crop : CropType.getImportedTypes()) {
			double subsidyRate = countryInput.getSubsidyRates().get(crop);
			if (DEBUG) LogWriter.println(String.format("%15s,\t %.4f", crop.getGamsName(), subsidyRate));
			setGamsParamValue(subsideRateP.addRecord(crop.getGamsName()), subsidyRate, -1);
		}
		
		if (DEBUG) LogWriter.println("\nImport-export, min trade/prod, max trade/prod, global import price, global export price, imports,   exports, ruminantFeed, monogastricFeed, seedAndWasteRate");
		
		GAMSParameter minTradeP = inDB.addParameter("minNetImport", 1);
		GAMSParameter maxTradeP = inDB.addParameter("maxNetImport", 1);
		GAMSParameter importPriceP = inDB.addParameter("importPrices", 1);
		GAMSParameter exportPriceP = inDB.addParameter("exportPrices", 1);
		GAMSParameter previousProdP = inDB.addParameter("previousProduction", 1);
		
		for (CropType crop : CropType.getImportedTypes()) {		
			
			TradeConstraint iec = countryInput.getTradeConstraints().get(crop);
			CountryPrice gp = countryInput.getCountryPrices().get(crop);			
			double minTrade = iec.getMinConstraint();
			double maxTrade = iec.getMaxConstraint();
			double importPrice = gp.getImportPrice();
			double exportPrice = gp.getProducerNetExportPrice();
			
			CropUsageData cu = countryInput.getPreviousCropUsageData().get(crop);
			double netImports = cu.getNetImportsExpected();
			double imports = netImports>0 ? netImports : 0;
			double exports = netImports<0 ? -netImports : 0;

			double ruminantFeed = cu.getRuminantFeed();
			double monogastricFeed = cu.getMonogastricFeed();
			double seedAndWasteRate = crop.getSeedAndWasteRate();
			double prevProd = cu.getProductionExpected();
					
			if (DEBUG) LogWriter.println(String.format("     %15s, \t %5.1f, \t %5.1f, \t %5.3f, \t %5.3f,     \t %5.1f, \t %5.1f, \t %5.1f, \t %5.1f, \t %5.3f", 
					crop.getGamsName(), minTrade, maxTrade, importPrice, exportPrice, imports, exports, ruminantFeed, monogastricFeed, seedAndWasteRate));
			
			setGamsParamValue(minTradeP.addRecord(crop.getGamsName()), minTrade, -1);
			setGamsParamValue(maxTradeP.addRecord(crop.getGamsName()), maxTrade, -1);
			setGamsParamValue(importPriceP.addRecord(crop.getGamsName()), importPrice, -1);
			setGamsParamValue(exportPriceP.addRecord(crop.getGamsName()), exportPrice, -1);
			setGamsParamValue(previousImportAmountP.addRecord(crop.getGamsName()), imports, -1);
			setGamsParamValue(previousExportAmountP.addRecord(crop.getGamsName()), exports, -1);
			setGamsParamValue(previousRuminantFeedP.addRecord(crop.getGamsName()), ruminantFeed, -1);
			setGamsParamValue(previousMonogastricFeedP.addRecord(crop.getGamsName()), monogastricFeed, -1);
			setGamsParamValue(seedAndWasteRateP.addRecord(crop.getGamsName()), seedAndWasteRate, -1);
			setGamsParamValue(previousProdP.addRecord(crop.getGamsName()), prevProd, -1);
		}

		// Assume pasture feed usage equals ruminant production in initial timestep
		if (ModelConfig.IS_CALIBRATION_RUN && inputData.getTimestep().isInitialTimestep()) {
			double ruminantProd = countryInput.getPreviousCropUsageData().get(CropType.RUMINANTS).getProductionExpected();
			setGamsParamValue(previousRuminantFeedP.addRecord(CropType.PASTURE.getGamsName()), ruminantProd, -1);
		}

		//below in case running without shocks to use original values in model config 
		
		double meatEff = ModelConfig.getAdjParam("MEAT_EFFICIENCY");
		double fertCost = ModelConfig.getAdjParam("FERTILISER_COST_PER_T");
		double otherIntCost = ModelConfig.getAdjParam("OTHER_INTENSITY_COST");
		double fertLimRestr = getNegativeExponentialProgression(ModelConfig.PROTECTED_CROPLAND_FERTILISER_LIMIT, false);
		double otherLimRestr = getNegativeExponentialProgression(ModelConfig.PROTECTED_CROPLAND_OTHER_LIMIT, false);
		double fertLimConv = getNegativeExponentialProgression(ModelConfig.UNPROTECTED_CROPLAND_FERTILISER_LIMIT, false);
		double otherLimConv = getNegativeExponentialProgression(ModelConfig.UNPROTECTED_CROPLAND_OTHER_LIMIT, false);
		

		LogWriter.print("\n", 3);	
		addScalar(inDB, "meatEfficency", meatEff, -1);
		addScalar(inDB, "fertiliserUnitCost", fertCost, -1);
		addScalar(inDB, "otherICost",otherIntCost, -1);
//		addScalar(inDB, "otherIParam", ModelConfig.OTHER_INTENSITY_PARAM, 6);
		addScalar(inDB, "unhandledCropRate", ModelConfig.UNHANDLED_CROP_RATE, -1);
		addScalar(inDB, "setAsideRate", ModelConfig.SETASIDE_RATE, -1);
		addScalar(inDB, "domesticPriceMarkup", ModelConfig.DOMESTIC_PRICE_MARKUP, -1);

		// Intensity limits for restricted cropland
		addScalar(inDB, "fertLimRestricted", fertLimRestr, -1);
		addScalar(inDB, "otherLimRestricted", otherLimRestr, -1);
		
		// Intensity limits for conventional cropland
		addScalar(inDB, "fertLimConventional", fertLimConv, -1);
		addScalar(inDB, "otherLimConventional", otherLimConv, -1);

		// Other parameters
		addScalar(inDB, "energyPrice", ModelConfig.ENERGY_PRICE, -1);
		addScalar(inDB, "monoculturePenalty", ModelConfig.MONOCULTURE_PENALTY, -1);
		addScalar(inDB, "pastureIntensityPenalty", ModelConfig.PASTURE_INTENSITY_PENALTY, -1);

		// AV system has lower yield by factor AV_YIELD_FACTOR due to space taken by PV panels
		// but also means cost is lower since crop area is reduced
		GAMSParameter avCropCostP = inDB.addParameter("agrivoltaicsCropCostFactor", 1);
		for (FarmingType farmingType : FarmingType.values()) {
			Vector<String> v = new Vector<>();
			v.add(farmingType.getName());
			setGamsParamValue(avCropCostP.addRecord(v), farmingType.equals(FarmingType.AGRIVOLTAICS) ? ModelConfig.AV_YIELD_FACTOR : 1.0, -1);
		}


		// Setting the same trade adjustment cost for all commodities apart from carbonCredits.
		// Could have individual values if needed

		GAMSParameter tradeAdjCostP = inDB.addParameter("tradeAdjustmentCostRate", 1);
		//double tradeAdjCost = ModelConfig.IS_CALIBRATION_RUN && inputData.getTimestep().getTimestep()
		//		< ModelConfig.END_FIRST_STAGE_CALIBRATION ? 0 : ModelConfig.getAdjParam("TRADE_ADJUSTMENT_COST_RATE");

		double tradeAdjCost = ModelConfig.getAdjParam("TRADE_ADJUSTMENT_COST_RATE");

		for (CropType crop : CropType.getImportedTypes()) {
			setGamsParamValue(tradeAdjCostP.addRecord(crop.getGamsName()), tradeAdjCost, -1);
		}

		for (WoodType wType : WoodType.values()) {
			setGamsParamValue(tradeAdjCostP.addRecord(wType.getName()), tradeAdjCost, -1);
		}

		setGamsParamValue(tradeAdjCostP.addRecord("carbonCredits"),
				ModelConfig.CARBON_TRADE_ADJ_COST_MULTIPLIER * tradeAdjCost, -1);

		// Forestry
		// Demand
		setGamsParamValue(demandP.addRecord("wood"), countryInput.getWoodDemand(), -1);
		// Trade limits
		TradeConstraint woodTradeConstraint = countryInput.getWoodTradeConstraint();
		setGamsParamValue(minTradeP.addRecord("wood"), woodTradeConstraint.getMinConstraint(), -1);
		setGamsParamValue(maxTradeP.addRecord("wood"), woodTradeConstraint.getMaxConstraint(), -1);
		// Prices
		setGamsParamValue(importPriceP.addRecord("wood"), countryInput.getWoodPrice().getImportPrice() , -1);
		setGamsParamValue(exportPriceP.addRecord("wood"), countryInput.getWoodPrice().getExportPrice(), -1);
		
		double timberLimPro = getNegativeExponentialProgression(ModelConfig.PROTECTED_TIMBER_INTENSITY_LIMIT, true);
		double timberLimUnpro = getNegativeExponentialProgression(ModelConfig.UNPROTECTED_TIMBER_INTENSITY_LIMIT, true);
		
		addScalar(inDB, "timberLimProtected", timberLimPro, -1);
		addScalar(inDB, "timberLimUnprotected", timberLimUnpro, -1);

		addScalar(inDB, "forestManagementCost",
				ModelConfig.getAdjParam("FOREST_MANAGEMENT_COST") * calibrationItem.getForestryCostFactor(), -1);

		{
			double netImport = countryInput.getPreviousWoodUsageData().getNetImport();
			double imports = netImport > 0 ? netImport : 0;
			double exports = netImport < 0 ? -netImport : 0;
			setGamsParamValue(previousExportAmountP.addRecord("wood"), exports, -1);
			setGamsParamValue(previousImportAmountP.addRecord("wood"), imports, -1);
		}
		
		// Yield from timber forest rotation
		Map<Integer, ? extends WoodYieldData> woodYieldData = inputData.getWoodYields();

		GAMSParameter woodRespMaxYieldP = inDB.addParameter("woodYieldMaxParam", 1);
		GAMSParameter woodRespSlopeP = inDB.addParameter("woodYieldSlopeParam", 1);
		GAMSParameter woodRespShapeP = inDB.addParameter("woodYieldShapeParam", 1);

		for (Entry<Integer, ? extends WoodYieldData> entry : woodYieldData.entrySet()) {
			Integer locationId = entry.getKey();
			String locString = Integer.toString(locationId);
			WoodYieldData wYield = entry.getValue();

			{
				Vector<String> w = new Vector<String>();
				w.add(locString);
				setGamsParamValue(woodRespMaxYieldP.addRecord(w), wYield.getMaxYieldParam(), -1);
				setGamsParamValue(woodRespSlopeP.addRecord(w), wYield.getSlopeParam(), -1);
				setGamsParamValue(woodRespShapeP.addRecord(w), wYield.getShapeParam(), -1);
			}			
			
		}

		// Carbon 
		setGamsParamValue(demandP.addRecord("carbonCredits"), countryInput.getCarbonDemand(), -1);
		addScalar(inDB, "carbonHorizon", ModelConfig.CARBON_HORIZON, -1);
		setGamsParamValue(importPriceP.addRecord("carbonCredits"), countryInput.getCarbonPrice().getImportPrice(), -1);
		setGamsParamValue(exportPriceP.addRecord("carbonCredits"), countryInput.getCarbonPrice().getExportPrice(), -1);
		
		TradeConstraint carbonTradeConstraint = countryInput.getCarbonTradeConstraint();
		setGamsParamValue(minTradeP.addRecord("carbonCredits"), carbonTradeConstraint.getMinConstraint(), -1);
		setGamsParamValue(maxTradeP.addRecord("carbonCredits"), carbonTradeConstraint.getMaxConstraint(), -1);

		double prevCarbonNetImport = countryInput.getPreviousCarbonUsage().getNetCarbonImport();
		setGamsParamValue(previousImportAmountP.addRecord("carbonCredits"), Math.max(0, prevCarbonNetImport), -1);
		setGamsParamValue(previousExportAmountP.addRecord("carbonCredits"), Math.max(0, -prevCarbonNetImport), -1);

		addScalar(inDB, "carbonForestMaxProportion", ModelConfig.CARBON_FOREST_MAX_PROPORTION, -1);

		GAMSParameter cFluxCreditRateP = inDB.addParameter("carbonCreditRate", 3);

		for (Entry<Integer, ? extends CarbonFluxItem> entry : inputData.getCarbonFluxes().entrySet()) {
			Integer locationId = entry.getKey();
			String locString = Integer.toString(locationId);
			CarbonFluxItem cFlux = entry.getValue();
			
			for (LandCoverType fromLc : LandCoverType.getConvertibleTypes()) {
				for (LandCoverType toLc : LandCoverType.getConvertibleTypes()) {
					Vector<String> v = new Vector<String>();
					v.add(fromLc.getName());
					v.add(toLc.getName());
					v.add(locString);
					
					if (ModelConfig.getCarbonOption(fromLc, toLc)) {
						setGamsParamValue(cFluxCreditRateP.addRecord(v), cFlux.getCarbonCreditRate(fromLc, toLc), 5);
					}
				} 
			}
		}

		// Solar
		GAMSParameter solarEnergyP = inDB.addParameter("solarEnergyDensity", 2);
		GAMSParameter solarNpvP = inDB.addParameter("solarCostRate", 2);
		GAMSParameter avYieldFactorP = inDB.addParameter("agrivoltaicsYieldFactor", 3);
		AgrivoltaicsYieldManager agrivoltParamManager = AgrivoltaicsYieldManager.getInstance();
		Map<Integer, ? extends SolarPotentialItem> solarPotentials = inputData.getSolarPotentials();

		for (Integer locationId : inputData.getLocations()) {
			String locString = Integer.toString(locationId);

			// If crop yields missing, set solar potential to zero otherwise GAMS will prioritise those areas
			boolean yieldMissing = inputData.getYields().get(locationId).isMissing();

			for (LandCoverType lc : Arrays.asList(LandCoverType.PHOTOVOLTAICS, LandCoverType.AGRIVOLTAICS)) {
				Vector<String> v = new Vector<>();
				v.add(lc.getName());
				v.add(locString);

				double energy, cost;

				if (ModelConfig.IS_PHOTOVOLTAICS_ON || ModelConfig.IS_AGRIVOLTAICS_ON) {
					energy = !yieldMissing ? solarPotentials.get(locationId).getEnergyDensity(lc) : 0;
					cost = !yieldMissing ? solarPotentials.get(locationId).getAnnualCost(lc) : 100;
				} else {
					energy = 0;
					cost = 100;
				}

				setGamsParamValue(solarEnergyP.addRecord(v), energy, -1);
				setGamsParamValue(solarNpvP.addRecord(v), cost, -1);
			}

			for (CropType crop : CropType.getNonMeatTypes()) {
				if (crop.equals(CropType.SETASIDE)) {
					continue;
				}
				for (FarmingType farmingType : FarmingType.values()) {
					Vector<String> v = new Vector<>();
					v.add(crop.getGamsName());
					v.add(farmingType.getName());
					v.add(locString);

					double yieldFactor;
					if (ModelConfig.IS_AGRIVOLTAICS_ON && farmingType.equals(FarmingType.AGRIVOLTAICS)) {
						double gcr = solarPotentials.containsKey(locationId) ? solarPotentials.get(locationId).getAgrivoltaicsGCR() : 0;
						yieldFactor = agrivoltParamManager.getYieldFactor(crop, gcr);
					} else {
						yieldFactor = 1;
					}
					setGamsParamValue(avYieldFactorP.addRecord(v), yieldFactor, -1);
				}
			}
		}
		
		// Land cover conversion cost
		GAMSParameter conversionCostP = inDB.addParameter("conversionCost", 3);
		Map<LandCoverChangeKey, Double> conversionCosts = ConversionCostManager.getInstance().getConversionCosts();


		for (Map.Entry<LandCoverChangeKey, Double> entry : conversionCosts.entrySet()) {
			LandCoverChangeKey key = entry.getKey();
			String fromName = key.getFromLc().getName();
			String toName = key.getToLc().getName();
			double baseCost = entry.getValue();

			for (Integer location : inputData.getLocations()) {
				double costFactor;
				// Additional infrastructure expansion cost if converting from natural
				if (key.getFromLc().equals(LandCoverType.NATURAL)) {
					LandUseItem luItem = inputData.getPreviousLandUse().get(location);
					double totalArea = luItem.getTotalLandArea();
					double naturalArea = luItem.getLandCoverArea(LandCoverType.NATURAL);
					costFactor = Math.exp(ModelConfig.INFRASTRUCTURE_COST_FACTOR * naturalArea / totalArea);
				} else {
					costFactor = 1;
				}

				double cost = baseCost * costFactor;

				// Allow prices to equilibrate before relaxing conversion costs
				if (ModelConfig.IS_CALIBRATION_RUN && inputData.getTimestep().getTimestep() < ModelConfig.END_FIRST_STAGE_CALIBRATION
					&& !key.getFromLc().equals(key.getToLc())) {
					if (key.getFromLc().equals(LandCoverType.TIMBER_FOREST) && key.getToLc().equals(LandCoverType.NATURAL)){
						cost = ModelConfig.TIMBER_FOREST_ABANDONMENT_COST; // forest intensity can't be zero so we need to allow abandonment in case there is too much production initially
					} else {
						cost = 1;
					}
				}

				Vector<String> v = new Vector<>();
				v.add(fromName);
				v.add(toName);
				v.add(location.toString());
				setGamsParamValue(conversionCostP.addRecord(v), cost, -1);
			}
		}

		// Maximum land cover conversion
		// This includes both constraints due to slope and accessibility (cropland, solar)
		// and forbidden protected conversions specified in RESTRICT_PROTECTED_LAND_CONVERSION_FOR
		GAMSParameter maxLandCoverChangeP = inDB.addParameter("maxLandCoverChange", 4);
		String[] restrictedLandCoversStrArr = ModelConfig.RESTRICT_PROTECTED_LAND_CONVERSION_FOR.split(",");
		Set<LandCoverType> restrictedLandCovers = new HashSet<>();
		for (String lcStr : restrictedLandCoversStrArr) {
			restrictedLandCovers.add(LandCoverType.getForName(lcStr));
		}

		for (Integer location : inputData.getLocations()) {
			Map<LandChangeKey, Double> maxLandChangeMap = inputData.getPreviousLandUse().get(location).getMaxLandCoverChangeMap();

			for (LandChangeKey lcKey : maxLandChangeMap.keySet()) {
				LandCoverType fromLc = lcKey.getFromLandKey().getLcType();
				LandCoverType toLc = lcKey.getToLandKey().getLcType();
				LandProtectionType lp = lcKey.getFromLandKey().getLpType();

				Vector<String> v = new Vector<>();
				v.add(fromLc.getName());
				v.add(toLc.getName());
				v.add(lp.getGamsName());
				v.add(location.toString());

				if (restrictedLandCovers.contains(fromLc) && lp.equals(LandProtectionType.PROTECTED) && !fromLc.equals(toLc)) {
					setGamsParamValue(maxLandCoverChangeP.addRecord(v), 0, 7);
				} else {
					double area = maxLandChangeMap.get(lcKey);
					setGamsParamValue(maxLandCoverChangeP.addRecord(v), area, 7);

				}
			}
		}

	}

	private void addScalar(GAMSDatabase gamsDb, String recordName, double val, int places) {
		GAMSParameter param = gamsDb.addParameter(recordName, 0);
		double dOut = setGamsParamValue(param.addRecord(), val, places);
		if (DEBUG) LogWriter.println(recordName + ": " + dOut, 3);
	}

	private double setGamsParamValue(GAMSParameterRecord param, double val, int places) {
		double dOut = places >= 0 ? Math.round(val * Math.pow(10,places)) / Math.pow(10,places) : val;
		param.setValue(dOut);
		return dOut;
	}
	
	private double setGamsParamValueTruncate(GAMSParameterRecord param, double val, int places) {
		double dOut = ((int)(val * Math.pow(10,places))) / Math.pow(10,places);
		param.setValue(dOut);
		return dOut;
	}
	
	@SuppressWarnings("serial")
	private GamsLocationOutput handleResults(GAMSDatabase outDB) {
		int modelStatusInt = (int) outDB.getParameter("ms").findRecord().getValue();
		ModelStat modelStatus = GAMSGlobals.ModelStat.lookup(modelStatusInt);
		String contextString = String.format("%s %s: Modelstatus %s, Solvestatus %s", 
    			inputData.getCountryInput().getCountry(),
			inputData.getTimestep().getYear(),
			modelStatus.toString(),
			GAMSGlobals.SolveStat.lookup((int) outDB.getParameter("ss").findRecord().getValue()));
        LogWriter.println("\n" + contextString);

		if (modelStatus != ModelStat.OPTIMAL_LOCAL) {
			LogWriter.printlnError("Critical!!! Land use incorrectly solved. " + contextString);
		}

		GAMSVariable varLandCoverArea = outDB.getVariable("landCoverArea");
		GAMSVariable varAreas = outDB.getVariable("cropArea");
		GAMSVariable varFertIntensities = outDB.getVariable("fertI");
		GAMSVariable varIrrigIntensities = outDB.getVariable("irrigI");
		GAMSVariable varOtherIntensities = outDB.getVariable("otherIntensity");
		GAMSVariable varFeed = outDB.getVariable("feedAmount");
		GAMSVariable varNetImports = outDB.getVariable("netImportAmount");
		GAMSParameter parmNetImportCost = outDB.getParameter("netImportCost");
		GAMSVariable varYields = outDB.getVariable("yield");
		GAMSVariable varUnitEnergies = outDB.getVariable("unitCost");
		GAMSVariable varProd = outDB.getVariable("totalProduction");
		GAMSParameter parmProdCost = outDB.getParameter("totalProdCost");
		GAMSParameter parmTotalCropArea = outDB.getParameter("totalCropArea");
		GAMSParameter parmProdShock = outDB.getParameter("productionShock");
		
		double totalCropArea = 0;
		double totalPastureArea = 0;
		double area, fertIntensity, irrigIntensity, otherIntensity = Double.NaN, ruminantFeed, monogastricFeed, netImport, netImportCost, yield, unitCost, prod, prodCost;	
		
		final LazyHashMap<Integer, LandUseItem> landUses = new LazyHashMap<Integer, LandUseItem>() { 
			protected LandUseItem createValue() { return new LandUseItem(); }
		};		
		
		Map<Integer, ? extends IrrigationItem> allIrrigationRefData = inputData.getIrrigationCosts();

		Map<CropType, CropUsageData> cropUsageData = new HashMap<CropType, CropUsageData>();

		for (GAMSVariableRecord rec : varAreas) {
			String itemName = rec.getKey(0);
			String farmingTypeName = rec.getKey(1);
			String locationName = rec.getKey(2);
			area = rec.getLevel();
			if (area == 0) {
				continue;
			}
			fertIntensity = varFertIntensities.findRecord(itemName, farmingTypeName, locationName).getLevel();
			irrigIntensity = varIrrigIntensities.findRecord(itemName, farmingTypeName, locationName).getLevel();
			otherIntensity = varOtherIntensities.findRecord(itemName, farmingTypeName, locationName).getLevel();
			yield = varYields.findRecord(itemName, farmingTypeName, locationName).getLevel();
			unitCost = varUnitEnergies.findRecord(itemName, farmingTypeName, locationName).getLevel();

			int locId = Integer.parseInt(locationName);
			CropType cropType = CropType.getForGamsName(itemName);
			FarmingType farmingType = FarmingType.getForName(farmingTypeName);

			LandCoverType lcType = farmingType.equals(FarmingType.AGRIVOLTAICS) ? LandCoverType.AGRIVOLTAICS : cropType.equals(CropType.PASTURE) ? LandCoverType.PASTURE : LandCoverType.CROPLAND;
			LandProtectionType lpType = farmingType.equals(FarmingType.RESTRICTED) ? LandProtectionType.PROTECTED : LandProtectionType.UNPROTECTED;
			LandKey landKey = new LandKey(lcType, lpType);

			LandUseItem landUseItem = landUses.lazyGet(locId); // returns landUseItem for location. If does not exist, creates new one

			if (DEBUG) LogWriter.println(String.format("\t location %s, %s:\tarea= %.1f,\tfert= %.3f,\tirrg= %.3f,\tintensity= %.3f", locationName, itemName, area, fertIntensity, irrigIntensity, otherIntensity));

			IrrigationItem irrigRefData = allIrrigationRefData.get(locId);

			double totalArea = varLandCoverArea.findRecord(lcType.getName(), lpType.getGamsName(), locationName).getLevel();

			AgricultureDetail agricultureDetail = (AgricultureDetail) landUseItem.getLandCoverDetail(landKey);
			agricultureDetail.setCropFraction(cropType, totalArea > 0 ? area / totalArea : 0);
			agricultureDetail.setIntensity(cropType, new Intensity(fertIntensity, irrigIntensity, otherIntensity, yield, unitCost, irrigRefData.getMaxIrrigAmount(cropType)));
			agricultureDetail.setArea(totalArea);

		}

		for (CropType cropType : CropType.getNonMeatTypes()) {
			String itemName = cropType.getGamsName();
			ruminantFeed = !cropType.equals(CropType.SETASIDE) ? varFeed.findRecord("ruminants", itemName).getLevel() : 0;
			monogastricFeed = !cropType.equals(CropType.SETASIDE) ? varFeed.findRecord("monogastrics", itemName).getLevel() : 0;
			netImport = cropType.isImportedCrop() ? varNetImports.findRecord(itemName).getLevel() : 0;
			netImportCost = cropType.isImportedCrop() ? getParmValue(parmNetImportCost, itemName) : 0;
			prod = varProd.findRecord(itemName).getLevel();
			prodCost = getParmValue(parmProdCost, itemName);
			double totalArea = getParmValue(parmTotalCropArea, itemName);
			double prodShock = getParmValue(parmProdShock, itemName);

			cropUsageData.put(cropType, new CropUsageData(ruminantFeed, monogastricFeed, netImport, netImportCost, prod, prodCost, totalArea, prodShock));

			if (DEBUG) {
				LogWriter.println(String.format("\n%s:\tarea= %.1f,\tmonogastricFeed= %.1f,\truminantFeed= %.1f,\tnetImports= %.3f,\tnetImportCost= %.3f,\tprod= %.3f, \tprodCost= %.3f,\tprodCostRate= %.3f,\tprodShock= %.3f", itemName, totalArea, monogastricFeed, ruminantFeed, netImport, netImportCost, prod, prodCost, prodCost / prod, prodShock));
			}
		}

		for (CropType meatTypes : CropType.getMeatTypes()) {
			netImport = varNetImports.findRecord(meatTypes.getGamsName()).getLevel();
			netImportCost= getParmValue(parmNetImportCost, meatTypes.getGamsName());
			prod = varProd.findRecord(meatTypes.getGamsName()).getLevel();
			prodCost = getParmValue(parmProdCost, meatTypes.getGamsName());

			cropUsageData.put(meatTypes, new CropUsageData(0.0, 0.0, netImport, netImportCost, prod, prodCost, Double.NaN, 0));
			if (DEBUG) LogWriter.println(String.format("\n%s:\t\t\t\t\tnetImports= %.3f,\tnetImportCost= %.3f,\tprod= %.3f,\tprodCost= %.3f", meatTypes.getGamsName(), netImport, netImportCost, prod, prodCost), 3); 
		}
		LogWriter.println(String.format("\n%s %s: Total area= %.1f (crop=%.1f, pasture %.1f)", 
				inputData.getCountryInput().getCountry(), inputData.getTimestep().getYear(), 
				totalCropArea+totalPastureArea, totalCropArea, totalPastureArea), 3);
			
		// Land cover change
		GAMSVariable varLandCoverChange = outDB.getVariable("landCoverChange");
		Map<Integer, List<LandCoverChangeItem>> landCoverChanges = new HashMap<>();
		
		for (GAMSVariableRecord rec : varLandCoverChange) {
			String fromLcStr = rec.getKey(0);
			String toLcStr = rec.getKey(1);
			String protectionStr = rec.getKey(2);
			String locationName = rec.getKey(3);
			int locId = Integer.parseInt(locationName);
			double change = rec.getLevel();
			
			// Skip if no change or no conversion
			if (change < 1e-6 || fromLcStr.equals(toLcStr))
				continue;

			LandCoverType fromLc = LandCoverType.getForName(fromLcStr);
			LandCoverType toLc = LandCoverType.getForName(toLcStr);
			LandProtectionType protectionType = LandProtectionType.getForGamsName(protectionStr);
			LandKey fromKey = new LandKey(fromLc, protectionType);
			LandKey toKey = new LandKey(toLc, protectionType);

			List<LandCoverChangeItem> changesList = landCoverChanges.computeIfAbsent(locId, k -> new ArrayList<>());
			changesList.add(new LandCoverChangeItem(fromKey, toKey, change));
		}

		// Wood production
		WoodUsageData newWoodUsage;
		GAMSVariable varWoodSupply = outDB.getVariable("woodSupply");
		GAMSParameter parWoodProdCost = outDB.getParameter("woodProdCost");
		{
			double netImports = varNetImports.findRecord("wood").getLevel();
			double supply = varWoodSupply.getFirstRecord().getLevel();
			double woodProdCost = parWoodProdCost.getFirstRecord().getValue();
			newWoodUsage = new WoodUsageData(supply, netImports, woodProdCost);
		}

		// Rotation intensity
		Map<LandProtectionType, Map<Integer, Double>> rotationIntensities = new HashMap<>();
		GAMSVariable varRota = outDB.getVariable("rotationIntensity");
		for (GAMSVariableRecord rec : varRota) {
			LandProtectionType lpType = LandProtectionType.getForGamsName(rec.getKey(0));
			int loc = Integer.parseInt(rec.getKey(1));
			double intensity = rec.getLevel();
			Map<Integer, Double> locMap = rotationIntensities.computeIfAbsent(lpType, k -> new HashMap<>());
			locMap.put(loc, intensity);
		}
		
		// Carbon
		double carbonSequestered = outDB.getParameter("netCarbonCredits").getFirstRecord().getValue();
		double netCarbonImport = varNetImports.findRecord("carbonCredits").getLevel();
		CarbonUsageData carbonUsageData = new CarbonUsageData(carbonSequestered, netCarbonImport, 0.0);

		LogWriter.println(String.format("%s NPV = %.1f", inputData.getCountryInput().getCountry(), outDB.getVariable("total_npv").getFirstRecord().getLevel()));

		return new GamsLocationOutput(modelStatus, landUses, cropUsageData, landCoverChanges, carbonUsageData,
				newWoodUsage, rotationIntensities);
	}

	private double getParmValue(GAMSParameter aParm, String itemName) {
		try {
			GAMSParameterRecord record = aParm.findRecord(itemName);
			double d = record.getValue();
			return d;
		}
		catch (GAMSException gamsEx) {
			//LogWriter.println("GAMSException thrown for " + itemName);
			return 0;
		}
	}

	private void addCommodityMapParm(GAMSParameter parm, Map<CommodityType, Double> itemMap, GAMSSet demandMap, int places) {
		for (Map.Entry<CommodityType, Double> entry : itemMap.entrySet()) {
			double d = entry.getValue();
			CommodityType commType = entry.getKey();
			if (DEBUG) LogWriter.println(String.format("     %15s,\t %.1f", entry.getKey().getGamsName(), d), 3);
			if (!Double.isNaN(d))
				setGamsParamValue(parm.addRecord(commType.getGamsName()), d, places);

			for (CropType cropType : commType.getCropTypes()) {
				Vector<String> v = new Vector<>();
				v.add(commType.getGamsName());
				v.add(cropType.getGamsName());
				demandMap.addRecord(v);
			}

		}
	}

	boolean deleteDirectory(File dir) {
	    File[] dirContents = dir.listFiles();
	    if (dirContents != null) {
	        for (File file : dirContents) {
	            deleteDirectory(file);
	        }
	    }
	    return dir.delete();
	}
	
	private void saveGDXFile(String ext, File tempDir) {
		// some hacky code for debug purposes that keeps each gams gdx file
		String country = inputData.getCountryInput().getCountry().getName().replaceAll("\\s+","");
		int year = inputData.getTimestep().getYear();
		try {
			Files.copy(
					FileSystems.getDefault().getPath(tempDir + File.separator + "_gams_java_gdb2.gdx"),
					FileSystems.getDefault().getPath(ModelConfig.TEMP_DIR + File.separator + country + year + ext + ".gdx"),
					StandardCopyOption.REPLACE_EXISTING);
			Files.copy(
					FileSystems.getDefault().getPath(tempDir + File.separator + "_gams_java_gjo1.lst"),
					FileSystems.getDefault().getPath(ModelConfig.TEMP_DIR + File.separator + country + year + ext + "Listing" + ".lst"),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			LogWriter.print(e);
		}
	}
	// alternative to getNegativeExponentialProgression (not used for now)
	private double getLinearProgression(double constraintFactor) {
		
		int currentYear = inputData.getTimestep().getYear();
		int startYear = ModelConfig.INTENSITY_LIMIT_START_YEAR;
		int endYear = ModelConfig.INTENSITY_LIMIT_END_YEAR;
		
		double linearProgression = 1.0 + (constraintFactor - 1.0) * 
				(currentYear - startYear) / (endYear - startYear);
		
        // Bound between 0 and 1
        linearProgression = Math.min(linearProgression, 1);
        linearProgression = Math.max(linearProgression, constraintFactor);  
        
        return linearProgression;
	}
	
	private double getNegativeExponentialProgression(double constraintFactor, boolean isForestryLimit) {
		
		int currentYear = inputData.getTimestep().getYear();
		int startYear = ModelConfig.INTENSITY_LIMIT_START_YEAR;
		int endYear = ModelConfig.INTENSITY_LIMIT_END_YEAR;
		double linearProgressionAdjustmentRatio = (double)(currentYear - startYear) / (endYear - startYear);
		double startValue = isForestryLimit ? 0.1 : 1.0;
		int decayFactor = ModelConfig.INTENSITY_LIMIT_DECAY_FACTOR;			    
		
        double exponentialProgression = constraintFactor + (startValue - constraintFactor) * 
        		Math.exp(-decayFactor * linearProgressionAdjustmentRatio);
        
        // Bound between constraintFactor and startValue
        exponentialProgression = Math.min(exponentialProgression, startValue);
        exponentialProgression = Math.max(exponentialProgression, constraintFactor);
        
        return exponentialProgression;
	}
}
