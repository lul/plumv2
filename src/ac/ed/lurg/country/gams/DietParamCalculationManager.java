package ac.ed.lurg.country.gams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.utils.StringTabularReader;

public class DietParamCalculationManager {
	// To change diet parameters of the elastic demand diet curves which changes the diet at high income country (saturation level)
	// Tau and Alpha are changed for each commodity
	// Change from START_YEAR from fitted coefficients (as per Gouel & Guimbard 2019) based on historical data
	// to a diet specified and created in the plumv2/data/target_diet.csv file (in this case a healthy diet from EAT LANCET article)
	// https://doi.org/10.1016/S0140-6736(18)31788-4

	private final Map<CommodityType, Double> shiftTauMap = new HashMap<>();
	private final Map<CommodityType, Double> shiftAlphaMap = new HashMap<>();
	
	private static DietParamCalculationManager tcm;
    public static DietParamCalculationManager getInstance() {
        if (tcm == null) 
        	tcm = new DietParamCalculationManager();
  
        return tcm; 
    }
    
	private DietParamCalculationManager() {
		readTauFromFile(); 
	}

	public double getFinalTau(int year, double initialTau, CommodityType commodity) {
		return interpolateParam(year, initialTau, shiftTauMap.get(commodity));
	}

	public double getFinalAlpha(int year, double initialAlpha, CommodityType commodity) {
		return interpolateParam(year, initialAlpha, shiftAlphaMap.get(commodity));
	}

	private double interpolateParam(int year, double initialParam, double shiftParam) {
		double offset = (ModelConfig.DIET_CHANGE_END_YEAR - ModelConfig.DIET_CHANGE_START_YEAR) / 2.0;
		double x = year - ModelConfig.DIET_CHANGE_START_YEAR;
		double ratio = 1 / (1 + Math.exp(-ModelConfig.DIET_CHANGE_RATE * (x - offset))); // sigmoid
		return ratio * shiftParam + (1 - ratio) * initialParam;
	}

	// Read file for target tau and alpha
	private void readTauFromFile() {
		StringTabularReader reader = new StringTabularReader(",", new String[]{"SSP", "Commodity", "Tau", "Alpha"});
		reader.read(ModelConfig.TARGET_DIET_FILE);

		Map<String, String> query = new HashMap<>();
		query.put("SSP", ModelConfig.TARGET_DIET_SCENARIO);
		List<Map<String, String>> queryResult = reader.query(query);
		for (Map<String, String> row : queryResult) {
			String commodityStr = row.get("Commodity");
			CommodityType commodity = CommodityType.getForGamsName(commodityStr);
			double tau = Double.parseDouble(row.get("Tau"));
			double alpha = Double.parseDouble(row.get("Alpha"));
			shiftTauMap.put(commodity, tau);
			shiftAlphaMap.put(commodity, alpha);
		}
	}
}