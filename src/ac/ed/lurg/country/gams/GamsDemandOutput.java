package ac.ed.lurg.country.gams;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.types.CommodityType;

public class GamsDemandOutput {
	private String status;
	
	private Collection<GamsCommodityDemand> demands;
	private double utility;
	private double desiredConsumpFactor;

	public GamsDemandOutput(String status, Collection<GamsCommodityDemand> demands, double utility, double desiredConsumpFactor) {
		this.status = status;
		this.demands = demands;
		this.utility = utility;
		this.desiredConsumpFactor = desiredConsumpFactor;
	}

	public Map<CommodityType, Double> getTotalDemands() {
		Map<CommodityType, Double> totalDemand = new HashMap<CommodityType, Double>();
		
		for (GamsCommodityDemand demand : demands)
			totalDemand.put(demand.getCommodity(), demand.getKcalTotal());
		
		return totalDemand;
	}
	
	public GamsCommodityDemand getGamsDemands(CommodityType comm) {
		for (GamsCommodityDemand demand : demands)
			if (demand.getCommodity() == comm)
				return demand;
		
		return null;
	}

	public double getUtility() {
		return utility;
	}
	public double getHungerFactor() {
		return 1-desiredConsumpFactor;
	}
	public String getStatus() {
		return status;
	}
}
