package ac.ed.lurg.country.gams;

import java.util.*;
import java.util.Map.Entry;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.carbon.CarbonFluxItem;
import ac.ed.lurg.forestry.WoodYieldData;
import ac.ed.lurg.forestry.WoodYieldItem;
import ac.ed.lurg.landuse.*;
import ac.ed.lurg.solar.SolarPotentialItem;
import ac.ed.lurg.types.*;
import ac.ed.lurg.utils.LazyTreeMap;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.yield.YieldRaster;
import ac.ed.lurg.yield.YieldResponsesItem;
import ac.sac.raster.IntegerRasterItem;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class GamsRasterOptimiser {
	public static final boolean DEBUG = false;

	private GamsRasterInput rasterInputData;
	private RasterSet<IntegerRasterItem> mapping;

	public GamsRasterOptimiser(GamsRasterInput rasterInputData, RasterSet<IntegerRasterItem> clusterMapping) {
		this.rasterInputData = rasterInputData;
		this.mapping = clusterMapping;
	}

	public GamsRasterOutput run() {
		// workout similar areas		
		GamsLocationInput gamsInput = convertFromRaster(rasterInputData);

		GamsLocationOptimiser opti = new GamsLocationOptimiser(gamsInput);
		GamsLocationOutput gamsOutput = opti.run();

		// map results back to raster
		return convertToRaster(gamsOutput);
	}

	private GamsRasterOutput convertToRaster(GamsLocationOutput gamsOutput) {
		RasterSet<LandUseItem> newIntensityRaster = allocAreas(gamsOutput);

		return new GamsRasterOutput(gamsOutput.getStatus(), newIntensityRaster, gamsOutput.getCommoditiesData(),
				gamsOutput.getCarbonUsageData(), gamsOutput.getWoodUsageData());
	}

	private void checkedTotalAreas(Map<?, LandUseItem> areaRaster, String comment) {
		for (LandCoverType l : LandCoverType.values()) {
			double total = 0;
			double unprotected = 0;
			for (LandUseItem a : areaRaster.values()) {
				if (a != null) {
					total += a.getLandCoverArea(l);
					unprotected += a.getLandCoverArea(new LandKey(l, LandProtectionType.UNPROTECTED));
				}
										
			}

			LogWriter.println("Total Area " + comment + ": " + l.getName() + ": total = " + total + ", unprotected = " + unprotected, 3);
		}

		double suitableArea=0, protectedArea=0;
		for (LandUseItem a : areaRaster.values()) {
			if (a != null) {
				protectedArea += a.getLandCoverArea(LandProtectionType.PROTECTED);
				suitableArea += a.getSuitableArea();
			}
		}
		LogWriter.println("Total protectedArea " + comment + ": " + protectedArea, 3);
		LogWriter.println("Total suitableArea " + comment + ": " + suitableArea, 3);
	}

	private RasterSet<LandUseItem> allocAreas(GamsLocationOutput gamsOutput) {
		RasterSet<LandUseItem> newLandUseRaster = rasterInputData.getPreviousLandUses();

		// *** Order is important here ***

		// Disaggregate cluster land cover changes to raster grid
		Map<RasterKey, Map<LandChangeKey, Double>> lcChanges = downscaleLandCoverChanges(gamsOutput.getLandCoverChanges(),
				newLandUseRaster);

		// Save land cover changes for output
		for (RasterKey key : newLandUseRaster.keySet()) {
			LandUseItem luItem = newLandUseRaster.get(key);
			luItem.setLandCoverChanges(lcChanges.getOrDefault(key, new HashMap<>()));
		}

		// Forest rotation intensity and yields
		Map<LandProtectionType, Map<Integer, Double>> rotaIntensities = gamsOutput.getRotationIntensities();
		for (RasterKey key : newLandUseRaster.keySet()) {
			LandUseItem luItem = newLandUseRaster.get(key);
			Integer locId = mapping.get(key).getInt();
			for (LandProtectionType lpType : LandProtectionType.values()) {
				double rotaInt = rotaIntensities.containsKey(lpType) ? rotaIntensities.get(lpType).getOrDefault(locId, 0.0) : 0.0;
				ForestDetail detail = (ForestDetail) luItem.getLandCoverDetail(new LandKey(LandCoverType.TIMBER_FOREST, lpType));
				detail.setRotationIntensity(rotaInt);
			}
		}

		// Crop areas and intensity
		for (Map.Entry<Integer, LandUseItem> entry : gamsOutput.getLandUses().entrySet()) {
			Integer locId = entry.getKey();
			LandUseItem newLandUseAggItem = entry.getValue();

			Set<RasterKey> keys = new HashSet<RasterKey>();
			for (Entry<RasterKey, IntegerRasterItem> mapEntry : mapping.entrySet()) {
				IntegerRasterItem iri = mapEntry.getValue();

				if (iri != null && locId == iri.getInt())
					keys.add(mapEntry.getKey());
			}
			
			RasterSet<LandUseItem> landUseItemsForLocation = newLandUseRaster.createSubsetForKeys(keys);

			for (RasterKey key : keys) {
				LandUseItem newLandUseItem = newLandUseRaster.get(key);

				for (LandKey landKey : newLandUseItem.getAgricultureLandKeys()) {
					AgricultureDetail detail = (AgricultureDetail) newLandUseItem.getLandCoverDetail(landKey);
					AgricultureDetail aggDetail = (AgricultureDetail) newLandUseAggItem.getLandCoverDetail(landKey);

					Set<CropType> aggCrops = aggDetail.getCrops();

					for (CropType crop : CropType.getNonMeatTypes()) {
						if ((landKey.getLcType().equals(LandCoverType.PASTURE) && !crop.equals(CropType.PASTURE)) ||
								(landKey.getLcType().equals(LandCoverType.CROPLAND) && crop.equals(CropType.PASTURE))) {
							continue;
						}

						if (aggCrops.contains(crop)) {
							detail.setCropFraction(crop, aggDetail.getCropFraction(crop));
							detail.setIntensity(crop, aggDetail.getIntensity(crop));
						} else  {
							detail.setCropFraction(crop, 0);
							detail.setIntensity(crop, new Intensity(0, 0, 0));
						}
					}
				}
			}
			
			if (DEBUG) checkedTotalAreas(landUseItemsForLocation, locId + " after");
		}

		return newLandUseRaster;
	}

	private Map<RasterKey, Map<LandChangeKey, Double>> downscaleLandCoverChanges(
			Map<Integer, List<LandCoverChangeItem>> aggLandCoverChanges, RasterSet<LandUseItem> luRaster) {

		Map<RasterKey, Map<LandChangeKey, Double>> rasterChanges = new HashMap<>(); // downscaled changes

		for (Integer locId : aggLandCoverChanges.keySet()) {
			List<LandCoverChangeItem> changes = aggLandCoverChanges.get(locId);

			Map<LandChangeKey, Double> changesMap = new HashMap<>();
			for (LandCoverChangeItem item : changes) {
				LandChangeKey lcKey = new LandChangeKey(item.getFromLandKey(), item.getToLandKey());
				changesMap.put(lcKey, item.getArea());
			}

			Set<RasterKey> keys = new HashSet<>();
			for (RasterKey key : mapping.keySet()) {
				if (mapping.get(key).getInt() == locId) {
					keys.add(key);
				}
			}

			Map<LandChangeKey, Double> unallocated = new HashMap<>();

			double totalUnallocated = 0;

			for (int i = 0; i < 1000; i++) { // iteratively attempt to allocate areas

				for (LandChangeKey lcKey : changesMap.keySet()) {

					double totalAvailable = 0;
					for (RasterKey rasKey : keys) {
						totalAvailable += luRaster.get(rasKey).getAvailableArea(lcKey);
					}

					double fraction = changesMap.get(lcKey) / totalAvailable; // fraction of available area converted
					fraction = Math.min(Math.max(fraction, 0), 1);

					for (RasterKey rasKey : keys) {
						LandUseItem luItem = luRaster.get(rasKey);
						double area = fraction * luRaster.get(rasKey).getAvailableArea(lcKey);

						if (area > 0) {
							luItem.moveAreas(lcKey.getFromLandKey(), lcKey.getToLandKey(), area);
							unallocated.merge(lcKey, -area, Double::sum);
							Map<LandChangeKey, Double> map = rasterChanges.computeIfAbsent(rasKey, k -> new HashMap<>());
							map.merge(lcKey, area, Double::sum);
						}
					}
				}

				// Break loop if all allocated, otherwise repeat
				totalUnallocated = unallocated.values().stream().reduce(0.0, Double::sum);
				if (totalUnallocated < 1e-6) {
					break;
				}

				changesMap.putAll(unallocated);
			}

			if (totalUnallocated > 1e-6) {
				LogWriter.printlnError("Can't downscale areas. Cluster " + locId + " " + rasterInputData.getCountryInput().getCountry() + " " + totalUnallocated);
			}
		}

		return rasterChanges;
	}


	@SuppressWarnings("serial")
	private GamsLocationInput convertFromRaster(GamsRasterInput rasterInputData) {
		YieldRaster yieldRaster = rasterInputData.getYields();
		RasterSet<LandUseItem> landUseRaster = rasterInputData.getPreviousLandUses();

		RasterSet<IrrigationItem> irrigRaster = rasterInputData.getIrrigationData();
		
		RasterSet<CarbonFluxItem> carbonFluxRaster = rasterInputData.getCarbonFluxes();
		
		RasterSet<WoodYieldItem> woodYieldRaster = rasterInputData.getWoodYields();

		RasterSet<SolarPotentialItem> solarPotentialRaster = rasterInputData.getSolarPotentialData();

		// look for inconsistencies
		for (Entry<RasterKey, YieldResponsesItem> entry : yieldRaster.entrySet()) {
			YieldResponsesItem yresp = entry.getValue();
			RasterKey key = entry.getKey();
			CropType crop = CropType.WHEAT;
			double fertMaxIrrigMax = yresp.getYield(YieldType.FERT_MAX_IRRIG_MAX, crop);
			double fertMaxNoIrrig = yresp.getYield(YieldType.FERT_MAX_NO_IRRIG, crop);
			double fertNoneIrrigMax = yresp.getYield(YieldType.NO_FERT_IRRIG_MAX, crop);

			if (fertMaxIrrigMax < fertMaxNoIrrig || fertNoneIrrigMax < fertMaxIrrigMax) {
				logWarningWithCoord("Inconsistency F only:" + fertMaxNoIrrig + ", I only" + fertNoneIrrigMax
						+ ", max " + fertMaxIrrigMax + " at ", key, yieldRaster);
			}
		}

		// Aggregation
		LazyTreeMap<Integer, YieldResponsesItem> aggregatedYields = new LazyTreeMap<Integer, YieldResponsesItem>() {
			protected YieldResponsesItem createValue() { return new YieldResponsesItem(); }
		};
		LazyTreeMap<Integer, LandUseItem> aggregatedAreas = new LazyTreeMap<Integer, LandUseItem>() { 
			protected LandUseItem createValue() { return new LandUseItem(); }
		};
		LazyTreeMap<Integer, IrrigationItem> aggregatedIrrigCosts = new LazyTreeMap<Integer, IrrigationItem>() { 
			protected IrrigationItem createValue() { return new IrrigationItem(); }
		};
		LazyTreeMap<Integer, CarbonFluxItem> aggregatedCarbonFluxes = new LazyTreeMap<Integer, CarbonFluxItem>() { 
			protected CarbonFluxItem createValue() { return new CarbonFluxItem(); }
		};
		LazyTreeMap<Integer, WoodYieldData> aggregatedWoodYields = new LazyTreeMap<Integer, WoodYieldData>() { 
			protected WoodYieldData createValue() { return new WoodYieldData(); }
		};
		LazyTreeMap<Integer, SolarPotentialItem> aggregatedSolarPotentials = new LazyTreeMap<Integer, SolarPotentialItem>() {
			protected SolarPotentialItem createValue() {
				return new SolarPotentialItem();
			}
		};

		for (RasterKey key : landUseRaster.keySet()) {
			int clusterId = mapping.get(key).getInt();

			LandUseItem landUseItem  = landUseRaster.get(key);
			YieldResponsesItem yresp = yieldRaster.get(key);
			IrrigationItem irrigItem = irrigRaster.get(key);
			WoodYieldItem woodYieldItem = woodYieldRaster.get(key);
			CarbonFluxItem carbonFluxItem = carbonFluxRaster.get(key);
			SolarPotentialItem solarPotentialItem = solarPotentialRaster.get(key);

			YieldResponsesItem aggYResp = aggregatedYields.lazyGet(clusterId);
			LandUseItem aggLandUse = aggregatedAreas.lazyGet(clusterId);
			IrrigationItem aggIrrig = aggregatedIrrigCosts.lazyGet(clusterId);
			WoodYieldData aggWYield = aggregatedWoodYields.lazyGet(clusterId);
			CarbonFluxItem aggCFlux = aggregatedCarbonFluxes.lazyGet(clusterId);
			SolarPotentialItem aggSolarPot = aggregatedSolarPotentials.lazyGet(clusterId);

			double suitableAreaThisTime  = landUseItem.getSuitableArea();
			double suitableAreaSoFar = aggLandUse.getSuitableArea();

			// Irrigation cost
			aggIrrig.setIrrigCost( aggregateMean(aggIrrig.getIrrigCost(), suitableAreaSoFar, irrigItem.getIrrigCost(), suitableAreaThisTime));
			aggIrrig.setIrrigConstraint(aggregateMean(aggIrrig.getIrrigConstraint(), suitableAreaSoFar, irrigItem.getIrrigConstraint(), suitableAreaThisTime));

			// Aggregate carbon fluxes and wood yields
			if (ModelConfig.IS_FORESTRY_ON) {
				LandKey landKey = new LandKey(LandCoverType.TIMBER_FOREST, LandProtectionType.UNPROTECTED);
				double areaThisTime = landUseItem.getLandCoverArea(landKey);
				double areaSoFar = aggLandUse.getLandCoverArea(landKey);

				// Wood yields
				for (Map.Entry<Integer, Double> entry : woodYieldItem.getYieldResponseMap().entrySet()) {
					int age = entry.getKey();
					double yieldThisTime = entry.getValue();
					double yieldSoFar = aggWYield.getYieldRota(age);
					double yieldAgg = aggregateMean(yieldSoFar, areaSoFar, yieldThisTime, areaThisTime);
					aggWYield.setYieldRota(age, yieldAgg);
				}

				// Rotation intensity
				ForestDetail forestDetail = (ForestDetail) landUseItem.getLandCoverDetail(landKey);
				ForestDetail aggForestDetail = (ForestDetail) aggLandUse.getLandCoverDetail(landKey);
				double rotaIntAgg = aggregateMean(aggForestDetail.getRotationIntensity(), areaSoFar, forestDetail.getRotationIntensity(), areaThisTime);
				aggForestDetail.setRotationIntensity(rotaIntAgg);
			}

			if (ModelConfig.IS_CARBON_ON) {
				for (LandCoverType fromLc : LandCoverType.getConvertibleTypes()) {
					for (LandCoverType toLc : LandCoverType.getConvertibleTypes()) {
						double areaThisTime = landUseItem.getLandCoverArea(fromLc);
						double areaSoFar = aggLandUse.getLandCoverArea(toLc);

						double cCreditThisTime = carbonFluxItem.getCarbonCreditRate(fromLc, toLc);
						double cCreditSoFar = aggCFlux.getCarbonCreditRate(fromLc, toLc);
						double cCreditAgg = aggregateMean(cCreditSoFar, areaSoFar, cCreditThisTime, areaThisTime);
						aggCFlux.setCarbonCreditRate(fromLc, toLc, cCreditAgg);
					}
				}
			}

			if (ModelConfig.IS_PHOTOVOLTAICS_ON || ModelConfig.IS_AGRIVOLTAICS_ON) {
				double areaThisTime = landUseItem.getTotalLandArea();
				double areaSoFar = landUseItem.getTotalLandArea();

				for (LandCoverType lc : Arrays.asList(LandCoverType.PHOTOVOLTAICS, LandCoverType.AGRIVOLTAICS)) {
					double energyAgg = aggregateMean(aggSolarPot.getEnergyDensity(lc), areaSoFar, solarPotentialItem.getEnergyDensity(lc), areaThisTime);
					double npvAgg = aggregateMean(aggSolarPot.getAnnualCost(lc), areaSoFar, solarPotentialItem.getAnnualCost(lc), areaThisTime);

					aggSolarPot.setEnergyDensity(lc, energyAgg);
					aggSolarPot.setAnnualCost(lc, npvAgg);
				}

				double avGCRAgg = aggregateMean(aggSolarPot.getAgrivoltaicsGCR(), areaSoFar, solarPotentialItem.getAgrivoltaicsGCR(), areaThisTime);
				aggSolarPot.setAgrivoltaicsGCR(avGCRAgg);
			}

			// Crops yields and area fractions
			for (CropType crop : CropType.getNonMeatTypes()) {
				if (crop.equals(CropType.SETASIDE)) {
					continue;
				}

				double irrigMax = irrigItem.getMaxIrrigAmount(crop);

				if (Double.isNaN(irrigMax))
					logWarningWithCoord("Can't find irrig max amount for ", key, yieldRaster, crop);
				else
					aggIrrig.setMaxIrrigAmount(crop, aggregateMean(aggIrrig.getMaxIrrigAmount(crop), suitableAreaSoFar, irrigMax, suitableAreaThisTime));

				for (YieldType yieldType : YieldType.values()) {
					double y = yresp.getYield(yieldType, crop);
					if (Double.isNaN(y))
						logWarningWithCoord("Problem getting yield for type:" + yieldType + ", ", key, yieldRaster, crop);
					else {
						double yieldSoFar = aggYResp.getYield(yieldType, crop);
						double updatedYield = aggregateMean(yieldSoFar, suitableAreaSoFar, y, suitableAreaThisTime);
						aggYResp.setYield(yieldType, crop, updatedYield);
					}
				}

				double s = yresp.getShockRate(crop);
				if (!Double.isNaN(s)) {
					double shockSoFar = aggYResp.getShockRate(crop);
					double updatedShock = aggregateMean(shockSoFar, suitableAreaSoFar, s, suitableAreaThisTime);
					aggYResp.setShockRate(crop, updatedShock);
				}


				double multiFactorSoFar = aggYResp.getMultiCropFactor(crop);
				double multiFactorThisTime = yresp.getMultiCropFactor(crop);
				double aggMultiFactor = aggregateMean(multiFactorSoFar, suitableAreaSoFar, multiFactorThisTime, suitableAreaThisTime);
				aggYResp.setMultiCropFactor(crop, aggMultiFactor);

				aggYResp.setMissingFlag(aggYResp.isMissing() || yresp.isMissing());

			}

			// Crop and pasture intensities and fractions
			for (LandKey landKey : landUseItem.getAgricultureLandKeys()) {
				AgricultureDetail detail = (AgricultureDetail) landUseItem.getLandCoverDetail(landKey);
				AgricultureDetail aggDetail = (AgricultureDetail) aggLandUse.getLandCoverDetail(landKey);

				for (CropType crop : detail.getCrops()) {
					double areaSoFar = aggDetail.getCropArea(crop);
					double areaThisTime = detail.getCropArea(crop);
					double updatedCroplandArea = landUseItem.getLandCoverArea(landKey) + aggLandUse.getLandCoverArea(landKey);
					if (updatedCroplandArea > 0) {
						aggDetail.setCropFraction(crop, (areaSoFar + areaThisTime) / updatedCroplandArea);
					}
					// intensity currently is always the same within a location, so can just be any.  If this changes need to average here.
					aggDetail.setIntensity(crop, detail.getIntensity(crop));
				}
			}

			// Land covers areas
			for (LandKey landKey : landUseItem.getLandKeys()) {
				double convAreaThisTime = landUseItem.getLandCoverArea(landKey);
				double convAreaSoFar = aggLandUse.getLandCoverArea(landKey);
				aggLandUse.setLandCoverArea(landKey, convAreaSoFar + convAreaThisTime);
			}

			// Maximum land cover area
			for (LandProtectionType lpType : LandProtectionType.values()) {
				for (LandCoverType lcType : LandCoverType.getConvertibleTypes()) {
					LandKey landKey = new LandKey(lcType, lpType);
					double aggArea = aggLandUse.getMaxLandCoverArea(landKey) + landUseItem.getMaxLandCoverArea(landKey);
					aggLandUse.setMaxLandCoverArea(landKey, aggArea);
				}
			}

			// Maximum land cover change
			landUseItem.calcMaxLandCoverChange();
			Map<LandChangeKey, Double> maxLandChangeMap = landUseItem.getMaxLandCoverChangeMap();
			for (LandChangeKey lcKey : maxLandChangeMap.keySet()) {
				double aggArea = aggLandUse.getMaxLandCoverChange(lcKey) + maxLandChangeMap.get(lcKey);
				aggLandUse.setMaxLandCoverChange(lcKey, aggArea);
			}

		}	

		checkedTotalAreas(landUseRaster, "before");
		checkedTotalAreas(aggregatedAreas, "after");

		Set<Integer> locations = new HashSet<>();
		for (IntegerRasterItem i : mapping.values()) {
			locations.add(i.getInt());
		}

		return new GamsLocationInput(rasterInputData.getTimestep(), locations, aggregatedYields, aggregatedAreas, aggregatedIrrigCosts,
				aggregatedCarbonFluxes, aggregatedWoodYields, aggregatedSolarPotentials, rasterInputData.getCalibrationItem(), rasterInputData.getCountryInput());
	}

	private void logWarningWithCoord(String message, RasterKey key, YieldRaster yieldRaster, CropType crop) {
		logWarningWithCoord(message + "crop:" + crop + ", ", key, yieldRaster);
	}

	private void logWarningWithCoord(String message, RasterKey key, YieldRaster yieldRaster) {
		LogWriter.println("Warning: " + message + key + ", x:" + yieldRaster.getXCoordin(key) + ", y:" + yieldRaster.getYCoordin(key), 3);
	}

	private double aggregateMean(double aggV, double aggArea, double newV, double newArea) {
		if (Double.isNaN(aggV))
			return newV;

		if (newArea == 0)
			return aggV;

		return (aggV*aggArea + newV*newArea) / (aggArea + newArea);
	}
}
