package ac.ed.lurg.country;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class AnimalRateManager {
	private StringTabularReader tabularReader;

	public AnimalRateManager() {
		tabularReader = new StringTabularReader(",", new String[]{"Iso3","plumDemandItem","LivestockItem","animalRate", "prod"});
		tabularReader.read(ModelConfig.ANIMAL_RATES_FILE);
	}

	private static class AnimalRateData {
		Double animalRate;
		Double prod;

		AnimalRateData(Double animalRate, Double prod) {
			this.animalRate = animalRate;
			this.prod = prod;
		}
	}

	public Map<String, Double> getAnimalRates(CompositeCountry cc, CropType animalType) {
		Map<String, Double> animalRates = new HashMap<String, Double>();
		Map<String, Double> prods = new HashMap<String, Double>();

		for (SingleCountry c : CountryManager.getInstance().getAllForCompositeCountry(cc)) {
			Map<String, AnimalRateData> singleCMap = getAnimalRates(c, animalType);

			for (Entry<String, AnimalRateData> entry : singleCMap.entrySet()) {
				String faoType = entry.getKey();
				double rate = entry.getValue().animalRate;
				double prod = entry.getValue().prod;
				double prevRate = getDoubleOrZero(animalRates.get(faoType));
				double prevProd = getDoubleOrZero(prods.get(faoType));

				// just takes max which isn't right. Should instead weight by production or something else
				double newRate = (rate * prod + prevRate * prevProd ) / (prod + prevProd);
				animalRates.put(faoType, newRate);
				prods.put(faoType, prod + prevProd);
			}
		}
		return animalRates;
	}

	private double getDoubleOrZero(Double d) {
		return (d==null ? 0.0 : d.doubleValue());
	}

	private Map<String, AnimalRateData> getAnimalRates(SingleCountry c, CropType animalType) {
		Map<String, AnimalRateData> rates = new HashMap<String, AnimalRateData>();

		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put("Iso3", c.getCountryCode());
		queryMap.put("plumDemandItem", animalType.getFaoName());

		try {
			List<Map<String, String>> rows = tabularReader.query(queryMap);
			for (Map<String, String> row : rows) {
				String livestockItem = row.get("LivestockItem");
				String rateS = row.get("animalRate");
				Double rate = Double.valueOf(rateS);
				String prodS = row.get("prod");
				Double prod = Double.valueOf(prodS);
				rates.put(livestockItem, new AnimalRateData(rate, prod));
			}
		}
		catch (Exception e) {
			LogWriter.println("Problem getting animal numbers: " + animalType.getFaoName() + ", " + c.getCountryName());
		}

		return rates;
	}
}
