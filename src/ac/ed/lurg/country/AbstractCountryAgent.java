package ac.ed.lurg.country;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.demand.AbstractDemandManager;
import ac.ed.lurg.landuse.CarbonUsageData;
import ac.ed.lurg.landuse.CropUsageData;
import ac.ed.lurg.landuse.WoodUsageData;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;

public abstract class AbstractCountryAgent {

	protected AbstractDemandManager demandManager;
	protected CompositeCountry country;
	protected Map<CropType, Double> tradeBarriers;
	protected Map<CropType, Double> currentFoodDemand;
	protected Map<CropType, Double> currentBioenergyDemand;
	protected Map<CropType, GlobalPrice> currentWorldPrices;
	protected Map<CropType, CountryPrice> currentCountryPrices;
	private Map<CropType, CountryPrice> previousCountryPrices;
	protected GlobalPrice globalWoodPrice;
	protected GlobalPrice globalCarbonPrice;
	protected CountryPrice currentCarbonPrice;
	protected CountryPrice currentWoodPrice;
	protected Timestep currentTimestep;
	protected Map<WoodType, Double> currentWoodDemand;
	protected double currentCarbonDemand;
	protected Map<LandCoverType, Double> currentSolarDemand;
	protected double exportTaxRate;
	
	public AbstractCountryAgent(AbstractDemandManager demandManager,CompositeCountry country, Map<CropType, Double> tradeBarriers) {

		this.demandManager = demandManager;
		this.country = country;
		this.tradeBarriers = tradeBarriers;
	}

	public CompositeCountry getCountry() {
		return country;
	}
	
	protected void setCurrentTimestep(Timestep timestep) {
		currentTimestep = timestep;
	}

	private void calculateCountryPrices(){
		Map<CropType, CountryPrice> countryPrices = new HashMap <>();

		for (CropType c : CropType.getImportedTypes()) {
			GlobalPrice worldPrice = currentWorldPrices.get(c);
			CountryPrice refPrice = currentTimestep.isInitialTimestep() ?
					createCountryPrices(c, worldPrice, null) : currentCountryPrices.get(c);
			CountryPrice prices = createCountryPrices(c, worldPrice, refPrice);
			countryPrices.put(c, prices);
		}
				
		currentCountryPrices = countryPrices;
		currentCarbonPrice = new CountryPrice(globalCarbonPrice); // no trade barriers
		currentWoodPrice = createWoodCountryPrice(globalWoodPrice);
	}
	
	protected void savePreviousProducerCropPrices() {
		previousCountryPrices = currentCountryPrices;
	}

	protected void calculateExportTax() {
		exportTaxRate = 0;
		
		if (previousCountryPrices != null) {
			for (CropType crop : CommodityType.CEREALS_STARCHY_ROOTS.getCropTypes()) {
				double newPrice = currentCountryPrices.get(crop).getConsumerPrice();
				double oldPrice = previousCountryPrices.get(crop).getConsumerPrice();
				double priceChangeRate = (newPrice - oldPrice)/ oldPrice;
				if (priceChangeRate > ModelConfig.EXPORT_TAX_THRESHOLD) {
					exportTaxRate = ModelConfig.EXPORT_TAX_RATE;
					LogWriter.println(String.format("\ncalculateExportTax: Price Spike, %s, %d, %s, %.6f, %.6f, %.2f\n", country, currentTimestep.getTimestep(), crop, oldPrice, newPrice, priceChangeRate*100), 3);
					break;
				}
				else
					LogWriter.println(String.format("calculateExportTax: Price change below threshold, %s, %d, %s, %.6f, %.6f, %.2f", country, currentTimestep.getTimestep(), crop, oldPrice, newPrice, priceChangeRate*100));
			}
		}

		LogWriter.println(String.format("calculateExportTax %s: exportTax is now %s", country, exportTaxRate));
	}
	
	abstract protected CountryPrice createCountryPrices(CropType crop, GlobalPrice worldPrice, CountryPrice refCountryPrice);

	abstract protected CountryPrice createWoodCountryPrice(GlobalPrice worldPrice);
	
	protected void calculateCountryPricesAndDemand(Map<CropType, GlobalPrice> worldPrices, GlobalPrice carbonPrice, GlobalPrice woodPrice, boolean outputGamsDemand) {
		LogWriter.println("\ncalculateCountryPricesAndDemand for " + country + " "+ currentTimestep.getTimestep(), 3);
		currentWorldPrices = worldPrices;
		globalWoodPrice = woodPrice;
		globalCarbonPrice = carbonPrice;
		calculateCountryPrices();

		currentFoodDemand = demandManager.getDemand(country, currentTimestep.getYear(), currentCountryPrices, outputGamsDemand);
		currentBioenergyDemand = demandManager.getBioenergyDemand(country, currentTimestep.getYear());

		Map<WoodType, CountryPrice> woodPrices = new HashMap<>();
		for (WoodType w : WoodType.values()) {
			woodPrices.put(w, currentWoodPrice);
		}
		currentWoodDemand = demandManager.getWoodDemandComposite(country, currentTimestep.getYear(), woodPrices);

		currentCarbonDemand = demandManager.getCarbonDemandComposite(country, currentTimestep.getYear());
		currentSolarDemand = demandManager.getSolarEnergyDemand(country, currentTimestep.getYear());

	}

	public double getCurrentConsumerPrice(CommodityType commodity) {
		double totalCost = 0;
		double totalDemand = 0;
		for (CropType crop : commodity.getCropTypes()) {
			double price = currentCountryPrices.get(crop).getConsumerPrice();
			double demand = currentFoodDemand.get(crop);
			totalCost += price * demand;
			totalDemand += demand;
		}
		return totalCost / totalDemand;
	}

	public Map<CommodityType, Double> getCurrentFoodCommodityDemand() {
		Map<CommodityType, Double> commDemands = new HashMap<>();
		for (CommodityType comm : CommodityType.getAllFoodItems()) {
			double d = 0;
			for (CropType crop : comm.getCropTypes()) {
				d += currentFoodDemand.getOrDefault(crop, 0.0);
			}
			commDemands.put(comm, d);
		}
		return  commDemands;
	}

	public Map<CommodityType, Double> getCurrentBioenergyCommodityDemand() {
		Map<CommodityType, Double> commDemands = new HashMap<>();
		for (CommodityType comm : CommodityType.getAllFoodItems()) {
			double d = 0;
			for (CropType crop : comm.getCropTypes()) {
				d += currentBioenergyDemand.getOrDefault(crop, 0.0);
			}
			commDemands.put(comm, d);
		}
		return  commDemands;
	}

	public Map<CropType, Double> getTotalDemand() {
		Map<CropType, Double> totalDemand = new HashMap<>();
		for (CropType crop : CropType.values()) {
			double d = 0;
			d += currentFoodDemand.getOrDefault(crop, 0.0);
			d += currentBioenergyDemand.getOrDefault(crop, 0.0);
			totalDemand.put(crop, d);
		}

		return  totalDemand;
	}
	
	public Map<CropType, CountryPrice> getCurrentCountryPrices() {
		return currentCountryPrices;
	}

	protected void updateNetImportsFromProdAndDemand(Map<CropType, Double> demands, Map<CropType, CropUsageData> cropUsages) {
		LogWriter.println("AbstractCountryAgent: updateNetImportsFromProdAndDemand for " + country, 3);
		
		for (CropType crop : CropType.getFoodTypes()) {
			double demand = demands.get(crop);
			CropUsageData cropUsage = cropUsages.get(crop);
			double prod =  cropUsage.getProductionExpected() * (1 - crop.getSeedAndWasteRate()) - cropUsage.getMonogastricFeed() - cropUsage.getRuminantFeed();
			double netImports = demand - prod;
			LogWriter.println("Updating net imports " + crop + " to " + netImports, 3);
			cropUsage.updateNetImports(netImports);
		}

	}

	public Map<CropType, Double> getCurrentBioenergyDemand() {
		return currentBioenergyDemand;
	}

	abstract public Map<CropType, CropUsageData> getCropUsageData();
	
	abstract public CarbonUsageData getCarbonUsageData();
	
	abstract public WoodUsageData getWoodUsageData();
	
	public Map<WoodType, CountryPrice> getCurrentCountryWoodPrices() {
		Map<WoodType, CountryPrice> woodPrices = new HashMap<>();
		for (WoodType w : WoodType.values()) {
			woodPrices.put(w, currentWoodPrice);
		}
		return woodPrices;
	}
	
	public CountryPrice getCurrentCountryCarbonPrice() {
		return currentCarbonPrice;
	}

	public double getCurrentCarbonDemand() {
		return currentCarbonDemand;
	}

	public double getCurrentWoodDemand(WoodType woodType) {
		return currentWoodDemand.get(woodType);
	}
	
}