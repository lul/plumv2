package ac.ed.lurg.country;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class SubsidyRateManager {
	private StringTabularReader tabularReader;

	public SubsidyRateManager() {
		tabularReader = new StringTabularReader(",", new String[]{"Iso3", "plumCropItem", "SubsidyPc"});
		tabularReader.read(ModelConfig.SUBSIDY_RATE_FILE);
	}

	public Map<CropType, Double> getSubsidyRates(CompositeCountry cc) {

		Map<CropType, Double> subsidyRates = new HashMap<CropType, Double>();
		for (SingleCountry c : CountryManager.getInstance().getAllForCompositeCountry(cc)) {
			Map<CropType, Double> singleCMap = getSubsidyRates(c);

			for (CropType crop : CropType.getAllItems()) {
				Double newRateD = singleCMap.get(crop);
				Double prevRateD = subsidyRates.get(crop);

				// TODO just takes max which isn't right. Should instead weight by production or something else
				double newRate = Math.max(getDoubleOrZero(newRateD), getDoubleOrZero(prevRateD));
				subsidyRates.put(crop, newRate);
			}
		}
		return subsidyRates;
	}

	private double getDoubleOrZero(Double d) {
		return (d==null ? 0.0 : d.doubleValue());
	}

	private Map<CropType, Double> getSubsidyRates(SingleCountry c) {
		Map<CropType, Double> rates = new HashMap<CropType, Double>();

		if (!ModelConfig.ENABLE_SUBSIDIES) {
			for (CropType crop : CropType.getAllItems()) {
				rates.put(crop, 0.0);
			}
			return rates;
		}

		for (CropType crop : CropType.getAllItems()) {
			Map<String, String> queryMap = new HashMap<String, String>();
			queryMap.put("Iso3", c.getCountryCode());
			queryMap.put("plumCropItem", crop.getFaoName());
			try {
				double subsidy = 0 ;
				List<Map<String, String>> rows = tabularReader.query(queryMap);
				if (rows.size() == 1) {
					String subsidyS = rows.get(0).get("SubsidyPc");
					subsidy = Double.valueOf(subsidyS);
				}
				rates.put(crop, subsidy);
			}
			catch (Exception e) {
					LogWriter.println("Problem finding subsidy rate: " + crop.getFaoName() + ", " + c.getCountryName());
			}
		}
		return rates;
	}
}
