package ac.ed.lurg.country;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;

public class CountryPrice {

	private CropType crop; // TODO wood and carbon?
	private double importPrice;
	private double grossExportPrice;
	private double consumerPrice;
	private double netExportPrice;
	private double referenceConsumerPrice; // for updating demand fractions

	public CountryPrice(CropType crop, GlobalPrice worldPrice, double tradeBarrier, double prodCost, double importWeighting,
						double exportTaxRate, CountryPrice refCountryPrice) {
		this.crop = crop;
		this.importPrice = worldPrice.getCountryImportPrice(tradeBarrier);
		this.grossExportPrice = worldPrice.getExportPrice();
		this.consumerPrice = calcConsumerPrice(prodCost, importWeighting);
		this.netExportPrice = calcProducerNetExportPrice(exportTaxRate);
		this.referenceConsumerPrice = refCountryPrice != null ? refCountryPrice.getConsumerPrice() : 0;
	}

	public CountryPrice(GlobalPrice worldPrice) { // for commodities with no trade costs - carbonCredits TODO handle with AbstractCountryPrice
		this.crop = null;
		this.importPrice = worldPrice.getExportPrice();
		this.grossExportPrice = worldPrice.getExportPrice();
		this.consumerPrice = worldPrice.getExportPrice();
	}

	public double getImportPrice() {
		return importPrice;
	}
	
	public double getExportPrice() {
		return grossExportPrice;
	}
	
	public double getConsumerPrice() {
		return consumerPrice;
	}
	
	public double getProducerNetExportPrice() {
		return netExportPrice;
	}

	public double getReferenceConsumerPrice() {
		return referenceConsumerPrice;
	}

	private double calcConsumerPrice(double prodCost, double importWeighting) {
		double exportOrProdCost = Double.isNaN(prodCost) || prodCost <= 0.0 ? getExportPrice() : prodCost;
		double consumerPrice;
		double importPrice = getImportPrice();

		switch (ModelConfig.PRICE_CALCULATION) {
			case IMPORTONLY:
				consumerPrice = importPrice;
				break;
			case WEIGHTEDIMPEXP:
				consumerPrice = weightPrice(importWeighting, importPrice, getExportPrice());
				break;
			case WEIGHTEDINCLPRODCOSTS:
				consumerPrice = weightPrice(importWeighting, importPrice, exportOrProdCost);
				break;
			default:
				consumerPrice = importPrice;
				LogWriter.printlnWarning("Warning, price calculation type not set, using only import price as default " + consumerPrice);
				break;
		}
		
		LogWriter.println(String.format("CountryPrice:   Crop %s: import %.4f, export %.4f, prodcost %.4f, importWeighting %.4f, newCropPrice %.4f", 
				crop, importPrice, getExportPrice(), prodCost, importWeighting, consumerPrice), 3);
		return consumerPrice;
	}

	private double weightPrice(double weight, double iPrice, double ePrice) {
		if (ePrice > iPrice)
			return iPrice; // consumer price is not be allowed to rise above import price, you could just import rather than buying domestically produced products
		else
			return iPrice*weight + ePrice*(1-weight);
	}
	
	private double calcProducerNetExportPrice(double exportTaxRate) {
		if (ModelConfig.APPLY_EXPORT_TAXES)
			return getExportPrice() / (1 + exportTaxRate);
		else
			return getExportPrice();
	}

	@Override
	public String toString() {
		return "CountryPrice [crop=" + crop + ", importPrice=" + importPrice + ", grossExportPrice=" + grossExportPrice
				+ ", consumerPrice=" + consumerPrice + ", netExportPrice=" + netExportPrice + "]";
	}
}