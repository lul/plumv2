package ac.ed.lurg.country;

import ac.ed.lurg.utils.LogWriter;

public class TradeConstraint {
	private double minValue;
	private double maxValue;

	public TradeConstraint(double min, double max) {
		minValue = min;
		maxValue = max;
	}

	public TradeConstraint(double min, double max, double exportRetriction, Double minLimit, Double maxLimit) {
		LogWriter.print(" TradeConstraint: min=" + min + ", max=" + max + ", exportRetriction=" + exportRetriction + ", minLimit=" + minLimit + ", maxLimit=" + maxLimit, 3);

		if (minLimit != null && min < minLimit) {
			min = minLimit.doubleValue();
			if (min > max)
				max = min;
		}
		
		if (maxLimit != null && max > maxLimit) {
			max = maxLimit.doubleValue();
			if (min > max)
				min = max;
		}
		
		minValue = applyExportRestriction(min, exportRetriction);
		maxValue = applyExportRestriction(max, exportRetriction);
		
		LogWriter.println(" - resulted in minValue=" + minValue + ", maxValue=" + maxValue, 3);
	}
	
	private double applyExportRestriction(double amountBefore, double restrictionRate) {
		if (amountBefore < 0 & restrictionRate > 0) // exporting and an export restriction is in place
			return amountBefore * (1 - restrictionRate);
		else
			return amountBefore;
	}
	
	public double getMinConstraint() {
		return minValue;
	}
	
	public double getMaxConstraint() {
		return maxValue;
	}

	@Override
	public String toString() {
		return "TradeConstraint [minValue=" + minValue + ", maxValue=" + maxValue + "]";
	}
}
