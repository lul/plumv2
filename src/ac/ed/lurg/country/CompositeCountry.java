package ac.ed.lurg.country;

import java.io.Serializable;

public class CompositeCountry implements Serializable {

	private static final long serialVersionUID = -5163523898024757734L;
	
	private String name;
	private String region;
	
	public CompositeCountry (String name, String region) {
		super();
		this.name = name;
		this.region = region;
	}
	
	public String getName() {
		return name;
	}
	
	public String getRegion() {
		return region;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompositeCountry other = (CompositeCountry) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
