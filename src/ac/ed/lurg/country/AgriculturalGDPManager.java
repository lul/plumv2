package ac.ed.lurg.country;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.utils.LogWriter;


public class AgriculturalGDPManager {

	private static final int COUNTRY_COL = 0; 
	private static final int FRACTION_COL = 1; 
	
	private Map<SingleCountry, Double> fractionsMap = new HashMap<SingleCountry, Double>();

	public AgriculturalGDPManager() {
		read();
	}


	public double getProportion(SingleCountry c) {
	LogWriter.println("country in get prop " + c, 3);
		return fractionsMap.get(c);
	}
	
	private void read() {

		String filename = ModelConfig.GDP_FRACTIONS_FILE;
		try {
			BufferedReader fitReader = new BufferedReader(new FileReader(filename)); 
			String line, countryCode;
			Double proportion;
			fitReader.readLine(); // read header

			while ((line=fitReader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 2)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				countryCode = tokens[COUNTRY_COL];
				proportion = Double.parseDouble(tokens[FRACTION_COL]);
				
				SingleCountry country = CountryManager.getInstance().getForCode(countryCode);
				
				fractionsMap.put(country,proportion);
				
			} 
			fitReader.close(); 
		
		} catch (IOException e) {
			LogWriter.printlnError("Failed in reading gdp fractions file");
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename);
	}
	
}
