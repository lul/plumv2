package ac.ed.lurg.country;

import java.util.Map;

import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class CountryBoundaryReader extends AbstractTabularRasterReader<CountryBoundaryItem> {	
	
	private static int MIN_COLS = 2;
	
	public CountryBoundaryReader (RasterSet<CountryBoundaryItem> dataset) {
		super(",", MIN_COLS, dataset);
	}

	@Override
	public void setData(RasterKey key, CountryBoundaryItem item, Map<String, Double> rowValues) {
		int isoCode = (int) getValueForCol(rowValues, "M49");
		SingleCountry c = CountryManager.getInstance().getForM49Code(isoCode);
		item.setCountry(c);
	}
}