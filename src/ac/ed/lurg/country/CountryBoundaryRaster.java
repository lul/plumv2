package ac.ed.lurg.country;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class CountryBoundaryRaster extends RasterSet<CountryBoundaryItem> {

	public CountryBoundaryRaster(RasterHeaderDetails header) {
		super(header);
	}
	
	private static final long serialVersionUID = -8449000692429399253L;
	protected CountryBoundaryItem createRasterData() {
		return new CountryBoundaryItem();
	}
	
	private Map<CompositeCountry, List<RasterKey>> countryMap;
	
	private Map<CompositeCountry, List<RasterKey>> getCountryToKeysMap() {

		if (countryMap == null) {
			countryMap = new HashMap<CompositeCountry, List<RasterKey>>();
	
			for (Map.Entry<RasterKey, CountryBoundaryItem> entry : entrySet()) {
				SingleCountry c = entry.getValue().getCountry();
				if (c == null)
					continue;
	
				CompositeCountry cc = CountryManager.getInstance().getForSingleCountry(c);
				List<RasterKey> keys = countryMap.get(cc);
	
				if (keys == null) {
					keys = new ArrayList<RasterKey>();
					countryMap.put(cc, keys);
				}
				keys.add(entry.getKey());
			}
		}
		return countryMap;
	}

	public List<RasterKey> getKeysFor(CompositeCountry cc) {
		List<RasterKey> keys = getCountryToKeysMap().get(cc);
		if (keys == null)
			keys = new ArrayList<RasterKey>();
		return keys;
	}
}
