package ac.ed.lurg.country;

import ac.sac.raster.RasterItem;

public class CountryBoundaryItem implements RasterItem {

	SingleCountry country;
	
	public SingleCountry getCountry() {
		return country;
	}
	
	public void setCountry(SingleCountry country) {
		this.country = country;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountryBoundaryItem other = (CountryBoundaryItem) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		return true;
	}
}
