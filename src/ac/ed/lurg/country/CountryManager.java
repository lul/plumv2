package ac.ed.lurg.country;

import java.util.*;
import java.util.stream.Collectors;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.utils.StringTabularReader;


public class CountryManager {

	private Map<String, SingleCountry> codeMap = new HashMap<String, SingleCountry>();
	private Map<Integer, SingleCountry> m49CodeMap = new HashMap<Integer, SingleCountry>();
	private Map<SingleCountry, CompositeCountry> mapFromSingleCountry = new HashMap<SingleCountry, CompositeCountry>();
	private Map<CompositeCountry, List<SingleCountry>> mapFromCompositeCountry = new HashMap<CompositeCountry, List<SingleCountry>>();
	private Map<String, CompositeCountry> mapCompositeCountryFromName = new HashMap<>();
	private Set<String> countryExclusionList = new HashSet<>();
	
	private static final CountryManager instance = new CountryManager();
	
	public static CountryManager getInstance() {
		return instance;
	}
	
	private CountryManager() {
		readCountries();
		readExcludedCountries();
	}
	
	private void readCountries() {
		String filename = ModelConfig.COUNTRIES_FILE;
		StringTabularReader reader = new StringTabularReader(",", new String[] {"Iso3", "Area", "M49", "Population", "FaoArea", "PlumGroup", "Region", "IncomeGroup"});
		reader.read(filename);
		List<Map<String, String>> rowList = reader.getRowList();
		for (Map<String, String> row : rowList) {
			String countryName = row.get("Area");
			String countryCode = row.get("Iso3");
			Integer m45Code = Integer.parseInt(row.get("M49"));
			String region = row.get("Region");
			String incomeGroup = row.get("IncomeGroup");
			String plumGroup = row.get("PlumGroup");
			
			SingleCountry sc = new SingleCountry(countryName, countryCode, region, incomeGroup);
			CompositeCountry cc;
			if (ModelConfig.PREDEFINED_COUNTRY_GROUPING) {
				cc = new CompositeCountry(plumGroup, region);
			} else {
				cc = new CompositeCountry(region, region);
			}
			codeMap.put(countryCode, sc);
			m49CodeMap.put(m45Code, sc);
			mapFromSingleCountry.put(sc, cc);
			mapCompositeCountryFromName.put(plumGroup, cc);
			List<SingleCountry> scList = mapFromCompositeCountry.computeIfAbsent(cc, k -> new ArrayList<SingleCountry>());
			scList.add(sc);
		}
	}

	private void readExcludedCountries() {
		if (ModelConfig.EXCLUDE_COUNTRIES_IN_LIST) {
			countryExclusionList = new StringTabularReader(",", new String[] {"Country"}).read(ModelConfig.EXCLUDED_COUNTRIES_FILE)
					.stream().map(m -> m.get("Country")).collect(Collectors.toSet());
		}
	}

	public SingleCountry getForCode(String isoCode) {
		SingleCountry c = codeMap.get(isoCode);
		return c;
	}
	
	public SingleCountry getForM49Code(int m49) {
		SingleCountry c = m49CodeMap.get(m49);
		return c;
	}
	
	public CompositeCountry getForSingleCountry(SingleCountry c) {
		return mapFromSingleCountry.get(c);
	}

	public CompositeCountry getCompositeCountryForName(String name) {
		return mapCompositeCountryFromName.get(name);
	}
	
	public Collection<SingleCountry> getAllForCompositeCountry(CompositeCountry cc) {
		return mapFromCompositeCountry.get(cc);
	}
	
	public Collection<SingleCountry> getAllSingleCountries() {
		return codeMap.values();
	}
	
	public Collection<CompositeCountry> getAllCompositeCountries() {
		return mapFromCompositeCountry.keySet();
	}

	public boolean isCountry(String isoCode) {
		return codeMap.containsKey(isoCode);
	}

	public int getCountryCount() {
		return mapFromCompositeCountry.size();
	}

	public boolean isCountryExcluded(CompositeCountry compositeCountry) {
		return countryExclusionList.contains(compositeCountry.getName());
	}

}
