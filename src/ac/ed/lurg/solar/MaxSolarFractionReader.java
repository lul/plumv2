package ac.ed.lurg.solar;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.landuse.LandCoverItem;
import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.util.Map;

public class MaxSolarFractionReader extends AbstractTabularRasterReader<LandCoverItem> {
    private static final String DELIM = ",";
    private static final int NUM_COL = 1;
    public MaxSolarFractionReader(RasterSet<LandCoverItem> dataset) {
        super(DELIM, NUM_COL, dataset);
    }

    @Override
    protected void setData(RasterKey key, LandCoverItem item, Map<String, Double> rowValues) {
        item.setMaxSolarFraction(Math.min(rowValues.get("maxsolar"), ModelConfig.MAX_SOLAR_FRACTION));
    }
}
