package ac.ed.lurg.solar;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.io.File;
import java.util.Map;

public class SolarPotentialReader extends AbstractTabularRasterReader<SolarPotentialItem> {
    
    private static final int MIN_COL = 3;
    private static final String DELIM = ",";
    private static final double CONVERSION_FACTOR = 10; // convert from kWh/m2 to MWh/ha
    private int year;

    public SolarPotentialReader(RasterSet<SolarPotentialItem> dataset) {
        super(DELIM, MIN_COL, dataset);
    }

    public void readData(Timestep timestep) {
        year = timestep.getYear();

        int dataYear = ModelConfig.IS_CALIBRATION_RUN ? ModelConfig.BASE_YEAR :
                Math.floorDiv(timestep.getYear(), ModelConfig.SOLAR_POTENTIAL_DATA_STEP_SIZE) * ModelConfig.SOLAR_POTENTIAL_DATA_STEP_SIZE;

        String filePath = ModelConfig.SOLAR_POTENTIAL_DIR_BASE + File.separator + ModelConfig.SOLAR_POTENTIAL_DIR_TOP +
                File.separator + dataYear + File.separator + ModelConfig.SOLAR_POTENTIAL_FILENAME;
        getRasterDataFromFile(filePath);
    }
    
    @Override
    protected void setData(RasterKey key, SolarPotentialItem item, Map<String, Double> rowValues) {
        double efficiency = ModelConfig.BASE_PV_EFFICIENCY + (year - ModelConfig.BASE_YEAR) * ModelConfig.PV_EFFICIENCY_GROWTH_RATE;
        double yield = getValueForCol(rowValues, "PVYield") * CONVERSION_FACTOR;
        double lat = dataset.getYCoordin(key);

        item.setData(efficiency, yield, lat);
    }
}
