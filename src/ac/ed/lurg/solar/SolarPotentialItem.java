package ac.ed.lurg.solar;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.LandCoverType;
import ac.sac.raster.RasterItem;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SolarPotentialItem implements RasterItem {
    private double pvPotential; // in MWh/ha
    private Map<LandCoverType, Double> energyDensity = new HashMap<>(); // MWh/ha
    private Map<LandCoverType, Double> installedCost = new HashMap<>(); // $1000/ha
    private Map<LandCoverType, Double> maintenanceCost = new HashMap<>(); // $1000/ha
    private Map<LandCoverType, Double> annualCost = new HashMap<>();
    private double optimalGCR; // optimal Ground Coverage Ratio (PV area) / (land area)
    private double agrivoltaicsGCR = Double.NaN;
    private double pvEfficiency;

    public void setData(double efficiency, double yield, double lat) {
        setPvEfficiency(efficiency);
        setPvPotential(yield);
        calcOptimalGCR(lat);
        calcEnergyDensity();
        calcCostPerHectare();
        calcAnnualCost();
    }

    public void setPvPotential(double pvPotential) {
        this.pvPotential = pvPotential;
    }

    public double getPvPotential() {
        return pvPotential;
    }

    public void calcOptimalGCR(double latitude) {
        // https://doi.org/10.1016/j.solener.2023.04.038
        // Optimal panel spacing and therefore Ground Coverage ratio depends on latitude
        this.optimalGCR = -0.55 / (1 + Math.exp(-0.138 * (Math.abs(latitude) - 43.4))) + 0.71;
        this.agrivoltaicsGCR = ModelConfig.AV_GCR;
    }

    public void calcEnergyDensity() {
        // MWh per ha including spacing between panels (GCR) and additional infrastructure area (GSR)
        energyDensity.put(LandCoverType.PHOTOVOLTAICS, pvPotential * pvEfficiency * optimalGCR * ModelConfig.PV_GSR);
        // Ground Coverage Ratio (GCR) for AV is fixed.
        energyDensity.put(LandCoverType.AGRIVOLTAICS, pvPotential * pvEfficiency * agrivoltaicsGCR * ModelConfig.PV_GSR);
    }

    public void calcCostPerHectare() { // photovoltaics cost $1000/ha
        // Capacity in kW/ha of total land used. Factor 10000 to convert from kW per m2 to hectare.
        double capacityPV = pvEfficiency * optimalGCR * ModelConfig.PV_GSR * 10000;
        installedCost.put(LandCoverType.PHOTOVOLTAICS, capacityPV * ModelConfig.PV_INSTALLED_COST);
        maintenanceCost.put(LandCoverType.PHOTOVOLTAICS, capacityPV * ModelConfig.PV_MAINTENANCE_COST);

        double capacityAV = pvEfficiency * agrivoltaicsGCR * ModelConfig.PV_GSR * 10000;
        installedCost.put(LandCoverType.AGRIVOLTAICS, capacityAV * ModelConfig.PV_INSTALLED_COST * ModelConfig.AV_COST_FACTOR);
        maintenanceCost.put(LandCoverType.AGRIVOLTAICS, capacityAV * ModelConfig.PV_MAINTENANCE_COST * ModelConfig.AV_COST_FACTOR);

    }

    public void calcAnnualCost() {
        for (LandCoverType lc : Arrays.asList(LandCoverType.PHOTOVOLTAICS, LandCoverType.AGRIVOLTAICS)) {
            double cost = getMaintenanceCost(lc) + getInstalledCost(lc) / ModelConfig.PV_LIFESPAN;
            setAnnualCost(lc, cost);
        }
    }

    public double getEnergyDensity(LandCoverType lc) {
        return ModelConfig.IS_PHOTOVOLTAICS_ON || ModelConfig.IS_AGRIVOLTAICS_ON ? energyDensity.getOrDefault(lc, Double.NaN) : 0;
    }

    public void setEnergyDensity(LandCoverType lc,  double energy) {
        energyDensity.put(lc, energy);
    }

    public double getInstalledCost(LandCoverType lc) {
        return installedCost.getOrDefault(lc, 0.0);
    }

    public double getMaintenanceCost(LandCoverType lc) {
        return maintenanceCost.getOrDefault(lc, 0.0);
    }

    public double getAnnualCost(LandCoverType lc) {
        return annualCost.getOrDefault(lc, Double.NaN);
    }

    public void setAnnualCost(LandCoverType lc, double cost) {
        annualCost.put(lc, cost);
    }

    public double getAgrivoltaicsGCR() {
        return agrivoltaicsGCR;
    }

    public void setAgrivoltaicsGCR(double agrivoltaicsGCR) {
        this.agrivoltaicsGCR = agrivoltaicsGCR;
    }

    public void setPvEfficiency(double efficiency) {
        this.pvEfficiency = efficiency;
    }

    public static SolarPotentialItem getDefault(double lat) {
        SolarPotentialItem item = new SolarPotentialItem();
        item.setData(ModelConfig.BASE_PV_EFFICIENCY, 0, lat);
        return item;
    }
}
