package ac.ed.lurg.solar;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.StringTabularReader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AgrivoltaicsYieldManager {

    private final StringTabularReader reader;

    private static AgrivoltaicsYieldManager apm;
    public static AgrivoltaicsYieldManager getInstance() {
        if (apm == null)
            apm = new AgrivoltaicsYieldManager();

        return apm;
    }

    private AgrivoltaicsYieldManager() {
        reader = new StringTabularReader(",", new String[]{"Crop", "ParamA", "ParamB"});
        reader.read(ModelConfig.AGRIVOLTAICS_YIELD_PARAM_FILE);

    }

    private double getParam(CropType crop, String param) {
        Map<String, String> query = new HashMap<>();
        query.put("Crop", crop.getGamsName());
        List<Map<String, String>> queryResult = reader.query(query);
        String result = queryResult.get(0).get(param);
        return Double.parseDouble(result);
    }

    public double getYieldFactor(CropType crop, double avGCR) {
        double shading = avGCR + 0.05;
        return Math.exp(getParam(crop, "ParamA") * shading + getParam(crop, "ParamB") * Math.pow(shading, 2)) * ModelConfig.AV_YIELD_FACTOR;
    }

}
