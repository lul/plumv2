package ac.ed.lurg.output;

import java.io.File;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.landuse.LandUseItem;
import ac.sac.raster.RasterSet;

public abstract class AbstractLandUseOutputer {

	protected RasterSet<LandUseItem> landUseRaster;
	protected int year;

	public AbstractLandUseOutputer(int year, RasterSet<LandUseItem> landUseRaster) {
		this.year = year;
		this.landUseRaster = landUseRaster;
	}

	abstract public void writeOutput();
	
	protected static File getOutputDir(int year) {
		String outputDirName = ModelConfig.OUTPUT_DIR + File.separator + year;
		File outputDir = new File(outputDirName);
		outputDir.mkdirs();
		return outputDir;
	}
}
