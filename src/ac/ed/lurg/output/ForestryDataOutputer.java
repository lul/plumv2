package ac.ed.lurg.output;

import ac.ed.lurg.forestry.WoodYieldItem;
import ac.ed.lurg.forestry.WoodYieldRasterSet;
import ac.ed.lurg.landuse.ForestDetail;
import ac.ed.lurg.landuse.LandKey;
import ac.ed.lurg.landuse.LandUseItem;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.utils.LogWriter;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.io.*;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

public class ForestryDataOutputer extends AbstractLandUseOutputer {
	private final WoodYieldRasterSet woodYieldData;
	
	public ForestryDataOutputer(int year, RasterSet<LandUseItem> landUseRaster, WoodYieldRasterSet woodYieldData) {
		super(year, landUseRaster);
		this.woodYieldData = woodYieldData;
	}

	@Override
	public void writeOutput() {

		File outputDir = getOutputDir(year);
		String outputFileName = outputDir.getPath() + File.separator + "Forestry.txt.gz";
		BufferedWriter outputWriter = null;
		try {
			GZIPOutputStream gzipOS = new GZIPOutputStream(new FileOutputStream(outputFileName));
			outputWriter = new BufferedWriter(new OutputStreamWriter(gzipOS));
			outputWriter.write("Lon Lat Protection RotationIntensity TimberForestArea Yield");
			outputWriter.newLine();
	
			for (Map.Entry<RasterKey, LandUseItem> entry : landUseRaster.entrySet()) {
				RasterKey key = entry.getKey();
				LandUseItem luItem = entry.getValue();
				if (luItem == null)
					continue;
				WoodYieldItem wyItem = woodYieldData.get(key);
				if (wyItem == null)
					continue;

				double lat = landUseRaster.getXCoordin(key);
				double lon = landUseRaster.getYCoordin(key);

				for (LandKey landKey : luItem.getLandKeysForLandCover(LandCoverType.TIMBER_FOREST)) {
					ForestDetail forestDetail = (ForestDetail) luItem.getLandCoverDetail(landKey);
					double rotaInt = forestDetail.getRotationIntensity();
					double area = luItem.getLandCoverArea(landKey);
					if (area == 0) { // skip if no forest
						continue;
					}
					double yield = wyItem.getYield(rotaInt);

					outputWriter.write(String.format("%.2f %.2f %s %.14f %.14f %.14f", lat, lon, landKey.getLpType().getGamsName(), rotaInt, area, yield));
					outputWriter.newLine();
				}
			}
		}
		catch (IOException e) {
			LogWriter.print(e);
		}
		finally {
			if (outputWriter != null) {
				try {
					outputWriter.close();
				} 
				catch (IOException e) {
					LogWriter.print(e);
				}
			}
		}
		

	}
}
