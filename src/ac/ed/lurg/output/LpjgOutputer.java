package ac.ed.lurg.output;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map.Entry;

import ac.ed.lurg.landuse.LandUseItem;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.utils.LogWriter;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class LpjgOutputer extends AbstractLandUseOutputer {

	public LpjgOutputer(int year, RasterSet<LandUseItem> landUseRaster) {
		super(year, landUseRaster);
	}

	@Override
	public void writeOutput() {
		File outputDir = getOutputDir(year);
		writeLandUseFile(outputDir);
		writeIntensity(outputDir);
		writeMarkerFile(year, false);
	}

	interface ItemValueExtractor { double extract(LandUseItem item, CropType cropType); }
	
	private void writeOutputFile(String outputFileName, ItemValueExtractor extractor) {
		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(outputFileName, false));
			outputWriter.write("Lon Lat Year CerealsC3 OilcropsNFix OilcropsOther StarchyRoots Pulses Maize Miscanthus Rice Pasture FruitVeg Sugar");
			outputWriter.newLine();
	
			for (Entry<RasterKey, LandUseItem> entry : landUseRaster.entrySet()) {
				RasterKey key = entry.getKey();
				LandUseItem item = entry.getValue();
				if (item == null)
					continue;
				
				double lat = landUseRaster.getXCoordin(key);
				double lon = landUseRaster.getYCoordin(key);
				
				double c3cereal = extractor.extract(item, CropType.WHEAT);
	
				double maize = extractor.extract(item, CropType.MAIZE);
				double miscanthus = extractor.extract(item, CropType.ENERGY_CROPS);
				double rice = extractor.extract(item, CropType.RICE);
				double oilcropsNFix = extractor.extract(item, CropType.OILCROPS_NFIX);
				double oilcropsOther = extractor.extract(item, CropType.OILCROPS_OTHER);
				double pulses = extractor.extract(item, CropType.PULSES);
				double starchyRoots = extractor.extract(item, CropType.STARCHY_ROOTS);
				double pasture = extractor.extract(item, CropType.PASTURE);
				double fruitVeg = extractor.extract(item, CropType.FRUITVEG);
				double sugar = extractor.extract(item, CropType.SUGAR);
				
				outputWriter.write(String.format("%.2f %.2f %d %.14f %.14f %.14f %.14f %.14f %.14f %.14f %.14f %.14f %.14f %.14f", lat, lon, year,
						c3cereal, oilcropsNFix, oilcropsOther, starchyRoots, pulses, maize, miscanthus, rice, pasture, fruitVeg, sugar));
				outputWriter.newLine();
			}
		}
		catch (IOException e) {
			LogWriter.print(e);
		}
		finally {
			if (outputWriter != null) {
				try {
					outputWriter.close();
				} 
				catch (IOException e) {
					LogWriter.print(e);
				}
			}
		}
	}
	
	protected void writeIntensity(File outputDir) {
		ItemValueExtractor irrigExtractor = new ItemValueExtractor() {
			@Override
			public double extract(LandUseItem item, CropType cropType) {
				return item.getAverageIrrigationIntensity(cropType);
			}
		};
		ItemValueExtractor fertExtractor = new ItemValueExtractor() {
			@Override
			public double extract(LandUseItem item, CropType cropType) {
				return item.getAverageFertiliserRate(cropType);
			}
		};

		writeOutputFile(outputDir.getPath() + File.separator + "Irrig.txt", irrigExtractor);
		writeOutputFile(outputDir.getPath() + File.separator + "Fert.txt", fertExtractor);
	}

	public static void writeMarkerFile(int year, boolean errorStatus) {
		File outputDir = getOutputDir(year);

		File markerFile = new File(outputDir.getPath() + File.separator + (errorStatus ? "error" : "done"));
		
		try {
			if (!markerFile.exists())
				new FileOutputStream(markerFile).close();
			markerFile.setLastModified(System.currentTimeMillis());
		}
		catch (IOException e) {
			LogWriter.printlnError("Problem writing marker file");
		}	
	}

	protected void writeLandUseFile(File outputDir) {
		ItemValueExtractor cropFracExtractor = new ItemValueExtractor() {
			@Override
			public double extract(LandUseItem item, CropType cropType) {
				return item.getAverageCropFraction(cropType);
			}
		};
		writeOutputFile(outputDir.getPath() + File.separator + "CropFract.txt", cropFracExtractor);
		
		BufferedWriter landCoverWriter = null;
		try {
			String landCoverFileName = outputDir.getPath() + File.separator + "LandCoverFract.txt";
			landCoverWriter = new BufferedWriter(new FileWriter(landCoverFileName, false));
			landCoverWriter.write("Lon Lat Year CROPLAND PASTURE TIMBER CARBON NATURAL BARREN URBAN");
			landCoverWriter.newLine();

			for (Entry<RasterKey, LandUseItem> entry : landUseRaster.entrySet()) {
				RasterKey key = entry.getKey();
				LandUseItem item = entry.getValue();
				double lat = landUseRaster.getXCoordin(key);
				double lon = landUseRaster.getYCoordin(key);

				double expectedArea = landUseRaster.getAreaMha(key);
				double area = item.getTotalLandArea();
				
				if (area > 0 && Math.abs((expectedArea-area)/expectedArea) > 0.01) {  // zero area, due to data not being found, already reported so ignored here
					LogWriter.printlnError("Land cover areas look strange " + key + ": expected=" + expectedArea + ", actual=" + area);
				}
				
				double crop = item.getLandCoverFract(LandCoverType.CROPLAND);
				double pasture = item.getLandCoverFract(LandCoverType.PASTURE);
				double timberForest = item.getLandCoverFract(LandCoverType.TIMBER_FOREST);
				double carbonForest = item.getLandCoverFract(LandCoverType.CARBON_FOREST);
				double otherNatural = item.getLandCoverFract(LandCoverType.NATURAL);
				double barren = item.getLandCoverFract(LandCoverType.BARREN);
				double urban = item.getLandCoverFract(LandCoverType.URBAN);
				landCoverWriter.write(String.format("%.2f %.2f %d %.14f %.14f %.14f %.14f %.14f %.14f %.14f", lat, lon, year, crop, pasture, timberForest, carbonForest, otherNatural, barren, urban));
				landCoverWriter.newLine();
			}
		}
		catch (IOException e) {
			LogWriter.print(e);
		}
		finally {
			if (landCoverWriter != null) {
				try {
					landCoverWriter.close();
				} 
				catch (IOException e) {
					LogWriter.print(e);
				}
			}
		}
	}
}
