package ac.ed.lurg.output;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Map.Entry;
import java.util.zip.GZIPOutputStream;

import ac.ed.lurg.landuse.*;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.FarmingType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.LandProtectionType;
import ac.ed.lurg.utils.LogWriter;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class LandUseOutputer extends AbstractLandUseOutputer {

	public LandUseOutputer(int year, RasterSet<LandUseItem> landUseRaster) {
		super(year, landUseRaster);
	}

	@Override
	public void writeOutput() {
		File outputDir = getOutputDir(year);
		BufferedWriter fertWriter = null;
		
		try {
			String landCoverFileName = outputDir.getPath() + File.separator + "LandUse.txt.gz";
			GZIPOutputStream gzipOS = new GZIPOutputStream(new FileOutputStream(landCoverFileName));
			fertWriter = new BufferedWriter(new OutputStreamWriter(gzipOS, StandardCharsets.UTF_8));

			fertWriter.write("Lon Lat Type Crop A FI FQ II IQ OI Y");
			fertWriter.newLine();

			for (Entry<RasterKey, LandUseItem> entry : landUseRaster.entrySet()) {
				RasterKey key = entry.getKey();
				LandUseItem item = entry.getValue();
				double lon = landUseRaster.getXCoordin(key);
				double lat = landUseRaster.getYCoordin(key);

				for (LandKey landKey : item.getAgricultureLandKeys()) {
					for (CropType crop : CropType.getNonMeatTypes()) {

						AgricultureDetail agricultureDetail = (AgricultureDetail) item.getLandCoverDetail(landKey);
						Intensity intensity = agricultureDetail.getIntensity(crop);
						double cropArea = agricultureDetail.getCropArea(crop);
						if (cropArea == 0) {
							continue;
						}

						ArrayList<Double> cropData = new ArrayList<>();
						cropData.add(cropArea);
						cropData.add(intensity==null ? 0.0 : intensity.getFertiliserIntensity());
						cropData.add(intensity==null ? 0.0 : intensity.getFertiliserRate());
						cropData.add(intensity==null ? 0.0 : intensity.getIrrigationIntensity());
						cropData.add(intensity==null ? 0.0 : intensity.getIrrigationRate());
						cropData.add(intensity==null ? 0.0 : intensity.getOtherIntensity());
						cropData.add(intensity==null ? 0.0 : intensity.getYield());

						String farmingType = "ERROR";
						if (landKey.getLcType().equals(LandCoverType.AGRIVOLTAICS)) {
							farmingType = FarmingType.AGRIVOLTAICS.getName();
						} else if (landKey.getLpType().equals(LandProtectionType.PROTECTED)) {
							farmingType = FarmingType.RESTRICTED.getName();
						} else if (landKey.getLpType().equals(LandProtectionType.UNPROTECTED)) {
							farmingType = FarmingType.CONVENTIONAL.getName();
						}

						StringBuilder sbData = new StringBuilder(String.format("%.2f %.2f %s %s", lon, lat,
								farmingType, crop.getGamsName()));

						for (Double d : cropData) {
							String formatStr = d > 0 ? " %.8f" : " %.0f";
							sbData.append(String.format(formatStr, d));
						}

						fertWriter.write(sbData.toString());
						fertWriter.newLine();
					}
				}
			}
		}
		catch (IOException e) {
			LogWriter.print(e);
		}
		finally {
			if (fertWriter != null) {
				try {
					fertWriter.close();
				} 
				catch (IOException e) {
					LogWriter.print(e);
				}
			}
		}
		
	}

	public void writeLandCoverFile() {
		File outputDir = getOutputDir(year);
		BufferedWriter writer;

		try {
			String fileName = outputDir.getPath() + File.separator + "LandCover.txt.gz";
			GZIPOutputStream gzipOS = new GZIPOutputStream(new FileOutputStream(fileName));
			writer = new BufferedWriter(new OutputStreamWriter(gzipOS));
			writer.write("Lon Lat Protection TotalArea Cropland Pasture TimberForest CarbonForest UnmanagedForest OtherNatural Photovoltaics Agrivoltaics Barren Urban");
			writer.newLine();

			for (Entry<RasterKey, LandUseItem> entry : landUseRaster.entrySet()) {
				RasterKey key = entry.getKey();
				LandUseItem item = entry.getValue();
				double lon = landUseRaster.getXCoordin(key);
				double lat = landUseRaster.getYCoordin(key);

				for (LandProtectionType lpType : LandProtectionType.values()) {
					StringBuilder sbData = new StringBuilder(String.format("%.2f %.2f %s", lon, lat, lpType.getGamsName()));
					appendData(sbData, item.getLandCoverArea(lpType));
					appendData(sbData, item.getLandCoverArea(new LandKey(LandCoverType.CROPLAND, lpType)));
					appendData(sbData, item.getLandCoverArea(new LandKey(LandCoverType.PASTURE, lpType)));
					appendData(sbData, item.getLandCoverArea(new LandKey(LandCoverType.TIMBER_FOREST, lpType)));
					appendData(sbData, item.getLandCoverArea(new LandKey(LandCoverType.CARBON_FOREST, lpType)));
					double totalNatural = item.getLandCoverArea(new LandKey(LandCoverType.NATURAL, lpType));
					NaturalDetail detail = (NaturalDetail) item.getLandCoverDetail(new LandKey(LandCoverType.NATURAL, lpType));
					double unmanagedForestFraction = detail.getUnmanagedForestFraction();
					appendData(sbData, totalNatural * unmanagedForestFraction);
					appendData(sbData, totalNatural * (1 - unmanagedForestFraction));
					appendData(sbData, item.getLandCoverArea(new LandKey(LandCoverType.PHOTOVOLTAICS, lpType)));
					appendData(sbData, item.getLandCoverArea(new LandKey(LandCoverType.AGRIVOLTAICS, lpType)));
					appendData(sbData, item.getLandCoverArea(new LandKey(LandCoverType.BARREN, lpType)));
					appendData(sbData, item.getLandCoverArea(new LandKey(LandCoverType.URBAN, lpType)));

					writer.write(sbData.toString());
					writer.newLine();
				}
			}
			writer.close();
		} catch (IOException e) {
			LogWriter.print(e);
		}
	}

	private void appendData(StringBuilder sbData, double value) {
		sbData.append(String.format(value != 0 ? " %.8f" : " %.0f", value));
	}
}
