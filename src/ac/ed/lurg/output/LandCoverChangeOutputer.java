package ac.ed.lurg.output;

import ac.ed.lurg.landuse.LandUseItem;
import ac.ed.lurg.landuse.LandChangeKey;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.LandProtectionType;
import ac.ed.lurg.utils.LogWriter;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.io.*;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

public class LandCoverChangeOutputer extends AbstractLandUseOutputer {

    public LandCoverChangeOutputer(int year, RasterSet<LandUseItem> landUseRaster) {
        super(year, landUseRaster);
    }

    @Override
    public void writeOutput() {
        File outputDir = getOutputDir(year);
        BufferedWriter outWriter = null;

        try {
            String filename = outputDir.getPath() + File.separator + "LandCoverChange.txt.gz";
            GZIPOutputStream gzipOS = new GZIPOutputStream(new FileOutputStream(filename));
            outWriter = new BufferedWriter(new OutputStreamWriter(gzipOS));
            outWriter.write("Lon Lat FromLandCover ToLandCover ProtectionType Area"); // header
            outWriter.newLine();

            for (RasterKey key : landUseRaster.keySet()) {
                LandUseItem luItem = landUseRaster.get(key);
                double lon = landUseRaster.getXCoordin(key);
                double lat = landUseRaster.getYCoordin(key);

                Map<LandChangeKey, Double> lccMap = luItem.getLandCoverChanges();
                if (!lccMap.isEmpty()) {
                    for (Map.Entry<LandChangeKey, Double> entry : lccMap.entrySet()) {
                        LandCoverType fromLc = entry.getKey().getFromLandKey().getLcType();
                        LandCoverType toLc = entry.getKey().getToLandKey().getLcType();
                        LandProtectionType lpType = entry.getKey().getFromLandKey().getLpType();
                        double area = entry.getValue();
                        outWriter.write(String.format("%.2f %.2f %s %s %s %.8f", lon, lat, fromLc.getName(), toLc.getName(), lpType.getGamsName(), area));
                        outWriter.newLine();
                    }
                }
            }

        } catch (IOException e) {
            LogWriter.print(e);
        } finally {
            if (outWriter != null) {
                try {
                    outWriter.close();
                }
                catch (IOException e) {
                    LogWriter.print(e);
                }
            }
        }
    }
}
