package ac.ed.lurg;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.Map.Entry;

import ac.ed.lurg.country.AbstractCountryAgent;
import ac.ed.lurg.country.GlobalPrice;
import ac.ed.lurg.country.PriceCapManager;
import ac.ed.lurg.country.StockReader;
import ac.ed.lurg.demand.CarbonDemandManager;
import ac.ed.lurg.landuse.CarbonUsageData;
import ac.ed.lurg.landuse.CropUsageData;
import ac.ed.lurg.landuse.WoodUsageData;
import ac.ed.lurg.shock.PriceShockManager;
import ac.ed.lurg.types.CropToDoubleMap;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;

public class InternationalMarket {

	private Map<CropType, GlobalPrice> worldPrices;
	private GlobalPrice carbonPrice;
	private GlobalPrice woodPrice;
	private PriceShockManager priceShockManager;
	private PriceCapManager priceCapManager;
	private CarbonDemandManager carbonDemandManager;

	@SuppressWarnings("unchecked")
	public InternationalMarket() {
		if(ModelConfig.IS_CALIBRATION_RUN) {
			worldPrices = new HashMap<CropType, GlobalPrice>();


			Map<CropType, Double> stockLevel = getInitialStockLevels();

			for (CropType crop : CropType.getImportedTypes()) {
				Double initialStock = stockLevel.get(crop);
				worldPrices.put(crop, GlobalPrice.createInitial(crop.getInitialPrice(), initialStock));
			}
			
			carbonPrice = GlobalPrice.createInitial(ModelConfig.INIT_CARBON_PRICE, ModelConfig.INIT_CARBON_STOCK);

			woodPrice = GlobalPrice.createInitial(ModelConfig.INIT_WOOD_PRICE, ModelConfig.INIT_WOOD_STOCK);

		}
		else {
			List<Object> deserializedPrices = deserializeGlobalPrice();
			worldPrices = (Map<CropType, GlobalPrice>) deserializedPrices.get(0);
			//carbonPrice = (GlobalPrice) deserializedPrices.get(1); TODO
			carbonPrice = GlobalPrice.createInitial(ModelConfig.INIT_CARBON_PRICE, ModelConfig.INIT_CARBON_STOCK);
			woodPrice = (GlobalPrice) deserializedPrices.get(2);
						
			if (ModelConfig.RESET_ENERGYCROP_PRICE) {
				double previousExportPrice = worldPrices.get(CropType.ENERGY_CROPS).getExportPrice();
				GlobalPrice newPrice = worldPrices.get(CropType.ENERGY_CROPS).createPriceAdjustedByFactor(ModelConfig.INITIAL_PRICE_ENERGYCROPS / previousExportPrice);
				worldPrices.put(CropType.ENERGY_CROPS, newPrice);
			}
		}
		
		priceShockManager = new PriceShockManager();
		priceCapManager = new PriceCapManager();
		if (ModelConfig.IS_CARBON_ON) carbonDemandManager = new CarbonDemandManager(); // already in AbstractDemandManager
	}

	public  Map<CropType, GlobalPrice> getWorldPrices() {
		return worldPrices;
	}
	
	public GlobalPrice getCarbonPrice() {
		if (ModelConfig.IS_CARBON_ON) {
			return carbonPrice;
		} else {
			return carbonPrice.createPriceAdjustedByFactor(0); // set price to 0 but keep export/import amounts for reference
		}
	}
	
	public GlobalPrice getWoodPrice() {
		return woodPrice;
	}

	private Map<CropType, Double> getInitialStockLevels() {
		Map<CropType, Double> initialStockLevels = new StockReader().read();
		return initialStockLevels;
	}
	
	void resetStocks() {
		Map<CropType, Double> initialStockLevels = getInitialStockLevels();
		for (Entry<CropType, GlobalPrice> entry : worldPrices.entrySet()) {
			Double initialStock = initialStockLevels.get(entry.getKey());
			if (initialStock != null) {
				entry.getValue().setStockToTargetLevel(entry.getKey().getStockToUseRatio());
			}
		}

		woodPrice.setStockToTargetLevel(ModelConfig.DEFAULT_STOCK_USE_RATIO);

		carbonPrice.setStockToTargetLevel(ModelConfig.DEFAULT_STOCK_USE_RATIO);
	}

	void determineInternationalTrade(Collection<AbstractCountryAgent> countryAgents, Timestep timestep) {
		CropToDoubleMap totalImportCommodities = new CropToDoubleMap();
		CropToDoubleMap totalExportCommodities = new CropToDoubleMap();
		CropToDoubleMap totalProduction = new CropToDoubleMap();
		for (AbstractCountryAgent ca : countryAgents) {

			// Get values for world input costs
			Map<CropType, CropUsageData> cropUsage = ca.getCropUsageData();

			for (Entry<CropType, CropUsageData> entry : cropUsage.entrySet()) {
				CropType c = entry.getKey();

				double countryNetImports = entry.getValue().getShockedNetImports();
				totalProduction.incrementValue(c, (entry.getValue().getProductionExpected()-entry.getValue().getProductionShock()));

				if (countryNetImports > 0)
					totalImportCommodities.incrementValue(c, countryNetImports);
				else
					totalExportCommodities.incrementValue(c, -countryNetImports);
			}
		}

		// Look at trade balance and adjust appropriately
		for (CropType crop : CropType.getImportedTypes()) {

			GlobalPrice prevPrice = worldPrices.get(crop);
			double imports = totalImportCommodities.containsKey(crop) ? totalImportCommodities.get(crop) : 0.0;
			double exportsBeforeTransportLosses = totalExportCommodities.containsKey(crop) ? totalExportCommodities.get(crop) : 0.0;
			LogWriter.println(timestep.getYear() + " Updating " + crop.getGamsName() + " prices", 2);
			GlobalPrice adjustedPrice = prevPrice.createWithUpdatedMarketPrices(imports, exportsBeforeTransportLosses,
					timestep, totalProduction.get(crop), true, crop.getStockToUseRatio());
			LogWriter.println( String.format("Price for %s updated from %s \nto %s \n", crop.getGamsName(), prevPrice, adjustedPrice), 2);
			if (adjustedPrice.getStockLevel() < 0)
				LogWriter.println("Global stocks are below zero" + crop.getGamsName() + ", " + timestep.getYear(), 2);

			worldPrices.put(crop, adjustedPrice);
		}
		
		// Update carbon price
		if (ModelConfig.IS_CARBON_ON) {
			double totalCarbonImport = 0;
			double totalCarbonExport = 0;
			double totalCarbonSequestered = 0;

			for (AbstractCountryAgent ca : countryAgents) {
				CarbonUsageData carbonUsage = ca.getCarbonUsageData();
				totalCarbonSequestered += carbonUsage.getCarbonCredits();
				double netCarbonFlux = carbonUsage.getNetCarbonImport();
				if (netCarbonFlux >= 0)
					totalCarbonImport += netCarbonFlux;
				else
					totalCarbonExport -= netCarbonFlux;
			}

			// Countries not importing explicitly so making imports equal global demand
			if (ModelConfig.CARBON_DEMAND_METHOD.equals("global")) {
				totalCarbonImport = carbonDemandManager.getGlobalCarbonDemand(timestep.getYear());
			}

			totalCarbonSequestered = Math.max(totalCarbonSequestered, 0.0000001); // avoid division by 0
			GlobalPrice prevCPrice = carbonPrice;
			LogWriter.println(timestep.getYear() + " Updating carbon price", 2);
			GlobalPrice adjustedCPrice;

			if (ModelConfig.CARBON_DEMAND_METHOD.equals("price")) { // price set exogenously
				double newPrice = carbonDemandManager.getGlobalCarbonPrice(timestep.getYear());
				adjustedCPrice = prevCPrice.createWithSetPrice(newPrice, totalCarbonImport, totalCarbonExport, timestep, totalCarbonSequestered);
			} else {
				adjustedCPrice = prevCPrice.createWithUpdatedMarketPrices(totalCarbonImport, totalCarbonExport, timestep,
						totalCarbonSequestered, false, ModelConfig.DEFAULT_STOCK_USE_RATIO);
			}

			LogWriter.println( String.format("Price for carbon updated from %s \nto %s \n", prevCPrice, adjustedCPrice), 2);
			if (adjustedCPrice.getStockLevel() < 0)
				LogWriter.println("Global stocks are below zero carbon, " + timestep.getYear(), 2);
			carbonPrice = adjustedCPrice;

		}

		// Update timber price
		if (ModelConfig.IS_FORESTRY_ON) {
			double totalWoodImport = 0;
			double totalWoodExport = 0;
			double totalWoodProduction = 0;

			for (AbstractCountryAgent ca : countryAgents) {
				WoodUsageData woodUsage = ca.getWoodUsageData();
				totalWoodProduction += woodUsage.getProduction();
				double netImport = woodUsage.getNetImport();
				if (netImport >= 0) {
					totalWoodImport += netImport;
				} else {
					totalWoodExport -= netImport;
				}
			}
			totalWoodProduction = Math.max(totalWoodProduction, 0.0000001); // avoid division by 0
			GlobalPrice prevTPrice = woodPrice;
			LogWriter.println(timestep.getYear() + " Updating wood price", 2);
			GlobalPrice adjustedTPrice = prevTPrice.createWithUpdatedMarketPrices(totalWoodImport, totalWoodExport,
					timestep, totalWoodProduction, true, ModelConfig.DEFAULT_STOCK_USE_RATIO);
			LogWriter.println( String.format("Price for wood updated from %s \nto %s \n", prevTPrice, adjustedTPrice), 2);
			if (adjustedTPrice.getStockLevel() < 0)
				LogWriter.println("Global stocks are below zero wood, " + timestep.getYear(), 2);
			woodPrice = adjustedTPrice;

		}

		// Cap prices
		//capPrices();
	}

	void writeGlobalMarketFile(Timestep timestep, BufferedWriter outputFile) throws IOException {
		for (CropType crop : CropType.getImportedTypes()) {
			StringBuffer sbData = new StringBuffer();
			GlobalPrice priceQuantity = worldPrices.get(crop);
			sbData.append(String.format("%d,%s", timestep.getYear(), crop.getGamsName()));
			sbData.append(String.format(",%.1f,%.1f", priceQuantity.getImportAmount(), priceQuantity.getExportsAfterTransportLosses()));
			sbData.append(String.format(",%.8f,%.3f", priceQuantity.getExportPrice(), priceQuantity.getStockLevel()));

			outputFile.write(sbData.toString());
			outputFile.newLine();
		}
		// Carbon price
		{
			StringBuffer sbData = new StringBuffer();
			sbData.append(String.format("%d,%s", timestep.getYear(), "carbon"));
			sbData.append(String.format(",%.1f,%.1f", carbonPrice.getImportAmount(), carbonPrice.getExportsAfterTransportLosses()));
			sbData.append(String.format(",%.8f,%.3f", carbonPrice.getExportPrice(), carbonPrice.getStockLevel()));
	
			outputFile.write(sbData.toString());
			outputFile.newLine();
		}
		// Timber price
		{
			StringBuffer sbData = new StringBuffer();
			sbData.append(String.format("%d,%s", timestep.getYear(), "wood"));
			sbData.append(String.format(",%.1f,%.1f", woodPrice.getImportAmount(), woodPrice.getExportsAfterTransportLosses()));
			sbData.append(String.format(",%.8f,%.3f", woodPrice.getExportPrice(), woodPrice.getStockLevel()));
	
			outputFile.write(sbData.toString());
			outputFile.newLine();
		}
	}

	public void serializeGlobalPrices() {
		List<Object> pricesToSerialize = new ArrayList<Object>();
		pricesToSerialize.add(worldPrices);
		pricesToSerialize.add(carbonPrice);
		pricesToSerialize.add(woodPrice);

		try {
			String fileStr = ModelConfig.IS_CALIBRATION_RUN ? ModelConfig.SERIALIZED_INTERNATIONAL_MARKET_FILE : ModelConfig.CHECKPOINT_INTERNATIONAL_MARKET_FILE;
			LogWriter.println("Starting serializing GlobalPrice to " + fileStr, 2);
			FileOutputStream fileOut = new FileOutputStream(fileStr);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(pricesToSerialize);
			out.close();
			fileOut.close();
			LogWriter.println("Serialized international market data is saved", 2);
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private List<Object> deserializeGlobalPrice() {
		try {
			List<Object> initGlobalPrices;
			FileInputStream fileIn = new FileInputStream(ModelConfig.SERIALIZED_INTERNATIONAL_MARKET_FILE);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			initGlobalPrices = (List<Object>) in.readObject();
			in.close();
			fileIn.close();
			LogWriter.println("Deserialized " + ModelConfig.SERIALIZED_INTERNATIONAL_MARKET_FILE, 2);
			return initGlobalPrices;
		} catch (IOException i) {
			LogWriter.printlnError("Problem deserializing " + ModelConfig.SERIALIZED_INTERNATIONAL_MARKET_FILE);
			LogWriter.print(i);
			return null;
		} catch (ClassNotFoundException c) {
			LogWriter.printlnError("GlobalPrice class not found");
			c.printStackTrace();
			return null;
		}
	}

	public void applyPriceShocks(Timestep timestep) {
		Map<CropType, Double> shocks = priceShockManager.getShocks(timestep);
		for (Map.Entry<CropType, Double> entry : shocks.entrySet()) {
			CropType crop = entry.getKey();
			GlobalPrice preShock = worldPrices.get(crop);
			double factor = 1.0 + entry.getValue();
			GlobalPrice adjustedPrice = preShock.createPriceAdjustedByFactor(factor);
			LogWriter.println(String.format("applyPriceShocks: %s adjusting price by %.4f to export price %.2f", crop.getFaoName(), factor, adjustedPrice.getExportPrice()), 2);
			worldPrices.put(crop, adjustedPrice);
		}
	}

	public boolean negativeStockLevelsExist() {
		for (Map.Entry<CropType, GlobalPrice> entry : worldPrices.entrySet()) {
			double stocklevel = entry.getValue().getStockLevel();
			if (stocklevel < 0 && !entry.getKey().equals(CropType.ENERGY_CROPS)) {
				LogWriter.println(String.format("negativeStockLevelsExist: %s has negative stock %.3f", entry.getKey().getFaoName(), stocklevel), 2);
				return true;
			}
		}
		LogWriter.println("No negative stocks found", 2);
		return false;
	}
	
	public void capPrices() {
		Map<CropType, GlobalPrice> cappedCropPrices = priceCapManager.capCropPrices(worldPrices);
		worldPrices.putAll(cappedCropPrices);
		
		GlobalPrice updatedWoodPrice = priceCapManager.capWoodPrices(woodPrice);
		woodPrice = updatedWoodPrice;
		
		GlobalPrice updatedCarbonPrice = priceCapManager.capCarbonPrices(carbonPrice);
		carbonPrice = updatedCarbonPrice;
	}

	/*	public double findMaxPriceDiff(Map<CropType, Double> previousExportPrices) {
		if (previousExportPrices == null)
			return Double.MAX_VALUE;

		double maxSoFar = 0;
		CropType crop = null;

		for (Entry<CropType, GlobalPrice> entry : worldPrices.entrySet()) {
			Double previousP = previousExportPrices.get(entry.getKey());

			if (previousP == null)
				return Double.MAX_VALUE;

			double diffThisTime = Math.abs(previousP - entry.getValue().getExportPrice());
			if (diffThisTime > maxSoFar) {
				maxSoFar = diffThisTime;
				crop = entry.getKey();
			}
		}

		LogWriter.println("findMaxPriceDiff: found " + maxSoFar + " for " + crop);
		return maxSoFar;
	} */
}
