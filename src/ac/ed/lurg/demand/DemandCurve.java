package ac.ed.lurg.demand;

import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.ModelFitType;


public class DemandCurve {

	private double a;
	private double b;
	private double c;
	private ModelFitType fitType;
	private CommodityType commodityType;

	DemandCurve(CommodityType commodityType, double a, double b, double c, ModelFitType fitType) {
		super();
		this.commodityType = commodityType;
		this.a = a;
		this.b = b;
		this.c = c;
		this.fitType = fitType;
	}
	
	public double getA() {
		return a;
	}

	public double getB() {
		return b;
	}

	public double getC() {
		return c;
	}

	public ModelFitType getFitType() {
		return fitType;
	}

	public CommodityType getCommodityType() {
		return commodityType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fitType == null) ? 0 : fitType.hashCode());
		result = prime * result + ((commodityType == null) ? 0 : commodityType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DemandCurve other = (DemandCurve) obj;
		if (fitType == null) {
			if (other.fitType != null)
				return false;
		} else if (!fitType.equals(other.fitType))
			return false;
		if (commodityType == null) {
			if (other.commodityType != null)
				return false;
		} else if (!commodityType.equals(other.commodityType))
			return false;
		return true;
	}
		
	double getProjectConsumptionPc(double gdpPc) {
		double cpc;
		if (fitType == ModelFitType.LOGISTIC)
			cpc = a/(1+Math.exp((b-gdpPc)/c));
		
		else {
			double l = Math.log(gdpPc);
			cpc = a + b * l + c * l * l;
		}
		
		return cpc < 0 ? 0.0 : cpc;
	}
}
