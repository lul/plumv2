package ac.ed.lurg.demand;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CountryPrice;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.country.gams.*;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodCommodityType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.FileWriterHelper;
import ac.ed.lurg.utils.LogWriter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ElasticDemandManager extends AbstractSSPDemandManager {

	private static final long serialVersionUID = -8605798315853420916L;
    private transient Map<SingleCountry, Map<String, GamsDemandOutput>> previousDecileDemands;
	private final Map<SingleCountry, Map<CommodityType, Double>> baseCpcCache;
	private final Map<SingleCountry, Map<WoodCommodityType, Double>> baseWoodCpcCache;
	private transient Map<SingleCountry, GamsDemandOutput> previousGamsDemands;
	private transient Map<SingleCountry, GamsWoodDemandOutput> previousGamsWoodDemands;
	private transient BufferedWriter outputFile;
	private transient Object lock;
	private final PriceMarkUps priceMarkups;
	private transient IncomeDistributionManager incomeDistributionManager;
	/** Constructor used in calibration only */
	public ElasticDemandManager(String ssp_scenario, BaseConsumpManager baseConsumpManager, CalorieManager calorieManager) {
		super(ssp_scenario, baseConsumpManager, calorieManager);

		this.priceMarkups = new PriceMarkUps();
		baseWoodCpcCache = new HashMap<>();
		baseCpcCache = new HashMap<SingleCountry, Map<CommodityType, Double>>();
		setup();
	}
	
	protected void setup() {
		previousGamsWoodDemands = new HashMap<>();
		if(ModelConfig.IS_INEQUALITY_ON){
			previousDecileDemands = new HashMap<>();
			incomeDistributionManager = new IncomeDistributionManager();
		} else {
			previousGamsDemands = new HashMap<>();
		}
		try {
			if(ModelConfig.IS_INEQUALITY_ON) {
				outputFile = FileWriterHelper.getFileWriter(true, true, ModelConfig.DEMAND_OPTIMISATION_OUTPUT_FILE, getDecileDemandOutputsHeader());
			} else {
				outputFile = FileWriterHelper.getFileWriter(true, true, ModelConfig.DEMAND_OPTIMISATION_OUTPUT_FILE, getDamsDemandOutputsHeader());
			}
		} catch (IOException e) {
			LogWriter.print(e);
		}

		lock = new Object();
	}

	public void setup(String ssp_scenario, BaseConsumpManager baseConsumpManager, CalorieManager calorieManager) {
		super.setup(ssp_scenario, baseConsumpManager, calorieManager);
		setup();
	}
	
	@Override
	 Map<CropType, Double> getFoodDemandMap(SingleCountry c, int year, double gdpPc, double population,
												   Map<CropType, CountryPrice> countryPrices, boolean outputGamsDemand) {

		boolean initYear = ModelConfig.BASE_YEAR == year;

		//calculate prices in $ per 2000 kcal per day
		Map<CommodityType, Double> producerPrices = getCommodityCaloriePrices(c, countryPrices);

		int inequality_year = ModelConfig.CHANGE_INEQUALITY_YEAR ? year : ModelConfig.BASE_YEAR;
        int demandYear = ModelConfig.CHANGE_DEMAND_YEAR ? year : ModelConfig.BASE_YEAR;

        // Cap income to avoid extrapolating beyond the fitted data
        gdpPc = Math.min(gdpPc, ModelConfig.DEMAND_INCOME_CAP);

        //calculate consumer prices from producer prices
        Map<CommodityType, Double> consumerPrices = calculateConsumerPrices(producerPrices, c, gdpPc);

        //run the projections with or without income groups disaggregated
        if (ModelConfig.IS_INEQUALITY_ON) {
            Map<String, Double> gdpByDecile = incomeDistributionManager.getGdpPc(gdpPc * population * (Math.pow(10, 6)), population * (Math.pow(10, 6)), inequality_year, ssp_scenario, c);

            Map<String, GamsDemandOutput> gamsDemand = new HashMap<String, GamsDemandOutput>();
            Map<String, Map<CommodityType, Double>> modelledDemands = new HashMap<String, Map<CommodityType, Double>>();

            //Iterate through the income deciles, storing demand in the foodDemand and GamsDemand maps
            //At the end of the loop, gamsDemand and modelledDemand map each income decile to the gamsOutput and average daily kcal per capita demand
            for (String decile : gdpByDecile.keySet()) {

                //allow for empty map in the first year
                GamsDemandOutput previousDemand = previousDecileDemands.containsKey(c)? previousDecileDemands.get(c).get(decile) : null;

                GamsDemandInput inputData = new GamsDemandInput(c, demandYear, gdpByDecile.get(decile), consumerPrices, previousDemand);

                // Do the projection
                GamsDemandOptimiser gamsDemandOptimiser = new GamsDemandOptimiser(inputData);
                GamsDemandOutput gamsOutput = gamsDemandOptimiser.getDemandPc();
                gamsDemand.put(decile, gamsOutput);
                modelledDemands.put(decile, gamsOutput.getTotalDemands());
            }

            //Save results in previousDecileDemands for the next timestep
            previousDecileDemands.put(c, gamsDemand);

            //Calculate average food calories by country
            Map<CommodityType, Double> countryDemandMap = new HashMap<CommodityType, Double>();
            for (CommodityType commodity : CommodityType.values()) {
                double totalDecileC = 0.0;
                int basecount = 0;
                for (String decile : modelledDemands.keySet()) {
                    totalDecileC += modelledDemands.get(decile).get(commodity);
                    basecount++;
                }
				countryDemandMap.put(commodity, totalDecileC / basecount);
            }

			//In the calibration only, store plumDemand in baseCpcCache
            Map<CommodityType, Double> baseExpCpcMap = storeOrGetBaseDemand(countryDemandMap, c, initYear);

            //Calculate rebased kcal demand per capita at the country level
			Map<CommodityType, Double> countryRebasedDemand = rebaseDemand(countryDemandMap, baseExpCpcMap, c, year);
			//allocate national rebased demand across income deciles
			Map<String, Map<CommodityType, Double>> rebasedDemands = new HashMap<String, Map<CommodityType, Double>>();
			for (String decile : modelledDemands.keySet()) {
				Map<CommodityType, Double> rebasedDecileDemand = new HashMap<CommodityType, Double>();
				for (CommodityType commodity : CommodityType.values()) {
					double cpcAdj = countryRebasedDemand.get(commodity);
					double frac = modelledDemands.get(decile).get(commodity) / countryDemandMap.get(commodity);
					rebasedDecileDemand.put(commodity, cpcAdj * frac);
				}
				rebasedDemands.put(decile, rebasedDecileDemand);
				//Adjust demand where incomes are too low to meet expected demand
				Map<CommodityType, Double>affordableDemand = affordableDemand(rebasedDemands.get(decile), consumerPrices, gdpByDecile.get(decile));
				rebasedDemands.put(decile, affordableDemand);
			}

			//transform commodity to crop demand
			Map<String, Map<CropType, Double>> rebasedCropDemands = new HashMap<>();
			Map<String, Map<CropType, Double>> modelledCropDemands = new HashMap<>();
			for (String decile: gdpByDecile.keySet()) {
				Map<CropType, Double> rebasedCropDemand = commodityToCropDemand(rebasedDemands.get(decile), c, countryPrices, population);
				Map<CropType, Double> modelledCropDemand = commodityToCropDemand(modelledDemands.get(decile), c, countryPrices, population);
				rebasedCropDemands.put(decile, rebasedCropDemand);
				modelledCropDemands.put(decile, modelledCropDemand);
			}

			//write food demand by decile to the result file
			if (outputGamsDemand) writeDecileDemandOutputs(c, year, gamsDemand, rebasedCropDemands, modelledCropDemands, rebasedDemands, population, gdpByDecile, consumerPrices);

			//Aggregate decile rebased crop demand to the country level for land use modelling
			Map<CropType, Double> totalRebasedCropDemands = new HashMap<CropType, Double>();
			for (CropType crop : CropType.getFoodTypes()) {
				double totalCropDemand = 0.0;
				int count = 0;
				for (String decile: gdpByDecile.keySet()) {
					totalCropDemand += rebasedCropDemands.get(decile).get(crop);
					count ++;
				}
				totalRebasedCropDemands.put(crop, totalCropDemand/count);
			}
			return totalRebasedCropDemands;

        } else {

            GamsDemandInput inputData = new GamsDemandInput(c, demandYear, gdpPc, consumerPrices, previousGamsDemands.get(c));

            // Do the projection
            GamsDemandOptimiser gamsDemandOptimiser = new GamsDemandOptimiser(inputData);
            GamsDemandOutput gamsOutput = gamsDemandOptimiser.getDemandPc();

            previousGamsDemands.put(c, gamsOutput);

            Map<CommodityType, Double> modelledDemands = gamsOutput.getTotalDemands(); // kcal per capita per day

            Map<CommodityType, Double> baseExpCpcMap = storeOrGetBaseDemand(modelledDemands, c, initYear);

            //Transform modelled demand to rebased demands
            Map<CommodityType, Double> rebasedDemands = rebaseDemand(modelledDemands, baseExpCpcMap, c, year);
            rebasedDemands = affordableDemand(rebasedDemands, consumerPrices, gdpPc);

            // Disaggregate commodity demand to crops
            Map<CropType, Double> rebasedCropDemands = commodityToCropDemand(rebasedDemands, c, countryPrices, population);
            Map<CropType, Double> modelledCropDemands = commodityToCropDemand(modelledDemands, c, countryPrices, population);

            if (outputGamsDemand)
                writeGamsDemandOutputs(inputData, year, gamsOutput, rebasedCropDemands, modelledCropDemands, rebasedDemands, population);

            return rebasedCropDemands;
        }
    }

    private Map<CropType, Double> commodityToCropDemand(Map<CommodityType, Double> commDemands, SingleCountry c, Map<CropType, CountryPrice> countryPrices,
														double population) {
        Map<CropType, Double> cropDemands = new HashMap<>();
        Map<CommodityType, Map<CropType, Double>> demandFractions = getFoodDemandFractions(c, countryPrices);
        Map<CropType, Double> energyContent = calorieManager.get(c);

        for (CommodityType comm : commDemands.keySet()) {
            for (CropType crop : comm.getCropTypes()) {
                double kcalDemand = commDemands.get(comm) * demandFractions.get(comm).get(crop);
                double kcalPerT = energyContent.get(crop);
                double cropDemand = kcalDemand * 365 * population / kcalPerT;
				cropDemands.put(crop, cropDemand);
            }
        }
        return cropDemands;
    }

    private Map<CommodityType, Double> rebaseDemand(Map<CommodityType, Double> newExpCpcMap, Map<CommodityType, Double> baseExpCpcMap,
                                      SingleCountry c, int year){
        Map<CommodityType, Double> cpcAdjMap = new HashMap<CommodityType, Double>();
        for (CommodityType commodity : CommodityType.values()) {
            double minTonIntake = (commodity != CommodityType.NONFOOD) ? commodity.getMinFAOCalorieIntake(): 0.0;
            double newExpCpc = newExpCpcMap.get(commodity);
            double baseExpCpc = baseExpCpcMap.get(commodity);
            double baseCpc = baseConsumpManager.get(c, commodity);
            double cpc = rebaseFoodConsumption(baseCpc, baseExpCpc, newExpCpc, year);
            double cpcAdj = Math.max(cpc, minTonIntake);
            cpcAdjMap.put(commodity, cpcAdj);
        }
        return cpcAdjMap;
    }

    private Map<CommodityType, Double> storeOrGetBaseDemand(Map<CommodityType, Double> modelledDemands, SingleCountry c, boolean initYear){
        if (ModelConfig.IS_CALIBRATION_RUN || initYear) {  // updated each year during calibration so that initial demand = reported
            baseCpcCache.put(c, modelledDemands);
        }
        return baseCpcCache.get(c);
	}

    private Map<CommodityType, Double> calculateConsumerPrices(Map<CommodityType, Double> producerPrices,
                                                               SingleCountry c, Double gdpPc) {
        Map<CommodityType, Double> consumerPrices = new HashMap<CommodityType, Double>();

        // Non-food price relative to food - a function of gdpPc (logistic function to avoid spurious extrapolation)
        double x = Math.exp(1.87 * (Math.log(gdpPc) - 10.37));
        double nonfoodPrice = (45.52 + 86.29 * x) / (1 + x) * 100;
        consumerPrices.put(CommodityType.NONFOOD, nonfoodPrice);

        // convert from producer to consumer prices
        for (CommodityType commodity : CommodityType.getAllFoodItems()) {
            double consumerPrice = priceMarkups.markedupPrice(c, commodity, producerPrices.get(commodity));
            consumerPrices.put(commodity, consumerPrice);
        }
        return consumerPrices;
    }


    private Map<CommodityType, Double> affordableDemand(Map<CommodityType, Double> foodDemands, Map<CommodityType, Double> consumerPrices,
                                                        Double gdpPc) {
        double foodSpend = 0;
        for (CommodityType commodity : CommodityType.getAllFoodItems()) {
            double consumerPrice = consumerPrices.get(commodity);
            double rebasedDemand = foodDemands.get(commodity)/2000;
            foodSpend += consumerPrice * rebasedDemand;
        }
        LogWriter.println("foodSpend " + foodSpend + " budget " + gdpPc*ModelConfig.MAX_INCOME_PROPORTION_FOOD_SPEND, 3);

        if(foodSpend > gdpPc*ModelConfig.MAX_INCOME_PROPORTION_FOOD_SPEND) {
            double overspend= 1/(foodSpend/(gdpPc*ModelConfig.MAX_INCOME_PROPORTION_FOOD_SPEND));
            LogWriter.println("overspend " + overspend, 3);
            for (CommodityType commodity : CommodityType.values()) {
                double rebasedDemand = foodDemands.get(commodity);
                double affordableDemand = rebasedDemand*overspend;
                foodDemands.put(commodity, affordableDemand);
            }
        }
        return foodDemands;
    }

	private String getDamsDemandOutputsHeader() {
		return "Country,Year,gdpPc,population,status,utility,hungerFactor,commodity,price,subsistence,discretionary,plumNotRebase,plumRebased,rebasedKcal";
	}

	private String getDecileDemandOutputsHeader() {
		return "Country,Year,decile,gdpPc,population,status,utility,hungerFactor,commodity,price,subsistence,discretionary,plumNotRebase,plumRebased,rebasedKcal";
	}

	private void writeGamsDemandOutputs(GamsDemandInput inputData, int year, GamsDemandOutput gamsOutput, Map<CropType, Double> rebasedCropDemands,
										Map<CropType, Double> modelledCropDemands, Map<CommodityType, Double> rebasedKcal, double population) {

		synchronized (lock) { // needed if using multithreaded to avoid jumbled output

			try {
				Map<CommodityType, Double> rebasedDemands = new HashMap<>();
				Map<CommodityType, Double> modelledDemands = new HashMap<>();
				for (CommodityType comm : CommodityType.getAllFoodItems()) {
					for (CropType crop : comm.getCropTypes()) {
						rebasedDemands.merge(comm, rebasedCropDemands.get(crop), Double::sum);
						modelledDemands.merge(comm, modelledCropDemands.get(crop), Double::sum);
					}
				}

				String initialData = String.format("%s,%d,%.2f,%.6f,%s,%.6f,%.6f",
						inputData.getCountry(), year, inputData.getGdpPc(), population,
						gamsOutput.getStatus(), gamsOutput.getUtility(), gamsOutput.getHungerFactor());

				for (CommodityType commodity : CommodityType.getAllFoodItems()) {
					StringBuilder sbData = new StringBuilder(initialData);
					GamsCommodityDemand gamsDemand = gamsOutput.getGamsDemands(commodity);

					sbData.append(String.format(",%s,%.4f", commodity.getGamsName(), inputData.getPrices().get(commodity)));
					sbData.append(String.format(",%.4f,%.4f", gamsDemand.getSubsistence(), gamsDemand.getDiscretionary()));
					sbData.append(String.format(",%.4f,%.4f,%.4f", modelledDemands.get(commodity) / population, rebasedDemands.get(commodity) / population,
							rebasedKcal.get(commodity)));
					outputFile.write(sbData.toString());
					outputFile.newLine();
				}
			} catch (IOException e) {
				LogWriter.print(e);
			}
		}
	}

	private void writeDecileDemandOutputs(SingleCountry c, int year , Map<String, GamsDemandOutput> gamsDemand, Map<String,Map<CropType, Double>> rebasedCropDemands,
										  Map<String,Map<CropType, Double>> modelledCropDemands, Map<String,Map<CommodityType, Double>> rebasedKcal, double population,
										  Map<String, Double> incomeDistribution, Map<CommodityType, Double> consumerPrices) {

		synchronized (lock) { // needed if using multithreaded to avoid jumbled output

			for (String decile : incomeDistribution.keySet()) {
				try {
					Map<CommodityType, Double> rebasedDemands = new HashMap<>();
					Map<CommodityType, Double> modelledDemands = new HashMap<>();
					for (CommodityType comm : CommodityType.getAllFoodItems()) {
						for (CropType crop : comm.getCropTypes()) {
							rebasedDemands.merge(comm, rebasedCropDemands.get(decile).get(crop), Double::sum);
							modelledDemands.merge(comm, modelledCropDemands.get(decile).get(crop), Double::sum);
						}
					}

					String initialData = String.format("%s,%d,%s,%.2f,%.6f,%s,%.6f,%.6f",
							c, year, decile, incomeDistribution.get(decile), population / 10,
							gamsDemand.get(decile).getStatus(), gamsDemand.get(decile).getUtility(), gamsDemand.get(decile).getHungerFactor());

					for (CommodityType commodity : CommodityType.getAllFoodItems()) {
						StringBuilder sbData = new StringBuilder(initialData);
						GamsCommodityDemand gamsCommodityDemand = gamsDemand.get(decile).getGamsDemands(commodity);

						sbData.append(String.format(",%s,%.4f", commodity.getGamsName(), consumerPrices.get(commodity)));
						sbData.append(String.format(",%.4f,%.4f", gamsCommodityDemand.getSubsistence(), gamsCommodityDemand.getDiscretionary()));
						sbData.append(String.format(",%.4f,%.4f,%.4f", modelledDemands.get(commodity)/ (population / 10), rebasedDemands.get(commodity) / (population / 10),
								rebasedKcal.get(decile).get(commodity)));
						outputFile.write(sbData.toString());
						outputFile.newLine();
					}
				} catch (IOException e) {
					LogWriter.print(e);
				}
			}
		}
	}
	
	@Override
	public Map<WoodType, Double> getWoodDemandMap(SingleCountry country, int year, double gdpPc, double population,
												  Map<WoodType, Double> producerPrices) {

		Map<WoodCommodityType, Double> consumerPrices = new HashMap<>();

		// Convert from producer to consumer prices
		for (WoodCommodityType commodity : WoodCommodityType.values()) {
			if (commodity.equals(WoodCommodityType.RESIDUAL)) {
				consumerPrices.put(commodity, 10000.0);
				continue;
			}
			double consumerPrice = woodDemandManager.getConsumerPrice(country, commodity, producerPrices);
			consumerPrices.put(commodity, consumerPrice);
		}

		GamsWoodDemandInput inputData = new GamsWoodDemandInput(country, year, gdpPc, consumerPrices,
				previousGamsWoodDemands.get(country));

		// Do the projection
		GamsWoodDemandOptimiser gamsWoodDemandOptimiser = new GamsWoodDemandOptimiser(inputData);
		GamsWoodDemandOutput gamsOutput = gamsWoodDemandOptimiser.getDemandPc();

		previousGamsWoodDemands.put(country, gamsOutput);
		Map<WoodCommodityType, Double> woodPc = gamsOutput.getPlumDemands();

		Map<WoodCommodityType, Double> woodDemands = new HashMap<>();
		Map<WoodCommodityType, Double> baseExpCpcMap;

		if (ModelConfig.IS_CALIBRATION_RUN) {  // we should only do this during calibration
			baseWoodCpcCache.put(country, woodPc);
			baseExpCpcMap = woodPc;
		} else {
			baseExpCpcMap = baseWoodCpcCache.get(country);
		}

		for (Map.Entry<WoodCommodityType, Double> entry : woodPc.entrySet()) {
			WoodCommodityType commodity = entry.getKey();
			if (commodity.equals(WoodCommodityType.RESIDUAL))
				continue;
			double newExpCpc = entry.getValue();
			double baseExpCpc = baseExpCpcMap.get(commodity);
			double baseCpc = woodDemandManager.getBaseDemand(country, commodity);
			double cpc = rebaseWoodConsumption(baseCpc, baseExpCpc, newExpCpc);

			// Rebased consumption
			double d = cpc * population;
			woodDemands.put(commodity, d);
		}

		// Aggregate to production level
		Map<WoodType, Double> woodTypeDemands = new HashMap<>();
		for (Map.Entry<WoodCommodityType, Double> entry : woodDemands.entrySet()) {
			WoodCommodityType comm = entry.getKey();
			double demand = entry.getValue();
			WoodType wType = comm.getWoodType();
			woodTypeDemands.merge(wType, demand, Double::sum);
		}

		return woodTypeDemands;
	}

}