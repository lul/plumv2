package ac.ed.lurg.demand;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.*;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;

public abstract class AbstractSSPDemandManager extends AbstractDemandManager {

	private static final long serialVersionUID = 1169375638148027645L;
	protected transient SspManager sspManager;
	protected transient String ssp_scenario;
	protected transient BaseConsumpManager baseConsumpManager;
	private transient AgriculturalGDPManager agriculturalGDPManager;

	public AbstractSSPDemandManager(String ssp_scenario, BaseConsumpManager baseConsumpManager,CalorieManager calorieManager) {
		super(calorieManager);
		setup(ssp_scenario, baseConsumpManager);
	}
	
	public void setup(String ssp_scenario, BaseConsumpManager baseConsumpManager) {
		this.ssp_scenario = ssp_scenario;
		this.baseConsumpManager = baseConsumpManager;
		sspManager = new SspManager();
		agriculturalGDPManager = new AgriculturalGDPManager();
	}

	protected void setup(String ssp_scenario, BaseConsumpManager baseConsumpManager, CalorieManager calorieManager) {
		super.setup(calorieManager);
		setup(ssp_scenario, baseConsumpManager);
	}
	
	@Override
	public void updateGdpLossesFromShock(CompositeCountry cc, int year, double shockMagnitude) {

		for (SingleCountry c : CountryManager.getInstance().getAllForCompositeCountry(cc)) {
			SspData sd = sspManager.get(ssp_scenario, year, c);
			LogWriter.println("sd data " + sd, 3);
			if(sd != null) {
				double gdp = sd.getGdp();
				double agriculturalGDPProportion = agriculturalGDPManager.getProportion(c);
				double gdpLost = gdp * agriculturalGDPProportion * shockMagnitude;
				sd.updateShockedGdpLosses(gdpLost);
			}
		}

	}

	@Override
	protected Map<CropType, Double> getFoodDemand(SingleCountry c, int year, Map<CropType, CountryPrice> prices, boolean outputGamsDemand) {
        int sspYear = (!ModelConfig.CHANGE_DEMAND_YEAR)? ModelConfig.BASE_YEAR: year;
        SspData sd = sspManager.get(ssp_scenario, sspYear, c);

        Map<CropType, Double> foodDemandMap = new HashMap<>();

		if (sd == null) {
			LogWriter.printlnWarning(String.format("No ssp for %s, baseYr:%d, year:%d, %s. Skipping", ssp_scenario, ModelConfig.BASE_YEAR, year, c.getCountryName()));
			return foodDemandMap;
		}

		double gdpPcYear= sd.getShockedGdpPc();

		LogWriter.println("Got ssp data for " + c.getCountryName() + " of " + sd, 3);
		foodDemandMap = getFoodDemandMap(c, year, gdpPcYear, sd.getPopulation(), prices, outputGamsDemand);

		return foodDemandMap;
	}

	protected double rebaseFoodConsumption(double baseCpc, double baseExpCpc, double newExpCpc, int year) {
		double gapRatio;
		if (ModelConfig.ADJUST_DIET_PREFS) {
			double offset = (ModelConfig.DIET_CHANGE_END_YEAR - ModelConfig.DIET_CHANGE_START_YEAR) / 2.0;
			double x = year - ModelConfig.DIET_CHANGE_START_YEAR;
			gapRatio = 1 - ModelConfig.DIET_CONVERGENCE_PARAM / (1 + Math.exp(-ModelConfig.DIET_CHANGE_RATE * (x - offset))); // sigmoid
		} else {
			gapRatio = 1;
		}

		// There is a residual between predicted and observed demand.
		// Here we gradually decrease this residual so that consumption converges to prediction.
		double adjFactor = (baseExpCpc==0 || ModelConfig.DONT_REBASE_DEMAND) ? 0.0 : (baseCpc - baseExpCpc) * gapRatio;

		return newExpCpc + adjFactor;
	}

	protected double rebaseWoodConsumption(double baseCpc, double baseExpCpc, double newExpCpc) {
		double adjFactor = (baseExpCpc == 0 || ModelConfig.DONT_REBASE_DEMAND) ? 0.0 : (baseCpc - baseExpCpc);
		return Math.max(0.0, newExpCpc + adjFactor);
	}

	abstract Map<CropType, Double> getFoodDemandMap(SingleCountry c, int year, double gdpPc, double population,
														 Map<CropType, CountryPrice> prices, boolean outputGamsDemand);
	

	protected Map<WoodType, Double> getWoodDemand(SingleCountry country, int year, Map<WoodType, Double> producerPrices) {

		SspData sd = sspManager.get(ssp_scenario, year, country);

		Map<WoodType, Double> woodDemandMap = new HashMap<WoodType, Double>();
		
		if (sd == null) {
			LogWriter.printlnWarning(String.format("No ssp for %s, baseYr:%d, year:%d, %s. Skipping", ssp_scenario, ModelConfig.BASE_YEAR, year, country.getCountryName()));
			return woodDemandMap;
		}

		double gdpPc = sd.getGdpPc();
		double population = sd.getPopulation();
		
		woodDemandMap = getWoodDemandMap(country, year, gdpPc, population, producerPrices);
		return woodDemandMap;
	}
	
	abstract Map<WoodType, Double> getWoodDemandMap(SingleCountry country, int year, double gdpPc, double population,
													Map<WoodType, Double> producerPrices);
	
	protected double getCarbonDemand(SingleCountry country, int year) {
		
		if (!ModelConfig.IS_CARBON_ON | ModelConfig.IS_CALIBRATION_RUN | !ModelConfig.CARBON_DEMAND_METHOD.equals("country")) {
			return 0;
		}
		
		SspData sd = sspManager.get(ssp_scenario, year, country);
		if (sd == null) {
			LogWriter.printlnWarning(String.format("No ssp for %s, baseYr:%d, year:%d, %s. Skipping", ssp_scenario, ModelConfig.BASE_YEAR, year, country.getCountryName()));
			return 0.0;
		}
		double gdp = sd.getGdp();
		double worldGDP = sspManager.getWorldGdp(ssp_scenario, year, CountryManager.getInstance().getAllSingleCountries());
		double worldCarbonDemand = getWorldCarbonDemand(year);
		double countryCarbonDemand = worldCarbonDemand * (gdp / worldGDP);
		return countryCarbonDemand;
	}

}
