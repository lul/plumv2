package ac.ed.lurg.demand;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.CountryPrice;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.landuse.WoodUsageData;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.LandCoverType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractDemandManager implements Serializable {

	private static final long serialVersionUID = 8622023691732185744L;
	protected transient CalorieManager calorieManager;
	protected transient DemandFractionsManager demandFractionsManager;
	protected transient CarbonDemandManager carbonDemandManager;
	protected WoodDemandManager woodDemandManager;
	protected transient EnergyDemandManager energyDemandManager;

	public AbstractDemandManager(CalorieManager calorieManager) {
		setup(calorieManager);
	}
	
	protected void setup(CalorieManager calorieManager) {
		this.calorieManager = calorieManager;
		demandFractionsManager = new DemandFractionsManager();
		if (ModelConfig.IS_CARBON_ON) carbonDemandManager = new CarbonDemandManager();
		if (ModelConfig.IS_FORESTRY_ON && ModelConfig.IS_CALIBRATION_RUN) woodDemandManager = new WoodDemandManager();
		energyDemandManager = new EnergyDemandManager();
	}

	public Map<CropType, Double> getDemand(CompositeCountry cc, int year, Map<CropType, CountryPrice> prices, boolean outputGamsDemand) {

        Map<CropType, Double> demandMap = new HashMap<>();

		for (SingleCountry c : CountryManager.getInstance().getAllForCompositeCountry(cc)) {

			try {
				demandMap = getFoodDemand(c, year, prices, outputGamsDemand);
			} catch (Exception e) {
				LogWriter.printlnError("Error: AbstractDemandManager not including " + c + " due to exception");
				LogWriter.print(e);
				continue;
			}

			for (CropType crop : CropType.getFoodTypes()) {
				demandMap.putIfAbsent(crop, 0.0);
			}
		}
		
		return demandMap;
	}

	protected abstract Map<CropType, Double> getFoodDemand(SingleCountry c, int year, Map<CropType, CountryPrice> prices, boolean outputGamsDemand);
	
	public void updateGdpLossesFromShock(CompositeCountry cc, int year, double shockMagnitude) {}

	protected abstract Map<WoodType, Double> getWoodDemand(SingleCountry country, int year, Map<WoodType, Double> producerPrices);

	public Map<WoodType, Double> getWoodDemandComposite(CompositeCountry cc, int year, Map<WoodType, CountryPrice> countryWoodPrices) {
		Map<WoodType, Double> compositeDemandMap = new HashMap<>();

		if (!ModelConfig.IS_FORESTRY_ON) { // Forestry Off so return dummy values
			for (WoodType woodType : WoodType.values()) {
				compositeDemandMap.put(woodType, 0.0);
			}
			return compositeDemandMap;
		}

		if (!ModelConfig.CHANGE_DEMAND_YEAR)
			year = ModelConfig.BASE_YEAR;

		Map<WoodType, Double> producerPrices = new HashMap<>();
		for (Map.Entry<WoodType, CountryPrice> entry : countryWoodPrices.entrySet()) {
			producerPrices.put(entry.getKey(), entry.getValue().getConsumerPrice());
		}

		Map<WoodType, Double> singleDemandMap;

		for (SingleCountry c : CountryManager.getInstance().getAllForCompositeCountry(cc)) {
			singleDemandMap = getWoodDemand(c, year, producerPrices);
			if (singleDemandMap.isEmpty()) {
				LogWriter.printlnWarning("Wood demand not found for: " + c.getCountryName());
				continue;
			}
			for (WoodType w : WoodType.values()) {
				double totalDemand = compositeDemandMap.getOrDefault(w, 0.0);
				double demand = singleDemandMap.get(w);
				totalDemand += demand;
				compositeDemandMap.put(w, totalDemand);
			}
		}
		return compositeDemandMap;
	}
	
	public double getWorldCarbonDemand(int year) {
		return carbonDemandManager.getGlobalCarbonDemand(year);
	}
	
	abstract double getCarbonDemand(SingleCountry country, int year);
	
	public double getCarbonDemandComposite(CompositeCountry cc, int year) {
		if (!ModelConfig.CHANGE_DEMAND_YEAR)
			year = ModelConfig.BASE_YEAR;

		double totalDemand = 0;
		
		for (SingleCountry c : CountryManager.getInstance().getAllForCompositeCountry(cc)) {
			totalDemand += getCarbonDemand(c, year);
		}
		
		return totalDemand;
	}

	public Map<CompositeCountry, WoodUsageData> getInitialWoodUsage() {
		if (ModelConfig.IS_FORESTRY_ON) {
			return woodDemandManager.getInitialWoodUsage();
		} else { // Forestry Off so use blank dummy data
			Map<CompositeCountry, WoodUsageData> countryMap = new HashMap<>();
			for (CompositeCountry cc : CountryManager.getInstance().getAllCompositeCountries()) {
				countryMap.put(cc, new WoodUsageData(0, 0, 0));
			}
			return countryMap;
		}

	}

	// Adjust baseline demand fractions based on change in price
	protected Map<CommodityType, Map<CropType, Double>> getFoodDemandFractions(SingleCountry sc, Map<CropType, CountryPrice> prices) {
		// Fraction of calories
		Map<CommodityType, Map<CropType, Double>> baseDemandFact = demandFractionsManager.getBaseFoodDemandFracts(sc);
		Map<CommodityType, Map<CropType, Double>> demandFraction = new HashMap<>();

		for (Map.Entry<CommodityType, Map<CropType, Double>> entry : baseDemandFact.entrySet()) {
			CommodityType comm = entry.getKey();
			Map<CropType, Double> baseFracts = entry.getValue();

			if (ModelConfig.IS_CALIBRATION_RUN || !ModelConfig.DEMAND_FRACT_BY_COST) {
				demandFraction.put(comm, baseFracts);
			}
			else {
				Map<CropType, Double> newFracts = new HashMap<>();
				double totalAdjQuantities = 0;

				for (Map.Entry<CropType, Double> cropEntry : baseFracts.entrySet()) {
					CountryPrice price = prices.get(cropEntry.getKey());
					double ratio = 1 + ModelConfig.DEMAND_FRACT_ELASTICITY * (price.getReferenceConsumerPrice() / price.getConsumerPrice() - 1);
					double adjFract = ratio * cropEntry.getValue(); // adjust by price ratio
					newFracts.put(cropEntry.getKey(), adjFract);
					totalAdjQuantities += adjFract;
				}

				Map<CropType, Double> cropFracts = new HashMap<>();
				for (Map.Entry<CropType, Double> cropEntry : newFracts.entrySet())
					cropFracts.put(cropEntry.getKey(), cropEntry.getValue() / totalAdjQuantities);  //rebase to 1 by dividing by totalAdjQuantities

				demandFraction.put(comm, cropFracts);
			}
		}

		return demandFraction;
	}

	// Calculate commodity prices in MAIDADS units: $ per 2000 kcal per day
	protected Map<CommodityType, Double> getCommodityCaloriePrices(SingleCountry sc, Map<CropType, CountryPrice> countryPrices) {
		Map<CommodityType, Map<CropType, Double>> demandFractions = getFoodDemandFractions(sc, countryPrices);
		Map<CropType, Double> energyContent = calorieManager.get(sc);
		// Prices per 2000 kcal/day
		Map<CommodityType, Double> prices = new HashMap<>();
		for (CommodityType commodity : CommodityType.getAllFoodItems()) {
			double weightedPrice = 0;
			for (CropType crop : commodity.getCropTypes()) {
				double fract = demandFractions.get(commodity).get(crop);
				double pricerPerT = countryPrices.get(crop).getConsumerPrice();
				double kcalPerT = energyContent.get(crop);
				// $1000 * $1000 per tonne / kcal per tonne * 2000 kcal * 365 days
				double pricePer2000kcal = 1000 * pricerPerT / kcalPerT * 2000 * 365;
				weightedPrice += fract * pricePer2000kcal;
			}
			prices.put(commodity, weightedPrice);
		}
		return  prices;
	}

	public Map<LandCoverType, Double> getSolarEnergyDemand(CompositeCountry cc, Integer year) {
		Map<LandCoverType, Double> demandMap = new HashMap<>();

		if (ModelConfig.IS_CALIBRATION_RUN || !ModelConfig.CHANGE_DEMAND_YEAR) {
			year = ModelConfig.BASE_YEAR;
		}

		double pvDemand = 0;
		double avDemand = 0;
		for (SingleCountry sc : CountryManager.getInstance().getAllForCompositeCountry(cc)) {
			pvDemand += energyDemandManager.getPhotovoltaicsDemand(sc, year);
			avDemand += energyDemandManager.getAgrivoltaicsDemand(sc, year);
		}

		demandMap.put(LandCoverType.PHOTOVOLTAICS, ModelConfig.IS_PHOTOVOLTAICS_ON ? pvDemand : 0);
		demandMap.put(LandCoverType.AGRIVOLTAICS, ModelConfig.IS_AGRIVOLTAICS_ON ? avDemand : 0);

		return demandMap;
	}

	public Map<CropType, Double> getBioenergyDemand(CompositeCountry cc, Integer year) {
		if (ModelConfig.IS_CALIBRATION_RUN || !ModelConfig.CHANGE_DEMAND_YEAR) {
			year = ModelConfig.BASE_YEAR;
		}
		Map<CropType, Double> totalDemand = new HashMap<>();
		for (SingleCountry sc : CountryManager.getInstance().getAllForCompositeCountry(cc)) {
			// First gen
			double firstGenCurrent = energyDemandManager.getBioenergy1stGenDemand(sc, year);
			double firstGenBase = energyDemandManager.getBioenergy1stGenDemand(sc, ModelConfig.BASE_YEAR);
			double demandChange = firstGenBase > 0 ? firstGenCurrent / firstGenBase : 1.0;

			for (CropType crop : CropType.getFoodTypes()) {
				double baseDemand = energyDemandManager.getFirstGenCropBaseDemand(sc, crop);
				totalDemand.merge(crop, baseDemand * demandChange, Double::sum);
			}

			// Second gen. Converting from TWh energy to Mt energycrops
			double secondGenTWh = energyDemandManager.getBioenergy2ndGenDemand(sc, year);
			double secondGenMt = ModelConfig.ENABLE_GEN2_BIOENERGY ? secondGenTWh / ModelConfig.ENERGYCROPS_CONVERSION_FACTOR : 0;
			totalDemand.merge(CropType.ENERGY_CROPS, secondGenMt, Double::sum);
		}

		return totalDemand;
	}

}
