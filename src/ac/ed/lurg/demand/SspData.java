package ac.ed.lurg.demand;


public class SspData {
	
	private double population;
	private double gdp;
	private double shockedGdp;
	
	public SspData(double population, double gdp) {
		super();
		this.population = population;
		this.gdp = gdp;
		this.shockedGdp = 0.0;
	}
	
	public double getPopulation() {
		return population;
	}
	public double getGdp() {
		return gdp;
	}
	
	public double getGdpPc() {
		return gdp/population;
	}

	public double getShockedGdpPc() {
		return (gdp - shockedGdp)/population;
	}
	
	public void updateShockedGdpLosses(double gdpLoss) {
		shockedGdp = gdpLoss;
	}

	@Override
	public String toString() {
		return "SspData [population=" + population + ", gdp=" + gdp + "]";
	}
}
