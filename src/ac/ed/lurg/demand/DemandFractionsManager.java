package ac.ed.lurg.demand;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

import java.util.HashMap;
import java.util.Map;

public class DemandFractionsManager {
	private final StringTabularReader demandFractionReader;

	public DemandFractionsManager() {
		demandFractionReader = new StringTabularReader(",", new String[]{"Iso3", "plumDemandItem", "plumCropItem",
				"food_t", "food_mkcal", "food_cal_cap",	"bioenergy_t", "mkcal_per_t", "prod", "import", "export"});
		demandFractionReader.read(ModelConfig.BASELINE_CONSUMP_FILE);
	}

	public Map<CommodityType, Map<CropType, Double>> getBaseFoodDemandFracts(SingleCountry sc) {
		return getBaseDemandFracts(sc, "food_mkcal");
	}

	public Map<CommodityType, Map<CropType, Double>> getBaseBioenergyDemandFracts(SingleCountry sc) {
		return getBaseDemandFracts(sc, "bioenergy_t");
	}

	private Map<CommodityType, Map<CropType, Double>> getBaseDemandFracts(SingleCountry sc, String demandType) {
		Map<CommodityType, Map<CropType, Double>> fracts = new HashMap<>();
		for (CommodityType comm : CommodityType.getAllFoodItems()) {
			Map<CropType, Double> cropDemands = getBaseDemand(sc, comm, demandType);
			Map<CropType, Double> map = fracts.computeIfAbsent(comm, k -> new HashMap<>());
			double commDemand = cropDemands.values().stream().reduce(Double::sum).orElse(0.0);
			for (CropType crop : cropDemands.keySet()) {
				double f = cropDemands.get(crop) / commDemand;
				map.put(crop, f);
			}
		}
		return fracts;
	}

	private Map<CropType, Double> getBaseDemand(SingleCountry c, CommodityType commodity, String demandType) {
		Map<CropType, Double> demandMap = new HashMap<CropType, Double>();

		for (CropType crop : commodity.getCropTypes()) {
			Map<String, String> queryMap = new HashMap<String, String>();
			queryMap.put("Iso3", c.getCountryCode());
			queryMap.put("plumCropItem", crop.getFaoName());
			queryMap.put("plumDemandItem", commodity.getFaoName());

			try {
				Map<String, String> row = demandFractionReader.querySingle(queryMap);
				String commodityS = row.get(demandType); // "food" or "bioenergy"
				Double commodityAmt = Double.valueOf(commodityS);
				demandMap.put(crop, commodityAmt);
			} catch (Exception e) {
				LogWriter.println("Problem finding " + commodity.getGamsName() + " demand: " + crop.getFaoName() + ", "
						+ c.getCountryName());
			}
		}
		return demandMap;
	}
}
