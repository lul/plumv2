package ac.ed.lurg.demand;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.landuse.WoodUsageData;
import ac.ed.lurg.types.WoodCommodityType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class WoodDemandManager implements Serializable {

	private static final long serialVersionUID = 171124405614624397L;
	private final Map<SingleCountry, Map<WoodCommodityType, Double>> baseWoodDemand = new HashMap<>();
	private final Map<SingleCountry, Map<WoodCommodityType, Double>> baseConsumerPrices = new HashMap<>();
	private final Map<SingleCountry, Map<WoodCommodityType, Double>> initialProduction = new HashMap<>();
	private final Map<SingleCountry, Map<WoodCommodityType, Double>> initialNetImports = new HashMap<>();
	private final Map<SingleCountry, Map<WoodType, Double>> priceMarkups = new HashMap<>();
	
	public WoodDemandManager() {
		readWoodDemand();

		// Initialise markups
		for (SingleCountry sc : baseConsumerPrices.keySet()) {
			Map<WoodType, Double> map = new HashMap<>();
			for (WoodType wt : WoodType.values()) {
				map.put(wt, 1.0);
			}
			priceMarkups.put(sc, map);
		}
	}
	
	private void readWoodDemand() {
		String filename = ModelConfig.WOOD_DEMAND_FILE;
		StringTabularReader reader = new StringTabularReader(",",
				new String[] {"Iso3", "Item", "DemandPc", "Price", "Production", "NetImport"});
		reader.read(filename);
		List<Map<String, String>> rowList = reader.getRowList();
		for (Map<String, String> row : rowList) {
			String countryCode = row.get("Iso3");
			String commTypeStr = row.get("Item");
			double demand = Double.parseDouble(row.get("DemandPc"));
			double price = Double.parseDouble(row.get("Price"));
			double prod = Double.parseDouble(row.get("Production"));
			double netImport = Double.parseDouble(row.get("NetImport"));
			SingleCountry sc = CountryManager.getInstance().getForCode(countryCode);
			WoodCommodityType woodCommodityType = WoodCommodityType.getForFaoName(commTypeStr);

			setData(baseWoodDemand, sc, woodCommodityType, demand);
			setData(baseConsumerPrices, sc, woodCommodityType, price);
			setData(initialProduction, sc, woodCommodityType, prod);
			setData(initialNetImports, sc, woodCommodityType, netImport);
		}
	}

	private void setData(Map<SingleCountry, Map<WoodCommodityType, Double>> map, SingleCountry sc, WoodCommodityType commType, double value) {
		Map<WoodCommodityType, Double> wMap = map.computeIfAbsent(sc, k -> new HashMap<WoodCommodityType, Double>());
		wMap.put(commType, value);
	}

	public double getData(Map<SingleCountry, Map<WoodCommodityType, Double>> map, SingleCountry country, WoodCommodityType comm) {
		if (!ModelConfig.IS_FORESTRY_ON) {
			return 0.0;
		}

		if (map.containsKey(country) && map.get(country).containsKey(comm)) {
			return map.get(country).get(comm);
		} else {
			LogWriter.printlnError("WoodDemandManager: can't get value for " + comm + ", " + country);
			return 0.0;
		}
	}
	
	public double getBaseDemand(SingleCountry country, WoodCommodityType comm) {
		return getData(baseWoodDemand, country, comm);
	}

	public double getBaseConsumerPrice(SingleCountry country, WoodCommodityType comm) {
		return getData(baseConsumerPrices, country, comm);
	}

	public double getProduction(SingleCountry country, WoodCommodityType comm) {
		return getData(initialProduction, country, comm);
	}

	public double getNetImport(SingleCountry country, WoodCommodityType comm) {
		return getData(initialNetImports, country, comm);
	}

	private double getMarkup(SingleCountry country, WoodCommodityType comm, Map<WoodType, Double> producerPrices) {
		WoodType prodType = comm.getWoodType();

		if (ModelConfig.IS_CALIBRATION_RUN) {
			double markup = getBaseConsumerPrice(country, comm) / producerPrices.get(prodType);
			priceMarkups.get(country).put(prodType, markup);
		}

		return priceMarkups.get(country).get(prodType);
	}

	public double getConsumerPrice(SingleCountry country, WoodCommodityType comm, Map<WoodType, Double> producerPrices) {
		double markup = getMarkup(country, comm, producerPrices);
		return producerPrices.get(comm.getWoodType()) * markup;
	}

	public Map<CompositeCountry, WoodUsageData> getInitialWoodUsage() {
		Map<CompositeCountry, WoodUsageData> initialWoodUsage = new HashMap<>();

		for (CompositeCountry cc : CountryManager.getInstance().getAllCompositeCountries()) {
			Collection<SingleCountry> singleCountries = CountryManager.getInstance().getAllForCompositeCountry(cc);
			WoodUsageData usageData = new WoodUsageData(0, 0, Double.NaN);

			for (SingleCountry sc : singleCountries) {
				for (WoodCommodityType comm : WoodCommodityType.values()) {
					if (comm.equals(WoodCommodityType.RESIDUAL))
						continue;

					double updatedNetImports = usageData.getNetImport() + getNetImport(sc, comm);
					double updatedProduction = usageData.getProduction() + getProduction(sc, comm);
					usageData.setNetImports(updatedNetImports);
					usageData.setProduction(updatedProduction);
					}

			}
			initialWoodUsage.put(cc, usageData);
		}
		return initialWoodUsage;
	}

}
