package ac.ed.lurg.demand;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CompositeCountry;
import ac.ed.lurg.country.CountryPrice;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.WoodType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class DemandManagerFromFile extends AbstractDemandManager {

	private static final long serialVersionUID = -3296655986819599788L;
	private StringTabularReader historicalConsumptionReader;
	private StringTabularReader histWoodConsumpReader;

	public DemandManagerFromFile(CalorieManager calorieManager) {
		super(calorieManager);
		historicalConsumptionReader = new StringTabularReader(",", new String[]{"Year", "Iso3", "Item", "population", "cpc"});
		historicalConsumptionReader.read(ModelConfig.DEMAND_CONSUMPTION_FILE);
		
		histWoodConsumpReader = new StringTabularReader(",", new String[]{"iso3", "year", "type", "demand_m3", "net_imports", "harvest"});
		histWoodConsumpReader.read(ModelConfig.WOOD_DEMAND_FILE);
	}

	@Override
	protected Map<CropType, Double> getFoodDemand(SingleCountry c, int year, Map<CropType, CountryPrice> prices, boolean outputGamsDemand) {
		Map<CropType, Double> foodDemandMap = new HashMap<>();
        if (!ModelConfig.CHANGE_DEMAND_YEAR)
            year = ModelConfig.BASE_YEAR;

        for (CropType crop : CropType.getFoodTypes()) {
			Map<String, String> queryMap = new HashMap<String, String>();
			queryMap.put("Year", Integer.toString(year));
			queryMap.put("Iso3", c.getCountryCode());
			queryMap.put("Item", crop.getFaoName());
			try {
				Map<String, String> row = historicalConsumptionReader.querySingle(queryMap);
				String popS = row.get("population");
				String cpcS = row.get("cpc");
				Double pop = Double.valueOf(popS);
				Double cpc = Double.valueOf(cpcS);
				double d = cpc * pop;
				foodDemandMap.put(crop, d);
			}
			catch (Exception e) {
				LogWriter.printlnWarning("Problem finding food demand: " + crop.getFaoName() + ", " + year + ", " + c.getCountryName());
			}
		}

		return foodDemandMap;
	}
	
	@Override
	protected Map<WoodType, Double> getWoodDemand(SingleCountry country, int year, Map<WoodType, Double> prices) {
		Map<WoodType, Double> woodDemandMap = new HashMap<WoodType, Double>();

		for (WoodType woodType : WoodType.values()) {
			
			if (!ModelConfig.IS_FORESTRY_ON) {
				woodDemandMap.put(woodType, 0.0);
				continue;
			}
			
			Map<String, String> queryMap = new HashMap<String, String>();
			queryMap.put("year", Integer.toString(year));
			queryMap.put("iso3", country.getCountryName());
			queryMap.put("type", woodType.getName());
			try {
				Map<String, String> row = histWoodConsumpReader.querySingle(queryMap);
				String demand = row.get("demand_m3");
				double d = Double.valueOf(demand) * ModelConfig.WOOD_BIOMASS_CONVERSION_FACTOR;
				woodDemandMap.put(woodType, d);
			}
			catch (Exception e) {
				LogWriter.printlnWarning("Problem finding wood demand: " + woodType.getName() + ", " + year + ", " + country.getCountryName());
				woodDemandMap.put(woodType, 0.0);
			}
		}

		return woodDemandMap;
	}

	@Override
	double getCarbonDemand(SingleCountry country, int year) {
		// TODO Auto-generated method stub
		return 0;
	}

}