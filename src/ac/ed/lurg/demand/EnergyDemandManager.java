package ac.ed.lurg.demand;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.Interpolator;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EnergyDemandManager {

    private static final int DATA_INTERVAL = 5;
    private static final int GEN1_BASE_COUNTRY_COL = 0;
    private static final int GEN1_BASE_CROP_COL = 2;
    private static final int GEN1_BASE_DEMAND_COL = 6;

    private final StringTabularReader demandReader; // future demand in TWh
    private final Map<SingleCountry, Map<CropType, Double>> firstGenBaseData = new HashMap<>(); // base 1st gen crop demand in Mt

    public EnergyDemandManager() {
        demandReader = new StringTabularReader(",",
                new String[] {"Model", "Scenario", "Year", "Iso3", "Photovoltaics_TWh", "Agrivoltaics_TWh",
                        "Bioenergy1stGen_TWh", "Bioenergy2ndGen_TWh"});

        demandReader.read(ModelConfig.ENERGY_DEMAND_FILE);

        readFirstGenBaseData();
    }

    private void readFirstGenBaseData() {

        String filename = ModelConfig.BASELINE_CONSUMP_FILE;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line, countryCode, cropName;
            double demand;
            reader.readLine(); // read header

            while ((line=reader.readLine()) != null) {
                String[] tokens = line.split(",");

                if (tokens.length < 12)
                    LogWriter.printlnError("Too few columns in " + filename + ", " + line);

                countryCode = tokens[GEN1_BASE_COUNTRY_COL];
                cropName = tokens[GEN1_BASE_CROP_COL];
                demand = Double.parseDouble(tokens[GEN1_BASE_DEMAND_COL]);

                SingleCountry country = CountryManager.getInstance().getForCode(countryCode);
                CropType crop = CropType.getForFaoName(cropName);

                Map<CropType, Double> countryData = firstGenBaseData.computeIfAbsent(country, k -> new HashMap<>());
                countryData.merge(crop, demand, Double::sum);
            }
            reader.close();

        } catch (IOException e) {
            LogWriter.printlnError("Failed in gen 1 bioenergy base data");
            LogWriter.print(e);
        }
        LogWriter.println("Processed " + filename + ", create " + firstGenBaseData.size() + " country commodity maps values");
    }

    private double getDemand(SingleCountry country, Integer year, String type) {
        int yearBefore = (year / DATA_INTERVAL) * DATA_INTERVAL;
        int yearAfter = yearBefore + DATA_INTERVAL;
        double demandBefore;
        double demandAfter;

        Map<String, String> queryMapBefore = new HashMap<>();
        queryMapBefore.put("Model", ModelConfig.ENERGY_DEMAND_MODEL);
        queryMapBefore.put("Scenario", ModelConfig.ENERGY_DEMAND_SCENARIO);
        queryMapBefore.put("Iso3", country.getCountryCode());
        queryMapBefore.put("Year", Integer.toString(yearBefore));
        Map<String, String> rowBefore = demandReader.querySingle(queryMapBefore);
        demandBefore = Double.parseDouble(rowBefore.get(type));

        if (yearBefore == ModelConfig.BASE_YEAR + ModelConfig.END_TIMESTEP * ModelConfig.TIMESTEP_SIZE) {
            // final data point so return without interpolation
            return demandBefore;
        }

        Map<String, String> queryMapAfter = new HashMap<>();
        queryMapAfter.put("Model", ModelConfig.ENERGY_DEMAND_MODEL);
        queryMapAfter.put("Scenario", ModelConfig.ENERGY_DEMAND_SCENARIO);
        queryMapAfter.put("Iso3", country.getCountryCode());
        queryMapAfter.put("Year", Integer.toString(yearAfter));
        Map<String, String> rowAfter = demandReader.querySingle(queryMapAfter);
        demandAfter = Double.parseDouble(rowAfter.get(type));

        double factor = (double) (year - yearBefore) / (yearAfter - yearBefore);
        double demand = Interpolator.interpolate(demandBefore, demandAfter, factor);
        return  demand;
    }

    public double getPhotovoltaicsDemand(SingleCountry country, Integer year) {
        return getDemand(country, year, "Photovoltaics_TWh");
    }

    public double getAgrivoltaicsDemand(SingleCountry country, Integer year) {
        return getDemand(country, year, "Agrivoltaics_TWh");
    }

    public double getBioenergy1stGenDemand(SingleCountry country, Integer year) {
        return getDemand(country, year, "Bioenergy1stGen_TWh") * ModelConfig.BIOENERGY_DEMAND_SHIFT;
    }

    public double getBioenergy2ndGenDemand(SingleCountry country, Integer year) {
        return getDemand(country, year, "Bioenergy2ndGen_TWh") * ModelConfig.BIOENERGY_DEMAND_SHIFT;
    }

    public double getFirstGenCropBaseDemand(SingleCountry country, CropType crop) {
        return firstGenBaseData.get(country).get(crop) * ModelConfig.BIOENERGY_DEMAND_SHIFT;
    }
}
