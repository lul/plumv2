package ac.ed.lurg.demand;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;

public class BaseConsumpManager {
	private static final int COUNTRY_COL = 0; 
	private static final int COMMODITY_COL = 1;
	private static final int CROP_COL = 2;
	private static final int BASE_CROP_COL = 3; // Mt crop per year
	private static final int BASE_CPC_COL = 5; // kcal per person per day

	private final Map<SingleCountry, Map<CommodityType, Double>> baseConsumpMap = new HashMap<>();
	private final Map<SingleCountry, Map<CropType, Double>> baseCropMap = new HashMap<>();

	public BaseConsumpManager() {
		super();
		read();
	}
	
	public void read() {
		String filename = ModelConfig.BASELINE_CONSUMP_FILE;
		try {
			BufferedReader fitReader = new BufferedReader(new FileReader(filename)); 
			String line, countryCode, commodityName, cropName;
			double cpc, cropAmount;
			fitReader.readLine(); // read header

			while ((line=fitReader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 12)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				countryCode = tokens[COUNTRY_COL];
				commodityName = tokens[COMMODITY_COL];
				cropName = tokens[CROP_COL];
				cpc = Double.parseDouble(tokens[BASE_CPC_COL]);
				cropAmount = Double.parseDouble(tokens[BASE_CROP_COL]);

				SingleCountry country = CountryManager.getInstance().getForCode(countryCode);
				if (country == null)
					LogWriter.printlnError("Null country for " + countryCode);
				else {
					CommodityType commodity = CommodityType.getForFaoName(commodityName);
					CropType crop = CropType.getForFaoName(cropName);

					Map<CommodityType, Double> commodityMap = baseConsumpMap.computeIfAbsent(country, k -> new HashMap<>());
					Map<CropType, Double> cropMap = baseCropMap.computeIfAbsent(country, k -> new HashMap<>());

					commodityMap.merge(commodity, cpc, Double::sum); // aggregate for multiple crops
					cropMap.put(crop, cropAmount);
				}
			} 
			fitReader.close(); 
		
		} catch (Exception e) {
			LogWriter.printlnError("Failed in reading base consumption");
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename + ", create " + baseConsumpMap.size() + " base consumption values");
	}
	
	public double get(SingleCountry country, CommodityType commodity) {
		Map<CommodityType, Double> commodityMap = baseConsumpMap.get(country);

		if (commodityMap == null || !commodityMap.containsKey(commodity)) {
			if (commodity != CommodityType.NONFOOD)  // don't report this, as expected for nonfood
				LogWriter.printlnError("BaseConsumpManager: can't get value for " + commodity + ", " + country);
			return 0.0;
		}

		return commodityMap.get(commodity);
	}

	public double getCrop(SingleCountry country, CropType crop) {
		return baseCropMap.get(country).getOrDefault(crop, 0.0);
	}

}
