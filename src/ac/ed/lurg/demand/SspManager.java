package ac.ed.lurg.demand;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.utils.LogWriter;

public class SspManager {
//	private static final int MODEL_COL = 0; 
	private static final int COUNTRY_COL = 0; 
	private static final int SCENARIO_COL = 1; 
	private static final int YEAR_COL = 2; 
	private static final int POPULATION_COL = 3; 
	private static final int GDP_COL = 4; 

	private Map<SspKey, SspData> sspMap = new HashMap<SspKey, SspData>();

	public SspManager() {
		super();
		read();
	}
	
	public void read() {
		String filename = ModelConfig.SSP_FILE;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename)); 
			String line, scenario, countryCode;
			double population, gdp;
			int year;
			reader.readLine(); // read header

			while ((line=reader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 5)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				try {
				//model = tokens[MODEL_COL];
					scenario = tokens[SCENARIO_COL];
					year = Integer.parseInt(tokens[YEAR_COL]);
					population = Double.parseDouble(tokens[POPULATION_COL]);
					countryCode = tokens[COUNTRY_COL];
					gdp = Double.parseDouble(tokens[GDP_COL]);
	
					SspKey sspKey = new SspKey(scenario, year, CountryManager.getInstance().getForCode(countryCode));
					SspData sspData = new SspData(population, gdp);
					sspMap.put(sspKey, sspData);
				} catch (Exception e) {
					LogWriter.printlnError("Failed in processing ssp line " + line);
					LogWriter.print(e);
				} 
			} 
			reader.close(); 
		
		} catch (Exception e) {
			LogWriter.printlnError("Failed in reading ssp data file");
			LogWriter.print(e);
		} 
		LogWriter.println("Processed " + filename + ", create " + sspMap.size() + " SSP entries");
	}
	
	public SspData get(String scenario, int year, SingleCountry country) {
		SspData targetYearData = lookup(scenario, year, country);

		// Adjust population and GDP for Monte Carlo
		double popFactor = ModelConfig.getAdjParam("SSP_POPULATION_FACTOR");
		double gdpFactor = ModelConfig.getAdjParam("SSP_GDP_PC_FACTOR");

		if (targetYearData != null) {
			return new SspData(targetYearData.getPopulation() * popFactor, targetYearData.getGdp() * gdpFactor);
		}

		SspData upYearData = null, downYearData = null;
		int downYear = year-1, upYear = year+1;
		
		for (; upYear <= year+10; upYear++) {
			upYearData = lookup(scenario, upYear, country);
			if (upYearData != null)
				break;
		}
		
		for (; downYear >= year-10; downYear--) {
			downYearData = lookup(scenario, downYear, country);
			if (downYearData != null)
				break;
		}
		
		if (upYearData!=null && downYearData != null) {
			double factor = ((double)(year - downYear)) / (upYear - downYear);
			
			double pop = interpolate(upYearData.getPopulation(), downYearData.getPopulation(), factor);
			double gdp = interpolate(upYearData.getGdp(), downYearData.getGdp(), factor);
			
			return new SspData(pop * popFactor, gdp * gdpFactor);
		}
		else {
			LogWriter.println("Warning: Can't find straddling year for " + year + ", " + scenario + ", " + country.getCountryName(), 3);
			return null;
		}
	}

	private double interpolate(double up, double down, double factor) {
		double res = down + factor * (up - down);
		return res;
	}
	
	private SspData lookup(String scenario, int year, SingleCountry country) {
		SspKey sspKey = new SspKey(scenario, year, country);
		return sspMap.get(sspKey);
	}
	
	public double getWorldGdp(String scenario, int year, Collection<SingleCountry> worldCountries) {
		double worldGdp = 0.0;
		for (SingleCountry c : worldCountries) {
			SspData countryData = get(scenario, year, c);
			double countryGdp = countryData != null ? countryData.getGdp() : 0.0;
			worldGdp += countryGdp;
		}
		return worldGdp;
	}

}
