package ac.ed.lurg.demand;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.utils.LogWriter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class CalorieManager {
	private static final int COUNTRY_COL = 0; 
	private static final int CROP_COL = 2;
	private static final int CALORIE_PER_T_COL = 7;


	private final Map<SingleCountry, Map<CropType, Double>> calorieMap = new HashMap<>();

	public CalorieManager() {
		super();
		read();
	}
	
	public void read() {
		String filename = ModelConfig.BASELINE_CONSUMP_FILE;
		try {
			BufferedReader fitReader = new BufferedReader(new FileReader(filename)); 
			String line, countryCode, cropName;
			double kcalPerT;
			fitReader.readLine(); // read header

			while ((line=fitReader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 12)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				countryCode= tokens[COUNTRY_COL];
				cropName = tokens[CROP_COL];
				kcalPerT = Double.parseDouble(tokens[CALORIE_PER_T_COL]);
			

				SingleCountry country = CountryManager.getInstance().getForCode(countryCode);
				if (country == null)
					LogWriter.printlnError("Null country for " + countryCode);
				else {
					CropType crop = CropType.getForFaoName(cropName);
					
					Map<CropType, Double> cropMap = calorieMap.computeIfAbsent(country, k -> new HashMap<>());
					cropMap.put(crop, kcalPerT);
				}
			} 
			fitReader.close(); 
		
		} catch (Exception e) {
			LogWriter.printlnError("Failed in reading country specific calorie per t values");
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename + ", create " + calorieMap.size() + " calorie per t values", 2);
	}
	
	public Map<CropType, Double> get(SingleCountry country) {
		return calorieMap.get(country);
	}
}
