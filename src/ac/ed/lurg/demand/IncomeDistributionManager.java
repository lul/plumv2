package ac.ed.lurg.demand;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.CountryManager;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.utils.LogWriter;

public class IncomeDistributionManager {
	private static final int COUNTRY_COL = 0; 
	private static final int YEAR_COL = 1; 
	private static final int SCENARIO_COL = 2; 
	private static final int DECILE_COL = 3; 
	private static final int GDP_SHARES_COL = 4; 
	
	private final Map<SspKey, Map<String, Double>> decilesMap = new HashMap<>();

	public IncomeDistributionManager() {
		super();
		read();
	}
	
	public void read() {
		String filename = ModelConfig.INCOME_DECILES_FILE;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename)); 
			String line, countryCode, scenario, decile; 
			int year;
			double predShares;
			reader.readLine(); // read header
			
			while ((line=reader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 5)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				try {
				//Load data by column
					countryCode = tokens[COUNTRY_COL];
					year = Integer.parseInt(tokens[YEAR_COL]);
					scenario = tokens[SCENARIO_COL];
					decile = tokens[DECILE_COL];
					predShares = Double.parseDouble(tokens[GDP_SHARES_COL]);
					SingleCountry country = (CountryManager.getInstance().getForCode(countryCode)!=null) ? CountryManager.getInstance().getForCode(countryCode) : null;
					
					//do not load data for countries not included in PLUMv2
					if (country != null) {
						SspKey sspKey = new SspKey(scenario, year, country);
						
						Map<String, Double> subDecileMap = decilesMap.getOrDefault(sspKey, new HashMap<>());
						subDecileMap.put(decile, predShares);
						decilesMap.put(sspKey, subDecileMap);
					}
				} catch (Exception e) {
					LogWriter.printlnError("Failed in processing income deciles line " + line);
					LogWriter.print(e);
				} 
			} 
			reader.close(); 
		
		} catch (Exception e) {
			LogWriter.printlnError("Failed in reading income deciles data file");
			LogWriter.print(e);
		} 
		LogWriter.println("Processed " + filename + ", create " + decilesMap.size() + " decile income shares.");
	}
	
	
	public Map<String, Double> getPredShare(String scenario, int year, SingleCountry country) {
		if (!ModelConfig.IS_INEQUALITY_ON) {
			return new HashMap<>();
		}
		SspKey ssp = new SspKey(scenario, year, country);
		if (decilesMap.containsKey(ssp)) {
			return decilesMap.get(ssp);
		} else { try{
			Map<String, Double> regionalAverages = new HashMap<String, Double>();
			ArrayList<String> incomeDeciles = new ArrayList<>();
		        for (int i = 1; i <= 10; i++) {
		            incomeDeciles.add("d" + i);
		        }
			for (String decile:incomeDeciles) {
				regionalAverages.put(decile, regionalAverageShare(scenario, year, country, decile));
			}
			return regionalAverages;
		} catch(Exception e) {
			LogWriter.printlnError("IncomeDistributionManager: can't get GDP shares for " + ssp.getScenario());
			return new HashMap<>();
		}
		}
	}
	
	public double getPredShare(String scenario, int year, SingleCountry country, String decile ) {
		if (!ModelConfig.IS_INEQUALITY_ON) {
			return 0.0;
		}
		SspKey ssp = new SspKey(scenario, year, country);
		if (decilesMap.containsKey(ssp) && decilesMap.get(ssp).containsKey(decile)) {
			return decilesMap.get(ssp).get(decile);
		} else { try {
			return regionalAverageShare(scenario, year, country, decile);
		} catch(Exception e) {
			LogWriter.printlnError("IncomeDistributionManager: can't get GDP shares for " + ssp.getScenario() + ", income group " + decile);
			return 0.0;
		}
		}
	}
	
	public double getGdpPc(double gdp, double pop, String scenario, int year, SingleCountry country, String decile ) {
		if (!ModelConfig.IS_INEQUALITY_ON) {
			return 0.0;
		} else {
			double decilePop = pop/10;
			double predShare = getPredShare(scenario, year, country, decile);
			return gdp * predShare / decilePop;
		} 
	}
	
	public Map<String, Double> getGdpPc(double gdp, double pop, int year, String scenario, SingleCountry country) {
		if (!ModelConfig.IS_INEQUALITY_ON) {
			return new HashMap<>();
		}
		else {
			double decilePop = pop/10;
			Map<String, Double> gdpPcMap = new HashMap<String, Double>();
			Map<String, Double> predShare = getPredShare(scenario, year, country);
			for (String decile : predShare.keySet()) {
	            double share = predShare.get(decile);
	            gdpPcMap.put(decile, gdp * share / decilePop);
	        }
			return gdpPcMap;
			
		}
	}
	
	//Function for calculating regional average income distributions where PLUM countries are absent in Narayan data
	//May need to edit to ignore empty fields!
	
	private Double regionalAverageShare(String scenario, int year, SingleCountry country, String decile){
		String region = country.getRegion();
		double sumIncomeShares = 0.0;
        int countryCount = 0;
        
        // Iterate through the entries of the decilesMap
        for (Map.Entry<SspKey, Map<String, Double>> entry : decilesMap.entrySet()) {
            Map<String, Double> incomeShares = entry.getValue();
            String entryRegion = entry.getKey().getCountry().getRegion();
            String entrySsp = entry.getKey().getScenario();
            int entryYear = entry.getKey().getYear();
            boolean match = entryRegion.equals(region) && entrySsp.equals(scenario) && entryYear == year;

            if (match) {
                sumIncomeShares += incomeShares.get(decile);
                countryCount++;
            }}

        // Calculate the average income share for the given decile
        if (countryCount > 0) {
            return sumIncomeShares / countryCount;
        } else {
			LogWriter.printlnError("IncomeDistributionManager: can't get GDP shares for " + country + ", income group " + decile);
            return 0.0;
        }
    }

	
}
