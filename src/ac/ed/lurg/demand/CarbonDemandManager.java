package ac.ed.lurg.demand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.utils.StringTabularReader;

public class CarbonDemandManager {
	private final Map<Integer, Double> globalCarbonDemand = new HashMap<>(); // global demand for carbon offsets
	private final Map<Integer, Double> globalCarbonPrice = new HashMap<>(); // $1000/t
	
	public CarbonDemandManager() {
		readCarbonDemandData();
	}
	
	public void readCarbonDemandData() {
		String filename = ModelConfig.CARBON_DEMAND_FILE;
		StringTabularReader reader = new StringTabularReader(",",
				new String[] {"Year", "Demand_Mt", "Price"});
		reader.read(filename);
		List<Map<String, String>> rowList = reader.getRowList();
		for (Map<String, String> row : rowList) {
			int year = Integer.parseInt(row.get("Year"));
			double demand = Double.parseDouble(row.get("Demand_Mt"));
			double price = Double.parseDouble(row.get("Price"));
			globalCarbonDemand.put(year, demand);
			globalCarbonPrice.put(year, price);
		}
	}
	
	public double getGlobalCarbonDemand(int year) {
		return globalCarbonDemand.getOrDefault(year, Double.NaN);
	}

	public double getGlobalCarbonPrice(int year) {
		return globalCarbonPrice.getOrDefault(year, Double.NaN);
	}
}
