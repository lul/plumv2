package ac.ed.lurg.demand;

import ac.ed.lurg.country.SingleCountry;


public class SspKey {
	private String scenario;
	private int year;
	private SingleCountry country;
	
	public SspKey(String scenario, int year, SingleCountry country) {
		super();
		this.scenario = scenario;
		this.year = year;
		this.country = country;
	}

	public String getScenario() {
		return scenario;
	}

	public int getYear() {
		return year;
	}

	public SingleCountry getCountry() {
		return country;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result
				+ ((scenario == null) ? 0 : scenario.hashCode());
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SspKey other = (SspKey) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (scenario == null) {
			if (other.scenario != null)
				return false;
		} else if (!scenario.equals(other.scenario))
			return false;
		if (year != other.year)
			return false;
		return true;
	}
}
