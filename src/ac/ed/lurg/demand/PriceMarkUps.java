package ac.ed.lurg.demand;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.country.SingleCountry;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.StringTabularReader;

public class PriceMarkUps implements Serializable {
	private static final long serialVersionUID = -5958346414043401117L;

	private transient StringTabularReader initialConsumerPrices;
	private Map<CountryCommodityKey, Double> priceMarkupFactors = new HashMap<CountryCommodityKey, Double>();

	public PriceMarkUps() {
		initialConsumerPrices = new StringTabularReader(",", new String[]{"Iso3", "Item", "Price"});
		initialConsumerPrices.read(ModelConfig.INITIAL_CONSUMER_PRICE_FILE);
	}
	
	@Override
	public String toString() {
		return "PriceMarkUps [" + priceMarkupFactors + "]";
	}

	public double markedupPrice(SingleCountry c, CommodityType commodity, double producerPrice) {
		CountryCommodityKey cc = new CountryCommodityKey(c, commodity);
		double priceMarkupFactor;

		if (!priceMarkupFactors.containsKey(cc) || ModelConfig.IS_CALIBRATION_RUN) {
			double consumerPrice = getInitialConsumerPrice(c, commodity);
			priceMarkupFactor = consumerPrice / producerPrice;
			LogWriter.println("priceMarkupFactor for " + c + ", " + commodity.getGamsName() + " is " + priceMarkupFactor, 3);
			priceMarkupFactors.put(cc, priceMarkupFactor);
		} else {
			priceMarkupFactor = priceMarkupFactors.get(cc);
		}
		
		double adjustedPrice = producerPrice * priceMarkupFactor;
		LogWriter.println(String.format("PriceMarkUps: %s consumer price after markup of %.4f is %.4f", commodity, priceMarkupFactor, adjustedPrice), 3);

		return adjustedPrice;
	}
	
	private double getInitialConsumerPrice(SingleCountry country, CommodityType commodity) {
		
		double initialPrice;
		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put("Iso3", country.getCountryCode());
		queryMap.put("Item", commodity.getGamsName());

		try {
			Map<String, String> row = initialConsumerPrices.querySingle(queryMap);
			String priceS = row.get("Price");
			initialPrice = Double.parseDouble(priceS);
		} catch (Exception e) {
			LogWriter.println("Problem finding initial consumer price " + commodity.getGamsName() + ", " + commodity.getGamsName() + ", " + country.getCountryName());
			initialPrice=commodity.getDefaultInitialPrice();
		}

		return initialPrice;
	}
	
	private class CountryCommodityKey implements Serializable {
		private static final long serialVersionUID = 1188805791760401424L;
		private SingleCountry country;
		private CommodityType commodity;
		CountryCommodityKey (SingleCountry country, CommodityType commodity) {
			this.country = country;
			this.commodity = commodity;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((commodity == null) ? 0 : commodity.hashCode());
			result = prime * result + ((country == null) ? 0 : country.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CountryCommodityKey other = (CountryCommodityKey) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (commodity != other.commodity)
				return false;
			if (country == null) {
				if (other.country != null)
					return false;
			} else if (!country.equals(other.country))
				return false;
			return true;
		}
		private PriceMarkUps getOuterType() {
			return PriceMarkUps.this;
		}

		@Override
		public String toString() {
			return "[" + country + ", " + commodity + "]";
		}
	}
}
