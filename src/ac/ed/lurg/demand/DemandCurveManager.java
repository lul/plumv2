package ac.ed.lurg.demand;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.CommodityType;
import ac.ed.lurg.types.ModelFitType;
import ac.ed.lurg.utils.LogWriter;

public class DemandCurveManager {
	private static final int ITEM_COL = 0; 
	private static final int A_COL = 1; 
	private static final int B_COL = 2; 
	private static final int C_COL = 3; 
	private static final int FIT_TYPE_COL = 6; 
	
	private Collection<DemandCurve> curves = new HashSet<DemandCurve>();

	public DemandCurveManager() {
		super();
		read();
	}
	
	public void read() {
		String filename = ModelConfig.DEMAND_CURVES_FILE;
		try {
			BufferedReader fitReader = new BufferedReader(new FileReader(filename)); 
			String line, faoItemName, fitType;
			double a, b, c;
			fitReader.readLine(); // read header

			while ((line=fitReader.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (tokens.length < 7)
					LogWriter.printlnError("Too few columns in " + filename + ", " + line);
				
				faoItemName = tokens[ITEM_COL];
				a = Double.parseDouble(tokens[A_COL]);
				b = Double.parseDouble(tokens[B_COL]);
				c = Double.parseDouble(tokens[C_COL]);
				fitType = tokens[FIT_TYPE_COL];

				DemandCurve dc = new DemandCurve(CommodityType.getForFaoName(faoItemName), a, b, c, ModelFitType.findByName(fitType));
				curves.add(dc);
			} 
			fitReader.close(); 
		
		} catch (IOException e) {
			LogWriter.printlnError("Failed in reading commodity demand fits");
			LogWriter.print(e);
		}
		LogWriter.println("Processed " + filename + ", create " + curves.size() + " demand curves", 2);
	}
	
	public DemandCurve get(CommodityType commodity, ModelFitType fitType) {		
		for (DemandCurve dc : curves) {
			if (dc.getFitType().equals(fitType) & dc.getCommodityType().equals(commodity)) {
				return dc;
			}
		}	
		throw new RuntimeException("Can't find DemandCurve for " + fitType + ", " + commodity);
	}
}
