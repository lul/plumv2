package ac.ed.lurg.types;

import java.util.HashMap;
import java.util.Map;
import ac.ed.lurg.ModelConfig;

public enum WoodType {
	IND_ROUNDWOOD("roundwood"),
	FUELWOOD("fuelwood");
	
	private String name;
	
	WoodType(String name) {
		this.name = name;
	}

	private static final Map<String, WoodType> nameCache = new HashMap<String, WoodType>();

	static {
		for (WoodType w : values()) {
			nameCache.put(w.getName(), w);
		}
	}

	public static WoodType getForName(String name) {
		WoodType woodType = nameCache.get(name);

		if (woodType == null) 
			throw new RuntimeException("Can't find woodType for name: " + name);
		
		return woodType;
	}
	
	public String getName() {
		return name;
	}

}
