package ac.ed.lurg.types;

import java.util.HashMap;
import java.util.Map;

public enum FarmingType {

    CONVENTIONAL("conventional"),
    RESTRICTED("restricted"),
    AGRIVOLTAICS("agrivoltaics");

    private String name;

    FarmingType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private static final Map<String, FarmingType> nameCache = new HashMap<>();
    static {
        for (FarmingType p : values()) {
            nameCache.put(p.getName(), p);
        }
    }

    public static FarmingType getForName(String name) {
        return getFromCache(name);
    }

    private static FarmingType getFromCache(String name) {
        return FarmingType.nameCache.get(name);
    }

}
