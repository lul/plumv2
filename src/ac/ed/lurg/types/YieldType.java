package ac.ed.lurg.types;

import java.util.HashMap;
import java.util.Map;

public enum YieldType {
	NO_FERT_NO_IRRIG(FertiliserRate.NO_FERT, IrrigationRate.NO_IRRIG),
	FERT_MID_NO_IRRIG(FertiliserRate.MID_FERT, IrrigationRate.NO_IRRIG),
	FERT_MAX_NO_IRRIG(FertiliserRate.MAX_FERT, IrrigationRate.NO_IRRIG),
	NO_FERT_IRRIG_MAX(FertiliserRate.NO_FERT, IrrigationRate.MAX_IRRIG),
	FERT_MID_IRRIG_MAX(FertiliserRate.MID_FERT, IrrigationRate.MAX_IRRIG),
	FERT_MAX_IRRIG_MAX(FertiliserRate.MAX_FERT, IrrigationRate.MAX_IRRIG);
	
	private FertiliserRate fertiliserRate;
	private IrrigationRate irrigationRate;

	YieldType(FertiliserRate fertiliserRate, IrrigationRate irrigationRate) {
		this.fertiliserRate = fertiliserRate;
		this.irrigationRate = irrigationRate;
	}

	private static final Map<FertiliserRate, Map<IrrigationRate, YieldType>> yieldTypes = new HashMap<FertiliserRate, Map<IrrigationRate, YieldType>>();
    static {
        for (YieldType yt : values()) {
         	Map<IrrigationRate, YieldType> irMap = yieldTypes.get(yt.getFertiliserRate());
        	if (irMap == null) {
        		irMap= new HashMap<IrrigationRate, YieldType>();
        		yieldTypes.put(yt.getFertiliserRate(), irMap);
        	}
        	
        	irMap.put(yt.getIrrigationRate(), yt);
       }
    }

	public static YieldType getYieldType(FertiliserRate fertiliserRate, IrrigationRate irrigationRate) {
		return yieldTypes.get(fertiliserRate).get(irrigationRate);
	}
	
	public FertiliserRate getFertiliserRate() {
		return fertiliserRate;
	}

	public IrrigationRate getIrrigationRate() {
		return irrigationRate;
	}
	
	public String getFertIrrigString() {
		return irrigationRate.getId() + fertiliserRate.getId();
	}
}
