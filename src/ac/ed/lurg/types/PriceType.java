package ac.ed.lurg.types;

import ac.ed.lurg.utils.LogWriter;

public enum PriceType {

	IMPORTONLY("importsOnly"),
	WEIGHTEDIMPEXP("weightedImportsExports"),
	WEIGHTEDINCLPRODCOSTS("weightedExportOrProductionCosts");
	
	private String name;
	
	PriceType(String name) {
		this.name = name;
	}
	
	public static PriceType findByName(String priceName){
	    for(PriceType p : values()){
	        if( p.name.equals(priceName)){
	            return p;
	        }
	    }
	    
	    LogWriter.printlnError("No PriceType found for " + priceName);
	    return null;
	}
}

