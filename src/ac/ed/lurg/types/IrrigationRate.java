package ac.ed.lurg.types;

public enum IrrigationRate {
	NO_IRRIG(""), 
	MAX_IRRIG("i");
	
	private String irrigId;
	
	IrrigationRate(String irrigId) {
		this.irrigId = irrigId;
	}

	public String getId() {
		return irrigId;
	}
}
