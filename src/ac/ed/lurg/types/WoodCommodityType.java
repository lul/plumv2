package ac.ed.lurg.types;

import java.util.HashMap;
import java.util.Map;

public enum WoodCommodityType {
    INDUSTRIAL_ROUNDWOOD("Roundwood", "Industrial roundwood", WoodType.IND_ROUNDWOOD),
    WOODFUEL("Woodfuel", "Wood fuel", WoodType.FUELWOOD),
    RESIDUAL("Residual", "Residual", null); // non-wood

    private String gamsName;
    private String faoName;
    private WoodType woodType;

    WoodCommodityType(String gamsName, String faoName, WoodType woodType) {
        this.gamsName = gamsName;
        this.faoName = faoName;
        this.woodType = woodType;
    }

    private static final Map<String, WoodCommodityType> gamsNameCache = new HashMap<>();
    private static final Map<String, WoodCommodityType> faoNameCache = new HashMap<>();

    static {
        for (WoodCommodityType c : values()) {
            gamsNameCache.put(c.getGamsName(), c);
            faoNameCache.put(c.getFaoName(), c);
        }
    }

    public String getGamsName() {
        return gamsName;
    }

    public String getFaoName() {
        return faoName;
    }

    public WoodType getWoodType() {
        return woodType;
    }

    public static WoodCommodityType getForGamsName(String gamsName, boolean allowNulls) {
        WoodCommodityType commodity = gamsNameCache.get(gamsName);

        if (commodity == null & !allowNulls)
            throw new RuntimeException("Can't find WoodCommodityType for gamsName: " + gamsName);

        return commodity;
    }

    public static WoodCommodityType getForGamsName(String gamsName) {
        return getForGamsName(gamsName, false);
    }

    public static WoodCommodityType getForFaoName(String faoame) {
        WoodCommodityType commodity = faoNameCache.get(faoame);

        if (commodity == null)
            throw new RuntimeException("Can't find WoodCommodityType for gamsName: " + faoame);

        return commodity;
    }

}
