package ac.ed.lurg.types;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import ac.ed.lurg.ModelConfig;

public enum CommodityType {

	//processedFull$cb_fs[is.na(fcr) & Country=="World" & Year == 2010, list(energy=sum(energy_kcal_pc*365/1000000*country_data[Country=="World" & Year == 2010]$population, na.rm=TRUE), food=sum(food)), by="plumDemandItem"][, list(plumDemandItem, energy/food)]
	//processedFull$cb_fs[!is.na(fcr) & Country=="World" & Year == 2010, list(energy=sum(energy_kcal_pc*365/1000000*country_data[Country=="World" & Year == 2010]$population, na.rm=TRUE), food=sum(food*fcr)), by="plumDemandItem"][, list(plumDemandItem, energy/food)]
	CEREALS_STARCHY_ROOTS("CerealsStarchyRoots", "CerealsStarchyRoots", false,
			new CropType[]{CropType.WHEAT, CropType.MAIZE, CropType.RICE, CropType.STARCHY_ROOTS},
			ModelConfig.INITAL_DEMAND_PRICE_CEREALS_STARCHY_ROOTS, 436.0),
	OILCROPS("Oilcrops", "Oilcrops", false, new CropType[]{CropType.OILCROPS_NFIX, CropType.OILCROPS_OTHER},
			ModelConfig.INITAL_DEMAND_PRICE_OILCROPS, 15.0),
	PULSES("Pulses", "Pulses", false, new CropType[]{CropType.PULSES},
			ModelConfig.INITAL_DEMAND_PRICE_PULSES, 8.0),
	MONOGASTRICS("Monogastrics", "Monogastrics", true, new CropType[]{CropType.MONOGASTRICS},
			ModelConfig.INITAL_DEMAND_PRICE_MONOGASTRICS, 3.0),
	RUMINANTS("Ruminants", "Ruminants", true, new CropType[]{CropType.RUMINANTS},
			ModelConfig.INITAL_DEMAND_PRICE_RUMINANTS, 20.0),
	FRUITVEG("FruitVeg", "FruitVeg", false, new CropType[]{CropType.FRUITVEG},
			ModelConfig.INITAL_DEMAND_PRICE_FRUITVEG, 21.0),
	SUGAR("Sugar", "Sugar", false, new CropType[]{CropType.SUGAR},
			ModelConfig.INITAL_DEMAND_PRICE_SUGAR, 35.0),
	NONFOOD("Nonfood", "Nonfood", false, new CropType[]{}, Double.NaN, 0.0);

	private String faoName;
	private String gamsName;
	private boolean isAnimalProduct;
	private Collection<CropType> cropTypes;
	private double initialPrice;
	private double minFAOCalorieIntake;

	CommodityType (String faoName, String gamsName, boolean isAnimalProduct, CropType[] cropTypeMapping,
			double initialPrice, double minFAOCalorieIntake) {
		this.faoName = faoName;
		this.gamsName = gamsName;
		this.isAnimalProduct = isAnimalProduct;
		this.cropTypes = Arrays.asList(cropTypeMapping);
		this.initialPrice = initialPrice;
		this.minFAOCalorieIntake = minFAOCalorieIntake;
	}

	private static final Map<String, CommodityType> faoNameCache = new HashMap<String, CommodityType>();
	private static final Map<String, CommodityType> gamsNameCache = new HashMap<String, CommodityType>();
	private static final Collection<CommodityType> foodsToConsider = new HashSet<CommodityType>();
	private static final Map<CropType, CommodityType> cropCache = new HashMap<>();
	static {
		for (CommodityType c : values()) {
			faoNameCache.put(c.getFaoName(), c);
			gamsNameCache.put(c.getGamsName(), c);

			if (!c.equals(NONFOOD))
				foodsToConsider.add(c);

			for (CropType crop : c.getCropTypes()) {
				cropCache.put(crop, c);
			}
		}
	}

	public static CommodityType getForFaoName(String faoName) {
		CommodityType commodity = faoNameCache.get(faoName);

		if (commodity == null) 
			throw new RuntimeException("Can't find CommodityType for faoName: " + faoName);

		return commodity;
	}

	public static CommodityType getForGamsName(String gamsName) {
		return getForGamsName(gamsName, false);
	}

	public static CommodityType getForGamsName(String gamsName, boolean allowNulls) {
		CommodityType commodity = gamsNameCache.get(gamsName);

		if (commodity == null & !allowNulls) 
			throw new RuntimeException("Can't find CommodityType for gamsName: " + gamsName);

		return commodity;
	}

	public static CommodityType getFroCrop(CropType crop) {
		return cropCache.get(crop);
	}

	public static Collection<CommodityType> getAllFoodItems() {
		return foodsToConsider;
	}

	public String getFaoName() {
		return faoName;
	}

	public String getGamsName() {
		return gamsName;
	}

	public boolean isAnimalProduct() {
		return isAnimalProduct;
	}

	public Collection<CropType> getCropTypes(){
		return cropTypes;
	}

	public double getDefaultInitialPrice() {
		return initialPrice;
	}

	public double getMinFAOCalorieIntake() {
		return minFAOCalorieIntake;
	}
}