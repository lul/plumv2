package ac.ed.lurg.types;

import ac.ed.lurg.utils.LogWriter;

public enum ModelFitType {

	LOGLINEAR("loglinear"),
	KUZNETS("kuznets"),
	LOGISTIC("logistic");
	
	private String name;
	
	ModelFitType(String name) {
		this.name = name;
	}
	
	public static ModelFitType findByName(String fitName){
	    for(ModelFitType f : values()){
	        if( f.name.equals(fitName)){
	            return f;
	        }
	    }
	    
	    LogWriter.printlnError("No ModelFitType found for " + fitName);
	    return null;
	}
}
