package ac.ed.lurg.types;

import java.util.HashMap;
import java.util.Map;

public class CropToDoubleMap extends HashMap<CropType, Double> {

	private static final long serialVersionUID = -4347578627666847612L;

	public void incrementValue(CropType crop, double inc) {
		if (containsKey(crop)) {
			double currentValue = get(crop);
			put(crop, currentValue + inc);
		}
		else {
			put(crop, inc);
		}
	}
	
	/** parameter map must have all the keys in instance*/
	public CropToDoubleMap divideBy(HashMap<CropType, Double> valuesToDivideBy) {
		CropToDoubleMap result = new CropToDoubleMap();
		
		for (Map.Entry<CropType, Double> entry : entrySet()) {
			CropType crop = entry.getKey();
			Double divideBy = valuesToDivideBy.get(crop);
			if (divideBy == null)
				throw new RuntimeException("CropToDoubleMap.divideBy parameter has no value for " + crop);
			
			result.put(crop, entry.getValue()/divideBy);
		}
		return result;
	}
	
	public void divideBy(int divideBy) {
		
		for (Map.Entry<CropType, Double> entry : entrySet()) {
			
			if (divideBy == 0)
				throw new RuntimeException("CropToDoubleMap.divideBy number of countries to divide by is zero");
			
			Double value = entry.getValue()/(double)divideBy;
			
			entry.setValue(value);
		}
	}

}
