package ac.ed.lurg.types;

import java.util.HashMap;
import java.util.Map;

public class CropToDouble extends HashMap<CropType, Double> {

	private static final long serialVersionUID = -5489510333965370914L;

	public CropToDouble scale(double factor) {
		CropToDouble out = new CropToDouble();
		for (Map.Entry<CropType, Double> e : entrySet())
			out.put(e.getKey(), e.getValue() * factor);
		
		return out;
	}

	public double getTotal() {
		double total = 0;
		for (double d : values())
			total += d;
		
		return total;
	}
}
