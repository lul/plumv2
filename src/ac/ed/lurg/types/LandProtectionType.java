package ac.ed.lurg.types;

import java.util.HashMap;
import java.util.Map;

public enum LandProtectionType {
	UNPROTECTED("unprotected"),
	PROTECTED("protected");

	private final String gamsName;

	LandProtectionType(String gamsName) {
		this.gamsName = gamsName;
	}

	public String getGamsName() {
		return gamsName;
	}

	private static final Map<String, LandProtectionType> gamsNameCache = new HashMap<>();
	static {
		for (LandProtectionType p : values()) {
			gamsNameCache.put(p.getGamsName(), p);
		}
	}

	public static LandProtectionType getForGamsName(String gamsName) {
		return getFromCache(gamsName);
	}

	private static LandProtectionType getFromCache(String name) {
		return LandProtectionType.gamsNameCache.get(name);
	}

}
