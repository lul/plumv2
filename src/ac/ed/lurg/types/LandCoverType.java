package ac.ed.lurg.types;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import ac.ed.lurg.utils.LogWriter;

public enum LandCoverType {

	TIMBER_FOREST("timberForest", true, false),
	CARBON_FOREST("carbonForest", true, false),
	NATURAL ("natural", true, false),
	CROPLAND ("cropland", true, true),
	PASTURE ("pasture", true, false),
	PHOTOVOLTAICS("photovoltaics", true, true),
	AGRIVOLTAICS("agrivoltaics", true, true),
	BARREN ("barren", false, false),
	URBAN("urban", false, false);

	private final String name;
	private final boolean isConvertible;
	private final boolean isConstrained; // maximum area in grid cell constrained due to factors like slope
	
	LandCoverType(String name, boolean isConvertible, boolean isConstrained) {
		this.name = name;
		this.isConvertible = isConvertible;
		this.isConstrained = isConstrained;
	}
	
	public String getName() {
		return name;
	}

	public boolean isConvertible() {
		return isConvertible;
	}

	public boolean isConstrained() {
		return isConstrained;
	}

	private static final Map<String, LandCoverType> nameCache = new HashMap<String, LandCoverType>();
    static {
        for (LandCoverType c : values()) {
        	nameCache.put(c.getName(), c);

       }
    }

	public static LandCoverType getForName(String name) {
		LandCoverType type = nameCache.get(name);
		
		if (type == null)
			LogWriter.printlnError("Can't find LandCoverType for " + name);
		
		return type;
	}

	public static Collection<LandCoverType> getConvertibleTypes() {

		Collection<LandCoverType> convertibleTypes = new HashSet<LandCoverType>();

		for (LandCoverType c : values())
			if (c.isConvertible)
				convertibleTypes.add(c);

		return convertibleTypes;

	}
}

