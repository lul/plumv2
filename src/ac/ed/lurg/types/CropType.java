package ac.ed.lurg.types;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.utils.LogWriter;

public enum CropType {

	WHEAT("WheatBarleyOats", "wheat", true, false, true, 8.7, ModelConfig.INITIAL_PRICE_WHEAT, 0.3),
	MAIZE("MaizeMilletSorghum", "maize", true, false, true, 6.3, ModelConfig.INITIAL_PRICE_MAIZE, 0.3),
	RICE("Rice (Paddy Equivalent)", "rice", true, false, true, 6.6, ModelConfig.INITIAL_PRICE_RICE, 0.3),
	OILCROPS_NFIX("Oilcrops N-fixing", "oilcropsNFix", true, false, true, 6.2, ModelConfig.INITIAL_PRICE_OILCROPS_NFIX, 0.3),
	OILCROPS_OTHER("Oilcrops Other", "oilcropsOther", true, false, true, 7.6, ModelConfig.INITIAL_PRICE_OILCROPS_OTHER, 0.3),
	PULSES("Pulses", "pulses", true, false, true, 10.9, ModelConfig.INITIAL_PRICE_PULSES, 0.3),
	STARCHY_ROOTS("Starchy Roots", "starchyRoots", true, false, true, 13.6, ModelConfig.INITIAL_PRICE_STARCHYROOTS, 0.3),
	ENERGY_CROPS("Energy crops", "energycrops", true, false, false, 5, ModelConfig.INITIAL_PRICE_ENERGYCROPS, 0.3),
	SETASIDE("setaside", "setaside", false, false, false, 0, 0, 0),
	MONOGASTRICS("Monogastrics", "monogastrics", true, true, true, 2.5, ModelConfig.INITIAL_PRICE_MONOGASTRICS, 0.3),
	RUMINANTS("Ruminants", "ruminants", true, true, true, 1.3, ModelConfig.INITIAL_PRICE_RUMINANTS, 0.3),
	FRUITVEG("FruitVeg", "fruitveg", true, false, true, 8.9, ModelConfig.INITIAL_PRICE_FRUITVEG, 0.3),
	SUGAR("Sugar", "sugar", true, false, true, 7.5, ModelConfig.INITIAL_PRICE_SUGAR, 0.3),
	PASTURE("pasture", "pasture", false, false, false, 0, 0, 0);  // confusing having a land cover and a crop type.  Needed here for yields (but not used for cropland area fractions).

	private String faoName;
	private String gamsName;
	private boolean importedCrop;
	private boolean isMeat;
	private boolean isFood;
	private double seedAndWasteRate;
	private double initialPrice;
	private double stockToUseRatio; // target stock to use (production) ratio
	
	private static Collection<CropType> importedCrops;

	CropType (String faoName, String gamsName, boolean importedCrop, boolean isMeat, boolean isFood, double seedAndWastePercent, double initialPrice,
			double stockToUseRatio) {
		this.faoName = faoName;
		this.gamsName = gamsName;
		this.importedCrop = importedCrop;
		this.isMeat = isMeat;
		this.isFood = isFood;
		this.seedAndWasteRate = seedAndWastePercent/100;
		this.initialPrice = initialPrice;
		this.stockToUseRatio = stockToUseRatio;
	}
	
	public static Collection<CropType> getCropsLessPasture() {
		Collection<CropType> comms = new HashSet<CropType>();
		
		for (CropType c : values())
			if (!c.equals(CropType.PASTURE) && !c.isMeat)
				comms.add(c);
		
		return comms;
	}

	public static synchronized Collection<CropType> getImportedTypes() {
		if (importedCrops == null) {
			importedCrops = new LinkedList<CropType>(); // what consistent order
		
			for (CropType c : values())
				if (c.importedCrop)
					importedCrops.add(c);
		}
		
		return importedCrops;
	}
	
	public static Collection<CropType> getNonMeatTypes() {
		Collection<CropType> comms = new HashSet<CropType>();
		
		for (CropType c : values())
			if (!c.isMeat)
				comms.add(c);
		
		return comms;
	}
	
	public static Collection<CropType> getMeatTypes() {
		Collection<CropType> comms = new HashSet<CropType>();
		
		for (CropType c : values())
			if (c.isMeat)
				comms.add(c);
		
		return comms;
	}

	public static Collection<CropType> getFoodTypes() {
		Collection<CropType> comms = new HashSet<CropType>();

		for (CropType c : values())
			if (c.isFood)
				comms.add(c);

		return comms;
	}


	private static final Map<String, CropType> faoNameCache = new HashMap<String, CropType>();
	private static final Map<String, CropType> gamsNameCache = new HashMap<String, CropType>();
    static {
        for (CropType c : values()) {
        	faoNameCache.put(c.getFaoName(), c);
        	gamsNameCache.put(c.getGamsName(), c);
       }
    }

	public static CropType getForFaoName(String faoName) {
		CropType crop = getFromCache(faoNameCache, faoName);
		if (crop == null) {
			throw new RuntimeException("Can't find CropType for faoName: " + faoName);
		}
		return crop;
	}
	
	public static CropType getForGamsName(String gamsName) {
		return getFromCache(gamsNameCache, gamsName);
	}
	
	private static CropType getFromCache(Map<String, CropType> cache, String name) {
		CropType type = cache.get(name);
		
		if (type == null)
			LogWriter.printlnError("Can't find Item for " + name);
		
		return type;
	}
	
	public static Collection<CropType> getAllItems() {
		return gamsNameCache.values();
	}

	public String getFaoName() {
		return faoName;
	}

	public String getGamsName() {
		return gamsName;
	}
	
	public boolean isImportedCrop() {
		return importedCrop;
	}

	public double getSeedAndWasteRate() {
		return seedAndWasteRate;
	}

	public double getInitialPrice() {
		return initialPrice;
	}

	public double getStockToUseRatio() {
		return stockToUseRatio;
	}
	public boolean isMeat() {
		return  isMeat;
	}
	
}
