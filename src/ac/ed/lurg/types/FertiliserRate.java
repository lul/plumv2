package ac.ed.lurg.types;

import ac.ed.lurg.ModelConfig;

public enum FertiliserRate {
	NO_FERT(ModelConfig.MIN_FERT_AMOUNT == 0 ? "0" : String.format( "%0" + ModelConfig.FERT_AMOUNT_PADDING + ".0f", ModelConfig.MIN_FERT_AMOUNT)),
	MID_FERT(String.format("%0" + ModelConfig.FERT_AMOUNT_PADDING + ".0f", ModelConfig.MID_FERT_AMOUNT)),
	MAX_FERT(String.format("%0" + ModelConfig.FERT_AMOUNT_PADDING + ".0f", ModelConfig.MAX_FERT_AMOUNT));

	private String fertId;
	
	FertiliserRate(String fertId) {
		this.fertId = fertId;
	}

	public String getId() {
		return fertId;
	}
}
