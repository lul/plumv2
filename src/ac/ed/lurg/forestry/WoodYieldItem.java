package ac.ed.lurg.forestry;

import java.util.Arrays;
import java.util.Map;
import ac.ed.lurg.ModelConfig;
import ac.sac.raster.RasterItem;

public class WoodYieldItem implements RasterItem {
	private final WoodYieldResponse yieldResponse = new WoodYieldResponse();
	private double maxYield = 0; // Yield at maximum age. Used for clustering

	public WoodYieldItem() {}
	
	public void setYieldResponse(Double[] yields, int modelYear) {
		int dataYear = Math.floorDiv(modelYear - 10, ModelConfig.CARBON_DATA_TIMESTEP_SIZE) * ModelConfig.CARBON_DATA_TIMESTEP_SIZE + 10;
		for (int age = modelYear - dataYear; age <= 160; age += ModelConfig.CARBON_DATA_TIMESTEP_SIZE) {
			yieldResponse.setYield(age, yields[age]);
		}
		yieldResponse.setYield(0, 0); // in case missing

		for (double d : yields) {
			maxYield = Math.max(maxYield, d);
		}
	}
	
	public double getYield(double rotationIntensity) {
		return yieldResponse.calcYield(rotationIntensity);
	}
	
	public double getMaxYield() {
		return maxYield;
	}

	public Map<Integer, Double> getYieldResponseMap() {
		return yieldResponse.getYieldMap();
	}

	public WoodYieldResponse getYieldResponse() {
		return yieldResponse;
	}

	public static WoodYieldItem getDefault() {
		return new WoodYieldItem();
	}
	
}
