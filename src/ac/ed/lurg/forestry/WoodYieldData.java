package ac.ed.lurg.forestry;

public class WoodYieldData {
	private final WoodYieldResponse yieldResponse = new WoodYieldResponse();

	public double getYieldRota(int age) {
		return yieldResponse.getYield(age);
	}
	
	public void setYieldRota(int age, double yield) {
		yieldResponse.setYield(age, yield);
	}
	
	public double getMaxYieldParam() {
		return yieldResponse.getMaxYield();
	}

	public double getSlopeParam() {
		return Math.min(yieldResponse.getSlope(), -1e-6);
	}

	public double getShapeParam() {
		return yieldResponse.getShape();
	}
}
