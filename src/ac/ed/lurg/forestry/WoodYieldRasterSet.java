package ac.ed.lurg.forestry;

import java.util.Collection;

import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class WoodYieldRasterSet extends RasterSet<WoodYieldItem> {

	private static final long serialVersionUID = -3816941524974512722L;

	public WoodYieldRasterSet(RasterHeaderDetails header) {
		super(header);
	}
	
	protected WoodYieldItem createRasterData() {
		return new WoodYieldItem();
	}
	
	@Override
	public WoodYieldRasterSet createSubsetForKeys(Collection<RasterKey> keys) {
		WoodYieldRasterSet subsetForestGrowthRaster = new WoodYieldRasterSet(getHeaderDetails());
		popSubsetForKeys(subsetForestGrowthRaster, keys);		
		return subsetForestGrowthRaster;
	}
}
