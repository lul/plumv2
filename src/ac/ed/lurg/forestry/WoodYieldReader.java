package ac.ed.lurg.forestry;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.landuse.LandUseItem;
import ac.ed.lurg.utils.LogWriter;
import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WoodYieldReader {
	private static final double CONVERSION_FACTOR = 10.0 * ModelConfig.WOOD_YIELD_CALIB_FACTOR / ModelConfig.WOOD_BIOMASS_CONVERSION_FACTOR; // convert kgC/m2 to m3/ha with calib factor
	private final RasterHeaderDetails rasterProj;
	private int modelYear;
	private Map<Integer, RasterKey> keyMap;

	public WoodYieldReader(RasterHeaderDetails rasterProj) {
		this.rasterProj = rasterProj;
	}
	
	public WoodYieldRasterSet getWoodYields(RasterSet<LandUseItem> landUseRaster, Timestep timestep) {
		WoodYieldRasterSet woodYieldData = new WoodYieldRasterSet(rasterProj);
		keyMap = getRasterKeys(woodYieldData);
		modelYear = ModelConfig.IS_CALIBRATION_RUN || !ModelConfig.CHANGE_YIELD_DATA_YEAR ? ModelConfig.BASE_YEAR : timestep.getYear();

		long startTime = System.currentTimeMillis();
			// Wood yield from rotation
			readLccData(woodYieldData, landUseRaster, ModelConfig.WOOD_YIELD_ROTA_FORS_FILENAME);

			LogWriter.println("Reading forestry data took " + (System.currentTimeMillis() - startTime) + " ms");
		return woodYieldData;
	}
	
	/* Data schema
	 * rows correspond to age class, columns correspond to locations
	 * serial binary format. data[i + j * nLoc] to get value for location i, age class j, number of locations nLoc
	 */
	
	public void readLccData(WoodYieldRasterSet woodYieldData, RasterSet<LandUseItem> landUseRaster, String fileName) {
		long startTime = System.currentTimeMillis();
		int maxYear = Math.floorDiv(modelYear - 10, ModelConfig.CARBON_DATA_TIMESTEP_SIZE) * ModelConfig.CARBON_DATA_TIMESTEP_SIZE + 10;
		int maxAge = modelYear - ModelConfig.CARBON_DATA_MIN_YEAR;
		Map<RasterKey, Double[]> map = new HashMap<>();
		for (RasterKey key : keyMap.values()) {
			map.put(key, new Double[maxAge + 1]);
		}
		// We need to extract data from each establishment year group
		for (int dataYear = maxYear; dataYear >= ModelConfig.CARBON_DATA_MIN_YEAR; dataYear -= ModelConfig.CARBON_DATA_TIMESTEP_SIZE) {
			int nCol = keyMap.size(); // number of locations
			int endAge = modelYear - dataYear; // oldest age class we need in this dataset
			int startAge = Math.max(0, endAge - ModelConfig.CARBON_DATA_TIMESTEP_SIZE + 1); // youngest age class
			String filePath = getDataDir(fileName, dataYear); 
			try {
				// Read binary data.
				RandomAccessFile reader = new RandomAccessFile(filePath, "r");
				int startPos = nCol * startAge * Double.BYTES;
				int nRead = (endAge - startAge + 1) * nCol * Double.BYTES;
				reader.seek(startPos);
				byte[] byteData = new byte[nRead];
				reader.read(byteData);
				Double[] data = byteArrayToDouble(byteData);
				for (int i = 0; i < nCol; i++) {
					Double[] locData = map.get(keyMap.get(i));
					for (int j = 0; j <= endAge - startAge; j++) {
						locData[j + startAge] = data[i + j * nCol] * CONVERSION_FACTOR;
					}
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (RasterKey key : map.keySet()) {
			if (landUseRaster.containsKey(key)) {
				WoodYieldItem wyItem = woodYieldData.get(key.getCol(), key.getRow());
				wyItem.setYieldResponse(map.get(key), modelYear);
			}
		}
		LogWriter.println("Reading " + fileName + " took " + (System.currentTimeMillis() - startTime) + " ms");
	}
	
	public Map<Integer, RasterKey> getRasterKeys(WoodYieldRasterSet woodYieldData) {
		Map<Integer, RasterKey> keyMap = new HashMap<>();
		String filePath = ModelConfig.FORESTRY_DIR_BASE + File.separator + "coordinates.dat";
		byte[] bytes;
		try {
			bytes = Files.readAllBytes(Paths.get(filePath));
			Double[] data = byteArrayToDouble(bytes);
			int nCol = 2;
			int nRow = data.length / nCol;
			for (int i = 0; i < nRow; i++) {
				RasterKey key = woodYieldData.getKeyFromCoordinates(data[i * nCol], data[i * nCol + 1]);
				keyMap.put(i, key);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return keyMap;
	}

	
	private Double[] byteArrayToDouble(byte[] bytes) {
		int n = bytes.length / Double.BYTES;
		Double[] d = new Double[n];
		for (int i = 0; i < n; i++) {
			ByteBuffer buff = ByteBuffer.allocate(Double.BYTES);
			byte[] doubleBytes = Arrays.copyOfRange(bytes, i * Double.BYTES, i * Double.BYTES + Double.BYTES);
			buff.put(doubleBytes);
			buff.flip();
			d[i] = buff.getDouble();
		}
		
		return(d);
	}
	
	private String getDataDir(String filename, int dataYear) {
		return ModelConfig.FORESTRY_DIR_BASE + File.separator + ModelConfig.FORESTRY_DIR_TOP + File.separator + dataYear + File.separator + filename;
	}

}
