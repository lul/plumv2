package ac.ed.lurg.forestry;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class WoodYieldResponse {
	private final Map<Integer, Double> yields = new HashMap<Integer, Double>();
	private Double maxYield;
	private Double slope;
	private Double shape;

	public void setYield(int age, double yield) {
		yields.put(age, yield);
	}
	
	public double getYield(int age) {
		return yields.getOrDefault(age, Double.NaN);
	}

	public void calcParams() {

		if (yields.size() == 0) {
			maxYield = 0.0;
			slope = 0.0;
			shape = 1.0;
			return;
		}

		// Scaling yields for better fitting
		Map<Integer, Double> scaledYield = new HashMap<>();
		for (Map.Entry<Integer, Double> entry : yields.entrySet()) {
			scaledYield.put(entry.getKey(), entry.getValue() / 100);
		}

		// Initial parameter estimates
		maxYield = scaledYield.values().stream().reduce(0.0, Double::max) + 1e-6;
		Vector<Double> kEst = new Vector<Double>();
		for (int a : scaledYield.keySet()) {
			if (a == 0)
				continue;
			double y = scaledYield.get(a);
			kEst.add(-Math.log(1 - y / maxYield) / (-a));
		}
		Collections.sort(kEst);
		slope = kEst.get(kEst.size() / 2);

		shape = 1.0;

		// Use Gauss-Newton algorithm to improve fit
		Double[] initialGuess = {maxYield, slope, 1.0};
		Double[] fittedParams = gaussNewton(scaledYield, initialGuess, 1000, 0.001, 0.1);

		// If fit improved update parameters. Sometimes the GN algorithm fails if fit is very poor so use initial fit instead.
		double resFitted = calcResidualSqr(scaledYield, fittedParams[0], fittedParams[1], fittedParams[2]);
		double resEst = calcResidualSqr(scaledYield, maxYield, slope, shape);

		if (resFitted < resEst) {
			maxYield = fittedParams[0];
			slope = fittedParams[1];
			shape = fittedParams[2];
		}

		maxYield = maxYield * 100; // scale back to original units
	}
	
	public double getMaxYield() {
		if (maxYield == null)
			calcParams();
		return maxYield;
	}
	
	public double getSlope() {
		if (slope == null)
			calcParams();
		return slope;
	}

	public double getShape() {
		if (shape == null)
			calcParams();
		return shape;
	}
	
	private double yieldFunction(double a, double b, double c, double t) {
		return a * Math.pow(1 - Math.exp(b * t), c);
	}
	
	private double calcResidualSqr(Map<Integer, Double> yieldMap, double maxYield, double slope, double shape) {
		double sumOfSquares = 0;
		for (int age : yieldMap.keySet()) {
			double y = yieldMap.get(age);
			double fit = yieldFunction(maxYield, slope, shape, age);
			double res2 = Math.pow(y - fit, 2);
			sumOfSquares += res2;
		}
		return sumOfSquares;
	}
	
	public Map<Integer, Double> getYieldMap() {
		return yields;
	}

	public double calcYield(double rotationIntensity) {
		return yieldFunction(getMaxYield(), getSlope(), getShape(), 1.0 / rotationIntensity) * rotationIntensity;
	}

	// Gradient of the function with respect to a and b
	public static Double[] gradient(double t, double a, double b, double c) {
		double expTerm = Math.exp(b * t);
		double base = 1 - expTerm;
		double powTerm = Math.pow(base, c);

		double dfa = powTerm;
		double dfb = -a * c * t * expTerm * Math.pow(base, c - 1);
		double dfc = a * powTerm * Math.log(base);

		return new Double[]{dfa, dfb, dfc};
	}

	// Gauss-Newton optimization algorithm
	private Double[] gaussNewton(Map<Integer, Double> yields, Double[] initialGuess, int maxIterations, double tolerance, double stepSize) {
		double a = initialGuess[0];
		double b = initialGuess[1];
		double c = initialGuess[2];

		for (int iter = 0; iter < maxIterations; iter++) {
			double sumA = 0, sumB = 0, sumC = 0;
			double sumAA = 0, sumAB = 0, sumAC = 0;
			double sumBB = 0, sumBC = 0, sumCC = 0;

			for (Map.Entry<Integer, Double> entry : yields.entrySet()) {
				double t = entry.getKey();
				if (t == 0) {
					continue;
				}
				double y = entry.getValue();
				double f = yieldFunction(a, b, c, t);
				double error = y - f;
				Double[] grad = gradient(t, a, b, c);

				sumA += grad[0] * error;
				sumB += grad[1] * error;
				sumC += grad[2] * error;
				sumAA += grad[0] * grad[0];
				sumAB += grad[0] * grad[1];
				sumAC += grad[0] * grad[2];
				sumBB += grad[1] * grad[1];
				sumBC += grad[1] * grad[2];
				sumCC += grad[2] * grad[2];
			}

			double[][] jacobian = {
					{sumAA, sumAB, sumAC},
					{sumAB, sumBB, sumBC},
					{sumAC, sumBC, sumCC}
			};

			double[] rhs = {sumA, sumB, sumC};

			double[] delta = solveLinearSystem(jacobian, rhs);

			if (delta == null) {
				System.out.println("Unable to solve linear system, stopping iteration.");
				break;
			}

			a += delta[0] * stepSize;
			b += delta[1] * stepSize;
			c += delta[2] * stepSize;

			a = Math.max(a, 0.01);
			a = Math.min(a, 50);
			b = Math.max(-0.5, b);
			b = Math.min(-0.001, b);
			c = Math.max(0.5, c);
			c = Math.min(3, c);

			if (Math.abs(delta[0]) < tolerance && Math.abs(delta[1]) < tolerance && Math.abs(delta[2]) < tolerance) {
				break;
			}
		}

		return new Double[]{a, b, c};
	}

	// Solve the linear system Ax = b using Gaussian elimination
	public static double[] solveLinearSystem(double[][] A, double[] b) {
		int n = b.length;
		double[][] augmentedMatrix = new double[n][n + 1];

		for (int i = 0; i < n; i++) {
			System.arraycopy(A[i], 0, augmentedMatrix[i], 0, n);
			augmentedMatrix[i][n] = b[i];
		}

		// Gaussian elimination
		for (int i = 0; i < n; i++) {
			// Find pivot
			int maxRow = i;
			for (int k = i + 1; k < n; k++) {
				if (Math.abs(augmentedMatrix[k][i]) > Math.abs(augmentedMatrix[maxRow][i])) {
					maxRow = k;
				}
			}

			// Swap rows
			double[] temp = augmentedMatrix[i];
			augmentedMatrix[i] = augmentedMatrix[maxRow];
			augmentedMatrix[maxRow] = temp;

			// Make all rows below this one 0 in current column
			for (int k = i + 1; k < n; k++) {
				double factor = augmentedMatrix[k][i] / augmentedMatrix[i][i];
				for (int j = i; j <= n; j++) {
					augmentedMatrix[k][j] -= factor * augmentedMatrix[i][j];
				}
			}
		}

		// Solve equation Ax=b for an upper triangular matrix A
		double[] x = new double[n];
		for (int i = n - 1; i >= 0; i--) {
			x[i] = augmentedMatrix[i][n] / augmentedMatrix[i][i];
			for (int k = i - 1; k >= 0; k--) {
				augmentedMatrix[k][n] -= augmentedMatrix[k][i] * x[i];
			}
		}

		return x;
	}
}
