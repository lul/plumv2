package ac.ed.lurg.yield;

import ac.sac.raster.RasterItem;

import java.util.HashMap;
import java.util.Map;

public class MultipleCroppingItem implements RasterItem {
    private final Map<LPJCropTypes, Double> multiCropFactors = new HashMap<>();

    public void setMultiCropFactor(LPJCropTypes crop, double multiCropFactor) {
        multiCropFactors.put(crop, multiCropFactor);
    }

    public double getMultiCropFactor(LPJCropTypes crop) {
        return multiCropFactors.get(crop);
    }
}
