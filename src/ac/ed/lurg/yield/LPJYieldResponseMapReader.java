package ac.ed.lurg.yield;

import java.io.File;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.Timestep;
import ac.ed.lurg.shock.YieldShockManager;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.FertiliserRate;
import ac.ed.lurg.types.IrrigationRate;
import ac.ed.lurg.types.YieldType;
import ac.ed.lurg.utils.LogWriter;
import ac.ed.lurg.utils.WatchForFile;
import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class LPJYieldResponseMapReader {	

	private RasterHeaderDetails rasterProj;
	private double techYieldAdj;
	private YieldShockManager yieldShockManager;
	private RasterSet<CropCalibrationItem> cropCalibRaster;
	private RasterSet<MultipleCroppingItem> multiCropRaster;

	public LPJYieldResponseMapReader(RasterHeaderDetails rasterProj) {
		this.rasterProj = rasterProj;
		yieldShockManager = new YieldShockManager(rasterProj);

		multiCropRaster = new RasterSet<MultipleCroppingItem>(rasterProj) {
			@Override
			protected MultipleCroppingItem createRasterData() {
				return new MultipleCroppingItem();
			}
		};

		MultipleCroppingReader reader = new MultipleCroppingReader(multiCropRaster);
		reader.getRasterDataFromFile(ModelConfig.SPATIAL_DATA_DIR + File.separator + "rice_multi_crop.csv");
	}

	public YieldRaster getRasterData(Timestep timestep, RasterSet<CropCalibrationItem> cropCalibRaster) {
		this.cropCalibRaster = cropCalibRaster;
		String rootDir = timestep.getYearSubDir(ModelConfig.YIELD_DIR);
		
		// wait for data to be available
		long startTime = System.currentTimeMillis();
		WatchForFile fileWatcher = new WatchForFile(new File(rootDir + File.separator + "done"));
		boolean foundFile = fileWatcher.await(ModelConfig.LPJG_MONITOR_TIMEOUT_SEC);
		if (!foundFile) {
			LogWriter.printlnError("Not able to find marker file.  May have timed out.");
			throw new RuntimeException("Not able to find marker file.  May have timed out.");
		}
		LogWriter.println("Found marker file in " + (System.currentTimeMillis() - startTime) + " ms");

		// arithmetic adjustment of yield for tech change  
		int timeStepForTechChange = ModelConfig.IS_CALIBRATION_RUN ? ModelConfig.START_TIMESTEP : timestep.getTimestep();  // we don't want technology to change in calibration, but sometime we run tech change backwards
		int yearsOfTech = (timeStepForTechChange - ModelConfig.TECHNOLOGY_CHANGE_START_STEP) * ModelConfig.TIMESTEP_SIZE;
		
		double techChangeAnnualRate = ModelConfig.getAdjParam("TECHNOLOGY_CHANGE_ANNUAL_RATE");
		
		techYieldAdj = 1.0 +  yearsOfTech * techChangeAnnualRate;
		LogWriter.println("Yield adjustment for technology for " + timestep + " is " + techYieldAdj);
		
		YieldRaster yieldRaster = new YieldRaster(rasterProj);
		readNppFile(yieldRaster, rootDir+File.separator+ModelConfig.ANPP_FILENAME);
		readYieldFile(yieldRaster, rootDir+File.separator+ModelConfig.YIELD_FILENAME);
		
		yieldShockManager.setShocks(yieldRaster, timestep);

		return yieldRaster;
	}
	
	private void readNppFile(YieldRaster yieldRaster, String fileToRead) {  // Passed in YieldRaster is updated
		
		AbstractTabularRasterReader<YieldResponsesItem> nppReader = new AbstractTabularRasterReader<YieldResponsesItem>("\\s+", 4, yieldRaster) {
			protected void setData(RasterKey key, YieldResponsesItem item, Map<String, Double> rowValues) {
				double pastHarvFract = ModelConfig.getAdjParam("PASTURE_HARVEST_FRACTION");
				double adjFactor = 10 * pastHarvFract / 0.446;  // 10 for kg/m2 to t/ha, 0.446 carbon units / dry matter, from Osaki, M., Shinano, T., Tadano, T., 1992. Carbon-nitrogen interaction in field crop production. Soil Science and Plant Nutrition 38, 553-564.
				adjFactor *= ModelConfig.CALIB_FACTOR_PASTURE;
				for (FertiliserRate fert : FertiliserRate.values()) {
					double pastureYield;
					if (ModelConfig.PASTURE_FERT_RESPONSE_FROM_LPJ)
						pastureYield = Math.max(getValueForCol(rowValues, LPJCropTypes.PASTURE_C3 + "N" + fert.getId()), getValueForCol(rowValues, LPJCropTypes.PASTURE_C4 + "N" + fert.getId())) * adjFactor;
					else
						pastureYield = Math.max(getValueForCol(rowValues, LPJCropTypes.PASTURE_C3.toString()), getValueForCol(rowValues, LPJCropTypes.PASTURE_C4.toString())) * adjFactor;

					item.setYield(YieldType.getYieldType(fert, IrrigationRate.NO_IRRIG), CropType.PASTURE, pastureYield);
					item.setYield(YieldType.getYieldType(fert, IrrigationRate.MAX_IRRIG), CropType.PASTURE, pastureYield);
				}
			}
		};
		
		nppReader.getRasterDataFromFile(fileToRead);
	}

	private void readYieldFile(YieldRaster yieldRaster, String fileToRead) {  	// Passed in YieldRaster is updated
		
		AbstractTabularRasterReader<YieldResponsesItem> yieldReader = new AbstractTabularRasterReader<YieldResponsesItem>("\\s+", 10, yieldRaster) {
			private double adjFactor = 10;  // 10 for kg/m2 to t/ha
			
			protected void setData(RasterKey key, YieldResponsesItem item, Map<String, Double> rowValues) {
				CropCalibrationItem calibItem = cropCalibRaster.get(key);
				double multiCropFactor = multiCropRaster.containsKey(key) ? multiCropRaster.get(key).getMultiCropFactor(LPJCropTypes.RICE) : 1;

				setSugarCrop(item, rowValues, calibItem);
									
				for (FertiliserRate fert : FertiliserRate.values()) {
					for (IrrigationRate irrig : IrrigationRate.values()) {
						YieldType yieldType = YieldType.getYieldType(fert, irrig);
						String fertIrrigString = yieldType.getFertIrrigString();
						
						/*
						 * WARNING:  if you change mapping below you also need to change LpjgOutputer and IrrigationMaxAmountReader to match
						 */
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.WHEAT, LPJCropTypes.C3_CEREALS, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.C3_CEREALS));
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.MAIZE, LPJCropTypes.C4_CEREALS, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.C4_CEREALS));
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.ENERGY_CROPS, LPJCropTypes.C4_CEREALS, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.MISCANTHUS));
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.RICE, LPJCropTypes.RICE, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.RICE));
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.OILCROPS_NFIX, LPJCropTypes.OILCROPS_NFIX, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.OILCROPS_NFIX));
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.OILCROPS_OTHER, LPJCropTypes.OILCROPS_OTHER, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.OILCROPS_OTHER));
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.PULSES, LPJCropTypes.PULSES, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.PULSES));
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.STARCHY_ROOTS, LPJCropTypes.STARCHY_ROOTS, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.STARCHY_ROOTS));
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.FRUITVEG, LPJCropTypes.FRUITVEG, fertIrrigString, calibItem.getCalibFactor(LPJCropTypes.FRUITVEG));
						double sugarCalibFactor  = item.getSugarCrop().equals(LPJCropTypes.SUGAR_BEET) ? calibItem.getCalibFactor(LPJCropTypes.SUGAR_BEET) : calibItem.getCalibFactor(LPJCropTypes.SUGAR_CANE);
						getAndSetCalibratedYield(item, yieldType, rowValues, CropType.SUGAR, item.getSugarCrop(), fertIrrigString, sugarCalibFactor);
					}
				}
				item.setMultiCropFactor(CropType.RICE, multiCropFactor);
			}
			
			protected void getAndSetCalibratedYield(YieldResponsesItem item, YieldType yieldType, Map<String, Double> rowValues, 
					CropType plumcrop, LPJCropTypes lpjcrop, String fertIrrigString, double calibFactor) {
				
				double yieldValue = getValueForCol(rowValues, lpjcrop + fertIrrigString);
				double yieldAdj = yieldValue * calibFactor * techYieldAdj  * adjFactor;
				item.setYield(yieldType, plumcrop, yieldAdj);  // applying the calibration factor can't go in YieldResponsesItem.setYield as this is also called from elsewhere
			}

			protected void setSugarCrop(YieldResponsesItem item, Map<String, Double> rowValues, CropCalibrationItem calibItem) {
				YieldType yieldType = YieldType.getYieldType(FertiliserRate.MAX_FERT, IrrigationRate.MAX_IRRIG);
				double sugarBeetYield = getValueForCol(rowValues, LPJCropTypes.SUGAR_BEET + yieldType.getFertIrrigString()) * calibItem.getCalibFactor(LPJCropTypes.SUGAR_BEET) * 0.175; // adjust for sugar content
				double sugarCaneYield = getValueForCol(rowValues, LPJCropTypes.SUGAR_CANE + yieldType.getFertIrrigString()) * calibItem.getCalibFactor(LPJCropTypes.SUGAR_CANE) * 0.125;
				LPJCropTypes sugarCrop = sugarBeetYield > sugarCaneYield ? LPJCropTypes.SUGAR_BEET : LPJCropTypes.SUGAR_CANE;
				item.setSugarCrop(sugarCrop);
			}
		};
		
		yieldReader.getRasterDataFromFile(fileToRead);
	}
}
