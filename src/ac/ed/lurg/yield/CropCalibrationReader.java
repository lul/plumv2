package ac.ed.lurg.yield;

import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.util.Map;

public class CropCalibrationReader extends AbstractTabularRasterReader<CropCalibrationItem> {

    private static final int MIN_COLS = 13;
    private static final String DELIM = ",";

    public CropCalibrationReader(RasterSet<CropCalibrationItem> dataset) {
        super(DELIM, MIN_COLS, dataset);
    }

    @Override
    protected void setData(RasterKey key, CropCalibrationItem item, Map<String, Double> rowValues) {
        for (String cropName : rowValues.keySet()) {
            item.setCalibFactor(LPJCropTypes.getLpjCropTypeForName(cropName), rowValues.get(cropName));
        }
    }

}
