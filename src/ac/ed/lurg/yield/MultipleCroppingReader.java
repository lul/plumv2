package ac.ed.lurg.yield;

import ac.sac.raster.AbstractTabularRasterReader;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

import java.util.Map;

public class MultipleCroppingReader extends AbstractTabularRasterReader<MultipleCroppingItem> {
    private static final int MIN_COLS = 3;
    private static final String DELIM =  ",";

    public MultipleCroppingReader(RasterSet<MultipleCroppingItem> dataset) {
        super(DELIM, MIN_COLS, dataset);
    }

    @Override
    protected void setData(RasterKey key, MultipleCroppingItem item, Map<String, Double> rowValues) {
        for (LPJCropTypes crop : LPJCropTypes.values()) {
            if (!crop.equals(LPJCropTypes.RICE)) {
                continue;
            }
            item.setMultiCropFactor(crop, rowValues.get(crop.toString().toLowerCase()));
        }
    }
}
