package ac.ed.lurg.yield;

import java.util.Collection;

import ac.sac.raster.RasterHeaderDetails;
import ac.sac.raster.RasterKey;
import ac.sac.raster.RasterSet;

public class YieldRaster extends RasterSet<YieldResponsesItem> {

	private static final long serialVersionUID = -4483319360580776344L;
	
	public YieldRaster(RasterHeaderDetails header) {
		super(header);
	}
	
	protected YieldResponsesItem createRasterData() {
		return new YieldResponsesItem();
	}
	
	// not very efficient, we could keep the mapping of country to area somewhere.
	@Override
	public YieldRaster createSubsetForKeys(Collection<RasterKey> keys) {
		YieldRaster subsetYieldRaster = new YieldRaster(getHeaderDetails());
		popSubsetForKeys(subsetYieldRaster, keys);		
		return subsetYieldRaster;
	}
}
