package ac.ed.lurg.yield;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.YieldType;
import ac.sac.raster.RasterItem;

public class YieldResponsesItem implements RasterItem {
	Map<CropType, YieldResponse> yieldResponses = new HashMap<CropType, YieldResponse>();
	LPJCropTypes sugarCrop;
	boolean isMissing = false; // not in LPJG data
	
	public void setYield(YieldType type, CropType crop, double yield) {
		getYieldResponseForCrop(crop).setYield(type, yield);
	}

	YieldResponse getYieldResponseForCrop(CropType crop) {
		YieldResponse yresp = yieldResponses.get(crop);
		if (yresp == null) {
			yresp = new YieldResponse();
			yieldResponses.put(crop, yresp);
		}
		return yresp;
	}
	
	public double getYield(YieldType yieldType, CropType crop) {
		YieldResponse yr = getYieldResponseForCrop(crop);
		double d = yr.getYield(yieldType);
		return d;
	}	

	public double getExtrapolatedYield(YieldType yieldType, CropType crop) {
		YieldResponse yr = getYieldResponseForCrop(crop);
		return Math.max(yr.getExtrapolatedYield(yieldType), 0.001);
	}	
	
	public double getFertParam(CropType crop) {
		return getYieldResponseForCrop(crop).getFertParam();
	}
	
	public double getIrrigParam(CropType crop) {
		return getYieldResponseForCrop(crop).getIrrigParam();
	}
	
	public void setShockRate(CropType crop, double yieldShock) {
		getYieldResponseForCrop(crop).setShock(yieldShock);
	}
	
	public double getShockRate(CropType crop) {
		return getYieldResponseForCrop(crop).getShock();
	}

	public static YieldResponsesItem getDefault() {
		YieldResponsesItem item = new YieldResponsesItem();
		for (CropType crop : CropType.values()) {
			for (YieldType yieldType : YieldType.values()) {
				item.setYield(yieldType, crop, 0.0);
			}
		}
		item.isMissing = true;
		return item;
	}

	public boolean isMissing() {
		return isMissing;
	}

	public void setMissingFlag(boolean isMissing) {
		this.isMissing = isMissing;
	}

	public void setSugarCrop(LPJCropTypes sugarCrop) {
		this.sugarCrop = sugarCrop;
	}

	public LPJCropTypes getSugarCrop() {
		return sugarCrop;
	}

	public void setMultiCropFactor(CropType crop, double multiCropFactor) {
		yieldResponses.get(crop).setMultiCroppingFactor(multiCropFactor);
	}

	public double getMultiCropFactor(CropType crop) {
		return  yieldResponses.get(crop).getMultiCroppingFactor();
	}

}