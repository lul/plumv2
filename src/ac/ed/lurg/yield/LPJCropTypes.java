package ac.ed.lurg.yield;

import ac.ed.lurg.ModelConfig;

import java.util.HashMap;
import java.util.Map;

public enum LPJCropTypes {
	// LPJ column names strings

	C3_CEREALS(ModelConfig.C3_CEREALS_COLUMN),
	C4_CEREALS(ModelConfig.C4_CEREALS_COLUMN),
	RICE(ModelConfig.RICE_COLUMN),
	PULSES(ModelConfig.PULSES_COLUMN),
	OILCROPS_NFIX(ModelConfig.OILCROPS_NFIX_COLUMN),
	OILCROPS_OTHER(ModelConfig.OILCROPS_OTHER_COLUMN),
	STARCHY_ROOTS(ModelConfig.STARCHY_ROOTS_COLUMN),
	FRUITVEG(ModelConfig.FRUITVEG_COLUMN),
	SUGAR_BEET(ModelConfig.SUGAR_BEET_COLUMN),
	SUGAR_CANE(ModelConfig.SUGAR_CANE_COLUMN),
	PASTURE_C3(ModelConfig.PASTURE_C3_COLUMN),
	PASTURE_C4(ModelConfig.PASTURE_C4_COLUMN),
	MISCANTHUS("Miscanthus"); // Not in the yield file as using C4 cereals but needed for calibration file
	
	private String lpjName;
	
	LPJCropTypes (String lpjName) {
		this.lpjName = lpjName;
	}

    @Override
    public String toString() {
        return lpjName;
    }

	private static final Map<String, LPJCropTypes> nameCache = new HashMap<>();
	static {
		for (LPJCropTypes c : values()) {
			nameCache.put(c.toString().toLowerCase(), c);
		}
	}

	public static LPJCropTypes getLpjCropTypeForName(String lpjName) {
		return nameCache.get(lpjName);
	}
}
