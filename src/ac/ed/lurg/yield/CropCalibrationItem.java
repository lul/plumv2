package ac.ed.lurg.yield;

import ac.sac.raster.RasterItem;

import java.util.HashMap;
import java.util.Map;

public class CropCalibrationItem implements RasterItem {
    private final Map<LPJCropTypes, Double> calibFactors = new HashMap<>();

    public void setCalibFactor(LPJCropTypes lpjCrop, double calibFactor) {
        calibFactors.put(lpjCrop, calibFactor);
    }

    public double getCalibFactor(LPJCropTypes lpjCrop) {
        return calibFactors.get(lpjCrop);
    }
}
