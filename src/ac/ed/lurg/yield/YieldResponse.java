package ac.ed.lurg.yield;

import java.util.HashMap;
import java.util.Map;

import ac.ed.lurg.ModelConfig;
import ac.ed.lurg.types.YieldType;
import ac.ed.lurg.utils.LogWriter;

public class YieldResponse {
	private Map<YieldType, Double> yields = new HashMap<YieldType, Double>();
	private Map<YieldType, Double> yieldsExtrapolated;
	private double yieldShock;
	private double fertParm;
	private double irrigParm;
	private double multiCroppingFactor = 1; // 1 = single cropping, 2 = double cropping etc.

	private static double defaultParam;
	
	static {
		defaultParam =  calcParam(0, 0.8, 1, 0.5);  // default assumes 80% at mid point
	}

	
	public void setYield(YieldType type, double yield) {
		yields.put(type, yield);
		fertParm = 0;
		irrigParm = 0;
	}
	
	public double getYield(YieldType type) {
		Double d = yields.get(type);
		
		if (d == null) {
		//	LogWriter.printlnError("YieldResponse: No yield found for type " + type);
			return Double.NaN;
		}
		
		return d.doubleValue();
	}		
	
	public double getExtrapolatedYield(YieldType type) {
		if (yieldsExtrapolated == null)
			calcParams();
		
		Double d = yieldsExtrapolated.get(type);
		
		if (d == null)
			return Double.NaN;
		
		return d.doubleValue();
	}
	
	public double getFertParam() {
		if (fertParm == 0)
			calcParams();

		return fertParm;
	}
	
	private double calcParams() {
		double yMinNoI = getYield(YieldType.NO_FERT_NO_IRRIG);
		double yMidNoI = getYield(YieldType.FERT_MID_NO_IRRIG);
		double yMaxNoI = getYield(YieldType.FERT_MAX_NO_IRRIG);
		double fMid = (ModelConfig.MID_FERT_AMOUNT-ModelConfig.MIN_FERT_AMOUNT)/(ModelConfig.MAX_FERT_AMOUNT-ModelConfig.MIN_FERT_AMOUNT);
		
		if (ModelConfig.EXTRAPOLATE_YIELD_FERT_RESPONSE) {
			double yMinI = getYield(YieldType.NO_FERT_IRRIG_MAX);
			double yMidI = getYield(YieldType.FERT_MID_IRRIG_MAX);
			double yMaxI = getYield(YieldType.FERT_MAX_IRRIG_MAX);

			double asymptoteYieldIncNoI = yieldAsymptoteSolve(yMidNoI-yMinNoI, yMaxNoI-yMinNoI, fMid);
			fertParm = -1/fMid * Math.log(1-(yMidI-yMinI)/asymptoteYieldIncNoI);
			double asymptoteYieldIncI = (yMaxI-yMinI) / (1 - Math.exp(-fertParm));
			//LogWriter.println(String.format("fertParm from solver: asymptoteYieldIncNoI: %.2f, asymptoteYieldIncI: %.2f, fertParm %.4f", asymptoteYieldIncNoI, asymptoteYieldIncI, fertParm));
			
			if (Double.isNaN(asymptoteYieldIncI) || Double.isNaN(fertParm)) {
				LogWriter.println("Not finding a suitable extrapolating solution.  Defaulting to old style", 2);
			}
			else {
				yieldsExtrapolated = new HashMap<YieldType, Double>();
				yieldsExtrapolated.put(YieldType.NO_FERT_NO_IRRIG, yMinNoI);
				yieldsExtrapolated.put(YieldType.FERT_MAX_NO_IRRIG, asymptoteYieldIncNoI+yMinI);
				yieldsExtrapolated.put(YieldType.NO_FERT_IRRIG_MAX, yMinI);
				yieldsExtrapolated.put(YieldType.FERT_MAX_IRRIG_MAX, asymptoteYieldIncI+yMinI);
			}
		}
		
		// old style, if not wanting extrapolate or data doesn't allow
		if (yieldsExtrapolated == null) {
			fertParm = calcParam(yMinNoI, yMidNoI, yMaxNoI, fMid);
			yieldsExtrapolated = yields;
		}

		for (YieldType yieldType : yieldsExtrapolated.keySet()) {
			double prevYield = yieldsExtrapolated.get(yieldType);
			double newYield = prevYield * multiCroppingFactor;
			yieldsExtrapolated.put(yieldType, newYield);
		}
		fertParm = fertParm / multiCroppingFactor;
		irrigParm = irrigParm / multiCroppingFactor;
		
		return fertParm;
	}
	
	public double getIrrigParam() {
		if (irrigParm == 0) {
			irrigParm = defaultParam; 
		}

		return irrigParm;
	}
	
	private static double calcParam(double yMin, double yMid, double yMax, double xMid) {
		if (yMid <= yMin || yMax <= yMid)
			return defaultParam;
		
		return -Math.log(1 - ((yMid-yMin)/(yMax-yMin))) / xMid; 					// 1-exp(-x) function
	//	return Math.log((yMid - yMin)/(yMax - yMin)) / Math.log((xMid - xMin)/(xMax - xMin)) / xMax;  	// Cobb-Douglas
	}
	
	public void setShock(double yieldShock) {
		this.yieldShock = yieldShock;
	}
	
	public double getShock() {
		return yieldShock;
	}
	
	private static double f(double x, double yM, double yH, double fM) {
	   	return 1-yM/x - Math.pow(1-yH/x, fM);
	}

	private double yieldAsymptoteSolve (double yM, double yH, double fM) {

		double a = yH;
		double b = yH * 20;
		double epsilon = 0.0001;
		
		if (yM < 0 || yH < 0 || fM * yH > yM) {
			LogWriter.printlnError(String.format("yieldAsymptoteSolve: less than zero yield or not diminishing yield increases to N: yM=%.3f, yH=%.3f, fM=%.3f", yM, yH, fM));
			return Double.NaN;
		}
		
		double m = Double.NaN;
		double fm;
		double fa = f(a, yM, yH, fM);
		double fb = f(b, yM, yH, fM);

		if (fa * fb > 0) {
			LogWriter.printlnError(String.format("yieldAsymptoteSolve: not bracketing root: yM=%.3f, yH=%.3f, fM=%.3f", yM, yH, fM));
			return Double.NaN;
		}
		
		while ((b-a) > epsilon)
		{
			m = (a+b)/2;           // Mid point
			fm = f(m, yM, yH, fM);

			if ( fm * fa > 0 )
			{  // f(a) and f(m) have same signs: move a
				a = m;
				fa = fm;
			}
			else
			{  // f(a) and f(m) have different signs: move b
				b = m;
				fb = fm;
			}
		}
		return m;
	}

	public void setMultiCroppingFactor(double factor) {
		multiCroppingFactor = factor;
	}

	public double getMultiCroppingFactor() {
		return  multiCroppingFactor;
	}
}