package ac.ed.lurg.yield;

import ac.ed.lurg.forestry.WoodYieldItem;
import ac.ed.lurg.landuse.IrrigationItem;
import ac.ed.lurg.solar.SolarPotentialItem;
import ac.ed.lurg.types.CropType;
import ac.ed.lurg.types.YieldType;
import ac.ed.lurg.utils.cluster.ClusteringPoint;
import ac.sac.raster.RasterKey;

import java.util.Arrays;
import java.util.Collection;

public class YieldClusterPoint implements ClusteringPoint<String> {

	private final static String PASTURE = "pas";
	private final static String WHEAT_MIN = "wheatMin";
	private final static String WHEAT_MAX = "wheatMax";
	private final static String RICE_MAX = "riceMin";
	private final static String MAIZE_MIN = "maizeMin";
	private final static String MAIZE_MAX = "maizeMax";
	private final static String IRRIG_WATER_AVAL = "irrigWaterAval";
	private final static String PROTECTED_AREA = "protectedArea";
	private final static String WOOD_MAX = "woodMax";
	private final static String WOOD_SLOPE = "woodSlope";
	private final static String SOLAR_POTENTIAL = "solarPotential";

	private RasterKey rasterKey;
	private double wheatMin;
	private double wheatMax;
	private double riceMax;
	private double maizeMin;
	private double maizeMax;
	private double pasture;
	private double irrigWaterAval;
	private double protectedArea;
	private double woodMax;
	private double woodSlope;
	private double solarPotential;

	public YieldClusterPoint(RasterKey rasterKey, YieldResponsesItem yields, IrrigationItem irrigItem, double protectedArea,
							 WoodYieldItem woodYields, SolarPotentialItem solarPotentialItem) {
		this.rasterKey = rasterKey;
		
		// not sure if we be better to get a reference to the YieldResponsesItem, rather than caching these values?
		// multiplying each variable by scaling factor so all are on a similar scale
		this.wheatMin = yields.getYield(YieldType.NO_FERT_NO_IRRIG, CropType.WHEAT);
		this.riceMax = yields.getYield(YieldType.FERT_MAX_IRRIG_MAX, CropType.RICE);
		this.maizeMin = yields.getYield(YieldType.NO_FERT_NO_IRRIG, CropType.MAIZE);
		this.pasture = yields.getYield(YieldType.NO_FERT_NO_IRRIG, CropType.PASTURE);
		this.wheatMax = yields.getYield(YieldType.FERT_MAX_IRRIG_MAX, CropType.WHEAT);
		this.maizeMax = yields.getYield(YieldType.FERT_MAX_IRRIG_MAX, CropType.MAIZE);
		this.irrigWaterAval = irrigItem == null ? 0.0 : irrigItem.getIrrigConstraint();
		this.protectedArea = protectedArea * 10;
		this.woodMax = woodYields == null ? 0.0 : woodYields.getYieldResponse().getMaxYield() * 0.1;
		this.woodSlope = woodYields == null ? 0.0 : -woodYields.getYieldResponse().getSlope();
		this.solarPotential = solarPotentialItem == null ? 0.0 : solarPotentialItem.getPvPotential() * 0.01;
	}

	public RasterKey getRasterKey() {
		return rasterKey;
	}

	@Override
	public double getClusteringValue(String key) {
		double v = getClusteringValueNaN(key);		
		return Double.isNaN(v) ? 0.0 : v;  // LPJ with emulator provides NaN yields.  How to cluster these is tricky, but assuming they are 0 
	}
	
	private double getClusteringValueNaN(String key) {
		switch (key) {
			case PASTURE:  return pasture;
			case WHEAT_MIN:  return wheatMin;
			case WHEAT_MAX:  return wheatMax;
			case RICE_MAX:  return riceMax;
			case MAIZE_MIN:  return maizeMin;
			case MAIZE_MAX:  return maizeMax;
			case IRRIG_WATER_AVAL:  return irrigWaterAval;
			case PROTECTED_AREA:  return protectedArea;
			case WOOD_MAX: return woodMax;
			case WOOD_SLOPE: return woodSlope;
			case SOLAR_POTENTIAL: return solarPotential;
			default:
				throw new RuntimeException("YieldClusterPoint.getClusteringValue: got unknown value " + key);
		}
	}

	@Override
	public Collection<String> getAllClusteringKeys() {
		return Arrays.asList(PASTURE, WHEAT_MIN, WHEAT_MAX, MAIZE_MIN, MAIZE_MAX, IRRIG_WATER_AVAL, PROTECTED_AREA,
				WOOD_MAX, WOOD_SLOPE, SOLAR_POTENTIAL);
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer(rasterKey.toString() + ": ");
		for (String i : getAllClusteringKeys())
			sb.append(i + "=" + getClusteringValue(i) + " ");
		
		return sb.toString();
	}
}
