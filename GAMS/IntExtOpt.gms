
 SET all_types                          / monogastrics, ruminants, wheat, maize, rice, oilcropsNFix, oilcropsOther, pulses, starchyRoots, fruitveg, sugar, energycrops, pasture, setaside, wood, carbonCredits, energyPV, energyAV /;

 SET crop(all_types)                                             / wheat, maize, rice, oilcropsNFix, oilcropsOther, pulses, starchyRoots, fruitveg, sugar, energycrops, pasture, setaside /;
 SET crop_less_pasture(crop)                                     / wheat, maize, rice, oilcropsNFix, oilcropsOther, pulses, starchyRoots, fruitveg, sugar, energycrops         , setaside /;
 SET crop_less_pasture_setaside(crop)                            / wheat, maize, rice, oilcropsNFix, oilcropsOther, pulses, starchyRoots, fruitveg, sugar, energycrops/;
 SET feed_crop(crop)                                             / wheat, maize, rice, oilcropsNFix, oilcropsOther, pulses, starchyRoots, fruitveg,                     pasture/;
 SET feed_crop_less_pasture(feed_crop)                           / wheat, maize, rice, oilcropsNFix, oilcropsOther, pulses, starchyRoots, fruitveg /;
 SET import_types(all_types)            / monogastrics, ruminants, wheat, maize, rice, oilcropsNFix, oilcropsOther, pulses, starchyRoots, fruitveg, sugar, energycrops,                    wood, carbonCredits, energyPV, energyAV/;
 SET animal(all_types)                  / monogastrics, ruminants /;
 SET agri_types(all_types)              / monogastrics, ruminants, wheat, maize, rice, oilcropsNFix, oilcropsOther, pulses, starchyRoots, fruitveg, sugar, energycrops, pasture /;
 SET not_feed(all_types)                / monogastrics, ruminants,                                                                                  sugar, energycrops /;

 SET land_cover / cropland, pasture, timberForest, carbonForest, natural, agrivoltaics, photovoltaics /;
 SET protection / unprotected, protected /;

 SET managed_forest(land_cover) / timberForest, carbonForest /;
 SET solar_land(land_cover) / agrivoltaics, photovoltaics /;
 ALIAS (land_cover, land_cover_before);
 ALIAS (land_cover, land_cover_after);
 
 SET farming_type / conventional, restricted, agrivoltaics /;
 SET farming_map(protection, farming_type) / unprotected.conventional, protected.restricted /;

 SET location;

 PARAMETER suitableLandArea(location)            areas of land in Mha;
 PARAMETER previousCropArea(crop, farming_type, location)      areas for previous timestep in Mha;
 PARAMETER previousFertIntensity(crop, farming_type, location);
 PARAMETER previousIrrigIntensity(crop, farming_type, location);
 PARAMETER previousOtherIntensity(crop, farming_type, location);
 PARAMETER previousRuminantFeed(crop);
 PARAMETER previousMonogastricFeed(crop);
 PARAMETER previousImportAmount(all_types);
 PARAMETER previousExportAmount(all_types);
 PARAMETER yieldNone(crop, location)         yield in t per ha;
 PARAMETER yieldFertOnly(crop, location)     yield in t per ha;
 PARAMETER yieldIrrigOnly(crop, location)    yield in t per ha;
 PARAMETER yieldBoth(crop, location)         yield in t per ha;
 PARAMETER yieldShock(crop, location)        rate of yield shock;
 PARAMETER fertParam(crop, location)         yield response to fertilizer parameter;
 PARAMETER irrigParam(crop, location)        yield response to irrigation parameter;
 PARAMETER demand(all_types)                 in t;
 PARAMETER exportPrices(all_types)           prices for exports;
 PARAMETER importPrices(all_types)           prices for imports;
 PARAMETER maxNetImport(all_types)         maximum net import for each crop based on world market;
 PARAMETER minNetImport(all_types)         minimum net import for each crop based on world market;
 PARAMETER irrigCost(location)               irrigation cost in cost per 1000 Mlitre or Mha for each litre per m2;
 PARAMETER irrigMaxRate(crop, location)      max water application rate irrigation in litre per m2;
 PARAMETER irrigConstraintLocation(location)         max water availability by location for irrigation in litre per m2;
 PARAMETER irrigConstraintFarmingType(farming_type, location)   max water availability by farming type and location for irrigation in litre per m2;
 PARAMETER seedAndWasteRate(all_types)       rate of use for seed and waste combined;
 PARAMETER subsidyRate(all_types)                 rates of subsidy compared to costs;
 PARAMETER tradeAdjustmentCostRate(import_types)  cost of adjusting net imports;
 
 PARAMETER previousLandCoverArea(land_cover, protection, location)                      land cover area in Mha;
 PARAMETER carbonCreditRate(land_cover_before, land_cover_after, location) potential carbon credits from LULUCF - tC-eq per ha;
 PARAMETER conversionCost(land_cover_before, land_cover_after, location)   cost of converting from one land cover to another - $1000 per ha;
 PARAMETER woodYieldMaxParam(location);
 PARAMETER woodYieldSlopeParam(location);
 PARAMETER woodYieldShapeParam(location);
 PARAMETER previousRotationIntensity(protection, location);
 PARAMETER maxLandCoverChange(land_cover_before, land_cover_after, protection, location);
 PARAMETER maxLandCoverArea(land_cover, protection, location);
 
 PARAMETER solarEnergyDensity(solar_land, location) MWh per ha;
 PARAMETER solarCostRate(solar_land, location) 1000$ per ha;
 PARAMETER agrivoltaicsYieldFactor(crop, farming_type, location);
 PARAMETER agrivoltaicsCropCostFactor(farming_type)  crop unit cost is lower since crop area is smaller due to space taken by PV panels;
 
 PARAMETER otherIParam(crop, location) yield response to other intensity;
 PARAMETER prodCost(crop) cost per ha before intensity values not including fertiliser or irrigation i.e. seed spray and other.  In 1000 $ per ha;
 PARAMETER animalProdCost(animal) cost per unit produced;
 PARAMETER previousProduction(all_types);

 SCALAR meatEfficency                        efficiency of converting feed and pasture into animal products;
 SCALAR fertiliserUnitCost                   fert cost at max fert rate;
 SCALAR otherICost                           cost of other intensity;
 SCALAR unhandledCropRate                    rate of fruit veg and other crops not modelled;
 SCALAR setAsideRate                         rate of set aside and failed crop;
 SCALAR domesticPriceMarkup                  factor price increased from cost of production;
 SCALAR fertLimRestricted;
 SCALAR irrigLimRestricted;
 SCALAR otherLimRestricted;
 SCALAR fertLimConventional;
 SCALAR irrigLimConventional;
 SCALAR otherLimConventional;
 SCALAR timberLimProtected;
 SCALAR timberLimUnprotected;

 SCALAR forestManagementCost    cost $1000 per ha;
 SCALAR carbonForestMaxProportion maximum proportion of land cover as carbon forest;
 SCALAR monoculturePenalty;
 SCALAR pastureIntensityPenalty;
 
 SCALAR energyPrice;

*$gdxin "C:/gitMaster/plumv2/GamsTmp/_gams_java_gdb1.gdx"
$gdxin %gdxincname%
$load location, suitableLandArea, demand
$load previousCropArea, previousFertIntensity, previousIrrigIntensity, previousOtherIntensity, previousRuminantFeed, previousMonogastricFeed, previousImportAmount, previousExportAmount
$load yieldNone, yieldFertOnly, yieldIrrigOnly, yieldBoth, yieldShock
$load fertParam, irrigParam, otherIParam, exportPrices, importPrices, maxNetImport, minNetImport, unhandledCropRate, setAsideRate, subsidyRate
$load meatEfficency, otherICost, irrigCost, irrigConstraintLocation, irrigConstraintFarmingType, irrigMaxRate, fertiliserUnitCost, domesticPriceMarkup, seedAndWasteRate
$load previousLandCoverArea, carbonCreditRate, conversionCost, woodYieldMaxParam, woodYieldSlopeParam, woodYieldShapeParam, tradeAdjustmentCostRate
$load forestManagementCost, carbonForestMaxProportion, previousRotationIntensity, maxLandCoverChange, maxLandCoverArea
$load fertLimRestricted, otherLimRestricted, fertLimConventional, otherLimConventional, timberLimProtected, timberLimUnprotected
$load solarEnergyDensity, agrivoltaicsYieldFactor, prodCost, animalProdCost, solarCostRate
$load energyPrice, monoculturePenalty, agrivoltaicsCropCostFactor, previousProduction, pastureIntensityPenalty
$gdxin

 SCALAR delta / 1e-6 /;

 PARAMETER cropDM(crop)  kg DM per kg of feed the conversion from feed to meat is done in the R animal product index.  Pasture is in DM terms already
          /   wheat             0.87
              maize             0.86
              rice              0.88
              oilcropsNFix      0.89
              oilcropsOther     0.60
              pulses            0.89
              starchyRoots      0.21
              fruitveg          0.15
              sugar             0.19
              pasture           1    / ;
              
 PARAMETER otherIntCost(crop);
 otherIntCost(crop) = prodCost(crop) * otherICost;

 PARAMETER previousNetImport(import_types);
 previousNetImport(import_types) = previousImportAmount(import_types) - previousExportAmount(import_types);
 
 PARAMETER totalLandArea(location);
 totalLandArea(location) = sum((land_cover, protection), previousLandCoverArea(land_cover, protection, location));
 
 PARAMETER previousFeedAmount(animal, feed_crop);
 previousFeedAmount('ruminants', feed_crop) = previousRuminantFeed(feed_crop);
 previousFeedAmount('monogastrics', feed_crop) = previousMonogastricFeed(feed_crop);
 
 PARAMETER feedDiversityCost(animal)
            /   monogastrics = 0.01
                ruminants = 0.04    /;
 
 VARIABLES
       unitCost(crop, farming_type, location)           cost per area for each crop - cost
       fertI(crop, farming_type, location)              fertilizer intensity for each crop - factor between 0 and 1
       irrigI(crop, farming_type, location)             irrigation intensity for each crop - factor between 0 and 1
       otherIntensity(crop, farming_type, location)     management intensity for each crop - factor between 0 and 1
       yield(crop, farming_type, location)              yield per area for each crop - t per ha
       cropArea(crop, farming_type, location)           total area for crops - Mha
       
       cropProduction(crop, location)           total crop production at each location - Mt
       totalProduction(all_types)              total country production - Mt
       feedAmount(animal, all_types)            feed consumption - Mt

       importAmount(all_types)            total imports - Mt
       exportAmount(all_types)            total exports - Mt
       netImportAmount(import_types)      net imports - Mt
       netImportIncrease(import_types)
       netImportDecrease(import_types)
       excessImports(import_types)

       landCoverArea(land_cover, protection, location)  land cover area in Mha
       landCoverChange(land_cover_before, land_cover_after, protection, location) land cover change in Mha
       totalConversionCost(location)      land cover conversion cost - $1000 per ha

       woodYieldRota(protection, location)              wood yield m3 per ha
       rotationIntensity(protection, location)          equivalent to 1 divided by rotation period
       woodSupply                           total wood supply - million m3
       
       carbonCredits(location)              carbon credits generated - Mt C
       
       total_npv                            objective - Net Present Value
*       A                                  "artificial variable for debugging https://www.gams.com/blog/2017/07/misbehaving-model-infeasible/"
;


 POSITIVE VARIABLE cropArea, fertI, irrigI, otherIntensity, yield, feedAmount, importAmount, exportAmount, cropProduction, excessImports,
                   landCoverArea, landCoverChange, woodYieldRota, rotationIntensity, woodSupply, totalProduction, netImportIncrease, netImportDecrease;


* POSITIVE VARIABLE A(location);

 EQUATIONS
       UNIT_COST_EQ(crop, farming_type, location)                cost per area - $1000 per ha or $billion per Mha
       YIELD_EQ(crop, farming_type, location)                    yield given chosen intensity - tonnes per hectare
       IRRIGATION_CONSTRAINT_LOCATION(location)                           constraint on water usage
       IRRIGATION_CONSTRAINT_FARMING_TYPE(farming_type, location)
       
       CROP_PROD_CALC(crop, location)
       TOTAL_CROP_PROD_CALC(crop)                 crop production calculation
       ANIMAL_PROD_CALC(animal) 
       SUPPLY_CONSTRAINT(agri_types)         production equals domestic consumption plus exports minus imports
       ANIMAL_GROWTH_CONSTRAINT(animal)
       
       NET_IMPORT_CALC(import_types)                     net imports
       MIN_NET_IMPORT_CONSTRAINT(import_types)
       MAX_NET_IMPORT_CONSTRAINT(import_types)
       NET_IMPORTS_CHANGE_CALC(import_types)

       SETASIDE_AREA_CALC(farming_type, location)                                 setaside area constraint
       CROPLAND_LAND_COVER_CALC(protection, farming_type, location)               cropland area equals sum of all crop areas
       PASTURE_LAND_COVER_CALC(protection, farming_type, location)                pasture area (land cover) equals pasture area (land use)
       AGRIVOLTAICS_LAND_COVER_CALC(location)                                     agrivoltaics area (land cover) equals sum of all agrivoltaic areas (farming type)
       LAND_COVER_CHANGE_CALC(land_cover, protection, location)                  "calc land cover change. landCoverChange(A, A, location) defined as unchanged land cover"
       LAND_COVER_CHANGE_CONSTRAINT(land_cover, protection, location)   conservation of land area
       CONVERSION_COST_CALC(location)                          cost of land cover conversion
       MAX_CROPLAND_CONSTRAINT(protection, location)                       limits on maximum cropland due to slope

       WOOD_YIELD_CALC(protection, location)              wood yield at rotation - tC per ha
       WOOD_SUPPLY_CALC                       wood supply calculation
       WOOD_DEMAND_CONSTRAINT                 wood demand constraint

       PV_ENERGY_DEMAND_CONSTRAINT        energy produced from photovoltaics
       AV_ENERGY_DEMAND_CONSTRAINT        energy produced from agrivoltaics
       MAX_PHOTOVOLTAICS_CONSTRAINT(protection, location)
       MAX_AGRIVOLTAICS_CONSTRAINT(protection, location)
 
       CARBON_CREDIT_CALC(location)          carbon credits generated
       CARBON_CREDIT_CONSTRAINT              production equals demand plus exports minus imports
       CARBON_FOREST_CONSTRAINT              maximum country reforested area
       
       NPV_CALC    calc objective
;

**************** Crop cost **************************

 UNIT_COST_EQ(crop, farming_type, location) .. unitCost(crop, farming_type, location) =E= (
                                                                     fertiliserUnitCost * fertI(crop, farming_type, location) +
                                                                     irrigCost(location) * irrigMaxRate(crop, location) * irrigI(crop, farming_type, location) +
                                                                     otherIntCost(crop) * otherIntensity(crop, farming_type, location)
                                                                     ) * agrivoltaicsCropCostFactor(farming_type);
                                                                  
**************** Crop yields *******************

 YIELD_EQ(crop, farming_type, location) .. yield(crop, farming_type, location) =E= (
               yieldNone(crop, location) +
               (yieldFertOnly(crop, location) - yieldNone(crop, location)) * (1 - exp(-fertParam(crop, location) * fertI(crop, farming_type, location))) +
               (yieldIrrigOnly(crop, location) - yieldNone(crop, location)) * (1 - exp(-irrigParam(crop, location) * irrigI(crop, farming_type, location))) +
               (yieldBoth(crop, location) + yieldNone(crop, location) - yieldFertOnly(crop, location) - yieldIrrigOnly(crop, location)) *
                                      (1 - exp(-fertParam(crop, location) * fertI(crop, farming_type, location))) * (1 - exp(-irrigParam(crop, location) * irrigI(crop, farming_type, location)))
            ) * (1 - exp(-otherIntensity(crop, farming_type, location) * otherIParam(crop, location))) * agrivoltaicsYieldFactor(crop, farming_type, location);
            

 IRRIGATION_CONSTRAINT_LOCATION(location) .. irrigConstraintLocation(location) * suitableLandArea(location) =G= sum((crop, farming_type), irrigMaxRate(crop, location) * irrigI(crop, farming_type, location) * cropArea(crop, farming_type, location));

 IRRIGATION_CONSTRAINT_FARMING_TYPE(farming_type, location) .. irrigConstraintFarmingType(farming_type, location) * suitableLandArea(location) =G= sum(crop, irrigMaxRate(crop, location) * irrigI(crop, farming_type, location) * cropArea(crop, farming_type, location));

* Intensities must be <= 1
 fertI.UP(crop, farming_type, location) = 1;
 irrigI.UP(crop, farming_type, location) = 1;
 otherIntensity.UP(crop, farming_type, location) = 1;

 fertI.UP(crop, 'conventional', location) = fertLimConventional;
 otherIntensity.UP(crop, 'conventional', location) = otherLimConventional;
 
 fertI.UP(crop, 'restricted', location) = fertLimRestricted;
 otherIntensity.UP(crop, 'restricted', location) = otherLimRestricted;
 
* Minimum crop intensity for agrivoltaics. Equivalent to 50% of potential yield.
 otherIntensity.LO(crop, 'agrivoltaics', location) = 0.2152631;
 
* Fixing irrigI at 0 where no water to avoid optimisation problems
 irrigI.FX(crop, farming_type, location) $ (irrigConstraintLocation(location) = 0) = 0;
 irrigCost(location) $ (irrigCost(location) = 0) = 1e-6;

*************** Crop and animal supply *************************

 CROP_PROD_CALC(crop, location) .. cropProduction(crop, location) =E= sum(farming_type, cropArea(crop, farming_type, location) * yield(crop, farming_type, location));

 TOTAL_CROP_PROD_CALC(crop) .. totalProduction(crop) =E= sum(location, cropProduction(crop, location)) * (1 - seedAndWasteRate(crop));
 
 ANIMAL_PROD_CALC(animal) .. totalProduction(animal) =E= meatEfficency * sum(feed_crop, feedAmount(animal, feed_crop) * cropDM(feed_crop)) * (1 - seedAndWasteRate(animal));
 
 SUPPLY_CONSTRAINT(agri_types) .. totalProduction(agri_types) + importAmount(agri_types) - exportAmount(agri_types) - sum(animal, feedAmount(animal, agri_types)) =E= demand(agri_types);
 
 ANIMAL_GROWTH_CONSTRAINT(animal) .. totalProduction(animal) =L= previousProduction(animal) * 1.2;
 
 feedAmount.FX('monogastrics', 'pasture') = 0;
 feedAmount.FX(animal, not_feed) = 0;

************** Land Cover *****************************

 SETASIDE_AREA_CALC(farming_type, location) .. cropArea('setaside', farming_type, location) =E= sum(crop_less_pasture_setaside, cropArea(crop_less_pasture_setaside, farming_type, location)) * (setAsideRate + unhandledCropRate);
 
 CROPLAND_LAND_COVER_CALC(protection, farming_type, location)$farming_map(protection, farming_type) .. landCoverArea('cropland', protection, location) =E= sum(crop_less_pasture, cropArea(crop_less_pasture, farming_type, location));
 
 PASTURE_LAND_COVER_CALC(protection, farming_type, location)$farming_map(protection, farming_type) .. landCoverArea('pasture', protection, location) =E= cropArea('pasture', farming_type, location);

 AGRIVOLTAICS_LAND_COVER_CALC(location) .. landCoverArea('agrivoltaics', 'unprotected', location) =E= sum(crop, cropArea(crop, 'agrivoltaics', location));

 LAND_COVER_CHANGE_CALC(land_cover, protection, location) .. landCoverArea(land_cover, protection, location) =E= previousLandCoverArea(land_cover, protection, location) +
                                                                                         sum(land_cover_before, landCoverChange(land_cover_before, land_cover, protection, location)) -
                                                                                         sum(land_cover_after, landCoverChange(land_cover, land_cover_after, protection, location));
                                                                                         

 LAND_COVER_CHANGE_CONSTRAINT(land_cover, protection, location) .. sum(land_cover_after, landCoverChange(land_cover, land_cover_after, protection, location)) =E= previousLandCoverArea(land_cover, protection, location);

 CONVERSION_COST_CALC(location) .. totalConversionCost(location) =E= sum((land_cover_before, land_cover_after, protection),
                                                                         landCoverChange(land_cover_before, land_cover_after, protection, location) * conversionCost(land_cover_before, land_cover_after, location));
        
* Cropland area restriction due to slope. Agrivoltaics counts as cropland                                                         
 MAX_CROPLAND_CONSTRAINT(protection, location) .. landCoverArea('cropland', protection, location) + landCoverArea('agrivoltaics', protection, location) =L= maxLandCoverArea('cropland', protection, location);
 
* Photovoltaics area restriction due to slope and accessibility. Agrivoltaics counts as photovoltaics  
 MAX_PHOTOVOLTAICS_CONSTRAINT(protection, location) .. landCoverArea('photovoltaics', protection, location) + landCoverArea('agrivoltaics', protection, location) =L= maxLandCoverArea('photovoltaics', protection, location);
 
 MAX_AGRIVOLTAICS_CONSTRAINT(protection, location) .. landCoverArea('agrivoltaics', protection, location) =L= maxLandCoverArea('agrivoltaics', protection, location);
 
* Used to constrain allowed land cover conversions
 landCoverChange.UP(land_cover_before, land_cover_after, protection, location) = maxLandCoverChange(land_cover_before, land_cover_after, protection, location);
 
************* Forestry ***********************************
 
 WOOD_YIELD_CALC(protection, location) .. woodYieldRota(protection, location) =E= woodYieldMaxParam(location) * rpower(1 - exp(woodYieldSlopeParam(location) / rotationIntensity(protection, location)), woodYieldShapeParam(location));
 
* Upper and lower bounds on rotation intensity

 rotationIntensity.LO(protection, location) = 1 / 160;
 rotationIntensity.UP('unprotected', location) = timberLimUnprotected;
 rotationIntensity.UP('protected', location) = timberLimProtected;

 WOOD_SUPPLY_CALC .. woodSupply =E= sum((protection, location), woodYieldRota(protection, location) * rotationIntensity(protection, location) * landCoverArea('timberForest', protection, location));

 WOOD_DEMAND_CONSTRAINT .. woodSupply =E= demand('wood') + exportAmount('wood') - importAmount('wood');
 
*********** Carbon ***********************************
                                       
 CARBON_CREDIT_CALC(location) .. carbonCredits(location) =E= sum((land_cover_before, land_cover_after, protection), landCoverChange(land_cover_before, land_cover_after, protection, location) * carbonCreditRate(land_cover_before, land_cover_after, location));
                                                        
 CARBON_CREDIT_CONSTRAINT .. sum(location, carbonCredits(location)) =E= demand('carbonCredits') + exportAmount('carbonCredits') - importAmount('carbonCredits');
 
 CARBON_FOREST_CONSTRAINT .. sum(location, landCoverArea('carbonForest', 'unprotected', location)) =L= sum(location, suitableLandArea(location)) * carbonForestMaxProportion;
                                                                           
*********** Energy *********************

 PV_ENERGY_DEMAND_CONSTRAINT .. sum(location, solarEnergyDensity('photovoltaics', location) * landCoverArea('photovoltaics', 'unprotected', location)) =E= demand('energyPV') - importAmount('energyPV');
 
 AV_ENERGY_DEMAND_CONSTRAINT .. sum(location, solarEnergyDensity('agrivoltaics', location) * landCoverArea('agrivoltaics', 'unprotected', location)) =E= demand('energyAV') - importAmount('energyAV');

*************** Trade *******************************

 NET_IMPORT_CALC(import_types) .. netImportAmount(import_types) =E= importAmount(import_types) - exportAmount(import_types);

 MAX_NET_IMPORT_CONSTRAINT(import_types) .. netImportAmount(import_types) =L= maxNetImport(import_types) + excessImports(import_types);
 
 MIN_NET_IMPORT_CONSTRAINT(import_types) .. netImportAmount(import_types) =G= minNetImport(import_types);
 
 NET_IMPORTS_CHANGE_CALC(import_types) .. netImportIncrease(import_types) - netImportDecrease(import_types) =E= netImportAmount(import_types) - previousNetImport(import_types);
 
* No imports or exports of pasture allowed
 importAmount.FX('pasture') = 0;
 exportAmount.FX('pasture') = 0;
 
*  Allow solar energy import in case cannot produce enough domestically.
 maxNetImport('energyPV') = demand('energyPV');
 maxNetImport('energyAV') = demand('energyAV');
 importPrices('energyPV') = 1000;
 importPrices('energyAV') = 1000;
 
************ Total Net Present Value ******************************
            
 NPV_CALC .. total_npv =E= 
*                           Value of exports
                            sum(import_types, exportPrices(import_types) * exportAmount(import_types))
*                           Cost of crop production                  
                            - sum((crop, farming_type, location), cropArea(crop, farming_type, location) * unitCost(crop, farming_type, location) * (1 - subsidyRate(crop)))
*                           Cost of animal production
                            - sum(animal, animalProdCost(animal) * totalProduction(animal) * (1 - subsidyRate(animal)))
*                           Cost of imports
                            - sum(import_types, importPrices(import_types) * importAmount(import_types))
*                           Cost of wood production                           
                            - sum((protection, location), forestManagementCost * rotationIntensity(protection, location) * landCoverArea('timberForest', protection, location))
*                           Cost of solar energy                           
                            - sum((solar_land, location), solarCostRate(solar_land, location) * landCoverArea(solar_land, 'unprotected', location))                     
*                           Land cover conversion costs
                            - sum(location, totalConversionCost(location))                      
*                           Reforestation cost                         
                            - sum((location, land_cover)$[not sameAs(land_cover, 'carbonForest')], landCoverChange(land_cover, 'carbonForest', 'unprotected', location) * forestManagementCost)
*                           Crop monoculture penalty. Derived from Simpson's diversity index, weighted by production                       
                            - sum(location, monoculturePenalty * sum(crop_less_pasture_setaside, sqr(cropProduction(crop_less_pasture_setaside, location))) / (sum(crop_less_pasture_setaside, cropProduction(crop_less_pasture_setaside, location)) + 0.01))
*                           Pasture intensity penalty. Controls how spread out pasture production is. 
                            - sum(location, pastureIntensityPenalty * sqr(cropProduction('pasture', location))) / (sum(location, cropProduction('pasture', location)) + delta)
*                           Small reward for increasing natural area to allow land abandonment
                            + sum((land_cover, protection, location)$(not sameAs(land_cover, 'natural')), 0.00001 * landCoverChange(land_cover, 'natural', protection, location))
*                           Feed diversity penalty                          
                            - sum(animal, feedDiversityCost(animal) * sum(feed_crop, sqr(feedAmount(animal, feed_crop) * cropDM(feed_crop))) / (sum(feed_crop, feedAmount(animal, feed_crop) * cropDM(feed_crop)) + delta))
*                           Net import change cost                            
                            - sum(import_types, tradeAdjustmentCostRate(import_types) * (netImportIncrease(import_types) + netImportDecrease(import_types)))
*                           Allow import constraint to be relaxed at a high cost if needed to avoid solution failture                            
                            - sum(import_types, 1000 * excessImports(import_types))
;

 MODEL LAND_USE /  ALL / ;
                       
 fertI.L(crop, farming_type, location) = previousFertIntensity(crop, farming_type, location);
 irrigI.L(crop, farming_type, location) = previousIrrigIntensity(crop, farming_type, location);
 otherIntensity.L(crop, farming_type, location) = previousOtherIntensity(crop, farming_type, location);
 cropArea.L(crop, farming_type, location) = previousCropArea(crop, farming_type, location);
 landCoverArea.L(land_cover, protection, location) = previousLandCoverArea(land_cover, protection, location);
 landCoverChange.L(land_cover, land_cover, protection, location) = previousLandCoverArea(land_cover, protection, location);
 feedAmount.L('ruminants', feed_crop) = previousRuminantFeed(feed_crop);
 feedAmount.L('monogastrics', feed_crop) = previousMonogastricFeed(feed_crop);
 importAmount.L(all_types) = previousImportAmount(all_types);
 exportAmount.L(all_types) = previousExportAmount(all_types);
 netImportAmount.L(import_types) = previousImportAmount(import_types) - previousExportAmount(import_types);
 rotationIntensity.L(protection, location) = previousRotationIntensity(protection, location);
 rotationIntensity.L(protection, location) $ (previousRotationIntensity(protection, location) = 0) = 1 / 160;
 woodYieldRota.L(protection, location) = woodYieldMaxParam(location) * rpower(1 - exp(woodYieldSlopeParam(location) / rotationIntensity.L(protection, location)), woodYieldShapeParam(location));
 totalProduction.L(agri_types) = previousProduction(agri_types);
 
 option nlp = conopt4;
 option iterlim = 10000;
 option seed = 42;
* Memory limit 8GB = 8192MB
 heapLimit = 8192;

 SOLVE LAND_USE USING NLP MAXIMIZING total_npv;

* Try solving again if not locally optimal
 if ((LAND_USE.modelstat ne 2),
  option nlp = conopt3;
  SOLVE LAND_USE USING NLP MAXIMIZING total_npv;
 )

*SCALAR x;
*x = sum((feed_crop, animal), 0.1 * sqr(feedAmount.l(animal, feed_crop) - previousFeedAmount(animal, feed_crop)));

* Calculate summary information used in Java process
 parameter totalProdCost(all_types);
 parameter totalCropArea(crop);
 parameter netImportCost(all_types);
 parameter importFraction(all_types);
 parameter prodCostRate(crop);
 parameter productionShock(all_types);
 parameter woodProdCost;
 scalar netCarbonCredits;

 productionShock(crop) = sum((farming_type, location), yield.l(crop, farming_type, location) * cropArea.l(crop, farming_type, location) * yieldShock(crop, location));

 totalProdCost(crop) = sum((location, farming_type), unitCost.l(crop, farming_type, location) * cropArea.l(crop, farming_type, location));
 totalCropArea(crop) = sum((location, farming_type), cropArea.l(crop, farming_type, location));
 
 netImportCost(import_types) = importAmount.L(import_types) * importPrices(import_types) - exportAmount.l(import_types) * exportPrices(import_types);

 importFraction(import_types) $ (importAmount.l(import_types) > 0) = importAmount.l(import_types) / (totalProduction.l(import_types) + importAmount.l(import_types));

 prodCostRate(crop)$[totalProduction.l(crop) <> 0] = totalProdCost(crop) / totalProduction.l(crop);
 
 totalProdCost(animal) = sum(feed_crop, importFraction(feed_crop) * feedAmount.l(animal, feed_crop) * importPrices(feed_crop)
                                     +  (1 - importFraction(feed_crop)) * prodCostRate(feed_crop)) + totalProduction.l(animal) * animalProdCost(animal);

 If(woodSupply.L > 0,
    woodProdCost = sum((protection, location), forestManagementCost * rotationIntensity.L(protection, location) * landCoverArea.L('timberForest', protection, location));
    );
 
 netCarbonCredits = SUM(location, carbonCredits.L(location));

 Scalar totalCostsLessLU;

 totalCostsLessLU = sum(crop,totalProdCost(crop)) + sum(animal,totalProdCost(animal)) + sum(import_types,netImportCost(import_types));

 Scalar ms 'model status', ss 'solve status';
 ms=LAND_USE.modelstat;
 ss=LAND_USE.solvestat;
 
* Diagnostics
 parameter totalLandCoverChange(land_cover, land_cover_after);
 parameter landCoverDiff(land_cover);
 parameter rotationPeriod(protection, location);
 parameter irrigWaterWithdrawn(crop, location);
 parameter totalIrrig;
 parameter fertiliserUsed(crop);
 parameter totalFert;
 parameter abandonedArea(crop);
 parameter otherIntUsed(crop, location);
 parameter totalOtherI;
 parameter woodProd(location);
 totalLandCoverChange(land_cover, land_cover_after) = sum((protection, location), landCoverChange.L(land_cover, land_cover_after, protection, location));
 landCoverDiff(land_cover) = sum((protection, location), landCoverArea.L(land_cover, protection, location)) - sum((protection, location), previousLandCoverArea(land_cover, protection, location));
 rotationPeriod(protection, location) = 1 / rotationIntensity.L(protection, location);
 irrigWaterWithdrawn(crop, location) = sum(farming_type, irrigMaxRate(crop, location) * irrigI.L(crop, farming_type, location) * cropArea.L(crop, farming_type, location) * 0.01);
 totalIrrig = sum((crop, location), irrigWaterWithdrawn(crop, location));
 fertiliserUsed(crop) = sum((farming_type, location), fertI.L(crop, farming_type, location) * cropArea.L(crop, farming_type, location));
 totalFert = sum(crop, fertiliserUsed(crop));
 abandonedArea(crop) = sum((farming_type, location)$(otherIntensity.L(crop, farming_type, location) = 0), cropArea.L(crop, farming_type, location));
 otherIntUsed(crop, location) = sum(farming_type, otherIntensity.L(crop, farming_type, location) * cropArea.L(crop, farming_type, location));
 totalOtherI = sum((crop, location), otherIntUsed(crop, location));
 woodProd(location) = sum(protection, woodYieldRota.L(protection, location) * landCoverArea.L('timberForest', protection, location) * rotationIntensity.L(protection, location));
