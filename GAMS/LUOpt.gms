 SET all_types                   / monogastrics, ruminants, cereals, oilcropspulses, wheat, maize, rice, oilcrops, pulses, starchyRoots, fruitveg, sugar, energycrops, pasture, setaside /;
 SET crop(all_types)                                                               / wheat, maize, rice, oilcrops, pulses, starchyRoots, fruitveg, sugar, energycrops, pasture, setaside /;
 SET crop_less_pasture(crop)                                                       / wheat, maize, rice, oilcrops, pulses, starchyRoots, fruitveg, sugar, energycrops,          setaside/;
 SET cereal_crop(crop)                                                             / wheat, maize, rice /;
 SET oilpulse_crop(crop)	                                                                               / oilcrops, pulses /;
 SET feed_crop(crop)                                                               / wheat, maize, rice, oilcrops, pulses, starchyRoots, fruitveg,                     pasture/;
 SET feed_crop_less_pasture(feed_crop)                                             / wheat, maize, rice, oilcrops, pulses, starchyRoots, fruitveg /;
 SET traded_commodity(all_types) / monogastrics, ruminants,                          wheat, maize, rice, oilcrops, pulses, starchyRoots, fruitveg, sugar, energycrops /;
 
 SET location;
 PARAMETER suitableLandArea(location)        areas of land in Mha;
 PARAMETER previousArea(crop, location)      areas for previous timestep in Mha;
 PARAMETER previousFertIntensity(crop, location);
 PARAMETER previousIrrigIntensity(crop, location); 
 PARAMETER previousOtherIntensity(crop, location);
 PARAMETER previousRuminantFeed(crop);
 PARAMETER previousMonogastricFeed(crop);
 PARAMETER previousImportAmount(all_types)   we still need imports and for feed that we is not grown domestically;
 PARAMETER previousExportAmount(all_types)
 PARAMETER yieldNone(crop, location)         yield in t per ha;
 PARAMETER yieldFertOnly(crop, location)     yield in t per ha;
 PARAMETER yieldIrrigOnly(crop, location)    yield in t per ha;
 PARAMETER yieldBoth(crop, location)         yield in t per ha;
 PARAMETER fertParam(crop, location)         yield response to fertilizer parameter;
 PARAMETER irrigParam(crop, location)        yield response to irrigation parameter;
 PARAMETER demand(all_types)                 in t;
 PARAMETER exportPrices(traded_commodity)    prices for exports and domestic farmgate price;
 PARAMETER importPrices(traded_commodity)    prices for imports;
 PARAMETER maxNetImport(all_types)           maximum Production crop based domestic demand and net imports;
 PARAMETER minNetImport(all_types)           minimum Production crop based domestic demand and net imports;
 PARAMETER irrigCost(location)               irrigation cost in cost per 1000 Mlitre or   Mha for each litre per m2;
 PARAMETER irrigMaxRate(crop, location)      max water application rate irrigation in litre per m2;
 PARAMETER irrigConstraint(location)         max water available for irrigation in litre per m2;
 PARAMETER minDemandPerCereal(cereal_crop)   min demand for each cereal crop as factor of all cereals;
 PARAMETER minDemandPerOilcrop(oilpulse_crop) min demand for each oilcrop and pulse crop as factor of all oilcrops and pulses;
 PARAMETER seedAndWasteRate(all_types)       rate of use for seed and waste combined;
 PARAMETER subsidyRate(crop)                 rates of subsidy compared to costs;
 
 PARAMETER agriExpansionCost(location)       price for increase agricultural area varies based on amount of managed or unmanaged forest;
 SCALAR pastureIncCost                       price for increasing pasture area;
 SCALAR cropIncCost                          price for increasing crop;
 SCALAR cropDecCost                          price for decreasing crop;
 SCALAR pastureDecCost                       price for decreasing pasture;
  
 SCALAR meatEfficency                        efficiency of converting feed and pasture into animal products;
 SCALAR animalFeedFromOtherSources           animal feed from source other than modelled crops;
 SCALAR fertiliserUnitCost                   fert cost at max fert rate;
 SCALAR otherIParam                          yield response to other intensity;
 SCALAR otherICost                           cost of other intensity;
 SCALAR unhandledCropRate                    rate of fruit veg and other crops not modelled;
 SCALAR setAsideRate                         rate of set aside and failed crop;
 SCALAR domesticPriceMarkup                  factor price increased from cost of production;
 SCALAR maxLandExpansionRate                 max rate of country land expansion;

*$gdxin "/Users/peteralexander/Documents/R_Workspace/UNPLUM/output/GamsTmp/China2010.gdx"
$gdxin %gdxincname% 
$load location, suitableLandArea, demand, agriExpansionCost, cropIncCost, pastureIncCost, cropDecCost, pastureDecCost
$load previousArea, previousFertIntensity, previousIrrigIntensity, previousOtherIntensity, previousRuminantFeed, previousMonogastricFeed, previousImportAmount, previousExportAmount
$load yieldNone, yieldFertOnly, yieldIrrigOnly, yieldBoth
$load fertParam, irrigParam, otherIParam, exportPrices, importPrices, maxNetImport, minNetImport, unhandledCropRate, setAsideRate, maxLandExpansionRate, subsidyRate
$load meatEfficency, otherICost, irrigCost, irrigMaxRate, irrigConstraint, fertiliserUnitCost, domesticPriceMarkup, minDemandPerCereal, minDemandPerOilcrop, seedAndWasteRate, animalFeedFromOtherSources

$gdxin    
 
 SCALAR delta use to smooth power function see 7.5 www.gams.com dd docs solversconopt.pdf / 0.00000000001 /;
     
 SCALAR ruminantOtherFeed;
 SCALAR monogastricOtherFeed;
 ruminantOtherFeed = animalFeedFromOtherSources * 0.25;
 monogastricOtherFeed = animalFeedFromOtherSources * 0.75;
 demand(cereal_crop) = demand('cereals') * minDemandPerCereal(cereal_crop);
 demand(oilpulse_crop) = demand('oilcropspulses') * minDemandPerOilcrop(oilpulse_crop);
 
 previousArea(crop_less_pasture, location) = previousArea(crop_less_pasture, location) * (1.0 - unhandledCropRate);
 
 PARAMETER cropDM(crop)  kg DM per kg of feed the conversion from feed to meat is done in the R animal product index.  Pasture is in DM terms already
          /   wheat         0.87   
              maize         0.86  
              rice          0.89
              oilcrops      0.96  
              pulses        0.31
              starchyRoots  0.25
              fruitveg      0.1
              sugar         1
              pasture       1    / ; 
   
 PARAMETER prodCost(crop)  cost per ha before intensity values not including fertiliser or irrigation i.e. seed spray and other.  In 1000 $ per ha
          /   wheat             0.32   
              maize             0.28 
              rice              0.36
              oilcrops          0.2
              pulses            0.31
              starchyRoots      3.14
              fruitveg          4.0
              sugar             3.0             
              energyCrops       0.34 / ;

 PARAMETER baseCost(crop);
 PARAMETER otherIntCost(crop);
 baseCost(crop) = prodCost(crop)*0.3;
 otherIntCost(crop) = baseCost(crop)*0.7 + otherICost;
 baseCost('pasture') = 0.04;
 otherIntCost('pasture') = 0.8 + otherICost;
                              
 VARIABLES
       area(crop, location)               total area for each crop - Mha
       fertI(crop, location)              fertilizer intensity for each crop - factor between 0 and 1
       irrigI(crop, location)             irrigation intensity for each crop - factor between 0 and 1
       otherIntensity(crop, location)
       ruminantFeed(crop)                 amount of feed for ruminant animals - Mt
       monogastricFeed(crop)              amount of feed for monogatric animals - Mt
       exportAmount(all_types)            exports of crops and meat - Mt
       importAmount(all_types)            imports of crops and meat - Mt
       net_supply(all_types)              supply after imports exports and feed - Mt
       domesticallyMetDemand(traded_commodity)    amount domestically produced and consumed as food - Mt
       yield(crop, location)              yield per area for each crop - t per ha
       unitCost(crop, location)           cost per area for each crop - cost
       agriLandExpansion(location)        addition agricultural land needed as it must be positive it deliberately does not account for abandonment
       cropIncrease(location)             
       cropDecrease(location)             
       pastureIncrease(location)         
       pastureDecrease(location)    
       totalFeedDM      
       totalProfit                        total profit from domestic production;
 
 POSITIVE VARIABLE area, fertI, irrigI, otherIntensity, ruminantFeed, monogastricFeed, importAmount, exportAmount, net_supply,
                   agriLandExpansion, cropIncrease, cropDecrease, pastureDecrease, pastureIncrease, totalFeedDM, domesticallyMetDemand;
  
 EQUATIONS
       UNIT_COST_EQ(crop, location)                     cost per area
       YIELD_EQ(crop, location)                         yield given chosen intensity
       CROP_NET_SUPPLY_EQ(crop)                         calc net supply for crops
       RUMINANT_NET_SUPPLY                              calc net supply for ruminant products
       MONOGASTRICS_NET_SUPPLY                          calc net supply for monogastric products
       TOTAL_CEREAL_NET_SUPPLY                          calc net supply for combined cereals
       TOTAL_OIL_PULSE_NET_SUPPLY                       calc net supply for combined oilcrops and pulses
       DEMAND_CONSTRAINT(all_types)                     satisfy demand
       PASTURE_IMPORT_CONSTRAINT                        constraint to not import pasture
       DOMESTICALLY_MET_DEMAND_CONSTRAINT1(traded_commodity)   constraint to determine commodity produced and consumed domestically
       DOMESTICALLY_MET_DEMAND_CONSTRAINT2(traded_commodity)   constraint to determine commodity produced and consumed domestically
       TOTAL_NON_PASTURE_FEED_DM_CALC                   calc total feed dry matter not including pasture
       FEED_MIX_CONSTRAINT(feed_crop_less_pasture)      limit amount of feed for each feed crop
       MAX_FERT_INTENSITY_CONSTRAINT(crop, location)    constraint on maximum fertilizer intensity
       MAX_IRRIG_INTENSITY_CONSTRAINT(crop, location)   constraint on maximum irrigation intensity
       MAX_OTHER_INTENSITY_CONSTRAINT(crop, location)
       SETASIDE_AREA_CALC(location)
       TOTAL_LAND_CHANGE_CONSTRAINT(location)           constraint on suitable land use 
       MAX_NET_IMPORT_CONSTRAINT(traded_commodity)      constraint on max net imports
       MIN_NET_IMPORT_CONSTRAINT(traded_commodity)      constraint on min net imports
       IRRIGATION_CONSTRAINT(location)                  constraint no water usage
       AGRI_LAND_EXPANSION_CALC(location)               calc agriLandExpansion
       CROP_INCREASE_CALC(location)                     
       CROP_DECREASE_CALC(location)                     
       PASTURE_INCREASE_CONV_CALC(location)             
       PASTURE_DECREASE_CONV_CALC(location)          
       PASTURE_TOTAL_CHANGE_CONSTRAINT(location)   
       LAND_RATE_CHANGE_CONSTRAINT_INCREASE
       PROFIT_EQ                                        total profit objective function;
 
 UNIT_COST_EQ(crop, location) .. unitCost(crop, location) =E=  (     baseCost(crop) +           
                                                                     fertiliserUnitCost * fertI(crop, location) + 
                                                                     irrigCost(location) * irrigMaxRate(crop, location) * irrigI(crop, location) +
                                                                     otherIntCost(crop) * otherIntensity(crop, location)
                                                                     ) ;
 
 YIELD_EQ(crop, location) .. yield(crop, location) =E= (
               yieldNone(crop, location) + 
               (yieldFertOnly(crop, location) - yieldNone(crop, location)) * (1 - exp(-fertParam(crop, location)*fertI(crop, location))) +
               (yieldIrrigOnly(crop, location) - yieldNone(crop, location)) * (1 - exp(-irrigParam(crop, location)*irrigI(crop, location))) +
               (yieldBoth(crop, location) + yieldNone(crop, location) - yieldFertOnly(crop, location) - yieldIrrigOnly(crop, location)) *
                                      (1 - exp(-fertParam(crop, location)*fertI(crop, location))) * (1 - exp(-irrigParam(crop, location)*irrigI(crop, location)))
            ) * (1 - exp(-otherIntensity(crop, location)*otherIParam));
    
 CROP_NET_SUPPLY_EQ(crop) .. net_supply(crop) =E= sum(location, area(crop, location) * yield(crop, location)) * (1 - seedAndWasteRate(crop)) - 
                         ruminantFeed(crop) - monogastricFeed(crop) + importAmount(crop) - exportAmount(crop);
 
 RUMINANT_NET_SUPPLY .. net_supply('ruminants') =E= 
                         meatEfficency*(sum(feed_crop, ruminantFeed(feed_crop) * cropDM(feed_crop)) + ruminantOtherFeed) * (1 - seedAndWasteRate('ruminants')) + 
                         importAmount('ruminants') - exportAmount('ruminants');
    
 MONOGASTRICS_NET_SUPPLY .. net_supply('monogastrics') =E= 
                         meatEfficency*(sum(feed_crop_less_pasture, monogastricFeed(feed_crop_less_pasture) * cropDM(feed_crop_less_pasture)) + monogastricOtherFeed) * (1 - seedAndWasteRate('monogastrics')) +
                         importAmount('monogastrics') - exportAmount('monogastrics');
   
 TOTAL_CEREAL_NET_SUPPLY .. net_supply('cereals') =E= sum(cereal_crop, net_supply(cereal_crop));
  
 TOTAL_OIL_PULSE_NET_SUPPLY .. net_supply('oilcropspulses') =E= sum(oilpulse_crop, net_supply(oilpulse_crop));
  
 DEMAND_CONSTRAINT(all_types) .. net_supply(all_types) =G= demand(all_types);
 
 PASTURE_IMPORT_CONSTRAINT .. importAmount('pasture') =E= 0;
 
 DOMESTICALLY_MET_DEMAND_CONSTRAINT1(traded_commodity) .. domesticallyMetDemand(traded_commodity) =L= net_supply(traded_commodity) - importAmount(traded_commodity) + exportAmount(traded_commodity);
 DOMESTICALLY_MET_DEMAND_CONSTRAINT2(traded_commodity) .. domesticallyMetDemand(traded_commodity) =L= demand(traded_commodity);
    
 TOTAL_NON_PASTURE_FEED_DM_CALC .. totalFeedDM =E= sum(feed_crop_less_pasture, (ruminantFeed(feed_crop_less_pasture) + monogastricFeed(feed_crop_less_pasture)) * cropDM(feed_crop_less_pasture));
 FEED_MIX_CONSTRAINT(feed_crop_less_pasture) .. (ruminantFeed(feed_crop_less_pasture) + monogastricFeed(feed_crop_less_pasture)) * cropDM(feed_crop_less_pasture) =L= totalFeedDM * 0.7;
 
 MAX_FERT_INTENSITY_CONSTRAINT(crop, location) .. fertI(crop, location) =L= 1;
 MAX_IRRIG_INTENSITY_CONSTRAINT(crop, location) .. irrigI(crop, location) =L= 1;
 MAX_OTHER_INTENSITY_CONSTRAINT(crop, location) .. otherIntensity(crop, location) =L= 1;
 
 TOTAL_LAND_CHANGE_CONSTRAINT(location) .. suitableLandArea(location) =G= sum(crop_less_pasture, area(crop_less_pasture, location)) / (1.0 - unhandledCropRate) + area('pasture', location);
 SETASIDE_AREA_CALC(location) .. area('setaside', location) =E= sum(crop_less_pasture, area(crop_less_pasture, location)) * setAsideRate;
  
 MAX_NET_IMPORT_CONSTRAINT(traded_commodity) .. importAmount(traded_commodity) - exportAmount(traded_commodity) =L= maxNetImport(traded_commodity);
 MIN_NET_IMPORT_CONSTRAINT(traded_commodity) .. importAmount(traded_commodity) - exportAmount(traded_commodity) =G= minNetImport(traded_commodity);
  
 IRRIGATION_CONSTRAINT(location) .. irrigConstraint(location) * suitableLandArea(location) * (1.0 - unhandledCropRate) =G= sum(crop, irrigMaxRate(crop, location) * irrigI(crop, location) * area(crop, location));

 AGRI_LAND_EXPANSION_CALC(location) .. agriLandExpansion(location) =G= sum(crop, area(crop, location) - previousArea(crop, location)); 
 
 CROP_INCREASE_CALC(location) .. cropIncrease(location) =G= sum(crop_less_pasture, area(crop_less_pasture, location) - previousArea(crop_less_pasture, location));
 CROP_DECREASE_CALC(location) .. cropDecrease(location) =G= -sum(crop_less_pasture, area(crop_less_pasture, location) - previousArea(crop_less_pasture, location));
 PASTURE_INCREASE_CONV_CALC(location) .. pastureIncrease(location) =G= area('pasture', location) - previousArea('pasture', location);
 PASTURE_DECREASE_CONV_CALC(location) .. pastureDecrease(location) =G= -(area('pasture', location) - previousArea('pasture', location));
 PASTURE_TOTAL_CHANGE_CONSTRAINT(location) .. pastureIncrease(location) -pastureDecrease(location) =G= area('pasture', location) - previousArea('pasture', location);
  
 LAND_RATE_CHANGE_CONSTRAINT_INCREASE .. sum(location, cropIncrease(location)) / sum(location, (suitableLandArea(location)*(1.0 - unhandledCropRate))-sum(crop, previousArea(crop, location))) =L= maxLandExpansionRate;

 PROFIT_EQ .. totalProfit =E= 
         ( 
              SUM(traded_commodity, domesticallyMetDemand(traded_commodity) * importPrices(traded_commodity) + exportAmount(traded_commodity) * exportPrices(traded_commodity)) - 
              
              (   SUM((crop, location), area(crop, location) * unitCost(crop, location) * (1-subsidyRate(crop))) +
                  
                  SUM(location, 
                     agriExpansionCost(location) * agriLandExpansion(location) +
                     cropIncCost * cropIncrease(location) + 
                     pastureIncCost * pastureIncrease(location) + 
                     cropDecCost * cropDecrease(location) + 
                     pastureDecCost * pastureDecrease(location)
                     )
               )
               
               - SUM(traded_commodity, importAmount(traded_commodity) * importPrices(traded_commodity)) 
         );
 
 MODEL LAND_USE /ALL/ ;
 fertI.L(crop, location) = previousFertIntensity(crop, location);
 irrigI.L(crop, location) = previousIrrigIntensity(crop, location);
 otherIntensity.L(crop, location) = previousOtherIntensity(crop, location);
 area.L(crop, location) = previousArea(crop, location);
 ruminantFeed.L(feed_crop) = previousRuminantFeed(feed_crop);
 monogastricFeed.L(feed_crop) = previousMonogastricFeed(feed_crop);
 importAmount.L(all_types) = previousImportAmount(all_types);
 exportAmount.L(all_types) = previousExportAmount(all_types);
  
 SOLVE LAND_USE USING NLP MAXIMIZING totalProfit;
       
 display agriLandExpansion.L, previousArea, irrigMaxRate, otherIntensity.L, fertI.L, irrigI.L, area.L, cropIncrease.L, cropDecrease.L, pastureIncrease.L, pastureDecrease.L;
 display domesticallyMetDemand.l, net_supply.l, demand, ruminantFeed.l, monogastricFeed.l, importAmount.l, exportAmount.l;

 
* Calculate summary information used in Java process 
 parameter totalProd(all_types);
 parameter totalProdCost(all_types);
 parameter totalArea(crop);
 parameter totalCropland(location);
 parameter netImportAmount(all_types);
 parameter netImportCost(all_types);
 parameter feedCostRate(feed_crop);
 
* Production quantities based on smaller area (before unhandledCropArea adjustment applied)
 totalProd(crop) = sum(location, area.l(crop, location) * yield.l(crop, location));
 totalProd('ruminants') = meatEfficency*(sum(feed_crop, ruminantFeed.l(feed_crop) * cropDM(feed_crop)) + ruminantOtherFeed);
 totalProd('monogastrics') = meatEfficency*(sum(feed_crop, monogastricFeed.l(feed_crop) * cropDM(feed_crop)) + monogastricOtherFeed);
 
* Cost based on adjusted area
 area.l(crop_less_pasture, location) = area.l(crop_less_pasture, location) / (1.0 - unhandledCropRate);
 totalProdCost(crop) = sum(location, unitCost.l(crop, location) * area.l(crop, location));
 totalCropland(location) = sum(crop_less_pasture, area.l(crop_less_pasture, location));
 totalArea(crop) = sum(location, area.l(crop, location));
          
 feedCostRate(feed_crop)$[totalProd(feed_crop) <> 0] = totalProdCost(feed_crop) / totalProd(feed_crop);
 totalProdCost('ruminants') = sum(feed_crop, ruminantFeed.l(feed_crop) * feedCostRate(feed_crop));
 totalProdCost('monogastrics') = sum(feed_crop, monogastricFeed.l(feed_crop) * feedCostRate(feed_crop));
 
 netImportCost(traded_commodity) = importAmount.l(traded_commodity) * importPrices(traded_commodity) - exportAmount.l(traded_commodity) * exportPrices(traded_commodity);
 netImportAmount(traded_commodity) = importAmount.l(traded_commodity) - exportAmount.l(traded_commodity);
 
 Scalar ms 'model status', ss 'solve status'; 
 ms=LAND_USE.modelstat;
 ss=LAND_USE.solvestat;
