Set i;

variable
  alpha(i)     Parameter of MAIDADS
  beta(i)      Parameter of MAIDADS
  kappa        Parameter of MAIDADS
  delta(i)     Parameter of MAIDADS
  tau(i)       Parameter of MAIDADS
  omega        Parameter of MAIDADS
;

$gdxin ../data/DemandParameters.gdx
$load i, alpha, beta, kappa, delta, tau, omega
$gdxin

Set plumi(*) Sector index / 'nonfood', 'cereals', 'oilcropspulses', 'ruminants', 'monogastrics', 'fruitveg', 'starchyRoots', 'sugar' /;

set mapindx(i,plumi) /
'3905'.'cereals'
'3906'.'starchyRoots'
'3908'.'oilcropspulses'
'3909'.'sugar'
'3918'.'fruitveg'
'3943'.'ruminants'
'3944'.'monogastrics'
'5000'.'nonfood' /;

parameter 
  alphaConv(plumi)
  betaConv(plumi)
  kappaConv
  deltaConv(plumi)
  tauConv(plumi)
  omegaConv
;

alphaConv(plumi) = sum(mapindx(i, plumi), alpha.l(i));
betaConv(plumi) = sum(mapindx(i, plumi), beta.l(i));
kappaConv = kappa.l;
deltaConv(plumi) = sum(mapindx(i, plumi), delta.l(i));
tauConv(plumi) = sum(mapindx(i, plumi), tau.l(i));
omegaConv = omega.l;

EXECUTE_UNLOAD "../data/DemandParamConv.gdx" plumi=i, alphaConv=alpha, betaConv=beta, deltaConv=delta, tauConv=tau, omegaConv=omega, kappaConv=kappa;
