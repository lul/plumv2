$ondotl

Set i(*);
ALIAS (i,j);

PARAMETER
  alpha(i)     Parameter of MAIDADS
  beta(i)      Parameter of MAIDADS
  kappa        Parameter of MAIDADS
  delta(i)     Parameter of MAIDADS
  tau(i)       Parameter of MAIDADS
  omega        Parameter of MAIDADS
  previousU
  previousSubs(i)
  previousDisc(i)
;

$gdxin %gdx_parameters%
*$gdxin "/Users/bart/Documents/plumv2/data/WoodDemandParamConv.gdx"
$load i, alpha, beta, kappa, delta, tau, omega
$gdxin

VARIABLE
  u                Utility
  error            Error term for NLS
  ressq            Residual squared
;

POSITIVE VARIABLE
  DiscretionaryC(i)  Discretionary consumption
  SubsistenceC(i)    Subsistence consumption
  w(i)               Budget share by commodity
;

PARAMETER
  p(i)               Price,
  m                  GDP per capita
;

$gdxin %gdx_prices_and_gdp% 
*$gdxin "/Users/bart/Documents/PLUM_Output/calib_forest/GamsTmp/Argentina2020wood.gdx" 
$load p=price, m=gdp_pc, previousU, previousSubs, previousDisc
$gdxin

display p, m, alpha, beta, kappa, delta, tau, omega;

w.up(j) = 1;

EQUATIONS
  EQ_w(i)      "Definition of budget share"
  EQ_u         "Implicit definition of utility"
  EQ_DiscretionaryC(i)
  EQ_SubsistenceC(j)
  EQ_u_NLS     "Relaxed constraint for NLS"
  EQ_ressq_NLS
;

EQ_w(j)..
  w(j) =e= (SubsistenceC(j) + DiscretionaryC(j)) * p(j) / m;

EQ_u ..     sum(i, (alpha(i)+ beta(i)* exp(u)) / (1 + exp(u)) * log(DiscretionaryC(i))) - u =e= kappa;
  
EQ_u_NLS .. sum(i, (alpha(i)+ beta(i)* exp(u)) / (1 + exp(u)) * log(DiscretionaryC(i))) - u =e= kappa + error;

EQ_DiscretionaryC(i)$p(i).. DiscretionaryC(i) =e= [(alpha(i) + beta(i) * exp(u))/(1 + exp(u))] / p(i) * [m - sum(j, p(j) * SubsistenceC(j))];

EQ_SubsistenceC(j).. SubsistenceC(j) =e= (delta(j) + tau(j) * exp(omega * u)) / (1 + exp(omega * u));
  
EQ_ressq_NLS .. ressq =e= sqr(error);

model MAIDADS_Sim_CNS "MAIDADS model for simulation" /
      EQ_w
      EQ_u
      EQ_DiscretionaryC
      EQ_SubsistenceC
/ ;

model MAIDADS_Sim_NLP /
      EQ_w
      EQ_u_NLS
      EQ_DiscretionaryC
      EQ_SubsistenceC
      EQ_ressq_NLS
/ ;

MAIDADS_Sim_CNS.optfile = 1;
option cns=path;

Parameter minSpending;
minSpending =  sum(i, p(i)*delta(i));

Scalar ms 'model status', ss 'solve status';
u = previousU;

If(minSpending < m, 

    SubsistenceC(i)  = previousSubs(i);
    DiscretionaryC(i) = previousDisc(i);
    DiscretionaryC.LO(i) = 1E-11;
    SubsistenceC.LO(j) = 0;
    w(j) = (SubsistenceC(j)+DiscretionaryC(j))*p(j)/m;

*  Initial solution by least square. If no exact solution then go back to least square
    solve MAIDADS_Sim_NLP using nlp minimising ressq;
    solve MAIDADS_Sim_CNS using cns;
    
    ms=MAIDADS_Sim_CNS.modelstat;
    ss=MAIDADS_Sim_CNS.solvestat;
 
    if ((MAIDADS_Sim_CNS.modelstat ne 16),
        solve MAIDADS_Sim_NLP using nlp minimising ressq;
        ms=MAIDADS_Sim_NLP.modelstat;
        ss=MAIDADS_Sim_NLP.solvestat;
    );
else
    SubsistenceC(i)  = delta(i);
    DiscretionaryC(i) = 0;
)



display SubsistenceC.l, DiscretionaryC.l;
