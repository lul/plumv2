#!/bin/sh
#$ -o /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs
#$ -e /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs
#$ -l h_vmem=8G 
#$ -R y

umask 002

common_prop_file=/exports/csce/eddie/geos/groups/LURG/models/PLUM/output/common_properties
output_dir=/exports/csce/eddie/geos/groups/LURG/models/PLUM/output
overwrite=0

for arg in "$@"
do
    case $arg in
        -O|-o)
        overwrite=1
        shift # Remove argument name from processing
        ;;
        -C|-c)
        common_prop_file="$2"
        shift # Remove argument name from processing
        shift # Remove argument value from processing
        ;;
        *)
        [[ ! -z $1 ]] && file="$1"
        shift # Remove argument value from processing
        ;;
    esac
done

echo scenariofile=$file, common_prop_file=$common_prop_file, overwrite=$overwrite
echo

# not keen on the way this directory is fixed but for backward compatiblity will not change it
filename="/exports/csce/eddie/geos/groups/LURG/models/PLUM/$file"

if [ ! -f "$filename" ]; then
    echo "File for scenario data $filename does not exist."
    echo "Need to specify scenario table file correctly. Stopping!"
    exit -1
fi

if [ ! -f "$common_prop_file" ]; then
    echo "Common properties file $common_prop_file does not exist."
    echo "Need to specify with -c or use default. Stopping!"
    exit -1
fi

echo "Generating scenarios into $output_dir"

while read -r datarow
do
  IFS=',' read -r -a array <<< "$datarow"

  if [ -z "$header" ]; then
     header=( "${array[@]}" )
  else
     ensemble="${array[0]}"
     scenario="${array[1]}"
     scenario_dir=$output_dir/$ensemble/$scenario
     echo "$scenario for $ensemble in $scenario_dir"

     if [ ! -d "$output_dir/$ensemble" ]; then
        mkdir $output_dir/$ensemble
     fi

     if [ ! -d "$scenario_dir" ]; then
        mkdir $scenario_dir
     elif [ $overwrite -ne 1 ]; then
        echo "Already have scenario $scenario, set -O to overwrite existing scenarios"
        continue;
     fi

     config_file=$scenario_dir/config.properties
     cp $common_prop_file $config_file

     for index in "${!array[@]}"
     do
        echo "${header[index]}"="${array[index]}" >> $config_file
     done
	 
     echo "$scenario generated"
  fi
  	
done < $filename

source /etc/profile

if [[ $* == *-ys* ]]; then
   if [[ $* == *-ys1* ]]; then singleYieldShockFile=1
   else singleYieldShockFile=0
   fi
   echo "Running R yield shock script"
   module load R
   R < /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/CreateYieldShockMaps.R  --no-save --args $1 $singleYieldShockFile
else
   echo "Not running R yield shock script"
fi


if [[ $* == *-s* ]]; then
   echo "Running R shock script"
   if [ "$#" -lt 2  ]; then
   echo "End year for scenarios not set, using 2100 as default"
   endYear=2100
   else
   endYear=$2
   fi

   module load R
   R < /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/createShockFiles.R  --no-save --args $1 $endYear
else
   echo "Not running R shock script"
fi
