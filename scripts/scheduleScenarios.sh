#!/bin/sh

umask 002

while getopts ":t:p:b:h:" opt; do
  case ${opt} in
    b )
      build=${OPTARG}
      ;;
    p )
      prune=${OPTARG}
      ;;
    t )
      filename=${OPTARG}
      ;;
    h )
      holdid=${OPTARG}
      ;;
  esac
done


echo Scenario table file $filename
if [ -z ${filename+x} ];
then
   echo "scenario table file is unset, use -t"
   exit -1
fi


#build="${build:-.}"
echo build version $build
if [ -z ${build+x} ];
then
   echo "build version is unset"
   exit -1
fi

if [ -z ${prune+x} ];
then
   prune="y"
fi

if [ -z ${holdid+x} ];
then
   holdid="calib"
   echo "submitting holding against $holdid"
fi

output_dir=/exports/csce/eddie/geos/groups/LURG/models/PLUM/output
echo "Starting scenarios in $output_dir"

while read -r datarow
do
  IFS=',' read -r -a array <<< "$datarow"

  if [ -z "$header" ]; then
     header=( "${array[@]}" )
  else
     ensemble="${array[0]}"
     scenario="${array[1]}"
     scenario_dir=$output_dir/$ensemble/$scenario
     echo $scenario for $ensemble

     if [ ! -d "$scenario_dir" ]; then
        echo "Scenario $scenario dir is missing, so skipping"
        continue;
     fi

     jobName="$(echo $ensemble | sed 's,\.\/,,' | sed 's,\/,_,g')_$scenario"

     echo "qsub -hold_jid $holdid -N $jobName /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/runPlum.sh -s $ensemble/$scenario -b $build -r y -p $prune"
     qsub -hold_jid $holdid -N $jobName /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/runPlum.sh -s $ensemble/$scenario -b $build -r y -p $prune
  fi
done < $filename

