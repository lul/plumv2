#!/bin/sh
#$ -o /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs
#$ -e /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs
#$ -l h_vmem=16G 
#$ -l h_rt=23:59:00

if [ $# -ne 1 ]; then
        echo need to specify scenario directory
        exit 0
fi

echo "Running R script"
source /etc/profile
module load R/4.3.0
R < /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/aggCountryLandUse.R  --no-save --args $1
