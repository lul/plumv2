#!/bin/sh

umask 002

if [ "$#" -lt 1  ]; then
   echo "Need to specify scenario table file"
   exit
fi


filename="$1"
output_dir=/exports/csce/eddie/geos/groups/LURG/models/PLUM/output/hind1970
echo "Starting scenarios in $output_dir"

while read -r datarow
do
  IFS=',' read -r -a array <<< "$datarow"

  if [ -z "$header" ]; then
     header=( "${array[@]}" )
  else
     ensemble="${array[0]}"
     scenario="${array[1]}"
     scenario_dir=$output_dir/$ensemble/$scenario
     echo $scenario for $ensemble

     if [ ! -d "$scenario_dir" ]; then
        echo "Scenario $scenario dir is missing, so skipping"
        continue;
     fi

     echo "qsub -N ${ensemble}_$scenario -hold_jid h_calib /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/runPlum.sh -s hind1970/$ensemble/$scenario"
     qsub -N ${ensemble}_$scenario -hold_jid h_calib /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/runPlum.sh -s hind1970/$ensemble/$scenario
#     echo "/exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/runCalibAndPlum.sh hind1970/$ensemble/$scenario"
#     /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/runCalibAndPlum.sh hind1970/$ensemble/$scenario
  fi
done < $filename

