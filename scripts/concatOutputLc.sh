#!/bin/bash
#$ -o /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs
#$ -e /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs

umask 002

if [ "$#" -ne 1  ]; then
   echo "Need to specify scenario output dir"
   exit
fi

cd $1
concatLcFile=lc_concat.txt
concatPriceFile=price_concat.txt
concatLUFile=LandUseSummary.csv
concatDomesticFile=domestic_concat.txt
concatCountryDemandFile=country_demand_concat.txt
concatCountryOptFile=country_opt_concat.txt
concatWoodFile=wood_concat.txt
echo "Scenario,Year,Cropland,Pasture,TimberForest,CarbonForest,UnmanagedForest,OtherNatural,Agrivoltaics,Photovoltaics,Suitable,EnergyCrop,FertCrop,IrrigCrop,ManIntCrop,ManIntPast" > $concatLcFile
echo "Scenario,Year,Crop,Imports,Exports,Price,Stocks" > $concatPriceFile
echo "Scenario,cropType,yield,irrig,fert,otherint,area,Year,dryMatter" > $concatLUFile
echo "Scenario,Year,Country,Crop,Area,Production,Production_cost,Import_price,Export_price,Consumer_price,Net_imports,Net_import_cost,Prod_shock,Rum_feed_amount,Mon_feed_amount" > $concatDomesticFile
echo "Scenario,Year,Country,Commodity,Demand,BioenergyDemand,ConsumerPrice" > $concatCountryDemandFile
echo "Scenario,Country,Year,gdpPc,population,status,utility,hungerFactor,commodity,price,subsistence,discretionary,plumNotRebase,plumRebased,rebasedKcal" > $concatCountryOptFile
echo "Scenario,Year,Country,RoundwoodDemand,WoodfuelDemand,Production,Import_price,Export_price,Net_imports" > $concatWoodFile

find . -name "*log.txt" -type f -exec grep -i -H "Modelstatus INFEASIBLE_LOCAL" '{}' \; >infes_text.txt
find . -name "*log.txt" -type f -exec grep -i -H "Exception" '{}' \; >exception_text.txt

find . -name "*lc.txt"|while read fname; do
        scenarioDir="$(dirname $fname)"
	scenario="$(echo $scenarioDir | sed 's,\.\/,,' | sed 's,\/,_,g')"
	echo "$scenario: $fname"

	i=1
	while read -r line
	do
		test $i -eq 1 && ((i=i+1)) && continue
		echo "${scenario},$line" >> $concatLcFile
	done < $fname

	# Should write a function for this, rather than duplicating, but do that later
	j=1
	while read -r line
	do
		test $j -eq 1 && ((j=j+1)) && continue
		echo "${scenario},$line" >> $concatPriceFile
	done < $scenarioDir/prices.txt

	k=1
	while read -r line
	do
		test $k -eq 1 && ((k=k+1)) && continue
		echo "${scenario},$line" >> $concatLUFile
	done < $scenarioDir/LandUseSummary.csv
	
	l=1
	while read -r line
	do
		test $l -eq 1 && ((l=l+1)) && continue
		echo "${scenario},$line" >> $concatDomesticFile
	done < $scenarioDir/domestic.txt
 
 	m=1
	while read -r line
	do
		test $m -eq 1 && ((m=m+1)) && continue
		echo "${scenario},$line" >> $concatCountryDemandFile
	done < $scenarioDir/countryDemand.txt
	 
	 n=1
	while read -r line
	do
		test $n -eq 1 && ((n=n+1)) && continue
		echo "${scenario},$line" >> $concatCountryOptFile
	done < $scenarioDir/countryDemandOpt.txt

	o=1
  while read -r line
  do
  	test $o -eq 1 && ((o=o+1)) && continue
  	echo "${scenario},$line" >> $concatWoodFile
  done < $scenarioDir/wood.txt

done
