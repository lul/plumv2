#!/bin/sh

umask 002

if [ $# -ne 1 ]; then
	echo need 1 argument to specify config/output directory
	exit 0
fi

jobName=${1//\//_} # remove slash from path as qsub doesnt allow it
jobName=${jobName/hind1970/h}
calibJob=${jobName}c
echo ""
echo $jobName, $calibJob

qsub -N $calibJob /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/runPlum.sh -s $1/calib
qsub -N $jobName -hold_jid $calibJob /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/runPlum.sh -s $1
