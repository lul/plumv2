#!/bin/sh

#$ -o /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs
#$ -e /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs

umask 002

if [ "$#" -ne 1  ]; then
   echo "Need to specify ensemble output dir"
   exit
fi

cd $1
source /etc/profile
module load R
R < /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2/scripts/summariseUKlanduse.R  --no-save

concatUKLCFile=ukLC_concat.txt
echo "Scenario,Year,IrrigCrops,FertCrops,Cropland,Pasture,ManForest,UnmanForest,Natural,AbPasture" > $concatUKLCFile

find . -name "*ukLC.csv"|while read fname; do
        scenarioDir="$(dirname $fname)"
	scenario="$(echo $scenarioDir | sed 's,\.\/,,' | sed 's,\/,_,g')"
	echo "$scenario: $fname"

	i=1
	while read -r line
	do
		test $i -eq 1 && ((i=i+1)) && continue
		echo "${scenario},$line" >> $concatUKLCFile
	done < $fname

done