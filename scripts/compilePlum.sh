#!/bin/sh
#$ -o /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs
#$ -e /exports/csce/eddie/geos/groups/LURG/models/PLUM/output/qlogs
#$ -l h_vmem=8G
#$ -l h_rt=0:20:00
#$ -R y
#$ -l rl9=false

umask 002

cd /exports/csce/eddie/geos/groups/LURG/models/PLUM/plumv2

buildbase=/exports/csce/eddie/geos/groups/LURG/models/PLUM/build
buildnum="$(git rev-list --count HEAD)"
dirty="$([[ -z $(git status -s) ]] || echo '+')"
buildver=$buildnum$dirty
builddir=$buildbase/$buildver

echo "Building version: $buildver into $builddir"

if [ ! -d "$builddir" ]; then
	mkdir $builddir

	javac -J-Xmx1g -cp .:/exports/csce/eddie/geos/groups/LURG/models/gams/gams39.2_linux_x64_64_sfx/apifiles/Java/api/GAMSJavaAPI.jar ./src/ac/ed/lurg/*.java ./src/ac/ed/lurg/carbon/*.java ./src/ac/ed/lurg/country/*.java ./src/ac/ed/lurg/country/crafty/*.java ./src/ac/ed/lurg/country/gams/*.java ./src/ac/ed/lurg/demand/*.java ./src/ac/ed/lurg/forestry/*.java ./src/ac/ed/lurg/landuse/*.java ./src/ac/ed/lurg/output/*.java ./src/ac/ed/lurg/shock/*.java ./src/ac/ed/lurg/solar/*.java ./src/ac/ed/lurg/types/*.java ./src/ac/ed/lurg/utils/*.java ./src/ac/ed/lurg/yield/*.java ./src/ac/sac/raster/*.java ./src/ac/ed/lurg/utils/calib/*.java ./src/ac/ed/lurg/utils/cluster/*.java -d $builddir

	echo "$buildver __$(date)" > $builddir/buildver
	echo "javac completed"

	echo "copying data"
        cp -r ./data ../data/$buildver
        cd ../data
        rm latest
        ln -s $buildver latest

	echo "updating link for latest"
	rm $buildbase/latest
	cd $buildbase
	ln -s $buildver latest
else
	echo "$builddir already exists." 1>&2
	echo "Delete it manually if you want to rebuild." 1>&2
fi
