#!/bin/sh
if [ $# -ne 1 ]; then
        echo need 1 argument to specify config/output directory
        exit 0
fi

find $1 -name "20[0-9][1-46-9]" | xargs rm -rf
find $1 -name "19[0-9][1-46-9]" | xargs rm -rf
