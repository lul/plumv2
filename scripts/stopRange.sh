#!/bin/sh
if [ $# -ne 2 ]; then
        echo need 2 argument to specify parameters
        exit 0
fi

for ((i=$1; i<=$2; i++ ))
do
   #echo "qdel $i"
   qdel $i
done
