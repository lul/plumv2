#!/bin/bash

umask 002

if [ ! -d "$1" ]; then
    echo "$1" "Specified file does not exist. Exiting!"
    exit 1
fi

cd $1

function extractAndConvertHeader {
   if [ "$#" -ne 2  ]; then
      echo "Illegal number of parameters to extractAndConvertHeader"
   fi

   #old runs 2016-01-..  change header e.g. C3G_pas C3G_pNhi TeWWhi -> PC3G PC3GN200 TeWW200
   head -n 1 $1 | sed 's,C\(.\)G_p,PC\1G,g' | sed 's,Gas,G,g' | sed 's,hi,200,g' | sed 's,me,020,g' | sed 's,lo,005,g'  > $2
   grep -i " $YEAR " $1 >> $2
}

YEAR=2000
while [ $YEAR -le 2095 ]; do
   echo item: $YEAR
   if [ ! -d "$YEAR" ]; then
      mkdir $YEAR
   fi

   extractAndConvertHeader gsirrigation.out $YEAR/gsirrigation_plum.out
   extractAndConvertHeader anpp.out $YEAR/anpp_plum.out
   extractAndConvertHeader yield.out $YEAR/yield_plum.out

   touch $YEAR/done
   let YEAR+=5
done
